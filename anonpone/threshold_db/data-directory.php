<?php

$dir = "/var/www/anonpone-dev/anonpone/threshold_db/DATA/";
$files = scandir($dir);

$fileObjects;
foreach($files as $file)
{
	if(endsWith($file,'.xml'))
	{
		$object = new stdClass();
		$object->name = $file;
		$object->mTime = filemtime($dir . $file);
		$fileObjects[] = $object;
	}
}

echo json_encode($fileObjects);

function endsWith($haystack, $needle) {
    // search forward starting from end minus needle length characters
    return $needle === "" || (($temp = strlen($haystack) - strlen($needle)) >= 0 && strpos($haystack, $needle, $temp) !== FALSE);
}
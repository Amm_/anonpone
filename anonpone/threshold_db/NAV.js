var cur_section=undefined,cur_sectionTop=0,cur_sectionBottom=404;
var tablet=false;
var last_y_pos=0;
if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {console.log("User is probably using a tablet/phone");tablet=true;}

function init_NAV(){
	document.getElementById('wrapper').addEventListener('scroll', NAV);
	if(tablet==false){// if they aren't using a tablet, use muh fancy scrollbar shit
		document.addEventListener('keydown', function(event) {scrollButtons(event.keyCode); });
		document.addEventListener("wheel", function(event) {if(event.deltaY>0){scrollButtons(40);}else scrollButtons(38); return false;});
		window.onwheel=function(){ return false; };
		for(i=0;i<23;++i){
			
		}
	} /*original shitty scroll buttons should probably be enabled here instead*/
}

function scrollButtons(input){
	if(cur_section==undefined) return;
	if(cur_sectionTop<0 && input==38){n_scroll(0,-16);}
	if(cur_sectionBottom>404 && input==40){n_scroll(0,16);}
}

function scrollDragInit(event){
	last_y_pos=event.clientY;
	document.onmousemove = function(event){scrollbarDrag(event);};
	document.onmouseup = scrollDragDest;
	document.onblur = scrollDragDest;
}
function scrollDragDest(){document.onmousemove="";document.onmouseup="";document.onblur="";}

function scrollbarDrag(event){
	if(event.clientY>(last_y_pos+16)){scrollButtons(40);last_y_pos=event.clientY;}
	if(event.clientY<(last_y_pos-16)){scrollButtons(38);last_y_pos=event.clientY;}
}

function NAV(){
	var c = document.getElementsByTagName("nav")[0].getElementsByTagName("a");
	var s = window.location.hash
	var i = 0;
	if(window.location.hash.length>1 ){
		for (i = 1; i < c.length; i++) {
			if(c[i].className =="AC") c[i].className="";
			if(c[i].hash.localeCompare(s)==0) c[i].className="AC";
		}
	}else { 
		if (c[1]==undefined) return;
		c[1].className="AC";
		s=c[1].hash;
		window.location.hash=c[1].hash;
	}
	if(s[0]=='#') s=s.substr(1);
	cur_section = document.getElementById(s);
	if(cur_section==undefined){  console.log("ERROR: cur_section is undefined"); return;}
	var rect = cur_section.getBoundingClientRect();
	//if (rect.top>=0){document.getElementById("pgup").style.display="none";}else document.getElementById("pgup").style.display="block";
	//if (rect.bottom<=436){document.getElementById("pgdn").style.display="none";}else document.getElementById("pgdn").style.display="block";
	var soffset=document.getElementsByTagName("html")[0].scrollTop;
	cur_sectionTop=Math.floor(rect.top+soffset), cur_sectionBottom=Math.floor(rect.bottom+soffset);
	var height=Math.ceil((cur_sectionBottom-cur_sectionTop-32)/16);
	var i2=((Math.abs(cur_sectionTop-4)/16)/height);
	document.getElementById("scrollbar_pos").style.cssText="height:"+(336*(23/height))+"px;top:"+((i2*336)+16)+"px;";
	if (cur_sectionTop>=0){document.getElementById("sc_up").style.cssText="display:none;";}else{ document.getElementById("sc_up").style.cssText="";}
	if (cur_sectionBottom<=404){document.getElementById("sc_dn").style.cssText="display:none;";}else{ document.getElementById("sc_dn").style.cssText="";}
}

function GetUrlValue(VarSearch){
    var SearchString = window.location.search.substring(1);
    var VariableArray = SearchString.split('&');
    for(var i = 0; i < VariableArray.length; i++){
        var KeyValuePair = VariableArray[i].split('=');
        if(KeyValuePair[0] == VarSearch){
            return KeyValuePair[1];
        }
    }
	return 0;
}

function n_scroll(x,y){
	var elmnt = document.getElementById("wrapper");
    elmnt.scrollLeft += x;
    elmnt.scrollTop += y;
	return false;
}

function load_xml_file(source,clear_files){
//	console.log(source);
	var xmlhttp, file,x,default_class="p_full",default_style="",para,i,i2;
	if(clear_files==undefined) clear_files=true;
	if (window.XMLHttpRequest){// code for IE7+, Firefox, Chrome, Opera, Safari
		xmlhttp=new XMLHttpRequest();
	}else{// code for IE6, IE5
		xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	}
	if(xmlhttp!=null){
		xmlhttp.open("GET",source,false);
		xmlhttp.send();
		xmlDoc=xmlhttp.responseXML;
		file=xmlDoc.getElementsByTagName("data")[0];
		if(clear_files){
			para=document.getElementsByTagName("nav")[0];
			while(para.getElementsByTagName("a")[1]!=undefined){
				para.removeChild(para.getElementsByTagName("a")[1]);
			}
			para=document.getElementById("temp");
			while(para.firstChild){
				para.removeChild(para.firstChild);
			}
		}
		if (file==undefined){ console.log("ERROR: Can't find file:",source); return;}
		if(file.getAttribute("CLASS")!=null){default_class=file.getAttribute("CLASS");}
		if(file.getAttribute("STYLE")!=null){default_style=file.getAttribute("STYLE");}
		for(i=0;i<file.childNodes.length;++i){
			if(file.childNodes[i].nodeType==1){
				if (file.childNodes[i].nodeName.localeCompare("script")==0){
					para = document.createElement("script");
					document.getElementById("temp").appendChild(para);
					if(file.childNodes[i].childNodes[1]!=undefined) para.innerHTML=file.childNodes[i].childNodes[1].nodeValue;
				}else{
					if(file.childNodes[i].getAttribute("NONAV")==null){
						para = document.createElement("a");
						document.getElementsByTagName("nav")[0].appendChild(para);
						para.href="#"+file.childNodes[i].nodeName;
						para.innerHTML=file.childNodes[i].nodeName;
					}
					para=document.getElementById(file.childNodes[i].nodeName);
					if(para==undefined){
						para = document.createElement("p");
						document.getElementById("temp").appendChild(para);
						para.id=file.childNodes[i].nodeName;
					}
					if(file.childNodes[i].getAttribute("CLASS")!=null){ para.className = file.childNodes[i].getAttribute("CLASS");}else para.className = default_class;
					if(file.childNodes[i].getAttribute("STYLE")!=null){ para.style.cssText = file.childNodes[i].getAttribute("STYLE");}else para.style.cssText = default_style;
					para.innerHTML="";
					for(i2=0;i2<file.childNodes[i].childNodes.length;++i2){
						if (file.childNodes[i].childNodes[i2].nodeType==3){ para.innerHTML+=file.childNodes[i].childNodes[i2].nodeValue;}
					}
				}
			}
		}
	}
}

function swapStyleSheet(sheet){
	document.getElementById('whatever').setAttribute('href', sheet);
}
<?php
	//include "include.php";
	
	$api_path = "https://api.titledb.com/v0";
	
	function http_req($data = array("action" => "list"), $endpoint = "/") {
		$curl = curl_init($GLOBALS["api_path"].$endpoint);
		$headers = array();
		$headers[] = "HBRW/1.0.0 HBRW-Proxy/1.0.0";
		curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($data));
		curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		$response = curl_exec($curl);
		curl_close($curl);
		return $response;
	}

	//header("Content-Disposition: attachment; filename=applist.json");
	//header("Content-Type: application/force-download");
	header("Connection: close");


	echo http_req(array("action"=>"list_fields"));
?>


<!-- <div id="result"></div>

<script>
if(typeof(EventSource) !== "undefined") {
    var source = new EventSource("/test2.php");
    source.onmessage = function(event) {
        document.getElementById("result").innerHTML += event.data + "<br>";
    };
} else {
    document.getElementById("result").innerHTML = "Sorry, your browser does not support server-sent events...";
}
</script> -->
var user = {};
user.isLoggedIn = false;
user.id;
user.username;
user.roles = [];

user.init = () => {
	let userData = JSON.parse(localStorage.getItem("user")) ?? {};
	if(userData["id"]){
		user.isLoggedIn = true;
		user.id = userData["id"];
		user.username = userData["username"];
		user.roles = userData["roles"];
	} else {
		user.isLoggedIn = false;
	}

	api.localRequest("/user",true,(error, response) => {
		if(!error){
			response = JSON.parse(response);
			if(!response["error"]){
				localStorage.setItem("user",JSON.stringify(response));

				if(!user.isLoggedIn){
					location.reload();
				}
			} else {
				localStorage.removeItem("user");
				if(user.isLoggedIn){
					location.reload();
				}
			}
		}
	});

	if(user.isLoggedIn){
		let adminParent = document.createElement("span");
		adminParent.classList.add("tooltip");
		let adminTitle = document.createElement("span");
		adminTitle.classList.add("icon");
		adminTitle.classList.add("arrowDown");
		adminTitle.innerText = "Admin";
		let adminDropdown = document.createElement("ul");
		adminDropdown.classList.add("tooltip-content");
		adminDropdown.classList.add("bottom");
		let adminList = {
			"Admin Home": "/admin",
			"CYOAS": "/admin/cyoa",
			"Webhooks": "/admin/webhook",
			"Reports": "/admin/report",
		}
		let badge = document.createElement("span");
		badge.classList.add("badge");
		let count = localStorage.getItem("reportCount");
		if(count >= 1){
			badge.innerText = count;
			badge.classList.add("red");
		} 
		api.localRequest("/report/count",true,(error, response) => {
			if(!error){
				response = JSON.parse(response);
				if(!response["error"]){
					localStorage.setItem("reportCount",response["count"]);
					badge.innerText = response["count"];
				} else {
					localStorage.removeItem("reportCount");
				}
			}
		});
		adminTitle.appendChild(badge);

		Object.keys(adminList).forEach((name) => {
			let listElement = document.createElement("li");
			let listLink = document.createElement("a");
			listLink.innerText = name;
			listLink.href = adminList[name];
			listElement.appendChild(listLink);
			adminDropdown.appendChild(listElement);
		});

		adminParent.appendChild(adminTitle);
		adminParent.appendChild(adminDropdown);
		
		let navBar = document.getElementById("navbar");
		navBar.appendChild(adminParent);
	}
}

user.hasRole = (role) => {
	if(user.roles.includes(role)) return true;
	return false;
}
user.init();

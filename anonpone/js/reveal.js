//Swaps data-reveal and src on any image, consuming the click.

var reveal = {}

reveal.init = () => {
	Array.from(document.getElementsByTagName("img")).forEach((img)=>{
		img.addEventListener('click', (e) => {
			if(img.dataset.reveal && img.dataset.reveal !== img.src){
			    img.src = img.dataset.reveal;
			    e.preventDefault();
			}
		});
	});
};

reveal.init();
var report = {}

report.close;
report.bodyElement;

report.init = () => {
	report.construct();

	Array.from(document.getElementsByClassName("report")).forEach((element) => {
		element.dataset.reportLink = element.href;
		element.href = "#modal-reportMenu";

		element.onclick = (e) => {
			api.request(e.target.dataset.reportLink,true,(error,response) => {
				if(!error){
					let parser = new DOMParser();
					let doc = parser.parseFromString(response, "text/html");
					report.bodyElement.innerHTML = doc.getElementsByClassName("body")[0].innerHTML;
					document.getElementById("Forms\\Report-return").value = window.location.pathname.match(/\/(thread|post)/)[1];
				} else {
					console.log(error);
				}
			});
		}
	});
}

report.construct = () => {
	var overlay = document.createElement("div");
	overlay.classList.add("modal");
	overlay.id = "modal-reportMenu";
	overlay.onclick = (evt) => {
		report.close.click();
	}

	var content = document.createElement("div");
	content.classList.add("content");
	content.classList.add("panel");
	content.onclick = (evt) => {
		evt.stopPropagation();
	}
	overlay.appendChild(content);

	var header = document.createElement("div");
	header.classList.add("row");
	header.classList.add("header");
	report.close = close = document.createElement("a");
	close.href = "#!";
	close.title = "Close";
	close.classList.add("icon");
	close.classList.add("close");
	close.classList.add("floatRight");
	var headerText = document.createElement("h5");
	headerText.innerText = "Report";
	header.appendChild(headerText);
	header.appendChild(close);
	content.appendChild(header);

	report.bodyElement = body = document.createElement("div");
	body.classList.add("row");
	body.classList.add("body");
	content.appendChild(body);

	document.body.appendChild(overlay);
}

report.init()
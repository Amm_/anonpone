var catalog = {};

catalog.update = true;

catalog.init = () => {
	if(localStorage.getItem("quests")){
		catalog.quests = JSON.parse(localStorage.getItem("quests"));
	}
	catalog.favorite();

	//Sorting stuff
	var sort = 0;
	if(localStorage.getItem("index-sort")){
		sort = localStorage.getItem("index-sort");
	}
	var sortSelect = document.createElement("select");
	var sortOptions = ["Sort","Newest","Latest","Alphabetical","Length (words)","length(posts)"];
	sortOptions.forEach((name,value) => {
		var option = document.createElement("option"); 
		option.value = value;
		option.innerText = name;
		sortSelect.appendChild(option);
	});
	sortSelect.value=sort;
	catalog.sortElement = sortSelect;
	sortSelect.onchange = () => {
		catalog.sort(catalog.sortElement.value);
		localStorage.setItem("index-sort",catalog.sortElement.value);
	}
	document.getElementById("index-controls").appendChild(sortSelect);

	//Change view size stuff
	var viewElement = document.createElement("span");
	var view = 0;
	viewElement.type="button";
	viewElement.classList.add("icon");
	viewElement.classList.add("btn");
	if(localStorage.getItem("index-view")){
		view = localStorage.getItem("index-view");
	}
	switch(parseInt(view)){
		case 1: 
			viewElement.classList.add("grid-large");
			document.getElementById("index-quests").classList.add("small");
			break;
		case 0:
		default:
			viewElement.classList.add("grid-small");
			break;
	}
	catalog.viewElement = viewElement;
	viewElement.onclick = () => {
		if(catalog.viewElement.classList.contains("grid-large")){
			catalog.viewElement.classList.remove("grid-large");
			catalog.viewElement.classList.add("grid-small");
			localStorage.setItem("index-view",0);
			document.getElementById("index-quests").classList.remove("small");
		} else if(catalog.viewElement.classList.contains("grid-small")){
			catalog.viewElement.classList.remove("grid-small");
			catalog.viewElement.classList.add("grid-large");
			localStorage.setItem("index-view",1);
			document.getElementById("index-quests").classList.add("small");
		}
	}
	document.getElementById("index-controls").appendChild(viewElement);

	//Search box stuff
	var search = document.createElement("input");
	search.type = "text";
	search.placeholder = "Search";
	search.classList.add("search");
	search.oninput = () => {
		catalog.search();
	}
	if(api.getParameter("search")) search.value = api.getParameter("search");
	catalog.searchElement = search;
	document.getElementById("index-controls").appendChild(search);
	catalog.search();

	//Badge search
	var list = document.getElementsByClassName("badge");
	Array.from(list).forEach((node) => {
		node.onclick = (event) => {
			catalog.searchElement.value = event.target.innerText;
			document.getElementById("index-controls").scrollIntoView();
			catalog.search();
		}
	});

	//RSS feed copy to Clipboard
	list = document.getElementsByClassName("linkRss");
	Array.from(list).forEach((node) => {
		node.onclick = (event) => {
			event.preventDefault();
			navigator.clipboard.writeText(event.target.href);
		}
	});

	catalog.sort(catalog.sortElement.value);
	catalog.checkQuests();
	catalog.live();
	catalog.adminLinks();
}
catalog.adminLinks = () => {
	if(user.isLoggedIn && user.hasRole("Admin")){
		var linkElement = document.createElement("a");
		linkElement.classList.add("icon");
		linkElement.classList.add("admin");
		var list = document.getElementsByClassName("links");
		Array.from(list).forEach((node) => {
			let clone = linkElement.cloneNode(true);
			clone.href="/admin/cyoa/" + node.parentElement.dataset.id;
			node.appendChild(clone);
		});
	}
}
catalog.checkQuests = () => {
	if(!catalog.update) return;
	var catalogRefresh = localStorage.getItem("catalogRefresh") || 0;
	if((new Date()).getTime() - catalogRefresh > 10*1000) {
		api.localRequest("/quest", true, (error, response) => {
			if(!error){
				try{
					catalog.quests = JSON.parse(response);
					localStorage.setItem("quests",response);
					localStorage.setItem("catalogRefresh",(new Date()).getTime());
					setTimeout(catalog.checkQuests, 10*1000);
					catalog.sort(catalog.sortElement.value);
					catalog.live();
				} catch(e) {
					console.log("Invalid JSON");
				}
			} else if(error.startsWith("304")){
				localStorage.setItem("catalogRefresh",(new Date()).getTime());
				setTimeout(catalog.checkQuests, 10*1000);
				catalog.sort(catalog.sortElement.value);
				catalog.live();
			} else {
				console.log(error);
			}
		});
	} else {
		catalog.quests = JSON.parse(localStorage.getItem("quests"));
		setTimeout(catalog.checkQuests, 10*1000);
		catalog.sort(catalog.sortElement.value);
		catalog.live();
	}
}
catalog.sort = (value) => {
	var sort = (a,b) => (a == b ? 0 : (a < b ? 1 : -1));

	if(!catalog.quests) return;
	var list = document.querySelector('#index-quests');

	[...list.children].sort((a, b) => {
		if(!a.dataset.id) return 1;
		if(!b.dataset.id) return -1;
		if(b.classList.contains("hidden")) return -1;
		if(a.classList.contains("hidden")) return 1;
		a = catalog.quests[a.dataset.id];
		b = catalog.quests[b.dataset.id];
		if(!a) return 1;
		if(!b) return -1;
		if(!(a.hasOwnProperty('_firstPost'))) return 1;
		if(!(b.hasOwnProperty('_firstPost'))) return -1;
		switch(parseInt(value)){
			case 2: //lastPost
				return sort(parseInt(a._lastPost._date),parseInt(b._lastPost._date));
			case 3: //Alphabeic
				return sort(b._title,a._title);
			case 4: //length (words)
				return sort(parseInt(a._stats.totalWordCount),parseInt(b._stats.totalWordCount));
			case 5: //length (posts)
				return sort(parseInt(a._stats.totalPosts),parseInt(b._stats.totalPosts));
			default:
			case 1: //firstPost
				return sort(parseInt(a._firstPost._date),parseInt(b._firstPost._date));
		}
	}).forEach(node=>list.appendChild(node));
	if(catalog.searchElement.value == ""){
		[...list.children].sort((a, b) => {
			if(!a.dataset.id) return 1;
			if(!b.dataset.id) return -1;
			if(b.children[0].children[3].classList.contains("gold")) return 1;
			if(a.children[0].children[3].classList.contains("gold")) return -1;
			return 0;
		}).forEach(node=>list.appendChild(node));
	}
}
catalog.live = () => {
	var list = document.getElementsByClassName("date");
	Array.from(list).forEach((node) => {
		var id = node.parentElement.parentElement.dataset.id;
		if(catalog.quests[id] !== undefined && parseInt(catalog.quests[id]._live)) node.classList.add("live");
		else node.classList.remove("live")
	});
}
catalog.favorite = () => {
	var favoriteNode = document.createElement("a");
	favoriteNode.classList.add("icon");
	favoriteNode.classList.add("star");
	favoriteNode.title = "Favorite";
	var favoriteList = JSON.parse(localStorage.getItem("favorites")) ?? [];

	var list = document.getElementsByClassName("links");
	Array.from(list).forEach((node) => {
		var id = node.parentElement.dataset.id;
		var clone = favoriteNode.cloneNode(true);
		clone.onclick = (event) => {
			var id = node.parentElement.dataset.id;
			var favoriteList = JSON.parse(localStorage.getItem("favorites")) ?? [];
			if(event.target.classList.contains("gold")){
				favoriteList = favoriteList.filter(item => item !== id)
				event.target.classList.remove("gold");
			} else {
				favoriteList.push(id);
				event.target.classList.add("gold");
			}
			localStorage.setItem("favorites",JSON.stringify(favoriteList));
			catalog.sort(catalog.sortElement.value);
		}
		if(favoriteList.includes(id)) clone.classList.add("gold");
		node.appendChild(clone);
	});
}
catalog.search = () => {
	if(!catalog.quests) return;
	var list = document.querySelector('#index-quests');

	var questCount = 0;
	var threadCount = 0;
	var postCount = 0;
	var imageCount = 0;
	var wordCount = 0;
	[...list.children].forEach((node) => {
		if(!node.dataset.id) return;
		var quest = catalog.quests[node.dataset.id];
		if(!quest) return;
		var searchLine = quest._cid + "," + quest._title + "," + quest._tags.join(",") + (parseInt(quest._live) ? ",live" : "");
		switch(parseInt(quest._status)){
			case 1:
				searchLine += ",Active";
				break;
			case 2:
				searchLine += ",Complete";
				break;
			case 3:
				searchLine += ",Hiatus";
				break;
			case 4:
				searchLine += ",Cancelled";
				break;
			case 5:
				searchLine += ",Hidden";
				break;
			case 6:
				searchLine += ",Disabled";
				break;
		}
		searchLine = searchLine.toLowerCase();
		var searchValue = catalog.searchElement.value.toLowerCase();

		if(catalog.searchElement.value){
			if(searchLine.includes(searchValue)){
				node.classList.remove("hidden");
				questCount++;
				threadCount += quest._stats.totalThreads;
				postCount += quest._stats.totalPosts;
				imageCount += quest._stats.totalImages;
				wordCount += quest._stats.totalWordCount;
			} else { 
				node.classList.add("hidden");
			}
		} else {
			if(searchLine.includes("miniquest") || searchLine.includes("hidden")){
				node.classList.add("hidden");
			} else{
				node.classList.remove("hidden");
				questCount++;
				threadCount += quest._stats.totalThreads;
				postCount += quest._stats.totalPosts;
				imageCount += quest._stats.totalImages;
				wordCount += quest._stats.totalWordCount;
			} 
		}
	});
	document.getElementById("stats").innerHTML = "Quests:" + questCount.toLocaleString() + "<br>" +
	"Threads:" + threadCount.toLocaleString() + "<br>" +
	"Posts:" + postCount.toLocaleString() + "<br>" +
	"Images:" + imageCount.toLocaleString() + "<br>" +
	"Words:" + wordCount.toLocaleString();
	catalog.sort(catalog.sortElement.value);
}

catalog.init();
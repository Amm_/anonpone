var postMenu = {};

postMenu.form;
postMenu.postids;
postMenu.init = () => {
	if(user.isLoggedIn && user.hasRole("Admin")){
		let adminParent = document.createElement("span");
		let form = document.createElement("form");
		form.method = "POST";
		form.action = "/admin/action";
		postMenu.form = form;
		let cyoaid = document.createElement("input");
		cyoaid.type = "hidden";
		cyoaid.name = "cyoa-id"
		cyoaid.value = document.getElementById("cyoaid").value;
		let postids = document.createElement("input");
		postids.name = "post-ids";
		postids.type = "hidden";
		postMenu.postids = postids;

		let select = document.createElement("select");
		select.name = "selection";
		let options = {
			"nan": "Tools",
			"notLewd":"Mark as not Lewd",
			"isLewd": "Mark as Lewd",
			"notQM": "Mark as not QM",  
			"isQM": "Mark as QM",
		};
		select.onchange = postMenu.onchange;
		for(let key in options){
			let option = document.createElement("option");
			option.value = key;
			option.innerText = options[key];
			select.appendChild(option); 
		}

		form.appendChild(cyoaid);
		form.appendChild(postids);
		form.appendChild(select);
		adminParent.appendChild(form);

		let navBar = document.getElementById("navbar");
		//navBar.insertBefore(adminParent,navBar.children[navBar.children.length]);
		navBar.appendChild(adminParent);
	}
}

postMenu.onchange = (e) => {
	let checkList = document.getElementsByClassName("multiselect");
	let posts = [];
	Array.from(checkList).forEach((check) => {
	    if(check.checked) posts.push(check.parentNode.parentNode.dataset.id);
	});
	postMenu.postids.value = posts.join(",");
	if(posts.length <= 0){
		postMenu.postids.value = document.getElementById("qmPost").children[1].dataset.id;
	}
	postMenu.form.submit();
}

postMenu.init();
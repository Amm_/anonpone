var gallery = {};

gallery.overlay;
gallery.image;
gallery.video;
gallery.title;
gallery.index;
gallery.currentGallery;
gallery.counterElement;
gallery.next;
gallery.prevoius;
gallery.directLink;

gallery.init = () => {
	if(!document.getElementsByClassName("gallery").length) return;
	
	gallery.initConstruct();

	Array.from(document.getElementsByClassName("gallery")).forEach((galleryNode) => {
		Array.from(galleryNode.getElementsByClassName("galleryItem")).forEach((itemNode) => {
			itemNode.onclick = (evt) => {
				if(evt.target.tagName !== "A"){
					evt.preventDefault();
					gallery.currentGallery = galleryNode;
					gallery.change(Array.from(galleryNode.getElementsByClassName("galleryItem")).indexOf(gallery.getItemDiv(evt.target)));
				}
			};
		});
	});

	if(window.location.hash && window.location.hash.match(/#\d*-\d*/)){
		gallery.currentGallery = document.getElementsByClassName("gallery")[window.location.hash.split("-")[0].substring(1)];
		gallery.change(window.location.hash.split("-")[1]-1);
	}

	document.addEventListener('keyup', gallery.initNav, false);
	window.addEventListener('hashchange', () => {
		if(window.location.hash && isFinite(window.location.hash.substring(1))){
    		gallery.change(window.location.hash.substring(1),false);
    	}
    });
}

gallery.initConstruct = () => {
	gallery.overlay = overlay = document.createElement("div");
	overlay.classList.add("galleryOverlay");
	overlay.classList.add("hidden");

	var container = document.createElement("div");
	container.classList.add("galleryContainer");
	gallery.image = image = document.createElement("img");
	image.onclick = (evt) => { evt.stopPropagation();};
	gallery.title = title = document.createElement("a");
	title.onclick = (evt) => { evt.stopPropagation();};
	container.onclick = (evt) => {
		gallery.close();
	};
	var video = document.createElement("video");
	video.onclick = (evt) => { evt.stopPropagation();};
	video.controls = true;
	gallery.video = source = document.createElement("source");
	video.appendChild(source);

	gallery.next = next = document.createElement("div");
	next.classList.add("galleryNext");
	next.title = "Next";
	next.onclick = () => {
		gallery.change(gallery.index + 1);
	}
	var nextArrow = document.createElement("div");
	nextArrow.classList.add("icon");
	nextArrow.classList.add("arrowRight");
	next.appendChild(nextArrow);
	gallery.previous = previous = document.createElement("div");
	previous.classList.add("galleryPrevious");
	previous.title = "Previous";
	previous.onclick = () => {
		gallery.change(gallery.index - 1);
	}
	var prevArrow = document.createElement("div");
	prevArrow.classList.add("icon");
	prevArrow.classList.add("arrowLeft");
	previous.appendChild(prevArrow);

	var controls = document.createElement("div");
	controls.classList.add("galleryControls");
	var close = document.createElement("span");
	close.classList.add("galleryControlRight");
	close.classList.add("icon");
	close.classList.add("close");
	close.title = "Close";
	close.onclick = () => {
		gallery.close();
	}
	var fullscreen = document.createElement("span");
	fullscreen.classList.add("galleryControlRight");
	fullscreen.classList.add("icon");
	fullscreen.classList.add("fullscreen-open");
	fullscreen.title = "Fullscreen Open/Close";
	fullscreen.onclick = (evt) => {
		if(evt.target.classList.contains("fullscreen-open")){
			gallery.overlay.requestFullscreen();
			evt.target.classList.remove("fullscreen-open");
			evt.target.classList.add("fullscreen-close");
		} else {
			document.exitFullscreen();
			evt.target.classList.add("fullscreen-open");
			evt.target.classList.remove("fullscreen-close");
		}
	}
	gallery.directLink = directLink = document.createElement("a");
	directLink.classList.add("galleryControlRight");
	directLink.classList.add("icon");
	directLink.classList.add("globe");
	directLink.title = "Direct Link";

	gallery.counterElement = counter = document.createElement("span");
	counter.classList.add("galleryControlLeft");

	controls.appendChild(close);
	controls.appendChild(fullscreen);
	controls.appendChild(directLink);
	controls.appendChild(counter);

	overlay.appendChild(next);
	overlay.appendChild(previous);
	overlay.appendChild(controls);
	container.appendChild(image);
	container.appendChild(video);
	container.appendChild(title);
	overlay.appendChild(container);
	document.body.appendChild(overlay);
};

gallery.initNav = (evt) => {
	if(!gallery.overlay.classList.contains("hidden")){
		if(evt.keyCode != 18 && (document.activeElement.nodeName != 'TEXTAREA' 
			&& document.activeElement.nodeName != 'INPUT'
			&& gallery.currentGallery.getElementsByClassName("galleryItem").length > 1
			))	{
		    switch(evt.keyCode) {
		        case 27: // esc key
		        	gallery.close();
		        break;
		        case 72: // h key
		        case 37: // left arrow
		         	gallery.change(gallery.index - 1);
		        break;
		        case 76: // l key
		        case 39: // right arrow
		         	gallery.change(gallery.index + 1);
		        break;
		        default: return;
		    }
	    	evt.preventDefault();
		}
	}
};

gallery.getItemDiv = (node) => {
	if(node.classList.contains("galleryItem")) return node;
	else return gallery.getItemDiv(node.parentNode);
};

gallery.open = () => {
	gallery.overlay.classList.remove("hidden");
	document.body.classList.add("overflow-disabled");
	if(typeof navigation !== 'undefined') navigation.enabled = false;
}

gallery.change = (index,pushState = true) => {
	if(gallery.overlay.classList.contains("hidden")) gallery.open();

	index = parseInt(index);

	index = gallery.getIndex(index);
	gallery.index = index;
	gallery.counterElement.innerText = (gallery.index+1) + " / " + gallery.currentGallery.getElementsByClassName("galleryItem").length;
	let node = gallery.currentGallery.getElementsByClassName("galleryItem")[index];
	node.scrollIntoView();

	if(node.dataset.image.endsWith(".webm") || node.dataset.image.endsWith(".gifv")){
		gallery.video.src = node.dataset.image;
		gallery.video.type = "video/" + node.dataset.image.split('.').pop();
		gallery.video.parentNode.classList.remove("hidden");
		if(!gallery.image.classList.contains("hidden")) gallery.image.classList.add("hidden");
		gallery.video.parentNode.load();
	} else {
		gallery.image.src = node.dataset.image;
		gallery.image.dataset.reveal = node.dataset.reveal;
		gallery.image.classList.remove("hidden");
		if(!gallery.video.parentNode.classList.contains("hidden")) gallery.video.parentNode.classList.add("hidden");
	}
	gallery.title.innerText = node.dataset.description;
	gallery.title.href = node.dataset.url;
	gallery.directLink.href = node.dataset.reveal || node.dataset.image;
	if(pushState) history.pushState({ind: index}, "", "#" 
		+ Array.from(document.getElementsByClassName("gallery")).indexOf(gallery.currentGallery) + "-" + (index+1));

	if(gallery.currentGallery.getElementsByClassName("galleryItem").length > 1){
		gallery.next.classList.remove("hidden");
		gallery.previous.classList.remove("hidden");
	} else {
		if(!gallery.next.classList.contains("hidden")){
			gallery.next.classList.add("hidden");
			gallery.previous.classList.add("hidden");
		}
	}

	let tmpImg = document.createElement("img");
	tmpImg.src = gallery.currentGallery.getElementsByClassName("galleryItem")[gallery.getIndex(index+1)].dataset.image;
	let tmpImg2 = document.createElement("img");
	tmpImg2.src = gallery.currentGallery.getElementsByClassName("galleryItem")[gallery.getIndex(index-1)].dataset.image;
};
gallery.getIndex = (index) => {
	if(index < 0){
		index = gallery.currentGallery.getElementsByClassName("galleryItem").length - 1;
	} else if (index > gallery.currentGallery.getElementsByClassName("galleryItem").length - 1){
		index = 0; 
	}
	return index;
};
gallery.close = () => {
	if(!gallery.overlay.classList.contains("hidden")) gallery.overlay.classList.add("hidden");
	if(document.body.classList.contains("overflow-disabled")) document.body.classList.remove("overflow-disabled");
	gallery.index = null;
	gallery.image.src = "";
	gallery.title.innerText = "";
	gallery.video.parentNode.pause();
	if(!!(document.fullscreenElement || document.mozFullScreenElement || document.webkitFullscreenElement || document.msFullscreenElement)){
		document.exitFullscreen();
	}
	history.pushState({ind: ""}, "","#");
	if(typeof navigation !== 'undefined') navigation.enabled = true;
};

gallery.init();
//handles swipe, arrow key, and hjkl navigation.

var navigation = {};

navigation.xDown = null;
navigation.yDown = null
navigation.enabled = true;

navigation.init = () => {
	document.addEventListener('touchstart', navigation.handleTouchStart, false);
	document.addEventListener('touchmove', navigation.handleTouchMove, false);
	document.addEventListener('keyup', navigation.handleKeyUp, false);
};

navigation.handleTouchStart = (evt) => {
	navigation.xDown = evt.touches[0].clientX;                                      
    navigation.yDown = evt.touches[0].clientY; 
};

navigation.handleTouchMove = (evt) => {
	if ( ! navigation.xDown || ! navigation.yDown ) {
        return;
    }

    var xUp = evt.touches[0].clientX;                                    
    var yUp = evt.touches[0].clientY;

    var xDiff = xDown - xUp;
    var yDiff = yDown - yUp;

    if(!readCookie("swipe") || "on" == readCookie("swipe")){
        if ( Math.abs( xDiff ) > Math.abs( yDiff ) ) {/*most significant*/
            if ( xDiff > 0 ) {
            	document.getElementById("nextPost").click();
            } else {
            	document.getElementById("prevPost").click();
            }                       
        } 
    }
    
    /* reset values */
    navigation.xDown = null;
    navigation.yDown = null; 
};

navigation.handleKeyUp = (evt) => {
	if(evt.keyCode != 18 && (document.activeElement.nodeName != 'TEXTAREA' 
		&& document.activeElement.nodeName != 'INPUT'
		&& navigation.enabled))	{
	    switch(evt.keyCode) {
	    	case 75: // k key
	         	if ( document.getElementById("nextThread").href){
	         		document.getElementById("nextThread").click();
	        	}
	        	break;
	        case 74: // j key
	         	if ( document.getElementById("prevThread").href){
	         		document.getElementById("prevThread").click();
	        	}
	        	break;
	        case 72: // h key
	        case 37: // left arrow
	         	if ( document.getElementById("prevPost").href){
	         		document.getElementById("prevPost").click();
	        	}
	        	break;
	        case 76: // l key
	        case 39: // right arrow
	       		if ( document.getElementById("nextPost").href){
	         		document.getElementById("nextPost").click();
	        	}
	        	break;
	        default: return;
	    }
    	evt.preventDefault();
	}
};

navigation.init();

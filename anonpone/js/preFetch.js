//pre-fetches list of images

var preFetch = {}

preFetch.init = () => {
	let links = ["nextPost", "prevPost", "nextThread","prevThread"];
	for(link of links){
		if(document.getElementById(link) && document.getElementById(link).href){
			preFetch.fetchPost(document.getElementById(link).href.split("/")[6].slice(0,-11));
		}
	}
}

preFetch.fetchPost = (id) => {
	var imageURL = "https://img.anonpone.com/image/";
	let url = "/quest/post/" + document.getElementById("cyoaid").value + "/" + id;
	api.localRequest(url,false,(error,post) => {
		post = JSON.parse(post);
		if(!error){
			if(post["_filename"]){
				let folder = post["_filename"].slice(0,2);
				if(folder == "ad") folder = "zzz";
				let tmpImg = new Image();
				tmpImg.src=imageURL + folder + "/" + post["_filename"];
			}
			if(post["_filenameLewd"]){
				let folder = post["_filenameLewd"].slice(0,2);
				if(folder == "ad") folder = "zzz";
				let tmpImg = new Image();
				tmpImg.src=imageURL + folder + "/" + post["_filenameLewd"];
			}
		}
	});
}

preFetch.init();
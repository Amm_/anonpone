var settingMenu = {};

settingMenu.bodyElement;
settingMenu.close;

settingMenu.init = () => {
	settingMenu.construct();
	settingMenu.register("Lewd","lewd","true",true,() => {window.location.reload();});
}

settingMenu.construct = () => {
	var gear = document.createElement("a");
	gear.title = "Option Menu";
	gear.classList.add("icon");
	gear.classList.add("gear");
	gear.classList.add("floatRight");
	gear.href="#modal-settingMenu";
	document.getElementById("navbar").appendChild(gear);

	var overlay = document.createElement("div");
	overlay.classList.add("modal");
	overlay.id = "modal-settingMenu";
	overlay.onclick = (evt) => {
		settingMenu.close.click();
	}

	var content = document.createElement("div");
	content.classList.add("content");
	content.classList.add("panel");
	content.onclick = (evt) => {
		evt.stopPropagation();
	}
	overlay.appendChild(content);

	var header = document.createElement("div");
	header.classList.add("row");
	header.classList.add("header");
	settingMenu.close = close = document.createElement("a");
	close.href = "#!";
	close.title = "Close";
	close.classList.add("icon");
	close.classList.add("close");
	close.classList.add("floatRight");
	var headerText = document.createElement("h5");
	headerText.innerText = "Settings";
	header.appendChild(headerText);
	header.appendChild(close);
	content.appendChild(header);

	settingMenu.bodyElement = body = document.createElement("div");
	body.classList.add("row");
	body.classList.add("body");
	content.appendChild(body);

	document.body.appendChild(overlay);
}

settingMenu.register = (label,name,options,cookie = false,callback = null) => {
	let labelElement = document.createElement("label");
	labelElement.htmlFor = "settings-" + name;
	labelElement.innerText = label;
	let currentValue = localStorage.getItem(name);

	let input;
	if(Array.isArray(options)){
		input = document.createElement("select");
		for(option of options){
			let optionElement = document.createElement("option");
			optionElement.value = option[1];
			optionElement.innerText = option[0];
			input.appendChild(optionElement);
		}
		input.value = currentValue;
		input.onchange = (evt) => {
			localStorage.setItem(evt.target.name,evt.target.value);
			if(cookie) cookies.set(evt.target.name,evt.target.value);
			if(callback) callback();
		}
	} else {
		input = document.createElement("input");
		input.type = "checkbox";
		input.classList.add("floatRight");
		input.value = options;
		if(currentValue) input.checked = true;
		input.onchange = (evt) => {
			if(evt.target.checked){
				localStorage.setItem(evt.target.name,"true");
				if(cookie) cookies.set(evt.target.name,"true");
			} else {
				localStorage.removeItem(evt.target.name);
				if(cookie) cookies.remove(evt.target.name);
			}
			if(callback) callback();
		}
	}
	input.name = name;
	input.id = "settings-" + name;

	settingMenu.bodyElement.appendChild(input);
	settingMenu.bodyElement.appendChild(labelElement);
}

settingMenu.init();

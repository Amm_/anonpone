var api = {};
api.ifModified = {};

api.localRequest = (address, isStored, callback) => {
	api.request("/api" + address,isStored,callback);
}

api.request = (address, isStored, callback) => {
	var xhr = new XMLHttpRequest();

	if ('withCredentials' in xhr) {
	    xhr.open('GET', address, true);
	} else if (typeof XDomainRequest != 'undefined') {
	    xhr = new XDomainRequest();
	    xhr.open('GET', address);
	} else {
	    alert('Update your browser or turn off javascript.');
	    return;
	}
	var ifModified;
	if(isStored){
		ifModified = JSON.parse(localStorage.getItem("apiModified")) ?? {};
	} else {
		ifModified = api.ifModified;
	}
	if(ifModified[address]){
		xhr.setRequestHeader("if-modified-since", ifModified[address]);
	}

	xhr.onreadystatechange = function connectionStateChanged() {
	    if (xhr.readyState == 4) {
	      	if (callback.hasOwnProperty('stop')) {
	        	callback.stop();
	      	}
	      	if(xhr.status === 304){
	      		callback('304 Not Modified');
	      		console.log(address + " checked");
	      	} else if (xhr.status != 200) {
	        	callback('Connection failed');
	      	} else {
	      		if(xhr.getResponseHeader("Last-Modified")){
	      			ifModified[address] = xhr.getResponseHeader("last-modified");
	      			if(isStored){
	      				localStorage.setItem("apiModified",JSON.stringify(ifModified));
	      			} else {
	      				api.ifModified = ifModified;
	      			}
	      		}
	      		console.log(address + " completed");
	        	callback(null, xhr.responseText);
	      	}
	    }
	};

	xhr.send();
};

api.getParameter = (parameter) => {
    var result = null, tmp = [];
    location.search.substr(1).split("&")
        .forEach((item) => {
          tmp = item.split("=");
          if (tmp[0] === parameter) result = decodeURIComponent(tmp[1]);
        });
    return result;
}


var cookies = {}

cookies.set = (name,value,days = 365) => {
    var expires = "";
    if (days) {
        var date = new Date();
        date.setTime(date.getTime() + (days*24*60*60*1000));
        expires = "; expires=" + date.toUTCString();
    }
    document.cookie = name + "=" + (value || "")  + expires + "; path=/";
}
cookies.get = (name) => {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for(var i=0;i < ca.length;i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1,c.length);
        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
    }
    return null;
}
cookies.remove = (name) => {   
    cookies.set(name,"",-1);  
}
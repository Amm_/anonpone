var theme = {};

theme.init = () => {
	if(typeof settingMenu !== 'undefined') settingMenu.register("Theme","theme",[
		["Yotsuba B","yotsubab"],
		["Yotsuba","yotsuba"],
		["Tomorrow","tomorrow"],
		],false,theme.set);

	if (!localStorage.getItem("theme") && window.matchMedia && window.matchMedia('(prefers-color-scheme: dark)').matches) {
    	localStorage.setItem("theme","tomorrow");
	}
	theme.set();
}

theme.set = () => {
	let currentValue = localStorage.getItem("theme") ?? "";
	if(currentValue){ 
		document.body.classList = "";
		document.body.classList.add(currentValue);
	}
}

theme.init();
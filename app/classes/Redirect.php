<?php

class Redirect {
	public static function to($location = null): void {
		if($location){
			if(is_numeric($location)){
				switch($location){
					case 404;
						header('HTTP/1.1 404 Not Found');
						include Config::get('folder/includes') . '/errors/404.php';
						exit();
					case 403;
						header('HTTP/1.1 403 Forbidden');
						include Config::get('folder/includes') . '/errors/404.php';
						exit();
					case 503;
						header('HTTP/1.1 503 Service Unavailable');
						include Config::get('folder/includes') . '/errors/503.php';
						exit();
					break;	
				}
			}
			header('Location: ' . $location,TRUE,301);
			exit();
		}
	}

	public static function back(){
		$list = \Config::get("website");
		foreach ($list as &$word) $word = preg_quote($word, '/');

		$num_found = preg_match_all('/('.join('|', $list).')/i', $string, $matches);
		if($num_found > 0){ 
			header('Location: ' . $_SERVER['HTTP_REFERER']);
		} else {
			\Redirect::to(404);
		}
	}
}

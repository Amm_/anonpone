<?php
class Paginator{
	private $_currentPage,$_itemsPerPage,$_totalItems;
	private $_ippArray,$_midRange,$_query;

	public function __construct(int $total=0,int $midRange=7,$ippArray=array(10,25,50,100,"All"),$ipp = null,$query = null) {
		$this->_totalItems = $total;
		$this->_midRange = $midRange;
		$this->_ippArray = $ippArray;
		$this->_itemsPerPage = (Input::exists("ipp")) ? Input::get("ipp") : $ipp ?? $this->_ippArray[0];
		$this->_currentPage = (Input::exists("page")) ? Input::get("page") : 1 ;
		$this->_query = $query;
		if($this->_currentPage < 0) $this->_currentPage = 0;

		if($this->midRange() <= 0) throw new Exception("Unable to paginate: Invalid mid range value (must be an integer > 0)");
		if(!is_array($this->ippArray())) throw new Exception("Unable to paginate: Invalid ipp array value");

		if($this->itemsPerPage() <= 0) $this->_itemsPerPage = $this->_ippArray[0];
		if(!strcasecmp($this->itemsPerPage(),"All") || $this->currentPage() <= 0) $this->_itemsPerPage = 0;
		if($this->midRange() % 2 == 0) $this->_midRange++;
	}

	public function currentPage(): int { return $this->_currentPage;}
	public function itemsPerPage(bool $alpha = false){ return (($alpha && $this->_itemsPerPage === 0) ? "All" : (int) $this->_itemsPerPage);}
	public function totalItems(): int { return $this->_totalItems;}
	public function ippArray(): array { return $this->_ippArray;}
	public function midRange(): int { return $this->_midRange;}
	public function limitStart(): int { return ((!$this->itemsPerPage()) ? 0 : ($this->currentPage()-1) * $this->itemsPerPage());}
	public function limitEnd(): int { return ((!$this->itemsPerPage()) ? $this->totalItems() : $this->itemsPerPage());}
	public function queryString(): string { 
		$queryString = "";
		if($_GET) {
			$args = explode("&",$_SERVER["QUERY_STRING"]);
			foreach($args as $arg) {
				$keyval = explode("=",$arg);
				if($keyval[0] != "page" && $keyval[0] != "ipp" && $keyval[0] != "url") $queryString .= "&" . $arg;
			}
		}
		if($_POST) {
			foreach($_POST as $key=>$val) {
				if(is_string($val) && is_string($key) && $key != "page" && $key != "ipp" && $key != "url") $queryString .= "&$key=$val";
			}
		} 
		return $queryString;
	}

	public function displayPages(): string {
		$return = "<ul class='btn-group shrink'>";
		$numPages = (!$this->itemsPerPage() ? 1 : ceil($this->totalItems()/$this->itemsPerPage()));
		if($numPages > 10) {
			$return .= ($this->currentPage() > 1 && $this->totalItems() >= 10) ? "<li class=\"btn\"><a href=\"/" . Input::get("url") . "?page=".($this->currentPage()-1)."&ipp={$this->itemsPerPage(true)}{$this->queryString()}\">Previous</a></li> ":"<li class='btn disabled'><span class=\"inactive\" href=\"#\">Previous</span></li> ";
			$startRange = $this->currentPage() - floor($this->midRange() / 2);
			$endRange = $this->currentPage() + floor($this->midRange() / 2);
			if($startRange <= 0) {
				$endRange += abs($startRange) + 1;
				$startRange = 1;
			}
			if($endRange > $numPages) {
				$startRange -= $endRange - $numPages;
				$endRange = $numPages;
			}
			$range = range($startRange,$endRange);

			for($i=1;$i<=$numPages;$i++) {
				if($i==1 || $i==$numPages || in_array($i,$range)) {
					$return .= ($i == $this->currentPage() && $this->itemsPerPage()) ? "<li class='btn active'><a title=\"Go to page {$i} of {$numPages}\" class=\"current\" href=\"#\">{$i}</a></li> \n":"<li class=\"btn\"><a title=\"Go to page {$i} of {$numPages}\" href=\"/" . Input::get("url") . "?page={$i}&ipp={$this->itemsPerPage(true)}{$this->queryString()}\">{$i}</a></li> \n";
				}
			}
			$return .= (($this->currentPage() < $numPages && $this->totalItems() >= 10) && $this->currentPage() > 0) ? "<li class=\"btn\"><a href=\"/" . Input::get("url") . "?page=".($this->currentPage() + 1)."&ipp={$this->itemsPerPage(true)}{$this->queryString()}\">Next</a></li>\n":"<li class='btn disabled'><span href=\"#\">Next</span></li>\n";
		} else {
			for($i=1;$i<=$numPages;$i++) {
				$return .= ($i == $this->currentPage()) ? "<li class='btn active'><a class=\"current\" href=\"#\">{$i}</a></li> ":"<li class=\"btn\"><a href=\"/" . Input::get("url") . "?page={$i}&ipp={$this->itemsPerPage(true)}{$this->queryString()}\">{$i}</a></li> ";
			}
		}
		$return = str_replace("&","&amp;",$return);

		$return .= "</ul>";
		return $return;
	}
	public function displayPageAmounts(): string {
		$construct = "<div>Displaying {$this->limitStart()} to ";
		$construct .= ($this->limitStart() + $this->itemsPerPage() > $this->totalItems()) ? $this->totalItems() : $this->limitStart() + $this->itemsPerPage();
		$construct .= " out of {$this->totalItems()}";
		$construct .= ($this->_query) ? " for <i>" . htmlentities($this->_query) . "</i>" : "";
		$construct .= "</div>";
		return $construct;
	}
	public function displayItemsPerPage(): string {
		$items = NULL;
		natsort($this->ippArray()); // This sorts the drop down menu options array in numeric order (with 'all' last after the default value is picked up from the first slot
		foreach($this->ippArray() as $ipp_opt) $items .= ($ipp_opt == $this->itemsPerPage()) ? "<option selected value=\"{$ipp_opt}\">{$ipp_opt}</option>\n":"<option value=\"{$ipp_opt}\">{$ipp_opt}</option>\n";

		return "<span class=\"paginate\">Items per page:</span><select class=\"paginate\" onchange=\"window.location='/" . Input::get("url") . "?page=1&amp;ipp='+this[this.selectedIndex].value+'{$this->$this->queryString()}';return false\">{$items}</select>\n";
	}
	public function displayJumpMenu(): string {
		$option=NULL;
		for($i=1;$i<=$this->num_pages;$i++) {
			$option .= ($i==$this->current_page) ? "<option value=\"{$i}\" selected>{$i}</option>\n":"<option value=\"{$i}\">{$i}</option>\n";
		}
		return "<span class=\"paginate\">Page:</span><select class=\"paginate\" onchange=\"window.location='/" . Input::get("url") . "?page='+this[this.selectedIndex].value+'&amp;ipp={$this->itemsPerPage(true)}{$this->$this->queryString()}';return false\">$option</select>\n";
	}
}
?>

<?php
// abstract class DaysOfWeek extends BasicEnum {
//     const Sunday = 0;
//     const Monday = 1;
//     const Tuesday = 2;
//     const Wednesday = 3;
//     const Thursday = 4;
//     const Friday = 5;
//     const Saturday = 6;
// }

// DaysOfWeek::isValidName('Humpday');                  // false
// DaysOfWeek::isValidName('Monday');                   // true
// DaysOfWeek::isValidName('monday');                   // true
// DaysOfWeek::isValidName('monday', $strict = true);   // false
// DaysOfWeek::isValidName(0);                          // false

// DaysOfWeek::isValidValue(0);                         // true
// DaysOfWeek::isValidValue(5);                         // true
// DaysOfWeek::isValidValue(7);                         // false
// DaysOfWeek::isValidValue('Friday');                  // false

abstract class Enum
{
    private static $constCacheArray = NULL;

    private static function getConstants()
    {
        if (self::$constCacheArray == NULL) {
            self::$constCacheArray = [];
        }
        $calledClass = get_called_class();
        if (!array_key_exists($calledClass, self::$constCacheArray)) {
            $reflect = new \ReflectionClass($calledClass);
            self::$constCacheArray[$calledClass] = array_filter($reflect->getConstants(), function($value){
                return (is_string($value) || is_int($value));
            });
        }
        return self::$constCacheArray[$calledClass];
    }

    public static function getValues(): array {
        return self::getConstants();
    }
    public static function getNames(): array {
        $values = self::getConstants();
        $strings = [];
        foreach($values as $value){
            $strings[] = static::toString($value);
        }
        return $strings;
    }

    public static function isValidName(string $name,bool $strict = false): bool {
        $constants = self::getConstants();

        if ($strict) {
            return array_key_exists($name, $constants);
        }

        $keys = array_map('strtolower', array_keys($constants));
        return in_array(strtolower($name), $keys);
    }

    public static function isValidValue(int $value,bool $strict = true): bool {
        $values = array_values(self::getConstants());
        return in_array($value, $values, $strict);
    }

    public static function fromString(string $name): ?int {
        if (self::isValidName($name, $strict = true)) {
            $constants = self::getConstants();
            return $constants[$name];
        }

        return null;
    }

    public static function toString(int $value): ?string {
        if (self::isValidValue($value, $strict = true)) {
            return array_search($value, self::getConstants());
        }

        return null;
    }
}
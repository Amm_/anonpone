<?php

class Validation{
	protected $errorHandler;
	protected $items;
	protected $rules;
	protected $badFields = [];

	public $messages = array(
		'required' => 'The :field field is required',
		'minlength' => 'The :field field must be a minimum of :satisfier length',
		'maxlength' => 'The :field field must be a maximum of :satisfier length',
		'email' => 'That is not a valid email address',
		'alnum' => 'The :field field must be alphanumeric',
		'num' => 'The :field field must be numeric',
		'min' => 'The :field field must be a minimum of :satisfier',
		'max' => 'The :field field must be a maximum of :satisfier',
		'bool' => 'The :field field must be boolean',
		'url' => 'The :field field must be a valid URL',
		'match' => 'The :field field must match the :satisfier field',
		'unique' => 'That :field field is already taken',
		'exists' => 'That value in the :field field doens\'t exist',
		'password' => 'The password entered is incorrect',
		'extension' => 'The extension must be one of :satisfier',
		'enum' => 'The :field field is not a valid value',
		'regex' => 'The :field field is not a valid regular expression',
		'whitelist' => 'The :field field includes a character that is not in the whitelist',
		'blacklist' => 'The :field field includes a character that is blacklisted',
		'hasCapital' => 'The :field field must include a capital letter',
		'hasLowercase' => 'The :field field must include a lowercase letter',
		'hasNumber' => 'The :field field must include a number',
		'hasSpecial' => 'The :field field must include a special character (!,@,#,$,%,+,^,&,?)',
		'includes' => 'The :field field must include :satisfier',
		'excludes' => 'The :field field must exclude :satisfier',
	);

	public function __construct(ErrorHandler $errorHandler)	{
		$this->errorHandler = $errorHandler;
		$this->rules = array_keys($this->messages);
	}

	public function check(array $items, array $rules): self {
		$this->items = $items;
		foreach($items as $item => $value){
			if(in_array($item, array_keys($rules)))	{
				$this->validate(array(
					'field' => $item,
					'value' => $value,
					'rules' => $rules[$item]
				));
			}
		}
		return $this;
	}

	public function fails(): bool {
		return $this->errorHandler->hasErrors();
	}

	public function errors(): ErrorHandler {
		return $this->errorHandler;
	}

	public function isBadField(string $field): bool {
		return in_array($field,$this->badFields);
	}

	protected function validate(array $item): void {
		$field = $item['field'];

		foreach($item['rules'] as $rule => $satisfier){
			if(in_array($rule, $this->rules)){
				if(!$this->$rule($field, $item['value'], $satisfier)){
					$this->errorHandler->addError(
						str_replace([':field', ':satisfier'],[$field,(is_array($satisfier) ? implode(", ",$satisfier) : $satisfier)],$this->messages[$rule]),
						'validation'
					);
					$this->badFields[] = $field;
				}
			}
		}
	}

	protected function required($field, $value, $satisfier): bool {
		if(is_string($value)) $value = trim($value);
		return !(is_null($value) || (is_string($value) && $value === '') || (is_array($value) && count($value) === 0));
	}

	protected function maxlength($field, $value, $satisfier): bool {
		return (!$this->required($field,$value,$satisfier) || strlen($value) <= $satisfier);
	}

	protected function minlength($field, $value, $satisfier): bool {
		return (!$this->required($field,$value,$satisfier) || strlen($value) >= $satisfier);
	}

	protected function max($field, $value, $satisfier): bool {
		return (!$this->required($field,$value,$satisfier) || $value <= $satisfier);
	}

	protected function min($field, $value, $satisfier): bool {
		return (!$this->required($field,$value,$satisfier) || $value >= $satisfier);
	}

	protected function email($field, $value, $satisfier): bool {
		return (!$this->required($field,$value,$satisfier) || filter_var($value, FILTER_VALIDATE_EMAIL));
	}

	protected function alnum($field, $value, $satisfier): bool {
		return (!$this->required($field,$value,$satisfier) || ctype_alnum($value));
	}
	protected function num($field, $value, $satisfier): bool {
		return (!$this->required($field,$value,$satisfier) || is_numeric($value));
	}
	protected function bool($field, $value, $satisfier): bool {
		return (!$this->required($field,$value,$satisfier) || !is_null(filter_var($value, FILTER_VALIDATE_BOOLEAN,FILTER_NULL_ON_FAILURE)));
	}
	protected function url($field, $value, $satisfier): bool {
		return (!$this->required($field,$value,$satisfier) || filter_var($value, FILTER_VALIDATE_URL));
	}

	protected function match($field, $value, $satisfier): bool {
		return (!$this->required($field,$value,$satisfier) || $value === $this->items[$satisfier]);
	}

	protected function extension($field,$value,$satisfier): bool {
		if(!$this->required($field,$value,$satisfier)) return true;
		$ext = pathinfo($value, PATHINFO_EXTENSION);
		return in_array($ext,$satisfier);
	}

	// protected function unique($field, $value, $satisfier){ //'unique' => array(<table>,<column>),
	// 	if(!$this->required($field,$value,$satisfier)) return true;
	// 	return !$this->_db->exists($satisfier[0],array($satisfier[1], '=', $value));
	// }
	// protected function exists($field, $value, $satisfier){
	// 	if(!$this->required($field,$value,$satisfier)) return true;
	// 	return $this->_db->exists($satisfier[0],array($satisfier[1], '=', $value));
	// }

	protected function password($field,$value,$satisfier): bool {
		if(!$this->required($field,$value,$satisfier)) return true;
		$user = \User\User::getInstance;
		$login = \User\Login($user);
		if($login->validPassword($value)){
			return true;
		} else {
			return false;
		}
	}

	protected function enum($field,$value,$satisfier): bool {
		if(!$this->required($field,$value,$satisfier)) return true;
		if(class_exists($satisfier)){
			return $satisfier::isValidValue($value,false);
		} else {
			return false;
		}
	}

	protected function regex($field,$value,$satisfier): bool {
		if(!$this->required($field,$value,$satisfier)) return true;
		if($value[0] !== "/"){ $value = "/" . $value . "/i";}
		return !(preg_match($value,null) === false);
	}

	protected function whitelist($field,$value,$satisfier): bool {
		if(!$this->required($field,$value,$satisfier)) return true;
		$regex = "/^[" . implode("",$satisfier) . "]+$/ux";
		if($this->regex($field,$regex,true)){
			return preg_match($regex, $value);
		}
	}
	protected function blacklist($field,$value,$satisfier): bool {
		if(!$this->required($field,$value,$satisfier)) return true;
		$regex = "/[" . implode("",$satisfier) . "]/ux";
		if($this->regex($field,$regex,true)){
			return !preg_match($regex, $value);
		}
	}

	protected function hasCapital($field,$value,$satisfier): bool {
		if(!$this->required($field,$value,$satisfier)) return true;
		$regex = "/[A-Z]/u";
		if($this->regex($field,$regex,true)){
			return preg_match($regex, $value);
		}
	}
	protected function hasLowercase($field,$value,$satisfier): bool {
		if(!$this->required($field,$value,$satisfier)) return true;
		$regex = "/[a-z]/u";
		if($this->regex($field,$regex,true)){
			return preg_match($regex, $value);
		}
	}
	protected function hasNumber($field,$value,$satisfier): bool {
		if(!$this->required($field,$value,$satisfier)) return true;
		$regex = "/[0-9]/u";
		if($this->regex($field,$regex,true)){
			return preg_match($regex, $value);
		}
	}
	protected function hasSpecial($field,$value,$satisfier): bool {
		if(!$this->required($field,$value,$satisfier)) return true;
		$regex = "/[!@#$%+^&?]/u";
		if($this->regex($field,$regex,true)){
			return preg_match($regex, $value);
		}
	}
	
	protected function includes($field,$value,$satisfier): bool {
		if(!$this->required($field,$value,$satisfier)) return true;
		if(!is_array($satisfier)) $satisfier = [$satisfier];
		foreach($satisfier as $s){
			$regex = "/" . preg_quote($s) . "/i";
			if($this->regex($field,$regex,true)){
				if(!preg_match($regex, $value)) return false;
			}
		}
		return true;
	}
	protected function excludes($field,$value,$satisfier): bool {
		if(!$this->required($field,$value,$satisfier)) return true;
		if(!is_array($satisfier)) $satisfier = [$satisfier];
		foreach($satisfier as $s){
			$regex = "/" . preg_quote($s) . "/i";
			if($this->regex($field,$regex,true)){
				if(preg_match($regex, $value)) return false;
			}
		}
		return true;
	}
}

<?php

class FormBuilder extends Enum
{
	private $_id;
	private $_action;
	private $_method;
	private $_errorHandler;
	private $_validation;
	private $_nameCount = [];
	private $_submit = [];
	private $_requestTimeMinimum = 1500;
	private $_requestTimeMaximum = 24*60*60*1000;
	private $_requestTimeKey = "Test";

	private $_botCheck = [
		"honeypot" => true,
		"requestTime" => true,
		"CSRFToken" => true,
		"recaptcha" => false,
	];

	private $_honeypotOptions = ["2email","NameTheFirst","contactadmin"];

	private $_fields = array();
	private $_arrayRegex = "/\[(\w*)\]/";
	private $_hash;

	const Title = 1;
	const Stat = 2;
	const Text = 3;
	const TextArea = 4;
	const Select = 5;
	const Multi = 6;
	const Submit = 7;
	const Hidden = 8;
	const Horizontal = 9;
	const Checkmark = 10;
	const Button = 11;
	const Password = 12;

	public function __construct($id = null, $action = null, $method = "POST") {
		$this->_hash = new Hash();
		if($id) $this->_id = $id;
		else if (get_class($this) !== "FormBuilder") $this->_id = get_class($this);
		else $this->_id = $this->_hash->UUID(5);
		$this->_action = $action;
		$this->_method = $method;
		$this->_errorHandler = new ErrorHandler();
	}

	public function addField(array ...$fields): self {
		$validFields = [];
		foreach($fields as $field){
			$vfield = $this->validateField($field);
			if($vfield){
				array_push($validFields,$vfield);
			}
		}
		$this->_fields = array_merge($this->_fields,$fields);
		return $this;
	}
	private function validateField(array $field) {
		if(array_key_exists('name', $field)){
			if(array_key_exists('type', $field) && static::isValidValue($field['type'])){
				switch($field['type']){
					case static::Title:
						if(!array_key_exists('label',$field)){ $this->_errorHandler->addError("{$this->_id}: Label field required for {$field['name']}",'create');
						return false; }
					case static::Hidden:
					case static::Stat:
					case static::Button:
						if(!array_key_exists('value',$field)){ $this->_errorHandler->addError("{$this->_id}: Value field required for {$field['name']}",'create');
						return false; }
					case static::Select:
						if(!array_key_exists('option',$field)){ $this->_errorHandler->addError("{$this->_id}: Option field required for {$field['name']}",'create');
						return false; }
					case static::Multi:
						if(!array_key_exists('fields',$field)){ $this->_errorHandler->addError("{$this->_id}: Fields field required for {$field['name']}",'create');
						return false; }
						foreach($field['fields'] as $key => $group){
							foreach($group as $k => $f){
								$temp = $this->validateField($f);
								if($temp) $field['fields'][$key][$k] = $temp;
								else unset($field['fields'][$key][$k]);
							}
						}
					break;
					case static::Horizontal:
						if(!array_key_exists('fields',$field)){ $this->_errorHandler->addError("{$this->_id}: Fields field required for {$field['name']}",'create');
						return false; }
						foreach($field['fields'] as $key => $f){
							$temp = $this->validateField($f);
							if($temp) $field['fields'][$key] = $temp;
							else unset($field['fields'][$key]);
						}
					break;
					case static::Submit:
						$this->_submit[] = $field;
					case static::Text:
					case static::TextArea:
					default:
						break;
				}
			} else {
				$this->_errorHandler->addError("{$this->_id}: Type Required for {$field['name']}",'create');
				return false;
			}
		} else {
			$this->_errorHandler->addError("{$this->_id}: Malformed input",'create');
			return false;
		}
		return $field;
	}

	public function antibot(array ...$args): self {
		if(in_array("honeypot",$args)) $_botCheck['honeypot'] = ($_botCheck['honeypot']) ? false : true;
		if(in_array("CSRFToken",$args)) $_botCheck['CSRFToken'] = ($_botCheck['CSRFToken']) ? false : true;
		if(in_array("recaptcha",$args)) $_botCheck['recaptcha'] = ($_botCheck['recaptcha']) ? false : true;
		return $this;
	}

	public function build(): string {
		$this->_nameCount = [];
		$return = "<form";
		if($this->_action) $return .= " action=\"{$this->_action}\"";
		if($this->_id) $return .= " id=\"{$this->_id}\"";
		$return .= " method=\"{$this->_method}\">";
		$return .= "<table class=\"table table-condensed\" style=\"margin-bottom:0px\">";
		$token = new Token();

		$fieldsCopy = $this->_fields;

		$botfields = [];
		if($this->_botCheck['honeypot']){
			$botfields[] =  [
				"name" => $this->_honeypotOptions[array_rand($this->_honeypotOptions)],
				"type" => \FormBuilder::Text,
				"class" => "hidden",
				"attributes" => [
					"autocomplete" => "off",
				]
			];
		}
		if($this->_botCheck['requestTime']){
			$botfields[] =  [
				"name" => "requestTime",
				"type" => \FormBuilder::Hidden,
				"value" => $this->_hash->encrypt(round(microtime(true) * 1000),$this->_requestTimeKey),
				"ignoreInput" => true,
				"attributes" => [
					"autocomplete" => "off",
				]
			];
		}
		if($this->_botCheck['CSRFToken']){
			$tokenName = $token->get(Config::get("csrf/name"),10);
			$tokenValue = $token->get(Config::get("csrf/value"),500);
			$botfields[] =  [
				"name" => $tokenName,
				"type" => \FormBuilder::Hidden,
				"value" => $tokenValue,
			];
		}

		foreach($botfields as $bot){
			$return .= $this->describeField($bot);
		}

		foreach($fieldsCopy as $field){
			if($field['type'] !== static::Hidden) {
				$return .= "<tr class=\" ";
				if(!is_null($this->_validation) && array_key_exists('name',$field) && $this->_validation->isBadField($field['name'])) $return .= "danger ";
				$return .="\"><td>";
			}
			if(array_key_exists('label',$field)){
				if(array_key_exists('name',$field)){	
			 		$return .= "<label for=\"{$this->_id}-{$field['name']}\">{$field['label']}</label>"; 
			 	} else if($field['type'] === static::Title) {
			 		$return = substr($return, 0, -1) . " colspan=\"100\"><center><h4>{$field['label']}</h4></center>";
			 	} else {
			 		$return .= "<div>{$field['label']}</div>";
			 	}
			 	$return .= "</td><td>";
			}
			$return .= $this->describeField($field);

			if($field['type'] !== static::Hidden) { $return .= "</td></tr>"; }
		}
		$return .= "</table>";
		$return .= "</form>";
		return $return;
	}
	private function describeField(array $field): string {
		$return = "";
		$value = "";
		$matches;
		if(array_key_exists("name", $field)){
			if(!array_key_exists($field["name"], $this->_nameCount)) $this->_nameCount[$field["name"]] = 0;
			else $this->_nameCount[$field["name"]]++;
		}

		if(array_key_exists("name",$field) && preg_match($this->_arrayRegex,$field['name'],$matches) && Input::exists(preg_replace($this->_arrayRegex,"",$field['name'])) && !(array_key_exists('ignoreInput', $field) && $field['ignoreInput']) ) {
			$inputArray = Input::get(preg_replace($this->_arrayRegex,"",$field['name']));
			$arrayKey =  $matches[1] ?: $this->_nameCount[$field["name"]];
			if(array_key_exists($arrayKey,$inputArray))	$value = $inputArray[$arrayKey];
		}  
		if($value === ""){
			if(array_key_exists("name",$field) && Input::exists($field['name']) && !(array_key_exists('ignoreInput', $field) && $field['ignoreInput']) ) {
				$value = Input::get($field["name"]);
			} else if(array_key_exists("value",$field) && $field['value']) {
				$value = $field['value'];
			} else if(array_key_exists("default",$field)) {
				$value = $field['default'];
			}
		}
		switch($field['type']){
			case static::Title:
				break;
			case static::Stat:
				$return .= $value;
				break;
			case static::Hidden:
				$return .= "<input type=\"hidden\" id=\"{$this->_id}-{$field['name']}\" name=\"{$field['name']}\"";
				$return .= " value=\"{$value}\"";
				$return .= ((array_key_exists('validation',$field) && array_key_exists('required',$field['validation'])) ? " required=\"required\"":"");
				$return .= " />";
				break;
			case static::Text:
				$return .= "<input type=\"text\" id=\"{$this->_id}-{$field['name']}\" name=\"{$field['name']}\"";
				$return .= " class=\"form-control";
				$return .= (array_key_exists('class',$field) ? " {$field['class']}" : "");
				$return .= "\" ";
				$return .= (array_key_exists('placeholder',$field) ? " placeholder=\"{$field['placeholder']}\"" : "");
				$return .= " value=\"{$value}\"";
				$return .= (array_key_exists('style',$field) ? " style=\"{$field['style']}\"" : "");
				if(array_key_exists("attributes",$field)){
					foreach($field["attributes"] as $name => $val){
						$return .= " {$name}=\"{$val}\"";
					}
				}
				$return .= ((array_key_exists('validation',$field) && array_key_exists('required',$field['validation'])) ? " required=\"required\"":"");
				$return .= " />";
				break;
			case static::Password:
				$return .= "<input type=\"password\" id=\"{$this->_id}-{$field['name']}\" name=\"{$field['name']}\"";
				$return .= " class=\"form-control";
				$return .= (array_key_exists('class',$field) ? " {$field['class']}" : "");
				$return .= "\" ";
				$return .= (array_key_exists('placeholder',$field) ? " placeholder=\"{$field['placeholder']}\"" : "");
				$return .= " value=\"{$value}\"";
				$return .= (array_key_exists('style',$field) ? " style=\"{$field['style']}\"" : "");
				if(array_key_exists("attributes",$field)){
					foreach($field["attributes"] as $name => $val){
						$return .= " {$name}=\"{$val}\"";
					}
				}
				$return .= ((array_key_exists('validation',$field) && array_key_exists('required',$field['validation'])) ? " required=\"required\"":"");
				$return .= " />";
				break;
			case static::Select:
				$return .= "<select id=\"{$this->_id}-{$field['name']}\" name=\"{$field['name']}\" class=\"form-control\"";
				$return .= ((array_key_exists('validation',$field) && array_key_exists('required',$field['validation'])) ? " required=\"required\"":"");
				$return .= " />";
				if($field['options'] is \Enum){
					$field['options'] = array_flip($field['options']::getValues());
				}
				foreach($field['options'] as $v => $key){
					$return .= "<option value=\"{$v}\" ";
					if($value == $v){
						$return .= " selected";
					}
					$return .= ">{$key}</option>";
				}
				
				$return .= "</select>";
				break;
			case static::TextArea:
				$return .= "<textarea id=\"{$this->_id}-{$field['name']}\" name=\"{$field['name']}\" ";
				$return .= " class=\"form-control";
				$return .= (array_key_exists('class',$field) ? " {$field['class']}" : "");
				$return .= "\" ";
				$return .= (array_key_exists('style',$field) ? "style=\"{$field['style']}\" " : "");
				$return .= ((array_key_exists('validation',$field) && array_key_exists('required',$field['validation'])) ? " required=\"required\"":"");
				$return .= ">{$value}</textarea>";
				break;
			case static::Horizontal:
				foreach($field['fields'] as $tempField){
					if($tempField['type'] !== static::Hidden) $return .= "</td><td>";
					$return .= $this->describeField($tempField);
				}
				break;
			case static::Multi:
				$return .= "<table>";
				foreach($field['fields'] as $group){
					$return .= "<tr>";
					foreach($group as $tempField){
						if($tempField['type'] !== static::Hidden) $return .= "<td>";
						$return .= $this->describeField($tempField);
						if($tempField['type'] !== static::Hidden) $return .= "</td>";
					}
					$return .= "</tr>";
				}
				$return .= "</table>";
				break;
			case static::Submit:
				$return .= "<input type=\"submit\" id=\"{$this->_id}-{$field['name']}\" name=\"{$field['name']}\" ";
				$return .= " class=\"btn btn-primary";
				$return .= (array_key_exists('class',$field) ? " {$field['class']}" : "");
				$return .= "\" ";
				$return .= " value=\"{$value}\"";
				$return .= (array_key_exists('style',$field) ? " style=\"{$field['style']}\"" : "");
				if(array_key_exists("attributes",$field)){
					foreach($field["attributes"] as $name => $val){
						$return .= " {$name}=\"{$val}\"";
					}
				}
				$return .= " />";
				break;
			case static::Button:
				$return .= "<button id=\"{$this->_id}-{$field['name']}\"";
				$return .= " class=\"btn btn-primary";
				$return .= (array_key_exists('class',$field) ? " {$field['class']}" : "");
				$return .= "\" ";
				$return .= (array_key_exists('style',$field) ? "style=\"{$field['style']}\" " : "");
				if(array_key_exists("attributes",$field)){
					foreach($field["attributes"] as $name => $val){
						$return .= "{$name}=\"{$val}\"";
					}
				}
				$return .= " >{$value}</button>";
				break;
			case static::Checkmark:
				$return .= "<input type=\"hidden\" name=\"{$field['name']}\" value=\"0\">";
				$return .= "<input type=\"checkbox\" name=\"{$field['name']}\" value=\"1\" ";
				if($value) $return .= "checked";
				$return .= " id=\"{$field['name']}\"><label class=\"checklabel\" for=\"{$field['name']}\">Toggle</label>";
				break;
		}
		return $return;
	}

	public function check(): bool {
		$this->_nameCount = [];
		$this->_validation = new Validation(new ErrorHandler());
		$token = new Token();
		if(Input::exists()){
			if($this->checkSubmits()){
				$submit = $this->checkSubmits();
				$requestTime = $this->_hash->decrypt(Input::get("requestTime"),$this->_requestTimeKey);
				$timeFromSubmit = round(microtime(true) * 1000) - (int)$requestTime;

				if(($this->_botCheck['requestTime'] && is_numeric($requestTime) && $timeFromSubmit > $this->_requestTimeMinimum && $timeFromSubmit < $this->_requestTimeMaximum) || !$this->_botCheck['requestTime']){
					if(($this->_botCheck['honeypot'] && $this->checkHoneypot()) || !$this->_botCheck['honeypot']){
						if(($this->_botCheck['CSRFToken'] && $token->check(Config::get("csrf/name"),Config::get("csrf/value"))) ||  !$this->_botCheck['CSRFToken']) {
							if((array_key_exists('ignoreValidation',$submit) && $submit['ignoreValidation']) || $this->validate($this->_fields)) return true;
							$this->_errorHandler->addErrors($this->_validation->errors()->all(),"validation");
						} else {
							$this->_errorHandler->addError("{$this->_id}: Invalid Token",'validation');
						}
					} else {
						$this->_errorHandler->addError("{$this->_id}: Fission mailed",'validation');
					}
				}
			}
		}
		return false;
	}
	private function checkSubmits(){
		foreach($this->_submit as $submit){
			if(Input::exists(preg_replace($this->_arrayRegex, '', $submit['name']))) return $submit;
		}
		return false;
	}
	private function checkHoneypot(): bool {
		foreach($this->_honeypotOptions as $option){
			if(Input::exists($option)){
				if(Input::get($option)) return false;
				return true;
			}
		}
		return false;
	}
	private function validate(array $fields): bool {
		$vfields = [];
		$matches;

		foreach($fields as $field){
			if($field['type'] === static::Multi){
				foreach($field['fields'] as $group){
					$this->validate($group);
				}
			} else if($field['type'] === static::Horizontal){
				$this->validate($field['fields']);
			} 

			if(array_key_exists("name",$field)){
				if(!array_key_exists($field["name"], $this->_nameCount)) $this->_nameCount[$field["name"]] = 0;
					else $this->_nameCount[$field["name"]]++;
				if(array_key_exists('validation',$field)){
					if(preg_match($this->_arrayRegex,$field['name'],$matches)) {
						$tempname = preg_replace($this->_arrayRegex,"",$field['name']);
						$tempinput = Input::get($tempname);
						$tempkey = $matches[1] ?: $this->_nameCount[$field['name']];
						$this->_validation->check([$tempname => $tempinput[$tempkey]],[$tempname => $field['validation']]);
					} else {
						$vfields[$field['name']] = $field['validation'];
					}
				}
			}
		}
		$this->_validation = $this->_validation->check($_POST,$vfields);
		return !$this->_validation->fails();
	}
	public function errors(): array {
		return $this->_errorHandler->all('validation');
	}
	public function hasErrors(): bool {
		return $this->_errorHandler->hasErrors('validation');
	}

}

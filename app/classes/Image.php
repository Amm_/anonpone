<?php

class Image {
	public static function filePath(string $name): string {
		return self::folderPath($name) . $name;
	}
	public static function folderPath(string $name): string {
		return ( substr($name,0,2) == "ad"? "zzz" : substr($name,0,2) ) . "/";
	}
	public static function save(string $url): ?string {
		$hash = new Hash();
		if(self::validName($url) && file_exists(Config::get("folder/files") . self::filePath($url))){ return $url;}
		if(preg_match("/^https:\/\/img\.anonpone\.com\/image\/[0-9a-z]{2}\/([0-9a-f]{32}_?(small|medium)?\.(png|jpg|jpeg|gif|webm))$/", trim($url), $output_array)){
			return $output_array[1];
		}
		try{
			if(trim($url) && Outbound::isImage($url)){
				$image = file_get_contents(trim($url));
				
				if($image && self::isImage($image)){
					$fileName = $hash->fileData($image) . "." . pathinfo($url, PATHINFO_EXTENSION);
					$folderPath = Config::get("folder/files") . self::folderPath($fileName);
					
					if(!file_exists($folderPath)){
						mkdir($folderPath);
					}
					if(!file_exists($folderPath . $fileName)){
						file_put_contents($folderPath . $fileName, $image);
						self::compress($folderPath . $fileName);
					}
					return $fileName;
				}
			}
		} catch (Exception $e){}
		return null;
	}

	public static function validName(string $string): bool {
		return preg_match("/^[0-9a-f]{32}_?(small|medium)?\.(png|jpg|jpeg|gif|webm)$/",trim($string));
	}

	public static function isImage($data): bool {
		$fileInfo = new finfo(FILEINFO_MIME_TYPE);
		$mimeType = $fileInfo->buffer($data);
		return ($mimeType === "image/png" || $mimeType === "image/jpeg" || $mimeType === "image/gif" || $mimeType === "video/webm");
	}

	public static function compress(string $filePath): void {
		if(file_exists($filePath)){
			switch(strtolower(pathinfo($filePath, PATHINFO_EXTENSION))){
				case "jpg":
				case "jpeg":
					$binaryPath = '/usr/bin/cjpeg';
					exec($binaryPath . " " . $filePath . " " . $filePath, $aOutput, $iResult);
					break;
				case "png":
					$binaryPath = '/usr/bin/optipng';
					exec($binaryPath . ' -quiet ' . $filePath, $aOutput, $iResult);
					break;
				default:
			}
		}
	}

	public static function delete($filename){
		$db = DB::getInstance();

		if($db->get('cyoa',array("thumbnail","=",$filename),array(),1)->count()) return;
		if($db->get('fanart',array("filename","=",$filename),array(),1)->count()) return;
		if($db->get('posts',array("filename","=",$filename),array(),1)->count()) return;
		if($db->get('posts',array("image_lewd","=",$filename),array(),1)->count()) return;

		$fullPath = Config::get("folder/files") . self::filePath($filename);
		if(file_exists($fullPath)){
			unlink($fullPath);
		}
		if(file_exists(Thumbnail::thumbPath($fullPath,"small"))){
			unlink(Thumbnail::thumbPath($fullPath,"small"));
		}
		if(file_exists(Thumbnail::thumbPath($fullPath,"medium"))){
			unlink(Thumbnail::thumbPath($fullPath,"medium"));
		}
	}
}

<?php 

class Settings {
	private $_settings;
	private $_path = "app/settings.json";

	public function __construct(){
		$filepath = Config::get('folder/base') . $this->_path;
		if(!file_exists($filepath)){
			$this->_settings = [];
			$this->save();
		} else {
			$string = file_get_contents($filepath);
			$this->_settings = json_decode($string, true);
		}
	}

	private function save(): void {
		$filepath = Config::get('folder/base') . $this->_path;
		if(!file_put_contents($filepath, json_encode($this->_settings))){
			throw new Exception("Cannot write to settings file");
		}
	}

	private function loaded(): bool {
		if(is_null($this->$_settings)) return false;
		return true;
	}

	public function get(string $name){
		if(array_key_exists($name,$this->_settings)) return $this->_settings[$name];
		else return null;
	}

	public function set(string $name,$value): void {
		$this->_settings[$name] = $value;
		$this->save();
	}
}
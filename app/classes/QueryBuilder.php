<?php

class QueryBuilder {
	private $_statement = [];
	private $_values = [];
	private $_debug = false;

	public function select(string $table,array $select = ["*"],bool $cache = true): self {
		$s = "SELECT ";
		if(!$cache) $s .= "SQL_NO_CACHE ";
		$this->_statement["select"] = $s . implode(",",$select) . " FROM {$table}";
		return $this;
	}

	public function delete(string $table): self {
		$this->_statement["select"] = "DELETE FROM {$table}";
		return $this;
	}
	public function update(string $table): self {
		$this->_statement["select"] = "UPDATE {$table}";
		return $this;
	}
	public function set(array $fields = []): self {
		$sql = "";
		if(count($fields)){
			$sql .= "SET ";
			$pieces = array();

			foreach($fields as $name => $value){
				$pieces[] = "{$name} = ?";
				$this->_values["set"][] = $value;
			}
			$sql .= implode(", ",$pieces);
		}
		$this->_statement["set"] = $sql;
		if($this->_debug) echo $sql;
		return $this;
	}

	public function insert(string $table): self {
		$this->_statement["select"] = "INSERT INTO {$table}";
		return $this;
	}
	public function replace(string $table): self {
		$this->_statement["select"] = "REPLACE INTO {$table} ";
		return $this;
	}
	public function values(array $fields = []): self {
		$keys = array_keys($fields);
		$this->_values["values"] = array_values($fields);
		$values = implode(",", array_fill(0, count($fields), "?"));
		$this->_statement["values"] = "(" . implode(", ", $keys) . ") VALUES ({$values})";

		return $this;
	}

	public function having($having): self {
		return $this->filter("HAVING",$having);
	}
	public function where($where): self {
		return $this->filter("WHERE",$where);
	}
	public function filter($type, $where): self {
		$sql = "";
		$value = [];
		if(is_array($where) && count($where)){
			$operators = ['=', '!=', '>', '<', '>=', '<=', 'LIKE', 'NOT LIKE', 'IN'];
			$sql .= " {$type} ";

			$pieces=[];
			for($x = 0; $x < count($where); $x+=3){
				$field 		= $where[$x + 0];
				$operator 	= $where[$x + 1];

				if($operator === "IN"){
					$pieces[] = "{$field} IN " . $this->wherein($value,$where[$x + 2]); 
				} else {
					if(in_array($operator, $operators)){
						$value[] = $where[$x + 2];
						$pieces[] = "{$field} {$operator} ?";
					}
				}
			}
			$sql .= implode(" AND ", $pieces);
			$this->_values[strtolower($type)] = $value;
		} else if(is_numeric($where)){
			$sql .= "$type id = ? ";
			$this->_values[strtolower($type)] = [$where];
		} else {
			throw new Exception('Invalid where clause');
		}

		$this->_statement[strtolower($type)] = $sql;
		if($this->_debug) echo $sql;
		return $this;
	}
	private function wherein(&$value,$where = null): string {
		$sql = "";
		if($where){
			if(!is_array($where)) $where = explode(",",$where);

			$sql = "(" . implode(",", array_fill(0, count($where), "?")) . ")";
			$value = array_merge($value,$where);
		}
		return $sql;
	}
	public function group(string $group): self {
		$this->_statement["group"] = "GROUP BY {$group}";
		return $this;
	}
	public function order(iterable $order): self {
		$sql = "";
		if(count($order)){
			$sql .= " ORDER BY ";
			$pieces = [];

			for($x = 0; $x < count($order); $x+=2){
				if(!strcasecmp($order[$x + 1],"ASC") && !strcasecmp($order[$x + 1],"DESC")) throw \Exception("Invalid order");
				$pieces[] = "{$order[$x]} {$order[$x + 1]}";
			}
			$sql .= implode(", ", $pieces);
		}
		if($this->_debug) echo $sql;
		$this->_statement["order"] = $sql;
		return $this;
	}
	public function limit($limit): self {
		$sql = "";
		if($limit){
			$sql .= " LIMIT ";
			$sql .= (is_array($limit)) ? implode(", ", $limit) : $limit;
		}
		if($this->_debug) echo $sql;
		$this->_statement["limit"] = $sql;
		return $this;
	}

	public function buildStatement(): string {
		$sql = "";
		if(isset($this->_statement["select"])) $sql .= $this->_statement["select"] . " ";
		if(isset($this->_statement["set"])) $sql .= $this->_statement["set"];
		if(isset($this->_statement["update"])) $sql .= $this->_statement["update"];
		if(isset($this->_statement["values"])) $sql .= $this->_statement["values"];
		if(isset($this->_statement["where"])) $sql .= $this->_statement["where"];
		if(isset($this->_statement["group"])) $sql .= $this->_statement["group"];
		if(isset($this->_statement["having"])) $sql .= $this->_statement["having"];
		if(isset($this->_statement["order"])) $sql .= $this->_statement["order"];
		if(isset($this->_statement["limit"])) $sql .= $this->_statement["limit"];
		return $sql;
	}

	public function buildValues(): array {
		$values = [];
		if(isset($this->_values["set"])) $values = array_merge($values,$this->_values["set"]);
		if(isset($this->_values["values"])) $values = array_merge($values,$this->_values["values"]);
		if(isset($this->_values["where"])) $values = array_merge($values,$this->_values["where"]);
		if(isset($this->_values["having"])) $values = array_merge($values,$this->_values["having"]);
		return $values;
	}

	public function activateDebug(): self {
		$this->_debug = true;
		return $this;
	}
}

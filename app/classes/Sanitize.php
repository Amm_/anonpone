<?php 

class Sanitize 
{

	public static function pprint($string): void {
		echo "<pre>";
		var_dump($string);
		echo "</pre>";
	}

	public static function escape(string $string): string {
		return htmlentities($string, ENT_QUOTES);
	}

	public static function closetags(string $html): string {
	    $doc = new DOMDocument();
	    @$doc->loadHTML($html);
	    $html = $doc->saveHTML();
	    return $html;
	} 

	public static function cleanFilename(string $file): string {
		$file = preg_replace("([^\w\s\d\-_~,;:\[\]\(\).])", '', $file);
		$file = preg_replace("([\.]{2,})", '', $file);
		return $file;
	}

    public static function displaySize(int $bytes): string {
        $symbols = array('B', 'KiB', 'MiB', 'GiB', 'TiB', 'PiB', 'EiB', 'ZiB', 'YiB');
        $exp = floor(log($bytes)/log(1024));

        return sprintf('%.2f '.$symbols[$exp], ($bytes/pow(1024, floor($exp))));
    }

    public static function isEnglish(string $str): bool {
        if (strlen($str) != strlen(utf8_decode($str))) {
            return false;
        } else {
            return true;
        }
    }

    private static $_regEx = array(
        "tumblr" => "/^https?:\/\/\d\d\.media\.tumblr\.com\/.*_\d\d\d\.(png|jpg|jpeg)$/i",
        "derpi" => "/^https?:\/\/derpicdn\.net\/img.*(large|small|medium|tall|thumb|thumb_small)\.(png|jpg|jpeg|gif)$/i",
        "anonpone" => "/^https:\/\/img\.anonpone\.com\/image\/[0-9a-z]{2}\/[0-9a-z]{32}_?(small|medium)?\.(png|jpg|jpeg|gif)/"
    );

    public static function modifyURL(string $url): string {
        $url = trim($url);
        if(preg_match(static::$_regEx["tumblr"],$url)) {
            $url = preg_replace('/\d\d\d\.(png|jpg|jpeg)/',"1280.$1",$url);
        } else if(preg_match(static::$_regEx["derpi"],$url)){
            $url = preg_replace("/img\//",'img/view/',$url);
            $url = preg_replace("/(\/large|\/small|\/medium\/tall|\/thumb_small|\/thumb)/","",$url);
            $url = preg_replace("/__.*\./",".",$url);
        } else if(preg_match(static::$_regEx["anonpone"],$url)){
            $url = preg_replace('/_(small|medium)/','',$url);
            $url = preg_match("/[0-9a-z]{32}\.(png|jpg|jpeg|gif)/",$url,$result);
            $url = $result[0];
        }
        return $url;
    }
}

<?php

class Outbound {
	private static $_types = array(
		"image" => array("png","jpg","jpeg","gif"),
		"json" => array("json"),
		"video" => array("mp4","webm"),
	);

	public static function parseHeaders($url): array{
		$result = [];
		$headers = (is_array($url)) ? $url : get_headers($url, 1);
		if(!$headers) throw new Exception("Failed to resolve URL: " . $url);
		foreach ($headers as $key=>$value) {
		    if (is_array($value)) {
		        $value = end($value);
		    }
		    $result[$key] = $value;
		}
		$result["Response-Code"] = substr($result[0], 9, 3);
		return $result;
	}

	public static function getResponseCode($headers): int {
		$headers = static::parseHeaders($headers);
		return $headers["Response-Code"];
	}
	public static function validResponse($headers): bool {
		if(static::getResponseCode($headers) < 400) return true;	
		return false;
	}

	public static function isFiletype(string $url,array $types): bool {
		$headers = static::parseHeaders($url);
		@$type = end(explode('/',$headers['Content-Type']));
		if(static::validResponse($headers) && in_array($type,$types)){
			return true;
		}
		return false;
	}
	public static function isImage(string $url): bool {	return static::isFiletype($url,static::$_types['image']);}
	public static function isJSON(string $url): bool {	return static::isFiletype($url,static::$_types['json']);}
	public static function isVideo(string $url): bool {	return static::isFiletype($url,static::$_types['video']);}
}
<?php

class DB {
	public $_query;

	private $_pdo,
			$_error = false,
			$_results,
			$_statements = [],
			$_totalCount = 0;
	private $_cache = false;

	public function __construct($settings,String $host = null, String $database = null, String $username = null, String $password = null, String $charset = "utf8mb4"){
		try {
			$this->_pdo = new PDO('mysql:host=' . ($host ?? Config::get('mysql/host')) . ';dbname=' . ($database ?? Config::get('mysql/db')) . ";charset={$charset}",($username ?? Config::get('mysql/username')),($password ?? Config::get('mysql/password')));
			if($settings->get('dbCache')) $this->_cache = true;
			else $this->_cache = false;
		} catch (PDOException $e){
			die($e->getMessage());
		}
		$this->_pdo->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_WARNING );
	}

	public function rawQuery(string $sql): self {
		if(!$this->_cache && substr($sql, 0, strlen("SELECT")) === "SELECT"){ $sql = preg_replace('/$SELECT/', "SELECT SQL_NO_CACHE", $sql);}
		$this->_query = $this->_pdo->prepare($sql);
		$this->_totalCount++;
		if($this->_query->execute()){
			$this->_results = $this->_query->fetchAll(PDO::FETCH_OBJ);
			$this->_count = $this->_query->rowCount();
		}else{
			$this->_error = "Failed to Execute - " . $sql;
			return false;
		}
		return $this;
	}

	public function execute(\QueryBuilder $query): self {
		$this->_error = null;
		$sql = $query->buildStatement();
		$params = $query->buildValues();
		$this->_totalCount++;
		if(isset($this->_statements[$sql])) $this->_query = $this->_statements[$sql];
		else {
			$this->_query = $this->_pdo->prepare($sql);
			$this->_statements[$sql] = $this->_query;
		}
		if($this->_query){
			$x = 1;
			if(count($params)){
				foreach($params as $param){
					if(is_bool($param)){
						if($param) $param = 1;
						else $param = 0;
					}
					$this->_query->bindValue($x, $param);
					$x++;
				}
			}

			if($this->_query->execute()){
				if(substr($sql, 0, strlen("SELECT")) === "SELECT"){
					$this->_results = $this->_query->fetchAll(PDO::FETCH_OBJ);
				}
			}else{
				$this->_error = "Failed to Execute - " . $sql;
				return $this;
			}
		}
		return $this;
	}

	public function results() {
		return $this->_results;
	}

	public function first() {
		$t = $this->results();
		if(isset($t[0])){
			return $t[0];
		}
		return null;
	}

	public function error():string {
		return $this->_error;
	}
	public function count(): int {
		return count($this->_results);
	}
	public function exists(): bool {
		return (count($this->_results) ? true : false);
	}

	public function debug(): void {
		print_r($this->_statements);
	}

	public function queryCount(): int {
		return $this->_totalCount;
	}
}

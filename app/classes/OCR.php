<?php 

class OCR {
	static function getText(string $path): string {
		$url = Config::get("ocr/url") . "?key=" . Config::get("ocr/key");
		$request = array(
			"requests" => array(
			    "image" => array(
			        "source" => array(
			          "imageUri" => $path
			        )
			    ),
			    "features" => array(
		          	"type" => "DOCUMENT_TEXT_DETECTION",
		          	"maxResults" => 1
		        )
			)
		);

		$opts = array(
            'http' => array(
               'method' => "POST",
               'header' => "Content-Type: application/json\r\n",
               'content' => json_encode($request),
            )
        );
        $context = stream_context_create($opts);
        $response = json_decode(file_get_contents($url, false, $context));

        array_shift($response->responses[0]->textAnnotations);
        $lines = array();
        $line = "";
        $lineHeight = 0;
        $y = $response->responses[0]->textAnnotations[0]->boundingPoly->vertices[3]->y;
        $x = $response->responses[0]->textAnnotations[0]->boundingPoly->vertices[2]->x;
        $punctuation = array(".","!","\"","'","?",",",";","-","..","...");
        foreach($response->responses[0]->textAnnotations as $word){
        	$lineHeight = $word->boundingPoly->vertices[3]->y - $word->boundingPoly->vertices[0]->y;

        	if($word->boundingPoly->vertices[3]->y - $y > ceil($lineHeight * 0.1)){
        		$line = preg_replace('/"+(\s)/', '"', $line,1);
        		array_push($lines,$line);
        		$line = "";
        	} 
        	if($word->boundingPoly->vertices[3]->y - $y > ceil($lineHeight * 1.5)){
        		array_push($lines,"");
        	}

        	if($x < $word->boundingPoly->vertices[0]->x){ 
        		if(!in_array($word->description,$punctuation)){
        			if(!endsWith($line,"-")){
        				$line .= " ";
        			}
        		}
        	}
        	$line .= $word->description;

        	$x = $word->boundingPoly->vertices[2]->x;
        	$y = $word->boundingPoly->vertices[3]->y;
        }
        array_push($lines,$line);
        return implode("<br>",$lines);
	}
}
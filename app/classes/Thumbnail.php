<?php

class Thumbnail{
	public static function thumbPath(string $sImagePath,string $size = "small"): string {
		$ePos = strrpos($sImagePath, '.');
		return substr($sImagePath, 0, $ePos) . '_' . $size . substr($sImagePath, $ePos);
	}

	public static function thumb($sImagePath,$thumb,$width,$height): string {
		if(!file_exists($thumb) && file_exists($sImagePath)){
			$img = self::create($sImagePath,$width,$height);
			if($img){
				self::generate($img,pathinfo($sImagePath, PATHINFO_EXTENSION),$thumb);
				Image::compress($thumb);
				if(filesize($thumb) > filesize($sImagePath)){
					unlink($thumb);
					copy($sImagePath,$thumb);
				}
			}
		}
		return $thumb;
	}
	public static function small(string $sImagePath): string {
		return self::thumb($sImagePath,self::thumbPath(trim($sImagePath),"small"),150,150);
	}

	public static function medium(string $sImagePath): string {
		return self::thumb($sImagePath,self::thumbPath(trim($sImagePath),"medium"),300,300);
	}

	public static function create(string $sImagePath,int $iMaxWidth = null,int $iMaxHeight = null,int $iThumbnailWidth = null,int $iThumbnailHeight = null){
		if ($iMaxWidth && $iMaxHeight) $sType = 'scale';
		else if ($iThumbnailWidth && $iThumbnailHeight) $sType = 'exact';
		
		$img = self::makeImage($sImagePath);

		if ($img) {
		    $iOrigWidth = imagesx($img);
		    $iOrigHeight = imagesy($img);
		    if ($sType == 'scale') {
		        // Get scale ratio
		        $fScale = min($iMaxWidth/$iOrigWidth,
		              $iMaxHeight/$iOrigHeight);
		        if ($fScale < 1) {
		            $iNewWidth = floor($fScale*$iOrigWidth);
		            $iNewHeight = floor($fScale*$iOrigHeight);
		 
		            $tmpimg = imagecreatetruecolor($iNewWidth,
		                               $iNewHeight);
		            imagealphablending( $tmpimg, false );
					imagesavealpha( $tmpimg, true );
		 
		            imagecopyresampled($tmpimg, $img, 0, 0, 0, 0,
		            $iNewWidth, $iNewHeight, $iOrigWidth, $iOrigHeight);
		 
		            imagedestroy($img);
		            $img = $tmpimg;
		        }     
		    } else if ($sType == "exact") {
		        $fScale = max($iThumbnailWidth/$iOrigWidth,
		              $iThumbnailHeight/$iOrigHeight);
		        if ($fScale < 1) {
		            $iNewWidth = floor($fScale*$iOrigWidth);
		            $iNewHeight = floor($fScale*$iOrigHeight);
		 
		            $tmpimg = imagecreatetruecolor($iNewWidth,
		                            $iNewHeight);
		            $tmp2img = imagecreatetruecolor($iThumbnailWidth,
		                            $iThumbnailHeight);
		 
		            imagecopyresampled($tmpimg, $img, 0, 0, 0, 0,
		            $iNewWidth, $iNewHeight, $iOrigWidth, $iOrigHeight);
		 
		            if ($iNewWidth == $iThumbnailWidth) {
		 
		                $yAxis = ($iNewHeight/2)-
		                    ($iThumbnailHeight/2);
		                $xAxis = 0;
		            } else if ($iNewHeight == $iThumbnailHeight)  {
		                $yAxis = 0;
		                $xAxis = ($iNewWidth/2)-
		                    ($iThumbnailWidth/2);
		            } 
		            imagecopyresampled($tmp2img, $tmpimg, 0, 0,
		                       $xAxis, $yAxis,
		                       $iThumbnailWidth,
		                       $iThumbnailHeight,
		                       $iThumbnailWidth,
		                       $iThumbnailHeight);
		 
		            imagedestroy($img);
		            imagedestroy($tmpimg);
		            $img = $tmp2img;
		        }    
		 
		    }
		}
		return $img;
	}

	public static function makeImage(string $sImagePath){
		$img = NULL;
		$sExtension = pathinfo($sImagePath, PATHINFO_EXTENSION);

		if ($sExtension == 'jpg' || $sExtension == 'jpeg') {
		    $img = @imagecreatefromjpeg($sImagePath);
		} else if ($sExtension == 'png') {
		    $img = @imagecreatefrompng($sImagePath);
		    if($img)
		    {
			    imagealphablending($img, false);
				imagesavealpha($img, true);
		    }
		} else if ($sExtension == 'gif') {
		    $img = @imagecreatefromgif($sImagePath);
		}
		return $img;
	}

	public static function generate(string $img,string $sExtension,string $thumb = null): void {
		$sExtension = strtolower($sExtension);
		if ($sExtension == 'jpg' || $sExtension == 'jpeg' || $sExtension == 'gif') {
		    if($thumb){
		    	imagejpeg($img,$thumb);
		    } else {
		    	header("Content-type: image/jpeg");
		    	imagejpeg($img);
		    }
		} else if ($sExtension == 'png') {
		    if($thumb){
		    	imagealphablending($img, false);
				imagesavealpha($img, true);
		    	imagepng($img,$thumb);
		    } else {
		    	header("Content-type: image/png");
		    	imagepng($img);
		    }
		}
	}
}

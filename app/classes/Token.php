<?php

class Token {
	private $_session;
	
	public function __construct(){
		$this->_session = new Session();
	}
	
	public function generate(string $name,int $length = 10): string {
		$hash = new Hash();
		$token = $hash->UUID($length);
		$this->_session->put($name,$token);
		return $token;
	}

	public function get(string $name,int $length = 10): string {
		if($this->_session->exists($name)) { 
            return $this->_session->get($name);
        } else {
            return $this->generate($name,$length);
        }
	}

	public function check(string $name,string $value): bool {
		$token = $this->_session->get($name);
		if(Input::exists($token) && Input::get($token) === $this->_session->get($value)){
			$this->_session->delete($name);
			$this->_session->delete($value);
			return true;
		}
		
		return false;
	}
}

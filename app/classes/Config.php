<?php 

class Config {
	public static function get(string $path) {
		if($path){
			$config = $GLOBALS['config'];
			$path = explode ('/', trim($path,"/"));


			foreach($path as $bit){
				if(isset($config[$bit])){
					$config = $config[$bit];
				}
			}

			return $config;
		}
		return null;
	}
}

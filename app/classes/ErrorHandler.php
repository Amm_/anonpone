<?php

class ErrorHandler
{
	protected $errors = array();

	public function addError(string $error, string $key = null): void {
		if($key){
			$this->errors[$key][] = $error;
		} else {
			$this->errors[] = $error;
		}
	}

	public function addErrors(iterable $errors, string $key = null):void {
		if($key){
			if(!array_key_exists($key,$this->errors)) $this->errors[$key] = [];
			$this->errors[$key] = array_merge($this->errors[$key],$errors);
		} else {
			$this->errors = array_merge($this->errors,$errors);
		}
	}

	public function all(string $key = null): iterable {
		if(isset($this->errors[$key])){
			return $this->errors[$key];
		} else if($key) {
			return [];
		} else {
			if(count($this->errors)){
				return call_user_func_array('array_merge', array_values($this->errors));
			} else {
				return [];
			}
		}
	}

	public function hasErrors(string $key = null): bool	{
		return count(is_countable($this->all($key)) ? $this->all($key) : []) ? true : false;
	}

	public function first(string $key)	{
		$errors = $this->all();
		$error = $errors[$key][0];
		return isset($error) ? $errors[$key][0] : '' ;
	}
}

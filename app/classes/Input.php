<?php 

class Input 
{
	public static function exists(?string $type = 'post'): bool {
		switch($type){
			case 'post':
				return (!empty($_POST)) ? true : false;
			break;
			case 'get':
				return (!empty($_GET)) ? true : false;
			break;
			default:
				if(isset($_POST[$type])) return true;
				else if (isset($_GET[$type])) return true;
				return false;
		};
	}

	public static function get(string $item) {
		if(isset($_POST[$item])){
			return $_POST[$item];
		} else if (isset($_GET[$item]))	{
			return $_GET[$item];
		}
		return null;
	}
}

<?php
class MySessionHandler implements SessionHandlerInterface {
    private $_db;

    public function __construct($db){
        $this->_db = $db;

        session_set_save_handler($this, true);
        session_start();
    }
    public function open($savepath,$id){
        return true;
    }
    public function read($id){
        $query = (new \QueryBuilder())->select('sessions',['data'])->where(["id","=",$id])->limit(1);
        if ($this->_db->execute($query)->count()) {
            return $this->_db->first()->data;
        } else {
            $query = (new \QueryBuilder())->insert("sessions")->values(["id" => $id, "access" => time(), "data" => ""]);
            $this->_db->execute($query)->error();
            return "";
        }
    }

    public function write($id,$data){
        $access = time();
        $query = (new \QueryBuilder())->update("sessions")->set(["access" => $access, "data" => $data])->where(["id","=",$id]);
        return !$this->_db->execute($query)->error();
    }

    public function destroy($id){
        $query = (new \QueryBuilder())->delete("sessions")->where(["id","=",$id]);
        return !$this->_db->execute($query)->error();
    }
    public function close(){
        return true;
    }

    public function gc($max){
        // Calculate what is to be deemed old
        $old = time() - $max;
        $query = (new \QueryBuilder())->delete("sessions")->where(["access", "<", $old]);
        return !$this->_db->execute($query)->error();
    }

    public function __destruct(){
        $this->close();
    }

}
<?php

class Session {
	public function exists(string $name): bool {
		return (isset($_SESSION[$name])) ? true : false;
	}

	public function put(string $name, $value){
		return $_SESSION[$name] = $value;
	}

	public function get(string $name): ?string {
		if($this->exists($name)){
			return $_SESSION[$name];
		}
		return null;
	}

	public function delete(string $name): void {
		if($this->exists($name)){
			unset($_SESSION[$name]);
		}
	}

	public function flash(string $name, string $string = ''): string {
		if($this->exists($name))	{
			$session = $this->get($name);
			$this->delete($name);
			return $session;
		} else {
			$this->put($name,$string);
		}
		return '';
	}
}

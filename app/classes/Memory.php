<?php

class Memory 
{
	private static $_instance = null;
	private $_memcached = null;
	private $_status = false;
	private $_defaultExp;

	private function __construct()
	{
		if(Config::get('memcached/enabled') && Settings::get("memcached") && class_exists('Memcached')) {
		    $memcached = new Memcached;

		    //$memcached->setOption(Memcached::OPT_BINARY_PROTOCOL, true);
		    $isMemcacheAvailable = $memcached->addServer(Config::get('memcached/host'),Config::get('memcached/port'));
		    if ($isMemcacheAvailable) {
		    	$this->_status = true;
		    	$this->_memcached = $memcached;
		   	}
		}
		$this->_defaultExp = 60*60*24*7;
	}

	public static function getInstance()
	{
		if(!isset(static::$_instance)){
			static::$_instance = new Memory();
		}
		return static::$_instance;
	}

	public function active()
	{
		return $this->_status;
	}

	public function getMulti($keys){
		if(!$this->active()) return;
		array_walk($keys, function(&$key){$key = str_replace(' ','_',Config::get('memcached/name') . '_' . $key);});
		$results = $this->_memcached->getMulti($keys);
		$output = array();
		foreach($results as $key => $value){
			$output[str_replace(Config::get('memcached/name') . '_','',$key)] = $value;
		}
		return $output;
	}
	public function get($key){
		if(!$this->active()) return;
		if(is_array($key)) return $this->getMulti($key);
		return $this->_memcached->get(str_replace(' ','_',Config::get('memcached/name') . '_' . $key));
	}
	public function forget($key){
		if(!$this->active()) return;
		return $this->_memcached->delete(str_replace(' ','_',Config::get('memcached/name') . '_' . $key));
	}
	public function clearAll(){
		if(!$this->active()) return;
		$this->_memcached->flush();
	}

	public function set($key,$value,$expiration = 0){

		if(!$this->active()) return;
		$this->_memcached->set(str_replace(' ','_',Config::get('memcached/name') . '_' . $key),$value,$expiration);
	}
	public function error()
	{
		return $this->_memcached->getResultCode();
	}

	public function stat($value = ""){
		if(!$this->active()) return;
		$stats = $this->_memcached->getStats();
		$stats = array_pop($stats);
		if($value) return $stats[$value];

		return $stats;
	}

	public function forgetCYOA($default = true)
	{
		if($this->active())
 		{
 			if($default)
 			{
 				CYOA::set(true);
 			} 
 			else 
 			{
 				CYOA::setPage();
 			}
 		}
	}
	public function forgetThread($id,$thread)
	{
		if($this->active())
 		{
			$this->forget($id . "_threads");
			$this->forget($id . "_thread_" . $thread);
		}
	}
}

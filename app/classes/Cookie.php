<?php 

class Cookie{
	public function exists(string $name): bool {
		return (isset($_COOKIE[$name])) ? true : false;
	}

	public function get(string $name){
		return $_COOKIE[$name];
	}

	public function put(string $name, $value, int $expiry): bool {
		if(setCookie($name, $value, time() + $expiry, '/'))
		{
			return true;
		}
		return false;
	}

	public function delete(string $name): void {
		$this->put($name,'', time() - 1);
	}
}

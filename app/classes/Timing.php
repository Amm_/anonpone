<?php

Class Timing 
{
	private static $verbose = false;

	public static function mark(string $name = null): void {
		if(!$name){
			$GLOBALS['config']['time'][] = self::getTime();
			$GLOBALS['config']['ram'][] = memory_get_usage();
		}else{
			if(isset($GLOBALS['config']['time'][$name])){
				$value = 0;
				while(true){
					if(!isset($GLOBALS['config']['time'][$name.$value])){
						$GLOBALS['config']['time'][$name.$value] = self::getTime();
						$GLOBALS['config']['ram'][$name.$value] = memory_get_usage();
						break;
					}
					$value++;
				}
			} else {
				$GLOBALS['config']['time'][$name] = self::getTime();
				$GLOBALS['config']['ram'][$name] = memory_get_usage();
			}
		}
		if(self::$verbose) echo $name . "<br>";
	}

	public static function total(): string {
		return number_format((float)($GLOBALS['config']['time']['End']-$GLOBALS['config']['time']['start']), 3,'.','');
	}

	public static function display(): string {
		$start = $GLOBALS['config']['time']['start'];
		$current = 0;
		$string = "";
		$string .= "<table>";
		foreach($GLOBALS['config']['time'] as $key => $time){
			$time -= $start;
			$string .= "<tr><td>" . $key . "</td><td>" . number_format((float)($time-$current), 5,'.','') . " s</td><td style='    padding-left: 10px;'>" . (array_key_exists($key,$GLOBALS['config']['ram']) ? Sanitize::displaySize($GLOBALS['config']['ram'][$key]) : "") . "</td></tr>";
			$current = $time;
		}
		$string .= "<tr><td>End</td><td>" . number_format((float)($GLOBALS['config']['time']['End']-$start), 5,'.','') . " s<br></td></tr>";
		$string .= "</table>";
		return $string;
	}

	public static function getTime(): float {
		$time = microtime();
		$time = explode(' ', $time);
		$time = $time[1] + $time[0];
		return $time;
	}
}

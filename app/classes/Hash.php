<?php

Class Hash {

	public function file(string $filename): string {
		return hash_file("md5", $filename, FALSE);
	}

	public function fileData(string $data): string {
		return hash("md5", $data, FALSE);
	}

	public function make(string $string, string $salt = ''): string {
		return hash('sha256', $string . $salt);
	}

	public function salt(int $length): string {
		return openssl_random_pseudo_bytes($length);
	}

	public function unique(int $len = 16): string{
		return substr($this->make(uniqid()),-1 * $len);
	}

	public function UUID(int $len = 16): string {
		if (function_exists("random_bytes")) {
	        $bytes = bin2hex(random_bytes(ceil($len / 2)));
	    } elseif (function_exists("openssl_random_pseudo_bytes")) {
	        $bytes = openssl_random_pseudo_bytes(ceil($len / 2));
	    } else {
	        throw new Exception("no cryptographically secure random function available");
	    }
	    return substr(bin2hex($bytes), 0, $len);
	}

	public function incremental(int $len = 5): string {
		$charset = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
		$base = strlen($charset);
		$result = '';

		$now = explode(' ', microtime())[1];
		while ($now >= $base){
		    $i = $now % $base;
		    $result = $charset[$i] . $result;
		    $now /= $base;
		}
  		return substr($result, -5);
	}

	public function xor(string $string,string $key): string {
		for($i = 0; $i < strlen($string); $i++) 
        	$string[$i] = ($string[$i] ^ $key[$i % strlen($key)]);
    	return $string;
	}

	public function encrypt(string $string, string $key) {
	  	$result = '';
	  	for($i=0; $i<strlen($string); $i++) {
	    	$char = substr($string, $i, 1);
	    	$keychar = substr($key, ($i % strlen($key))-1, 1);
	    	$char = chr(ord($char)+ord($keychar));
	    	$result.=$char;
		}
	  	return base64_encode($result);
	}

	public function decrypt(string $string, string $key): string {
	  	$result = '';
	  	$string = base64_decode($string);

	  	for($i=0; $i<strlen($string); $i++) {
	    	$char = substr($string, $i, 1);
	    	$keychar = substr($key, ($i % strlen($key))-1, 1);
	    	$char = chr(ord($char)-ord($keychar));
	    	$result.=$char;
	  	}
	    return $result;
	} 
}

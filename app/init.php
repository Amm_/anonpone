<?php

ini_set('memory_limit', '256M');
$time = microtime();
$time = explode(' ', $time);
$time = $time[1] + $time[0];
$start = $time;

function autoloader($class){
	$class = str_replace('\\', '/', $class);
	
	if (is_file(__DIR__.'/classes/'.$class.'.php')) {
	    require __DIR__.'/classes/'.$class.'.php';
	} else if (is_file(__DIR__.'/models/'.$class.'.php')) {
	    require __DIR__.'/models/'.$class.'.php';
	} else if (is_file(__DIR__."/".$class.'.php')) {
	    require __DIR__."/".$class.'.php';
	}
}

spl_autoload_register('autoloader');

\Timing::mark("Init Autoloader Loaded");

$base = "/var/www/anonpone/";
if(@array_shift((explode(".",$_SERVER['HTTP_HOST']))) === "test"){
	$base = "/var/www/anonpone-dev/";
}

$GLOBALS['config'] = [
	'error' => [
		'report' => true,
		'timing' => false,
		'dev' => ($base === "/var/www/anonpone/") ? false : true,
	],
	'memcached' => [
		'host' => 'localhost',
		'port' => '11211',
		'name' => 'anonpone',
		'enabled' => false,
	],
	'mysql' => [
		'host' => '127.0.0.1',
		'username' => 'username',
		'password' => 'password',
		'db' => 'db'
	],
	'remember' => [
		'cookie_name' => 'hash',
		'cookie_expiry' => 30*60*60*24
	],
	'session' => [
		'session_name' => 'user',
		'token_name' => 'token'
	],
	'csrf' => [
		'name' => 'csrfName',
		'value' => 'csrfValue'
	],
	'website' => [
		'url' => 'https://www.anonpone.com/',
		'email' => '',
		'img' => 'https://img.anonpone.com/',
		'image' => 'https://img.anonpone.com/image/',
		'data' => 'https://data.anonpone.com/',
		'dat' => 'http://data.anonpone.com/',
	],
	'time' => ["start" => $start],
	'folder' => [
		'base' => $base,
		'includes' => $base . 'app/includes',
		'classes' => $base . 'app/classes',
		'controller' =>  $base . 'app/controllers/',
		'model' =>  $base . 'app/models/',
		'view' =>  $base . 'app/views/',
		'template' =>  $base . 'app/templates/',
		'files' => $base . 'anonpone/image/',
		'public' => $base . 'anonpone/',
	]
];

\Timing::mark('Init Globals Loaded');

require_once 'secrets.php';

if(PHP_SAPI !== "cli" && "https://$_SERVER[HTTP_HOST]/" == $GLOBALS['config']['website']['img']){
	header('Location: ' . $GLOBALS['config']['website']['url']);
}

ini_set('default_charset', 'UTF-8');
mb_internal_encoding('UTF-8');
header('Content-type: text/html; charset=utf-8');

\Timing::mark('Init Headers Set');

require_once 'core/App.php';
require_once 'core/Controller.php';
require_once 'core/View.php';
require_once 'models/Exceptions.php';

\Timing::mark('Init Required classes Set');

date_default_timezone_set("America/Chicago");

$settings = new Settings();
$db = new DB($settings);

new \MySessionHandler($db);
$session = new Session();

$user = new \User\User($db);
$cookie = new Cookie();

if($cookie->exists(Config::get('remember/cookie_name')) && !$session->exists(Config::get('session/session_name'))){
 	$hash = $cookie->get(Config::get('remember/cookie_name'));
 	$query = (new \QueryBuilder())->select('user_sessions')->where(['hash', '=', $hash]);
 	$hashCheck = $db->execute($query);

 	if($hashCheck->count()){
 		$user = new \User\User($db, $hashCheck->first()->user_id);
 		$login = new \User\Login($db,$user);
 		$login->login();
 	}
}

function startsWith($haystack, $needle) {
    // search backwards starting from haystack length characters from the end
    return $needle === "" || strrpos($haystack, $needle, -strlen($haystack)) !== FALSE;
}
function endsWith($haystack, $needle) {
    // search forward starting from end minus needle length characters
    return $needle === "" || (($temp = strlen($haystack) - strlen($needle)) >= 0 && strpos($haystack, $needle, $temp) !== false);
}

$app = new \Core\App($db,$user,$settings);

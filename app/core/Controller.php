<?php 
namespace Core;

class Controller {
	protected $_errorHandler;
	protected $_db;
	protected $_user;
	protected $_session;
	protected $_settings;

	public function __construct(\DB $db,\User\User $user,\Settings $settings){
		$this->_errorHandler = new \ErrorHandler();
		$this->_db = $db;
		$this->_user = $user;
		$this->_session = new \Session();
		$this->_settings = $settings;
	}

	protected function ifModified($timestamp){
		if (isset($_SERVER['HTTP_IF_MODIFIED_SINCE']) && strtotime($_SERVER['HTTP_IF_MODIFIED_SINCE']) >= strtotime($timestamp)){
		    header('HTTP/1.0 304 Not Modified');
		    exit;
		}
	}
}

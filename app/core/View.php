<?php
namespace Core;

abstract class View {
	protected $_document = "";
	private $_data = [];
	protected $_user;
	protected $_db;

	public function __construct(\DB $db,\User\User $user, string $string = "index"){
		$this->_db = $db;
		$this->_user = $user;
		$this->_document .= file_get_contents(\Config::get("folder/template") . 'common/mainTemplate.html');
		$this->_data['mainContent'] = file_get_contents(\Config::get("folder/template") . "{$string}.html");
		$this->_data['css'] = "";
		$this->_data['js'] = "";
		$this->_hints = "Link:";
	}
	public function __set(string $name,$value): void {	$this->_data[$name] = $value; }
	public function __get(string $name){ return (isset($this->_data[$name]) ? $this->_data[$name] : null); }
	public function __isset(string $name): bool { return isset($this->_data[$name]); }

	public function bustCache(): void {
		preg_match_all('/https:\/\/(data|test).anonpone.com\/(css\/\w+.css)/', $this->_document, $css);
		$css = $css[2];
		if(count($css)){
			foreach($css as $file){
				$fileTime = 
				$this->_document = str_replace($file,$file . "?v=" . filemtime(\Config::get("folder/public") . $file), $this->_document);
			}
		}
		preg_match_all('/https:\/\/(data|test).anonpone.com\/(js\/\w+.js)/', $this->_document, $js);
		$js = $js[2];
		if(count($js)){
			foreach($js as $file){
				$this->_document = str_replace($file,$file . "?v=" . filemtime(\Config::get("folder/public") . $file), $this->_document);
			}
		}
	}

	public function registerStatic(string $string): void {
		if(endsWith($string,".css")){ $this->_data['css'] .= "<link rel=\"stylesheet\" href=\"https://test.anonpone.com/css/{$string}\">"; }
		if(endsWith($string,".js")){ $this->_data['js'] .= "<script src=\"https://test.anonpone.com/js/{$string}\" defer></script>";}
	}
	
	public function render(): string {
		$keys = array_map(function($val) { return "__{$val}__";},array_keys($this->_data));
		foreach(array_combine($keys,$this->_data) as $key => $value){
			if(!is_object($value) && !is_array($value)) $this->_document = str_replace($key,$value,$this->_document);
		}
		$this->bustCache();
		return str_replace("\n","",$this->_document);
	}
}
<?php
namespace Core;

class App {
	protected $defaultController = 'home';
	protected $controller = 'home';
	protected $method = 'index';
	protected $params = [];

	public function __construct(\DB $db,\User\User $user,\Settings $settings){
		\Timing::mark("Core:App Start");
		$url = $this->parseURL();

		if(!(count($url) && $url[0] === "admin") && $settings->get("disableSite") && !$user->isLoggedIn()){
			\Redirect::to(503);
		}

		if(@array_shift((explode(".",$_SERVER['HTTP_HOST']))) === "data" && $url[0] !== "serve"){
			\Redirect::to(\Config::get("website/url"));
		}

		global $argc, $argv;
		if (PHP_SAPI === 'cli') {
			$this->controller = "update";
			$this->method = $argv[1];
		} else {
			$tController = $this->parseController($url);
			if($tController){
				$this->controller = $tController;
			}
		}

		\Timing::mark("Core:App Controller parsed");
		
		require_once \Config::get("folder/controller") . $this->controller . '.php';
		$this->controller = "\\Controller\\" .  str_replace('/', '\\', $this->controller);
		$this->controller = new $this->controller($db,$user,$settings);

		if(isset($url[0])){
			if($this->methodExists($url[0])){
				unset($url[0]);
			} else {
				$this->methodExists($this->method);
			}
		} else {
			$this->methodExists($this->method);
		}

		$this->params = $url ? array_values($url) : [];
		\Timing::mark("Core:App End");
		$this->controller->{$this->method}(...$this->params);
	}

	private function methodExists(string $string): bool {
		if($_SERVER['REQUEST_METHOD'] === "POST" && method_exists($this->controller, $string."Post")){
			$this->method = $string."Post";
			return true;
		} else if($_SERVER['REQUEST_METHOD'] === "PUT" && method_exists($this->controller, $string."Put")){
			$this->method = $string."Put";
			return true;
		} else if($_SERVER['REQUEST_METHOD'] === "DELETE" && method_exists($this->controller, $string."Delete")){
			$this->method = $string."Delete";
			return true;
		} else if(method_exists($this->controller, $string."Get")){
			$this->method = $string."Get";
			return true;
		}
		return false;
	}

	private function parseController(array &$url,string $path = ""): string {
		$directory = scandir(\Config::get("folder/controller") . $path);
		if(count($url)){
			$slice = array_shift($url);
			if(in_array($slice . '.php',$directory)){
				return $path . $slice;
			} else if(in_array($slice,$directory)){
				$return = $this->parseController($url,$path . $slice . "/");
				if(!$return) array_unshift($url,$slice);
				return $return;
			} else if(in_array($this->defaultController . '.php',$directory)){
				array_unshift($url,$slice);
				return $path . $this->defaultController;
			}
		} else if(in_array($this->defaultController . '.php',$directory)){
			return $path . $this->defaultController;
		}
		return "";
	}

	public function parseURL(): array {
		if(isset($_GET['url'])){
			return $url = explode('/',strtolower($_GET['url'])) ?? [];
		} else { return []; }
	}
}

<?php
namespace Controller;

class Quest extends \Core\Controller{
	protected $_archiveFactory;
	protected $_cyoa;

	public function __construct(\DB $db,\User\User $user,\Settings $settings){
		parent::__construct($db,$user,$settings);

		$this->_archiveFactory = new \Archive\Factory($db);
	}

	public function indexGet($story = "", $id = "", $third = ""){
		\Timing::mark("Controller:Index start");
		try{
			$this->_cyoa = new \CYOA\CYOA($this->_db,$story);
		} catch (\Exception $e){
			\Redirect::to(404);
		}

		if($_SERVER['REQUEST_METHOD'] === "POST"){
			$this->indexPost($story, $id, $third );
			return;
		}

		\Timing::mark("Controller:Submethod start");
		if(method_exists($this,strtolower($id) . "Get")){ 
			$this->{strtolower($id) . "Get"}($story,$third); 
			return; 
		}

		$this->postGet($story,$third);
		return;		
	}
	public function indexPost($story = "", $id = "", $third = ""){

		\Timing::mark("Controller:Submethod start");
		if(method_exists($this,strtolower($id) . "Post")){ 
			$this->{strtolower($id) . "Post"}($story,$third); 
			return; 
		} else if (method_exists($this,strtolower($id) . "Get")){
			$this->{strtolower($id) . "Get"}($story,$third); 
			return; 
		}
		return;
	}

	//Utility function
	private function postExists($postID){
		if(!$postID || !is_numeric($postID)){ 
			if($postID === "last") {
				$post = $this->_cyoa->lastPost();
			} else {
				$post = $this->_cyoa->firstPost();
			}
		}
		else {
			try{
				$post = new \CYOA\Post($this->_db,$postID,$this->_cyoa->cid());
			} catch (\Exception $e){
				return null;
			}
		}
		return $post;
	}

	public function postGet($story, $id = ""){
		\Timing::mark("Controller:Post Start");

		$post = $this->postExists($id);
		if(!$post) \Redirect::to(404);
		\Timing::mark("Controller:Got Current Post");
		
		$thread = $this->_cyoa->threads($post->tid());
		\Timing::mark("Controller:Got Threads");
		if($thread) $thread->posts();
		\Timing::mark("Controller:Got Posts");

		if(!$post->isQM()){
			$temppost = $thread->nextPost($post);
			if(!$temppost || !$temppost->isQM()){
				$temppost = $this->_cyoa->nextThread($thread);
			}
			if(!$temppost){
				$temppost = $thread->prevPost($post);
				if(!$temppost || !$temppost->isQM()){
					$temppost = $thread->firstPost();
				}
			}
			if(!$temppost) \Redirect::to(404);
			\Redirect::to("/quest/{$this->_cyoa->shortname()}/post/{$temppost->pid()}");
		}

		$view = new \Views\CYOA\Post($this->_db,$this->_user,'post');
		$view->cyoa = $this->_cyoa;
		$view->archive = $this->_archiveFactory->getArchive($this->_cyoa);
		$view->thread = $thread;
		$view->post = $post;
		echo $view->render();
	}

	public function threadGet($story,$id = ""){
		$post = $this->postExists($id);
		if(!$post) \Redirect::to(404);

		$view = new \Views\CYOA\Thread($this->_db,$this->_user,'thread');
		$view->cyoa = $this->_cyoa;
		$view->archive = $this->_archiveFactory->getArchive($this->_cyoa);
		$view->thread = $this->_cyoa->threads($post->tid());
		$view->alerts = $this->_errorHandler;
		echo $view->render();
	}

	public function galleryGet($story,$id = ""){
		$post = $this->postExists($id);
		if(!$post) \Redirect::to(404);

		$view = new \Views\CYOA\Gallery($this->_db,$this->_user,'gallery');
		$view->cyoa = $this->_cyoa;
		$view->archive = $this->_archiveFactory->getArchive($this->_cyoa);
		$view->thread = $this->_cyoa->threads($post->tid());
		$view->posts = $this->_cyoa->threads($post->tid())->QMPosts();;
		$view->alerts = $this->_errorHandler;
		echo $view->render();
	}

	public function reportGet($story, $id = "", $form = null){
		$post = $this->postExists($id);
		if(!$post) \Redirect::to(404);

		if(!$form) $form = new \Forms\Report($post,\Report\Type::Post);

		$view = new \Views\Common($this->_db,$this->_user,'common/form');
		$view->cyoa = $this->_cyoa;
		$view->title = "Report: " . $this->_cyoa->title() . " " . $post->pid();
		$view->form =  $form->build();
		$view->alerts = $this->_errorHandler;
		echo $view->render();
		return;
	}

	public function reportPost($story, $id = ""){
		$post = $this->postExists($id);
		if(!$post) \Redirect::to(404);

		$form = new \Forms\Report($post,\Report\Type::Post);
		if($form->check()){
			$report = new \Report\Report($this->_db,[
				'cyoa_id' => $post->cid(),
				'other_id' => $post->pid(),
				'type' => \Report\Type::Post,
				'category' => \Input::get('category'),
				'comment' => \Input::get('comment'),
			]);
			try{
				$report->insert();
		    	$this->_session->flash("success","Report Recieved!");
				\Redirect::to("/quest/{$post->cid()}/" . (\Input::get('return') ?: "post") . "/{$post->pid()}");
			} catch(\Exception $e){
				$this->_errorHandler->addError("Invalid content", 'error');
			}
		} else {
			$this->_errorHandler->addErrors($form->errors(),'error');
		}
		$this->reportGet($story, $id, $form);
		
	}

	public function lewdGet($story){
		$view = new \Views\CYOA\Lewd($this->_db,$this->_user,'galleryCommon');
		$view->cyoa = $this->_cyoa;
		$view->archive = $this->_archiveFactory->getArchive($this->_cyoa);
		$view->posts = $this->_cyoa->postsLewd();
		$view->alerts = $this->_errorHandler;
		echo $view->render();
		return;

	}

	public function searchGet($story){
		$search = "kkkkkkkkkkkkkkkkkkkkkkk";
		if(\Input::exists("get")){
			if(\Input::get("q")){
				$search="%" . \Input::get("q") . "%";
			}
		}

		$query = (new \QueryBuilder())->select("posts")->where(["cyoa_id","=",$this->_cyoa->cid(),"isOP","=","1","comment","LIKE",$search]);
		$this->_db->execute($query);
		$pages = new \Paginator($this->_db->count(),7,array(20,40,60,80,100),null,\Input::get("q"));
		$posts = [];

		$query = (new \QueryBuilder())->select("posts")->where(["cyoa_id","=",$this->_cyoa->cid(),"isOP","=","1","comment","LIKE",$search])->order(["date","DESC"])->limit([$pages->limitStart() ?: 0,$pages->limitEnd() ?: 0]);
		if($this->_db->execute($query)->count()){
			foreach($this->_db->results() as $post){
				$posts[] = new \CYOA\Post($this->_db,$post);
			}
		}
		$view = new \Views\CYOA\Search($this->_db,$this->_user,'search');
		$view->cyoa = $this->_cyoa;
		$view->archive = $this->_archiveFactory->getArchive($this->_cyoa);
		$view->posts = $posts;
		$view->pages = $pages;
		$view->alerts = $this->_errorHandler;
		echo $view->render();
	}

	public function fanartGet($story){
		$fanartFactory = new \Fanart\Factory($this->_db);
		$pages = new \Paginator($fanartFactory->count($this->_cyoa->cid()),7,[14,28,56],28); 

		$view = new \Views\CYOA\Fanart($this->_db,$this->_user,'fanart');
		$view->cyoa = $this->_cyoa;
		$view->alerts = $this->_errorHandler;
		$view->pages = $pages;
		$view->fanarts = $fanartFactory->get($this->_cyoa->cid(),[$pages->limitStart(),$pages->limitEnd()]);
		echo $view->render();
	}

	public function atomGet($story){

		$lastMonth = strtotime('-2 week');

		$query = (new \QueryBuilder())->select("posts")->where(["cyoa_id","=",$this->_cyoa->cid(),"date",">",strtotime('-2 week'),"isOP","=","1"])->order(["post_id","DESC"]);
		$this->_db->execute($query);
		if(!$this->_db->first()){
			$query = (new \QueryBuilder())->select("posts")->where(["cyoa_id","=",$this->_cyoa->cid(),"isOP","=","1"])->order(["post_id","DESC"])->limit(20);
			$this->_db->execute($query);
		}

		$testFeed = new \ATOM();
		$testFeed->setLink(\Config::get("website/url") . "quest/{$this->_cyoa->shortname()}");
		$testFeed->setDate(new \DateTime());
		$testFeed->setSelfLink(\Config::get("website/url") . "quest/{$this->_cyoa->shortname()}/atom");
		$testFeed->setTitle($this->_cyoa->title());


		if($this->_db->first()){
			$posts = $this->_db->results();

			foreach($posts as $post){
				$newItem = $testFeed->createNewItem();
				$newItem->setTitle($post->post_id);
				$newItem->setLink(\Config::get("website/url") . "quest/{$this->_cyoa->shortname()}/post/{$post->post_id}");
				$newItem->setDate(date("Y-m-d H:i:s",$post->date));
				$newItem->setAuthor($this->_cyoa->shortname());

				$img = "";
				if($post->filename != ""){
					$image = \Config::get("website/image") . \Image::filePath($post->filename);
					//$size = getimagesize($_SERVER["DOCUMENT_ROOT"] . $image);
					$img = "<img src=\"{$image}\"><br>";
				}
				$newItem->setContent($img . $post->comment);

				$testFeed->addItem($newItem);
			}
		}
		
		header('Content-Type: application/rss+xml; charset=utf-8');
		$testFeed->printFeed();
	}

	public function latestGet($story){
		$query = (new \QueryBuilder())->select("posts")->where(['cyoa_id','=',$this->_cyoa->cid()])->order(['date','DESC'])->limit(1);
		$this->_db->execute($query);

		if($this->_cyoa->live()){
			$archive = $this->_archiveFactory->get($this->_cyoa);
		} 
		if(!$archive) $archive = $this->_archiveFactory->getArchive($this->_cyoa);

		if($archive) \Redirect::to($archive::getURL($this->_cyoa,$archive::LinkURL,$this->_cyoa->lastPost()->tid()));
		\Redirect::to(404);
	}
}

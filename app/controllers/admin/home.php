<?php
namespace Controller\Admin;

class Home extends \Core\Controller{

	public function indexGet(){
		\Timing::mark("Admin:Index Start");

		if(!$this->_user->isLoggedIn()){
			\Redirect::to("/user/login");
		}

		$months = \Input::get("monthsInput") ?: 1;
		$query = (new \QueryBuilder())->select("posts",["cyoa_id, count(cyoa_id) as count"])->where(["isOP","=",1,"date",">",time() - (60*60*24*30*$months)])
			->group("cyoa_id")->having(["count(cyoa_id)",">","0"])->order(["count(cyoa_id)","DESC"]);
		$activity = $this->_db->execute($query)->results();
		$cyoaFactory = new \CYOA\Factory($this->_db);

		if(is_null($this->_settings->get("updateInterval"))) $this->_settings->set("updateInterval",1);
		if(is_null($this->_settings->get("memcached"))) $this->_settings->set("memcached",true);
		if(is_null($this->_settings->get("disableSite"))) $this->_settings->set("disableSite",false);
		if(is_null($this->_settings->get("display\Timing"))) $this->_settings->set("display\Timing",false);
		if(is_null($this->_settings->get("dbCache"))) $this->_settings->set("dbCache",true);

		$view = new \Views\Admin\Index($this->_db,$this->_user,'admin/index');
		$view->cyoaFactory = $cyoaFactory;
		$view->activity = $activity;
		$view->settings = $this->_settings;
		$view->alerts = $this->_errorHandler;
		echo $view->render();
	}
	
	public function indexPost(){
		if(!$this->_user->isLoggedIn()){
			\Redirect::to(403);
		}
		if(\Input::get("save-settings-submit")){
			if(is_numeric(\Input::get("updateInterval")) && \Input::get("updateInterval") >= 0) $this->_settings->set("updateInterval",\Input::get("updateInterval"));

			if(\Input::get("disableSite")) $this->_settings->set("disableSite",true);
			else $this->_settings->set("disableSite",false);

			$this->_settings->set("disableSiteReason",\Input::get("disableSiteReason"));

			if(\Input::get("displayTiming")) $this->_settings->set("displayTiming",true);
			else $this->_settings->set("displayTiming",false);

			if(\Input::get("dbCache")) $this->_settings->set("dbCache",true);
			else $this->_settings->set("dbCache",false);
			\Redirect::to("/admin");
		}
		$this->indexGet();
	}

	public function reportGet(){
		if(!$this->_user->isLoggedIn()){
			\Redirect::to(403);
		}

		$cyoaFactory = new \CYOA\Factory($this->_db);
		$reportFactory = new \Report\Factory($this->_db);

		$view = new \Views\Admin\Reports($this->_db,$this->_user,'admin/report');
		$view->cyoaFactory = $cyoaFactory;
		$view->reports = $reportFactory;
		$view->alerts = $this->_errorHandler;
		echo $view->render();
	}

	public function reportPost(){
		if(!$this->_user->isLoggedIn()){
			\Redirect::to(403);
		}

		if(\Input::get('completeButton') && \Input::get("id")){
			if((new \Report\Report($this->_db, \Input::get("id")))->markComplete()){
				$this->_errorHandler->addError('Completed report','success');
			}
		}

		$this->reportGet();
	}

	public function actionPost(){
		if(!$this->_user->isLoggedIn()){
			\Redirect::to(403);
		}

		$query = null;
		$report = null;
		$posts = [];

		if(\Input::exists("report-id")){
			$report = new \Report\Report($this->_db,\Input::get("report-id"));
			$posts[] = new \CYOA\Post($this->_db,$report->otherID(),$report->cid());
		} else if(\Input::exists("post-id")) {
			$posts[] = new \CYOA\Post($this->_db,\Input::get("post-id"),\Input::get("cyoa-id"));
		} else if(\Input::exists("post-ids")) {
			foreach(explode(",",\Input::get("post-ids")) as $post_id){
				$posts[] = new \CYOA\Post($this->_db,$post_id,\Input::get("cyoa-id"));
			}
		}
		

		foreach($posts as $post){
			switch(\Input::get("selection")){
				case "notCanon":
					break;
				case "notLewd":
					$query = (new \QueryBuilder())->update("posts")->set(["lewd" => 0])->where(["cyoa_id","=",$post->cid(),"post_id","=",$post->pid()]);
					$this->_session->flash("success","Post marked not lewd");
					break;
				case "isLewd":
					$query = (new \QueryBuilder())->update("posts")->set(["lewd" => 1])->where(["cyoa_id","=",$post->cid(),"post_id","=",$post->pid()]);
					$this->_session->flash("success","Post marked lewd");
					break;
				case "notQM":
					$query = (new \QueryBuilder())->update("posts")->set(["isOP" => 0])->where(["cyoa_id","=",$post->cid(),"post_id","=",$post->pid()]);
					$this->_session->flash("success","Post marked not OP");
					break;
				case "isQM":
					$query = (new \QueryBuilder())->update("posts")->set(["isOP" => 1])->where(["cyoa_id","=",$post->cid(),"post_id","=",$post->pid()]);
					$this->_session->flash("success","Post marked OP");
					break;
				case "genThumb":
					break;
			}
			if($query !== null){
				$this->_db->execute($query);
				if($report !== null){
					$report->markComplete();
				}
			}
		}
		\Redirect::back();
	}
}

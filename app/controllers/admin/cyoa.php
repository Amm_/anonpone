<?php
namespace Controller\Admin;

class CYOA extends \Core\Controller{

	public function indexGet($shortname = ""){
		if(!$this->_user->isLoggedIn()){
			\Redirect::to(403);
		}

		$cyoaFactory = new \CYOA\Factory($this->_db);
		$cyoa = $cyoaFactory->get($shortname);
		
		$view = new \Views\Admin\cyoa\Quest($this->_db,$this->_user,'admin/CYOA/quest');
		$view->alerts = $this->_errorHandler;
		$view->cyoaFactory = $cyoaFactory;
		$view->cyoa = $cyoa;
		$view->updateForm = new \Forms\CYOAUpdate($this->_db,$cyoa);
		$view->updateThread = new \Forms\UpdateThread($cyoa);
		$view->searchArchive = new \Forms\SearchArchive($cyoa);
		$view->massInclude = new \Forms\MassInclude($cyoa);
		echo $view->render();
	}

	public function indexPost($shortname = ""){
		if(!$this->_user->isLoggedIn()){
			\Redirect::to(403);
		}

		$cyoaFactory = new \CYOA\Factory($this->_db);
		$cyoa = $cyoaFactory->get($shortname);

		$updateForm = new \Forms\CYOAUpdate($this->_db,$cyoa);
		$updateThread = new \Forms\UpdateThread($cyoa);
		$searchArchive = new \Forms\SearchArchive($cyoa);
		$massInclude = new \Forms\MassInclude($cyoa);

		$archiveFactory = new \Archive\Factory($this->_db);
		if($shortname){
			$archive = $archiveFactory->get($cyoa);
			$archiveHist = $archiveFactory->getArchive($cyoa);
		}

		if($updateForm->check()){
			if(\Input::get('cyoa-submit')){
				try{
					$hooks = "";
					if(\Input::exists("webhooks")) {
						$hooks = \Input::get("webhooks");
						for($count = 0;$count < count($hooks); $count++){
							if(!$hooks[$count]) unset($hooks[$count]);
						}
						$cyoa->setWebhooks(implode(",",array_unique($hooks)));
					}

					$cyoa->setTitle(\Input::get("title"))
						->setShortname(\Input::get("shortname"))
						->setDescription(\Input::get("description"))
						->setTags(\Input::get("tags"))
						->setType(\Input::get("type"))
						->setBoard(explode("/",\Input::get("boardname"))[1])
						->setChan(explode("/",\Input::get("boardname"))[0])
						->setStatus(\Input::get("status"))
						->setFilename(\Input::get("thumbnail"));

					$cyoa->update()->updateDetails($_POST);
				} catch (\ValidationException $e){
					$this->_errorHandler->addError($e->getMessage(),'error');
					$this->_errorHandler->addErrors($e->getMessages(),'error');
				} catch (Exception $e){
					$this->_errorHandler->addError($e->getMessage(),'error');
				}

				$cyoa = new \CYOA\CYOA($this->_db,\Input::get("shortname"));
				$archive = $archiveFactory->get($cyoa);
				
				if(!$this->_errorHandler->hasErrors('error')){
					if($archive){
						$archive->process($cyoa);
						$this->_session->flash('success',"CYOA {$cyoa->title()} Updated!");
						\Redirect::to("/admin/cyoa/{$cyoa->shortname()}");
					}
				}
			}  else if(\Input::get('delete-filter-submit')){
				$this->_db->delete('search',['id', "=", array_key_first(\Input::get('delete-filter-submit'))]);
				$this->_session->flash('success',"Filter deleted");
				\Redirect::to("/admin/cyoa/{$cyoa->shortname()}");
			}
		}
		$this->_errorHandler->addErrors($updateForm->errors(),'error');

		if($updateThread->check()){
			if($archive){
				try{
					$archive->parseThread($cyoa,\Input::get('thread-id'),true);
					$this->_errorHandler->addError(\Input::get('thread-id') . " Successfully added from Live Chan",'success');
				} catch(\Exception $e){
					$this->_errorHandler->addError($e->getMessage(),'error');
					try {
						if($archiveHist){
							$archiveHist->parseThread($cyoa,\Input::get('thread-id'),true);
							$this->_errorHandler->addError(\Input::get('thread-id') . " Successfully added from Archive",'success');
						} else {
							$this->_errorHandler->addError("No Valid Archive",'error');
						}
					} catch(\Exception $e){
						$this->_errorHandler->addError($e->getMessage(),'error');
					}
				}
			} else {
				$this->_errorHandler->addError("No Valid Live Chan",'error');
			}
			$cyoa->updateStats();
		}
		$this->_errorHandler->addErrors($updateThread->errors(),'error');

		if($searchArchive->check()){
			try{
				if($archiveHist){
					$archiveHist->process($cyoa,\Input::get("search"));
					$this->_errorHandler->addErrors($archiveHist->process($cyoa,\Input::get("search")),'error');
				} else {
					$this->_errorHandler->addError("No Valid Archive",'error');
				}
			} catch (Exception $e){
				$this->_errorHandler->addError($e->getMessage(),'error');
			}
			$cyoa->updateStats();
		}
		$this->_errorHandler->addErrors($searchArchive->errors(),'error');

		if($massInclude->check()){
			$include = (\Input::get('include-submit')) ? true : false;
			$vals = explode(',',trim(trim(\Input::get("values"),','),'>>'));

			$values = array();
			foreach($vals as $val){
				$values = array_merge($values,explode('>>',$val));
			}
			$values = array_filter(array_unique($values));

			if(count($values)){
				foreach($values as $postID){
					try{
						$post = new \CYOA\Post($postID,$cyoa->cid());
						if($include && $post->isQM()) { $this->_errorHandler->addError("Post <a href=\"/{$cyoa->shortname()}/{$postID}\">{$postID}</a> is already included",'error'); continue;}
						if(!$include && !$post->isQM()) { $this->_errorHandler->addError("Post <a href=\"/{$cyoa->shortname()}/{$postID}\">{$postID}</a> is already excluded",'error'); continue;}

						$post->assignArray(array("isOP" => $include))->update();
						if($cyoa->threads($post->tid())) $cyoa->threads($post->tid())->markModified();
						$this->_errorHandler->addError("Post <a href=\"/{$cyoa->shortname()}/{$postID}\">{$postID}</a> has been " . (($include) ? "included" : "excluded"),'success');
					} catch (\ValidationException $e){
						$this->_errorHandler->addError($e->getMessage(),'error');
						$this->_errorHandler->addErrors($e->getMessages(),'error');
					} catch(\Exception $e){
						$this->_errorHandler->addError("Post {$postID} doesn't exist",'error');
					}
				}
			} else {
				$this->_errorHandler->addError("No posts detected from the \Input",'error');
			}
			$cyoa->updateStats();
		}
		$this->_errorHandler->addErrors($massInclude->errors(),'error');


		$this->indexGet();
	}

	public function post($shortname = "", $postID = ""){
		$user = \User\User::getInstance();
		$error = new \ErrorHandler();
		\DB::getInstance();
		$shortname = strtolower($shortname);

		if(!$user->isLoggedIn()){
			\Redirect::to(403);
		}

		$cyoa = \CYOA\Handler::get($shortname);
		if($cyoa){
			try{
				$post = new \CYOA\Post($postID,$cyoa->cid());
				$thread = $cyoa->threads($post->tid());
			} catch(\Exception $e){
				$post = false;
				$thread = false;
			}
		} else {
			$post = false;
			$thread = false;
		}

		$form = new \Forms\PostUpdate($cyoa,$thread,$post);

		if($form->check()){
			try{
				if(\Input::get('update-submit'))	{
					$newpost = (new \CYOA\Post($postID,$cyoa->cid()))->assignArray($_POST)->update();
					$cyoa->markModified();
				} else {
					$newpost = (new \CYOA\Post($_POST))->insert($error);
				}
			} catch (\ValidationException $e){
				$error->addError($e->getMessage(),'error');
				$error->addErrors($e->getMessages(),'error');
			} catch (\Exception $e){
				$error->addError($e->getMessage(),'error');
			}

			if(!$error->hasErrors()){
				$this->_session->flash('success',"Post Updated");
				\Redirect::to("/admin/cyoa/post/{$newpost->cid()}/{$newpost->pid()}");
			}
		}
		$error->addErrors($form->errors(),'error');

		$this->view('admin/cyoa/post', [
			"error" => $error,
			"cyoas" => \CYOA\Handler::get(array()),
			"cyoa" => $cyoa,
			"post" => $post,
			"thread" => $thread,
			"forms" => [
				"postUpdate" => $form,
			]

		]);
	}

	public function thread($shortname, $thread_id){
		$user = \User\User::getInstance();
		$error = new \ErrorHandler();
		\DB::getInstance();
		$shortname = strtolower($shortname);

		if(!$user->isLoggedIn()){
			\Redirect::to(403);
		}

		$cyoas = \CYOA\Handler::get(array());
		$cyoa = \CYOA\Handler::get($shortname);
		if(!$cyoa){
			$this->_session->flash('error',"Invalid CYOA");
			\Redirect::to("/admin");
		} 

		if(\Input::exists()){
			if(\Input::get('thread-submit'))	{
				try{
					$thread = (new \CYOA\Thread($thread_id,$cyoa->cid()))->assignArray($_POST)->update($error);
					$cyoa->markModified();
				} catch (ValidationException $e){
					$error->addError($e->getMessage(),'error');
					$error->addErrors($e->getMessages(),'error');
				} catch (Exception $e){
					$error->addError($e->getMessage(),'error');
				}

				if(!$error->hasErrors()){
					$this->_session->flash('success',"Thread Updated");
					\Redirect::to("/admin/thread/{$cyoa->shortname()}/{$thread_id}");
				}
			}
		}

		try{
			$thread = new \CYOA\Thread($thread_id,$cyoa->cid());
		} catch(\Exception $e){
			$this->_session->flash('error',"Invalid CYOA");
			\Redirect::to("/admin/{$cyoa->shortname()}");
		}
		
		$this->view('admin/cyoa/thread', [
			"error" => $error,
			"cyoas" => $cyoas,
			"cyoa" => $cyoa,
			"thread" => $thread,
		]);
	}

	public function update(){
		$user = \User\User::getInstance();
		$errorHandler = new \ErrorHandler();

		if(!$user->isLoggedIn()){
			\Redirect::to(403);
		}

		$posts = array();
		$cyoa = \CYOA\Handler::get(\Input::get("cyoa_id"));

		if(\Input::get("post_id")){
			$postIDs = explode(",",\Input::get("post_id"));
			foreach($postIDs as $postID){
				try{
					$posts[] = new \CYOA\Post($postID, $cyoa->cid());
				} catch(\Exception $e){}
			}
		}

		if(\Input::exists()){
			if(\Input::get('selection')){
				$modify = array();
				switch(\Input::get('selection')){
					case "remImage":
						if($post->imagePath()){
							$image = basename($post->imagePath());
							$post->assignArray(array("_filename" => $image))->update($errorHandler);
							\Image::delete($image);
						}
					break;
					case "notCanon":
						$modify["_canon"] = 0;
					break;
					case "canon":
						$modify["_canon"] = 1;
					break;
					case "notLewd":
						$modify["_lewd"] = 0;
					break;
					case "lewd":
						$modify["_lewd"] = 1;
					break;
					case "notQM":
						$modify["_QM"] = 0;
					break;
					case "QM":
						$modify["_QM"] = 1;
					break;
					case "forgetThread":
						$thread = new \CYOA\Thread($posts[0]->tid(),$posts[0]->cid());
						$thread->forget();
						\Redirect::to("/admin/cyoa/{$cyoa->shortname()}/");
					break;
					case "genThreadStat":
						$thread = new \CYOA\Thread($posts[0]->tid(),$posts[0]->cid());
						$thread->assignArray(array("_used" => 1));
						$thread->updateStats(true);
						$thread->update();
						$cyoa->forget();
						\Redirect::to("/admin/cyoa/{$cyoa->shortname()}/");
					break;
					case "removeThread":
						$thread = new \CYOA\Thread($posts[0]->tid(),$posts[0]->cid());
						$thread->delete();
						\Redirect::to("/admin/cyoa/{$cyoa->shortname()}/");
					break;
				}
				if(count($modify)){
					foreach($posts as $post){
						$post->assignArray($modify)->update();
						$cyoa->threads($post->tid())->markModified();
					}
				}
			}
		}

		$reportID = \Input::get("report-id") ?: false;
		if($reportID) (new \Report\Report($reportID))->markComplete();
		if($post) \Redirect::to("/{$cyoa->shortname()}/{$post->pid()}");
		\Redirect::to("/{$cyoa->shortname()}");
	}

	

	public function pastebin($id = ""){
		$user = \User\User::getInstance();
		$error = new \ErrorHandler();

		if(!$user->isLoggedIn() || $id == ""){
			\Redirect::to(403);
		}
		try{
			$pastebin = new Pastebin($id);
		} catch(Exception $e){
			$this->_session->flash('error', 'Invalid paste ID');
			\Redirect::to('/admin');
		}

		if(\Input::exists())	{

			$info['_description'] = \Input::get('description');
			$info['_tags'] = \Input::get('tags');
			$info['_cid'] = \Input::get('cyoaid');
			$info['_pid'] = \Input::get('postid');
			$pastebin->assignArray($info)->update($error);
		}
		$this->view('admin/cyoa/pastebin', [
			"error" => $error,
			"cyoas" => \CYOA\Handler::get(),
			"pastebin" => $pastebin,
		]);
	}

	public function fanart($id = 0){
		$user = \User\User::getInstance();
		$error = new \ErrorHandler();

		if(!$user->isLoggedIn()){
			\Redirect::to(403);
		}

		try{
			$fanart = new Fanart((int) $id);
		} catch(Exception $e){
			$this->_session->flash('error',"Invalid Fanart");
			\Redirect::to("/admin");
		}

		$cyoa = \CYOA\Handler::get($fanart->cid());

		if(!$cyoa){
			$this->_session->flash('error',"Invalid CYOA id");
			\Redirect::to("/admin");
		} 

		if(\Input::exists()){
			if(\Input::get('update-submit')){
				$_POST['lewd'] = \Input::exists('lewd'); 

				try{
					$fanart->assignArray($_POST)->update();
				} catch (\ValidationException $e){
					$error->addError($e->getMessage(),'error');
					$error->addErrors($e->getMessages(),'error');
				} catch (\Exception $e){
					$error->addError($e->getMessage(),'error');
				}

				if(!$$error->hasErrors()){
					$this->_session->flash('success',"Fanart Updated");
					\Redirect::to("/admin/fanart/{$fanart->id()}");
				}
			}
		}
		$this->view('admin/cyoa/fanart', [
			"cyoas" => \CYOA\Handler::get(array()),
			"error" => $error,
			"fanart" => $fanart,
			"cyoa" => $cyoa,
		]);
	}

	public function webhook(){
		$user = \User\User::getInstance();
		$error = new \ErrorHandler();
		$form = new \Forms\Webhooks();

		if(!$user->isLoggedIn()){
			\Redirect::to(403);
		}

		if($form->check()){
			if(\Input::get('update-submit') || \Input::get('update-submit')){
				try{
					if(\Input::get('update-submit')){
						$values["id"] = array_key_first(\Input::get('update-submit'));
						$values["name"] = \Input::get('name')[$values["id"]];
						$values["url"] = \Input::get('wurl')[$values["id"]];
						$values["interval"] = \Input::get('interval')[$values["id"]];
						$values["dedAlert"] = \Input::get('dedAlert')[$values["id"]];
						$values["active"] = \Input::get('active')[$values["id"]];
						$webhook = (new \Webhook\Webhook($values))->update();
						$this->_session->flash('success',"Webhook Updated");
					} else if(\Input::get('new-submit')) {
						$values["id"] = array_key_first(\Input::get('new-submit'));
						$values["name"] = \Input::get('new-name')[$values["id"]];
						$values["url"] = \Input::get('new-wurl')[$values["id"]];
						$values["interval"] = \Input::get('new-interval')[$values["id"]];
						$values["dedAlert"] = \Input::get('new-dedAlert')[$values["id"]];
						$values["active"] = \Input::get('new-active')[$values["id"]];
						$webhook = (new \Webhook\Webhook($values))->insert();
						$this->_session->flash('success',"Webhook Inserted");
					}
					\Redirect::to("/admin/webhook");
				} catch (\ValidationException $e){
					$error->addError($e->getMessage(),'error');
					$error->addErrors($e->getMessages(),'error');
				} catch (\Exception $e){
					$error->addError($e->getMessage(),'error');
				}
			} else if(\Input::get('test-submit')){
				$id = array_key_first(\Input::get('test-submit'));
				$webhook = new \Webhook\Webhook($id);
				$result = $webhook->send("This is an Anonpone test",true);
				$form = new \Forms\Webhooks([$id => $result[0]]);
			} else if(\Input::get('delete-submit')){
				$id = array_key_first(\Input::get('delete-submit'));
				$webhook = (new \Webhook\Webhook($id))->delete();
				$cyoas = \CYOA\Handler::get(array());
				foreach($cyoas as $cyoa){
					$webhooks = $cyoa->webhooks();
					if(array_key_exists($id,$webhooks)){
						unset($webhooks[$id]);
						$cyoa->assignArray(array("webhooks" => implode(",",array_keys($webhooks))))->update();
					}
				}
				$this->_session->flash('success',"Webhook Deleted");
				\Redirect::to("/admin/webhook");
			}
		}
		$error->addErrors($form->errors(),'error');

		$this->view('admin/cyoa/webhook', [
			"webhooks" => \Webhook\Handler::getAll(),
			"error" => $error,
			"forms" => [
				"webhooks" => $form,
			]
		]);
	}
		
}

<?php
namespace Controller;

class Pastes extends \Core\Controller
{
	public function index()
	{
		$db = \DB::getInstance();
		$data['error'] = new \ErrorHandler();


		if(\Input::exists()){
			if(\Input::get('submitPaste') ){
				$ids = \Input::get("pasteID");
				$pIDs = array();
				foreach($ids as $id){
					$id = trim($id,"/");
					$pIDs[] = \Pastebin::getID($id);
				}

				$descriptions = \Input::get("description");
				$tags = \Input::get("tags");
				$cyoaid = \Input::get("cyoa_id");

				for($x = 0;$x < count($pIDs);$x++){
					try {
						$pastebin = new \Pastebin($pIDs[$x]);
						if($pastebin->exists($data['error'])){
							$pastebin->addRevision($data['error'])->update($data['error']);
						}
					} catch (\Exception $e){
						$pastebin = new \Pastebin(array("_id" => $pIDs[$x],"_description" => $descriptions[$x],"_tags" => $tags[$x]));
						$pastebin->insert($data['error']);
					}
				}
			}
		}

		$data['pages'] = new \Paginator(\PastebinHandler::count(),7,array(20,40,60,80,100));

		$order = array("added","DESC");

		if(\Cookie::exists('paste_sort')){
			switch(\Cookie::get('paste_sort')){
				case 'AuthorAsc':
					$order = array("author","ASC");
					break;
				case 'AuthorDesc':
					$order = array("author","DESC");
					break;
				case 'AddedAsc':
					$order = array("added","ASC");
					break;
				case 'AddedDesc':
					$order = array("added","DESC");
					break;
				case 'CreationAsc':
					$order = array("created","ASC");
					break;
				case 'CreationDesc':
					$order = array("created","DESC");
					break;
			}
		}
		

		$data['pastebins'] = \PastebinHandler::get("",array($data['pages']->limitStart(),$data['pages']->limitEnd()),$order[0],$order[1]);

		$data['cyoas'] = \CYOA\Handler::get();
		$this->view('pastebin/index', $data);

	}

	public function story($id = ""){
		if($id === ""){
			\Session::flash('error',"Invalid paste ID");
			\Redirect::to("/pastes/");
		}
		$data['error'] = new \ErrorHandler();
		
		try{
			$data['pastebin'] = new \Pastebin($id);
			if($data['pastebin']->exists($data['error'])){
				$data['pastebin']->addRevision($data['error'])->update($data['error']);
			}
		} catch(Exception $e){
			\Session::flash('error',"Invalid paste ID");
			\Redirect::to("/pastes/");
		}

		$data['cyoas'] = \CYOA\Handler::get();
		$this->view('pastebin/story', $data);
	}
}

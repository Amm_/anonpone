<?php
namespace Controller;

class Home extends \Core\Controller{
	public function indexGet($story = "", $postID = "",$thirdVariable = ""){
		\Timing::mark("Home:Index Start");
		$cyoaFactory = new \CYOA\Factory($this->_db);
		$cyoaFactory->get();
		\Timing::mark("Home:Index Got CYOAs");
		
		if($story){
			$link = "/quest/{$story}/";
			if($postID) $link .= $postID . "/";
			if($postID && $thirdVariable) $link .= $thirdVariable . "/";
			\Redirect::to($link);
			return;
		}
		$fanartFactory = new \Fanart\Factory($this->_db,$cyoaFactory);
		$fanart = array_filter($fanartFactory->get("%",6), function($obj){
			static $imageList = [];
		    if(in_array($obj->imagePath(),$imageList)) {
		        return false;
		    }
		    $imageList[] = $obj->imagePath();
		    return true;
		});
		\Timing::mark("Home:Index End");

		$view = new \Views\Index($this->_db,$this->_user,'index');
		$view->cyoas = array_reverse($cyoaFactory->get(),true);
		$view->fanarts = $fanart;
		echo $view->render();
	}

	public function allGet(){
		\Timing::mark("Fanart:Index Start");
		if(\Input::exists('get') && \Input::get("artist")){
			$pages = new \Paginator(FanartHandler::countByArtist(\Input::get("artist")),7,array(24,40,60,80,100));
			$fanart = \FanartHandler::getByArtist(\Input::get("artist"),array($pages->limitStart(),$pages->limitEnd()));
		} else {
			$pages = new \Paginator(\FanartHandler::count(),7,array(24,40,60,80,100));
			$fanart = \FanartHandler::get("%",array($pages->limitStart(),$pages->limitEnd()));
		}

		$this->view('fanart', [
			'cyoas' => \CYOA\Handler::get(),
			'pages' => $pages,
			'fanart' => $fanart,
			'artists' => \FanartHandler::artists(),
		]);
	}

	public function contactGet(){
		$contactForm = new \Forms\ContactUs();

		$view = new \Views\Common($this->_db,$this->_user,'contact');
		$view->form =  $contactForm->build();
		$view->alerts = $this->_errorHandler;
		echo $view->render();
	}
	public function contactPost(){
		$contactForm = new \Forms\ContactUs();

		if($contactForm->check()){
		    $visitor_email = "";
		    $email_title = "/mlp/ CYOA - ";
			$email_body = "";
			$to_email = "joelaw9@gmail.com";

			if(\Input::get('email')) {
		        $visitor_email = str_replace(array("\r", "\n", "%0a", "%0d"), '', Input::get('email'));
		        $visitor_email = filter_var($visitor_email, FILTER_VALIDATE_EMAIL);
		        $email_body .= "
		        Visitor Email: {$visitor_email}";
		    } else {
		    	$email_body .= "wat";
		    }
		    if(\Input::get('subject')) {
		        $email_title .= filter_var(Input::get('subject'), FILTER_SANITIZE_STRING);
		        $email_body .= "
		        Title: {$email_title}";
		    } else {
		    	 $email_title = "wat";
		    }
		    if(\Input::get('body')) {
		        $visitor_message = htmlspecialchars(Input::get('body'));
		        $email_body .= "
		        Message: {$visitor_message}";
		    }

		    $email_body .= "";

		    $headers = array("From: " . "joelaw9@gmail.com",
			    "Reply-To: {$to_email}",
			    "X-Mailer: PHP/" . PHP_VERSION
			);
			$headers = implode("\r\n", $headers);
		      
		    if(mail($to_email, $email_title, $email_body, $headers)) {
		        $this->_session->flash('success', 'Your message has been sent!');
		    } else {
		        $this->_session->flash('error', 'Fission Mailed');
		    }
		    
			\Redirect::to('/contact');
		} else {
			$this->_errorHandler->addErrors($contactForm->errors(),'error');
			$this->contactGet();
		}
	}

	public function adviceGet(){
		$view = new \Views\Common($this->_db,$this->_user,"advice");
		echo $view->render();
	}
}

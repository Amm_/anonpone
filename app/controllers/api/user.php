<?php
namespace Controller\api;

class User extends \Core\Controller
{
	public function indexGet(){
		if($this->_user->isLoggedIn()){
			$result = [];
			$result['id'] = $this->_user->id();
			$result['username'] = $this->_user->id();
			$result['roles'] = $this->_user->getRoles();

			header('Content-Type: application/json');
			echo json_encode($result);
		} else {
			header('Content-Type: application/json');
			echo json_encode(['error' => "User not found"]);
		}
	}
}
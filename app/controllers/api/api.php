<?php
namespace Controller\Api;

class Api extends \Core\Controller
{
	public function indexGet(){
		$view = new \Views\Common($this->_db,$this->_user,'api');
		$view->website_url = \Config::get('website/url');
		echo $view->render();
	}
}

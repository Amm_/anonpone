<?php
namespace Controller\Api;

class Report extends \Core\Controller
{
	public function countGet(){
		$count = 0;
		if($this->_user->isLoggedIn() && $this->_user->hasPrivilege("viewReports")){
			$factory = new \Report\Factory($this->_db);
			$count = $factory->count(true);
		}
		echo json_encode(['count' => $factory->count(true)]);
		exit;
	}
}

<?php
namespace Controller\api;

class Fanart extends \Core\Controller
{
	public function indexGet($cyoa_id,$limit = 5){
		$fanarts = [];
		$results = [];
		$cyoa_id = strtolower($cyoa_id);


		if($cyoa_id == "new" && is_numeric($limit)){
			$fanarts = \FanartHandler::get("%", $limit);
		} else if($cyoa_id == "artist" && $limit != 5){
			$fanarts = \FanartHandler::getByArtist($limit);
		} else if ($cyoa_id == "artists") {
			$artists = [];
			foreach(\FanartHandler::artists() as $artist){
				$artists[] = $artist->artist;
			}
			header('Content-Type: application/json');
			echo json_encode($artists);
			return;
		} else {
			try{ 
				$cyoa = new \CYOA\CYOA($cyoa_id);
				if($cyoa){
					$fanarts = \FanartHandler::get($cyoa);
				}
			} catch(\Exception $e){}
		}

		if($fanarts){
			foreach($fanarts as $fanart){
				if(\CYOA\Handler::get($fanart->cid())){
					$results[] = [
						"cyoa_id" => $fanart->cid(),
						"cyoa_title" => \CYOA\Handler::get($fanart->cid())->title(),
						"lewd" => $fanart->lewd(),
						"title" => $fanart->title(),
						"artist" => $fanart->artist(),
						"url" => $fanart->imagePath()
					];
				}
			}
		}

		header('Content-Type: application/json');
		echo json_encode($results);
	}
}
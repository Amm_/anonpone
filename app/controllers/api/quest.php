<?php
namespace Controller\Api;

class Quest extends \Core\Controller
{
	public function indexGet($id = -1){
		if($id !== -1 && is_int($id))  $this->id($id);

		$lastModified = 0;
		$cyoas = [];
		foreach((new \CYOA\Factory($this->_db))->get() as $cyoa){
			$cyoas[$cyoa->cid()] = $cyoa->toArray();
			$lastModified = max($lastModified,$cyoa->lastModified(),$cyoa->lastPost()->rawDate());
		}

		$timestamp = gmdate('D, d M Y H:i:s T', $lastModified);
		$this->ifModified($timestamp);

		$cyoa = $cyoa->toArray();
		header('Content-Type: application/json');
		header("Last-Modified: {$timestamp}");
		echo json_encode($cyoas);
		exit;
	}

	public function id($id = -1){
		try{
			$cyoa = new \CYOA\CYOA($this->_db,$id);
			$timestamp = gmdate('D, d M Y H:i:s T', max($cyoa->lastModified(),$cyoa->lastPost()->rawDate()));
			$this->ifModified($timestamp);

			$cyoa = $cyoa->toArray();
			header('Content-Type: application/json');
			header("Last-Modified: {$timestamp}");
			echo json_encode($cyoa);
			exit;
		} catch(\Exception $e){
			header('Content-Type: application/json');
			echo json_encode(['error' => $e->getMessage()]);
			exit;
		}
	}

	public function threadsGet($cyoa_id = -1){
		try{
			$lastModified = 0;
			$threads = [];
			$cyoa = new \CYOA\CYOA($this->_db,$cyoa_id);
			foreach($cyoa->threads() as $thread){
				$threads[$thread->tid()] = $thread->toArray();
				$lastModified = max($lastModified,$thread->lastModified(),$thread->rawDate());
			}

			$timestamp = gmdate('D, d M Y H:i:s T', $lastModified);
			$this->ifModified($timestamp);
			header('Content-Type: application/json');
			header("Last-Modified: {$timestamp}");
			echo json_encode($threads);
			exit;
		} catch(\Exception $e){
			header('Content-Type: application/json');
			echo json_encode(['error' => $e->getMessage()]);
			exit;
		}
	}

	public function threadGet($cyoa_id = -1,$thread_id = -1, $op = 'true'){
		try{
			$lastModified = 0;
			$cyoa = new \CYOA\CYOA($this->_db,$cyoa_id);
			$thread = new \CYOA\Thread($this->_db,$thread_id,$cyoa->cid());
			$posts = [];
			if($op === "false"){
				$postsArray = $thread->posts();
			} else {
				$postsArray = $thread->QMPosts();
			}
			foreach($postsArray as $post){
				$posts[$post->pid()] = $post->toArray();
				$lastModified = max($lastModified,$post->lastModified(),$post->rawDate());
			}
			
			$timestamp = gmdate('D, d M Y H:i:s T', $lastModified);
			$this->ifModified($timestamp);
			header('Content-Type: application/json');
			header("Last-Modified: {$timestamp}");
			echo json_encode($posts);
			exit;
		} catch(\Exception $e){
			header('Content-Type: application/json');
			echo json_encode(['error' => $e->getMessage()]);
			exit;
		}
	}
	public function postGet($cyoa_id = 0,$post_id = 0)
	{
		header('Content-Type: application/json');

		try{
			$cyoa = new \CYOA\CYOA($this->_db,$cyoa_id);
			$post = new \CYOA\Post($this->_db,$post_id,$cyoa->cid());

			$timestamp = gmdate('D, d M Y H:i:s T', max($post->lastModified(),$post->rawDate()));
			$this->ifModified($timestamp);
			header('Content-Type: application/json');
			header("Last-Modified: {$timestamp}");
			echo json_encode($post->toArray());
		} catch(\Exception $e){
			echo json_encode(['error' => $e->getMessage()]);
			return;
		}
	}
}
<?php
namespace Controller;

class FIMfiction extends \Core\Controller{

	public function index(){

	}
	public function foe(){
		$memory = \Memory::getInstance();

		$stories = json_decode($memory->get('fimfiction'),true);
		if(!$stories){
			$storyIDs = array(
				"Anywhere But Here" => 22731,
				"Black and White" => 44457,
				"Crystal Hearts" => 256322,
				"Dance of the Orthrus" => 263354,
				"Ditzy Doo Chronicles" => 6663,
				"Frozen Skies" => 201326,
				"Heroes" => 662,
				"Homelands" => 298834,
				"Longtalons" => 172805,
				"Make Love Not War" => 301900,
				"Murkey Number Seven" => 47300,
				"Old souls" => 73868,
				"Ouroboros" => 48696,
				"Outlaw" => 16293,
				"Project Horizons" => 208056,
				"Rising Dawn" => 159701,
				"Sisters" => 168405,
				"The Chrysalis" => 288365,
				"The Eerie" => 274372,
				"The Things We've Handed Down" => 285142,
				"Transient" => 317965,
				"Treasure Hunting" => 53502,
				"Tribes" => 301880,
				"Wasteland Bouquet" => 35229,
				"Wasteland Economics" => 163079,
				"Dead Tree" => 393681,
				);
				
			$FIMRetriever = $this->model("FIMRetriever");
			$stories = array();

			foreach($storyIDs as $id){
				if($FIMRetriever->getStory($id)->FOEFormat()){
					$stories = $stories + $FIMRetriever->FOEFormat();
				}
			}

			krsort($stories);
			$memory->set("fimfiction",json_encode($stories),24*60*60);
		}
		$data['stories'] = $stories;
		$this->view('fimfiction', $data);
		return;
	}
}

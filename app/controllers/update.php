<?php
namespace Controller;

class Update extends \Core\Controller{
	public function index($story = "")
	{
		$errorHandler = new \ErrorHandler();
		$cyoa = \CYOA\Handler::get($story);
		$archive = \Archive\Handler::get($cyoa);

		try{
			$archive->process($cyoa,true);
		} catch (\Exception $e){
			$errorHandler->addError($e->getMessage(),'error');
		}

		if($errorHandler && $errorHandler->hasErrors())
		{
			foreach($errorHandler->all() as $error)
			{
				echo $error . "<br>";
			}
		}
		echo "END";
	}

	public function threadsGet(){
		$string = "";

		if(!$this->_settings->get("updateInterval") && PHP_SAPI == "cli") return;

		if(time() - $this->_settings->get("updateTime") < $this->_settings->get("updateInterval") * 60) return;
		$this->_settings->set("updateTime",time());


		$cyoaFactory = new \CYOA\Factory($this->_db);
		$archiveFactory = new \Archive\Factory($this->_db);
		
		foreach($cyoaFactory->get([\CYOA\Status::Disabled]) as $cyoa){
			try {
				$archive = $archiveFactory->get($cyoa);
				
				if($archive){
					$response = $archive->process($cyoa);
					if($response){
						echo "{$cyoa->title()} " . implode("//",$response);
					} else {
						echo "{$cyoa->title()} checked";
					}
				} else {
					echo "{$cyoa->title()} Invalid Imageboard";
				}
			} catch (\ValidationException $e){
				echo "{$cyoa->title()} {$e->getMessage()}";
				\Sanitization::pprint($e->getMessages());
			} catch (\Exception $e){
				echo "{$cyoa->title()} {$e->getMessage()}";
			}
			echo (PHP_SAPI !== "cli") ? "<br>" : "\n";
		}
	}
	

	public function pastes()
	{
		$db = \DB::getInstance();
		$data['error'] = new \ErrorHandler();

		if($db->get("pastebinRevisions",["editedDate",">",date('Y-m-d H:i:s',strtotime("-3 month"))],["editedDate","ASC"])->first())
		{
			$results;

			foreach($db->results() as $result)
			{
				$results[] = $result->id;
			}
			$results = array_unique($results);

			foreach($results as $id)
			{
				$pastebin = $this->model("Pastebin");
				$data['error'] = $pastebin->retrieve($id,"","",$data['error']);
			}
		}

		if($data['error']->hasErrors())
		{
			foreach($data['error']->all() as $error)
			{
				echo $error . "\n<br>";
			}
		}
	}

	public function report()
	{

		$db = \DB::getInstance();
		$token = new \Token();

		if(\Input::exists() && $token->check(Config::get("csrf/name"),\Config::get("csrf/value")) && \Sanitize::isEnglish(Input::get("comment")) && \Input::get("recaptcha_response")){

		    $recaptcha = file_get_contents(Config::get('recaptcha/url') . '?secret=' . \Config::get('recaptcha/secretKey') . '&response=' . \Input::get('recaptcha_response'));
    		$recaptcha = json_decode($recaptcha);

		    if($recaptcha != null && $recaptcha->success){
		    	$errorHandler = new \ErrorHandler;
		    	$report = new \Report\Report($_POST);

		    	$report->insert($errorHandler);
		    	$this->_session->flash("success","Report Recieved!");

				// if((Input::get('post_id') || Input::get('fanart_id')) && Input::get('cyoa_id') && Input::get('report')){
				// 	if($db->get("cyoa",array("id","=",Input::get('cyoa_id')))){
				// 		$cyoa = $db->first();

				// 		Report::submit($_POST);

				// 		Session::flash("success","Report Recieved!");
				// 		if(Input::get("post_id")){
				// 			Redirect::to("/{$cyoa->shortname}/" . Input::get("post_id"));
				// 		} else if(Input::get("fanart_id")){
				// 			Redirect::to("/{$cyoa->shortname}/fanart");
				// 		}
				// 	}
				// }else if(Input::get('paste_id') && Input::get('report')){

				// 	Report::submit($_POST);

				// 	Session::flash("success","Report Recieved!");
				// 	Redirect::to("/pastes/story/" . Input::get("paste_id"));
				// }
			}
		}
		if(\Input::get('redirect')){
    		\Redirect::to(Input::get('redirect'));
    	}

		\Redirect::to('/');
	}

	public function statsGet($shortname = ""){
		$cyoaFactory = new \CYOA\Factory($this->_db);

		if($shortname){
			$cyoa = $cyoaFactory->get($shortname);
			if($cyoa) $cyoa->updateStats(true);
		} else {
			foreach($cyoaFactory->get([]) as $cyoa){
				$cyoa->updateStats(true);
			}
		}
	}


	public function test($story="",$postid = ""){
		require_once \Config::get("folder/view") . "common" . '.php';
		$view = new \View\Common();
	}


	
}
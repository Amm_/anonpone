<?php
namespace Controller;

class User extends \Core\Controller{

	public function indexGet(){
		\Redirect::to("/user/login");
	}

	public function loginGet(){
		if($this->_user->isLoggedIn()) \Redirect::to('/');

		$view = new \Views\Common($this->_db,$this->_user,'common/form');
		$view->form = (new \Forms\Login())->build();
		$view->title = "Login";
		$view->alerts = $this->_errorHandler;
		echo $view->render();
	}
	public function loginPost(){
		$loginForm = new \Forms\Login();
		if($loginForm->check()){
			$login = new \User\Login($this->_db,$this->_user);

			if($login->login(\Input::get('username'), \Input::get('password'), true)){
				if(in_array('HTTP_REFERER', $_SERVER)){ 
					\Redirect::to($_SERVER['HTTP_REFERER']);
				} else{
					\Redirect::to('/');
				}
			} else {
				$this->_session->flash('error', $login->error());
				\Redirect::to('/user/login');
			}
		}
		$this->_errorHandler->addErrors($loginForm->errors(),'error');
		$this->loginGet();
	}

	public function logoutGet(){
		if(!$this->_user->isLoggedIn()) \Redirect::to('/user/login');
		(new \User\Login($this->_db,$this->_user))->logout();
		die();
		\Redirect::to('/');
	}

	public function validateGet($hash = null){
		if($this->_user->isLoggedIn())	\Redirect::to('/');

		$validationType = (int) \Input::get("type");
		if(!\User\ValidationType::isValidValue($validationType)){
			\Redirect::to('/user/login');
		} 

		if(isset($hash) && $hash){
			$validation = new \User\Validation($this->_db,$this->_user);
			$validation->validate($hash,$validationType);
		}

		$view = new \Views\Common($this->_db,$this->_user,'common/form');
		$view->form = (new \Forms\Validation())->build();
		$view->title = "Validation";
		$view->alerts = $this->_errorHandler;
		echo $view->render();
	}
	public function validatePost($hash = null){
		$validationType = (int) \Input::get("type");
		if(!\User\ValidationType::isValidValue($validationType)){
			\Redirect::to('/user/login');
		} 

		$form = new \Forms\Validation();
		if($form->check()){
			$user = new \User\User(\Input::get('email'));

			(new \User\Login($user))->sendValidation($validationType);
			
			$this->_session->flash('success', 'A new validation email has been sent!');
			\Redirect::to('/');
		} else {
			$this->_errorHandler->addErrors($form->errors(),'error');
			$this->validateGet();
		}
	}

	public function register(){die();
		$errorHandler = new \ErrorHandler;
		$token = new \Token();

		if(Input::exists())	{
			if($token->check(Input::get('token'))){
				$validation = new Validation($errorHandler);

				$validate = $validation->check($_POST, array(
					'username' => array(
						'required' => true,
						'maxlength' => 20,
						'minlength' => 3,
						'alnum' => true,
						'unique' => 'users'
					),
					'name' => array(
						'required' => true,
						'maxlength' => 255
					),
					'email' => array(
						'required' => true,
						'email' => true,
						'maxlength' => 64,
						'unique' => 'users'
					),
					'password' => array(
						'required' => true,
						'minlength' => 6
					),
					'password_repeat' => array(
						'match' => 'password'
					)
				));

				if(!$validate->fails())	{
					$user = new User();
					$hash = new Hash();

					$salt = $hash->salt(32);

					try {

						$user->create(array(
							'username' => Input::get('username'),
							'password' => $hash->make(Input::get('password'),$salt),
							'email' => Input::get('email'),
							'salt' => $salt,
							'name' => Input::get('name'),
							'joined' => time(),
							'g' => 3
						));
						$user_data = get_object_vars($user->getLast());

						$user = new User($user_data['id']);

						$user->send_validation();
						
						$this->_session->flash('home', 'You have been registered! Please confirm your account via email!');
						Redirect::to('/home/index');

					} catch(Exception $e) {
						$errorHandler->addError('Account creation failed', 'validation');
					}
				}
			}
		}

		$this->view('user/register', array(
			'error' => $errorHandler
		));
	}

	public function forgot($hash = null){die();
		$user = new \User\User();
		$errorHandler = new \ErrorHandler();
		$db = \DB::getInstance();
		if($user->isLoggedIn()) \Redirect::to('/');

		$form = new \Forms\Validation();

		if(isset($hash)){
			$userID = $db->get('user_validation',array('hash','=',$hash,'type',"=",\User\ValidationType::PasswordReset))->first();

			if($userID){
				$form = new \Forms\PassReset();
				if($resetForm->check()){
					$db->delete('user_validation',array('hash','=',$hash,'type',"=",\User\ValidationType::PasswordReset));
					if($db->count() > 0){
						// $salt = Hash::salt(32);
						// $user = new User($user_data['u_id']);

						// $user->update(array(
						// 	'password' => Hash::make(Input::get('password_new'), $salt),
						// 	'salt' => $salt
						// ), $user_data['u_id']);

						$this->_session->flash('home', 'Your password has been updated');
						Redirect::to('/home/index');
					}
				}
				$errorHandler->addErrors($resetForm->errors(),'error');
			} else {
				$errorHandler->addError('Invalid Reset Code','validation');
			}
		} 

		if($validationForm->check()){
			$user = new \User\User(\Input::get('email'));

			(new \User\Login($user))->sendValidation(\User\ValidationType::PasswordReset);
			
			$this->_session->flash('success', 'A new validation email has been sent!');
			\Redirect::to('/');
		}
		$errorHandler->addErrors($validationForm->errors(),'error');

		$this->view('genericThird', array(
			'error' => $errorHandler,
			'passToken' => $hash,
			'forms' => $form,
		));
	}

	public function password(){die();
		$user = new User();
		$errorHandler = new ErrorHandler();
		$token = new Token();

		if(!$user->isLoggedIn() || !$user->hasPermission('user')){
			Redirect::to(503);
		}

		if(Input::exists()){
			if($token->check(Input::get('token'))){
				$validate = new Validation($errorHandler);
				$validation = $validate->check($_POST, array(
					'password_current' => array(
						'required' => true,
						'minlength' => 6
					),
					'password_new' => array(
						'required' => true,
						'minlength' => 6
					),
					'password_new_again' => array(
						'match' => 'password_new'
					)
				));

				if(!$validation->fails()){
					try{
						$hash = new Hash();
						if($hash->make(Input::get('password_current'), $user->data()->salt) !== $user->data()->password)	{
							echo "Your current password is wrong";
						} else {
							$salt = $hash->salt(32);
							$user->update(array(
								'password' => $hash->make(Input::get('password_new'), $salt),
								'salt' => $salt
							));

							$this->_session->flash('home', 'Your password has been updated');
							Redirect::to('/home/index');
						}
					} catch ( Exception $e) {
						$errorHandler->addError('Password Update Failed','validation');
					}
				}
			}
		}

		$this->view('user/changepassword', array(
			'error' => $errorHandler
		));
	}
}

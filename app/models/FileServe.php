<?php 
use PHPePub\Core\EPub;
use PHPePub\Core\EPubChapterSplitter;
use PHPePub\Core\Logger;
use PHPePub\Core\Structure\OPF\DublinCore;
use PHPePub\Core\Structure\OPF\MetaValue;
use PHPePub\Helpers\CalibreHelper;
use PHPePub\Helpers\URLHelper;
use PHPZip\Zip\File\Zip;

class FileServe{
	public static function serve($cyoa,$thread = "",$type = "text"){
		$archivePath = Config::get("folder/base") . "anonpone/archive_zip/";

		$cyoa = \CYOA\Handler::get($cyoa);
		if(!$cyoa){ 
			throw new Exception("Invalid CYOA ID");
		}

		if($thread){
			$thread = $cyoa->threads($thread);
			if(!$thread){
				throw new Exception("Invalid Thread ID");
			}
		} else {
			$thread = false;
		}

		$filename = $cyoa->title();
		if($thread){
			$filename .= " - {$thread->tid()}";
		}

		if($type === "image"){
			$filename .= ".zip";
			$zip = new ZipArchive;
			if($zip->open($archivePath . $filename, ZipArchive::CREATE | ZipArchive::OVERWRITE) !== TRUE){
				die("zip fail");
			}
		} else {
			$filename .= ".txt";
		}
		
		if(!file_exists($archivePath . $filename)){
			if($thread){
				$posts = $thread->QMPosts();
			} else {
				$posts = array();
				foreach($cyoa->threads() as $tThread){
					$posts = array_merge($posts,$tThread->QMPosts());
				}
			}

			$fullText = $cyoa->title() . "\n\n";

			foreach($posts as $post){
				$fullText .= "[{$post->pid()}]\n";
				$fullText .= strip_tags(preg_replace("/<br\/>/","\n",static::cleanText($post->comment())));
				$fullText .= "\n\n------------------------\n";

				if($type === "image"){
					if($post->imagePath()){
						$file = $post->imagePath(true);
						$ext = pathinfo($file, PATHINFO_EXTENSION);
						if(file_exists($file)){
							$zip->addFile($file,$post->pid() . "." . $ext);
						}
					}
					if($post->imagePathLewd()){
						$file = $post->imagePathLewd(true);
						$ext = pathinfo($file, PATHINFO_EXTENSION);
						if(file_exists($file)){
							$zip->addFile($file,$post->pid() . "-lewd." . $ext);
						}
					}	
				}
			}

			if($type === "image"){
				// if(!$thread){
				// 	foreach(FanartHandler::get($cyoa->cid()) as $fanart){
				// 		$file = $fanart->imagePath(true);
				// 		$ext = pathinfo($file, PATHINFO_EXTENSION);
				// 		if(file_exists($file)){
				// 			if($fanart->pid()){
				// 				$zip->addFile($file,$fanart->pid() . "-fan." . $ext);
				// 			} else {
				// 				$zip->addFile($file,pathinfo($file, PATHINFO_FILENAME) . "." . $ext);
				// 			}
				// 		}
				// 	}
				// }
				$zip->addFromString("story.txt",$fullText);
			}
			file_put_contents($archivePath . $filename,$fullText);
		} else {
			$fullText = file_get_contents($archivePath . $filename);
		}

		header("Pragma: public");
		header("Expires: 0");
		header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
		header("Cache-Control: public");
		header("Content-Description: File Transfer");
		header("Content-Disposition: attachment; filename=\"{$filename}\"");
		header("Content-Transfer-Encoding: binary");
		if($type === "image"){
			$zip->close();
		    header("Content-Type: application/zip");
		    header("Content-Length: " . filesize($archivePath . $filename));

		    readfile($archivePath . $filename);
		    exit;
		}else{
			header("Content-Type: application/force-download");
			header("Content-Length: " . mb_strlen($fullText));

			echo $fullText;
			exit;
		}
	}

	public static function serveEpub($cyoa,$cThread = ""){
		$archivePath = Config::get("folder/base") . "anonpone/archive_zip/";

		$cyoa = \CYOA\Handler::get($cyoa);
		if(!$cyoa){ 
			throw new Exception("Invalid CYOA ID");
		}

		if($cThread){
			$cThread = $cyoa->threads($cThread);
			if(!$cThread){
				throw new Exception("Invalid Thread ID");
			}
		} else {
			$cThread = false;
		}

		$filename = $cyoa->title();
		if($cThread){
			$filename .= " - {$cThread->tid()}";
		}
		$filename .= ".epub";

		if(!file_exists($archivePath . $filename)){
			$content_start =
			"<?xml version=\"1.0\" encoding=\"utf-8\"?>\n"
			. "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.1//EN\"\n"
			. "    \"http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd\">\n"
			. "<html xmlns=\"http://www.w3.org/1999/xhtml\">\n"
			. "<head>"
			. "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />\n"
			. "<link rel=\"stylesheet\" type=\"text/css\" href=\"styles.css\" />\n";

			if($cThread){
				$content_start .= "<title>{$cyoa->title()} {$cThread->tid()}</title>\n";
			} else {
				$content_start .= "<title>{$cyoa->title()}</title>\n";
			}
			
			$content_start .= "</head>\n"
			. "<body>\n";

			$bookEnd = "</body>\n</html>\n";

			$log = new Logger("Example", TRUE);

			$book = new EPub();
			$log->logLine("new EPub()");
			$log->logLine("EPub class version.: " . EPub::VERSION);
			$log->logLine("Zip version........: " . Zip::VERSION);

			if($cThread)	{
				$book->setTitle($cyoa->title() . " " . $cThread->tid());
			} else{
				$book->setTitle($cyoa->title());
			}
			$book->setIdentifier("https://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]", EPub::IDENTIFIER_URI);
			$book->setDescription($cyoa->description());
			$book->setSourceURL(Config::get('website/url') . $cyoa->shortname());


			$log->logLine("Set up parameters");
			$cssData = "body {\n  margin-left: .5em;\n  margin-right: .5em;\n  text-align: justify;\n}\n\np {\n  font-family: serif;\n"
			. "font-size: 10pt;\n  text-align: justify;\n  text-indent: 1em;\n  margin-top: 0px;\n  margin-bottom: 1ex;\n}\n\n"
			. "h1, h2 {\n  font-family: sans-serif;\n  font-style: italic;\n  text-align: center;\n  background-color: #6b879c;\n  color: white;\n  width: 100%;\n}\n\n"
			. "h1 {\n    margin-bottom: 2px;\n}\n\nh2 {\n    margin-top: -2px;\n    margin-bottom: 2px;\n}\n\n .greentext, .quote{ color: #789922;}\n"
			. ".spoiler, s {\n  color: #000000;\n  background: #000000;\n  text-decoration:none;\n }\n"
			. "spoiler:hover,s:hover {\n  color: #ffffff;\n  color: #eeeeee;\n  text-decoration:none;\n}";

			$log->logLine("Add css");
			$book->addCSSFile("styles.css", "css1", $cssData);

			
			
			$log->logLine("Add Cover Image");
			// if(@array_pop(explode('.',$cyoa->thumbnail)) == 'png')
			// {
				//$book->setCoverImage("Cover.png", file_get_contents($this->_imagePath . $cyoa->shortname . "/" . basename($cyoa->thumbnail)), "image/png");
			// }
			$book->setCoverImage($cyoa->imagePath(true));
			$log->logLine("Set Cover Image");


			//$book->buildTOC(NULL, "toc", "Table of Contents", TRUE, TRUE);

			if($cThread)	{
				$threads = array($cThread);
			} else{
				$threads = $cyoa->threads();
			}

			foreach($threads as $thread){
				$threadtxt = $content_start . "<h2>{$thread->tid()}</h2>\n";
				foreach($thread->posts() as $post)	{
					if($post->isQM()){
						if($post->imagePath()){
							if($post->imagePathLewd()){
								$filepath = $post->imagePathLewd(true);
							}else{
								$filepath = $post->imagePath(true);
							}
							$threadtxt .= "<p><img src=\"images/" . basename($filepath) . "\" alt=\"\"></img></p>\n";
							$ext = pathinfo($filepath, PATHINFO_EXTENSION);
							$tmpext = ($ext === "jpg") ? "jpeg" : $ext;
							$book->addLargeFile("images/". basename($filepath), "i".pathinfo($filepath,PATHINFO_FILENAME), $filepath, "image/" . $tmpext);
						}
						if($post->comment()){
							$threadtxt .= "<p>" . static::epubClean($post->comment()) . "</p>\n";
						}
					}
				}
				$threadtxt .= $bookEnd;

				$log->logLine("Add Chapter {$thread->tid()}");
				$book->addChapter("Thread {$thread->tid()}", "{$thread->tid()}.html",$threadtxt);
			}

			$log->logLine("Add TOC");
			$book->buildTOC();

			$book->finalize();

			if($cThread){
				$book->saveBook("{$cyoa->title()} {$cThread->tid()}", $archivePath);
				$zipData = $book->sendBook("{$cyoa->title()} {$cThread->tid()}");
			}else{
				$book->saveBook("{$cyoa->title()}", $archivePath);
				$zipData = $book->sendBook("{$cyoa->title()}");
			}
			exit;
		}

		header("Content-Type: application/zip");
	    header("Content-Length: " . filesize($archivePath . $filename));
	    header("Content-Disposition: attachment; filename=\"{$filename}\"");

	    readfile($archivePath . $filename);
	    exit;
	}

	private static function epubClean($string){
		$string = preg_replace("/<\/?s>/","",static::cleanText($string));
		return $string;
	}
	private static function cleanText($string){
		$string = preg_replace("/#p[0-9]{8}/","",preg_replace("/<wbr>/","",preg_replace("/<br>/","<br/>",preg_replace("/&/","&amp;",preg_replace('#<br>(\s*<br>)+#', '<br><br>', preg_replace("/>>[0-9]{5,}/","",str_replace("&#039;","'",str_replace("&gt;",">",html_entity_decode($string)))))))));
		$string = preg_replace("/<3/","	&#60;3",preg_replace("/\/mlp\/thread\/#p[0-9]{8}/","",$string));
		$string = preg_replace("/<3/","	&#60;3",preg_replace("/\/mlp\/thread\/[0-9]{8}/","",$string));
		$string = preg_replace("/(\n){3,}/","\n\n",$string);
		return $string;
	}

	public static function delete($cyoa,$cThread = ""){
		$exts = array(".txt",".zip",".epub");
		$archivePath = Config::get("folder/base") . "anonpone/archive_zip/";

		$cyoa = \CYOA\Handler::get($cyoa);
		if(!$cyoa){ 
			throw new Exception("Invalid CYOA ID");
		}

		if($cThread){
			$cThread = $cyoa->threads($cThread);
			if(!$cThread){
				throw new Exception("Invalid Thread ID");
			}
		} else {
			$cThread = false;
		}

		$filename = $cyoa->title();
		if($cThread){
			$filename .= " - {$cThread->tid()}";
		}

		foreach($exts as $ext){
			if(file_exists($archivePath . $filename . $ext)) unlink($archivePath . $filename . $ext);
		}
	}
}

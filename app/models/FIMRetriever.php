<?php

class FIMRetriever
{
	public $result;

	public function __construct() {
   	}

   	public function getStory($storyID)
   	{
   		if(is_numeric($storyID))
   		{
   			// if(file_exists())
   			$html = file_get_contents("http://www.fimfiction.net/api/story.php?story={$storyID}");

   			$story = json_decode($html);
   			if(!isset($story->error))
   			{
   				$this->result = $story->story;
   				return $this;
   			}
   		}
   		$this->result = "";
   		return $this;
   	}

   	public function FOEFormat()
   	{
   		if($this->result)
   		{
	   		$story = $this->result;
	   		$chapters = array();

	   		$chapter = end($story->chapters);

	   		$story->title = str_replace("Fallout Equestria: ", "",$story->title);
	   		$story->title = str_replace("Fallout Equestria - ", "",$story->title);
	   		$story->title = str_replace("Fallout: Equestria - ", "",$story->title);
	   		$story->title = str_replace("Fallout Equestria : ", "",$story->title);
	   		$text = $story->title . " (";
			if($story->date_modified < strtotime("-6 months"))
			{
				$text .= date("d/m/y",$story->date_modified);
			}
			else
			{
				$text .= date("F jS",$story->date_modified);
			}
			$text .= ")";
			if($story->status == "Complete")
			{
				$text .= " - Complete";
			}
			$text .= "<br>";
			$text .= "<a href=\"{$story->url}\">http://www.fimfiction.net/story/{$story->id}/</a><br>";

			$chapters[$story->date_modified] = $text;

			return $chapters;
   		}
   	}
}

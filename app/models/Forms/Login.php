<?php
namespace Forms;

class Login extends \FormBuilder
{
	public function __construct() {
		parent::__construct(null, "/user/login");

		$this->addField([
			"label" => "",
			"type" => \FormBuilder::Title,
			"value" => "Login"
 		],[
			"label" => "Email",
			"name" => "username",
			"type" => \FormBuilder::Text,
			"validation" => [
				"required" => true,
				"email" => true,
				"maxlength" => 64
			]
 		],[
			"label" => "Password",
			"name" => "password",
			"type" => \FormBuilder::Password,
			"validation" => [
				"required" => true,
				"minlength" => 6,
			],
			"ignoreInput" => true,
			// 'attributes' => [
			// 	"autocomplete" => "off",
			// ]
 		],[
			"name" => "submit",
			"type" => \FormBuilder::Submit,
			"value" => "Log In",
 		]);
	}
}

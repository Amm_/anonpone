<?php
namespace Forms;

class PostUpdate extends \FormBuilder
{
	public function __construct($cyoa = null,$thread = null,$post = null) {
		parent::__construct(null, "/admin/cyoa/post/" . (($cyoa) ? $cyoa->shortname() . "/" : "") . (($post) ? $post->pid() : ""));

		$this->addField([
			"label" => "Posts",
			"type" => \FormBuilder::Title,
		]);

		if($post){
			$value = "";
			if($thread->prevPost($post)) $value .= "<a href=\"/admin/cyoa/post/{$cyoa->shortname()}/{$thread->prevPost($post)->pid()}\">Previous</a> | ";
			if($thread->nextPost($post)) $value .= "<a href=\"/admin/cyoa/post/{$cyoa->shortname()}/{$thread->nextPost($post)->pid()}\">Next</a>";
			$this->addField([
				"label" => "",
				"type" => \FormBuilder::Stat,
				"value" => $value
			]);
		}
		
		if($post){
			$value = "";
			$value .= "<input type='hidden' name='post-id' value='{$post->pid()}'>";
			$value .= "<a href=\"/{$cyoa->shortname()}/{$post->pid()}\">{$post->pid()}</a> / ";
			$value .= "<input type='hidden' name='thread-id' value='{$post->tid()}'>";
			$value .= "<a href=\"/admin/thread/{$cyoa->shortname()}/{$post->tid()}\">{$post->tid()}</a> / ";
			$value .= "<input type='hidden' name='cyoa-id' value='{$post->cid()}'>";
			$value .= "<a href=\"/admin/cyoa/{$cyoa->shortname()}/\">{$cyoa->title()}</a>";
			$this->addField([
				"label" => "Post/Thread/CYOA",
				"type" => \FormBuilder::Stat,
				"value" => $value
			]);
		} else {
			$cyoas = \CYOA\Handler::get();
			$cyoaValues = [];
			foreach($cyoas as $cyoa){
				$cyoaValues[$cyoa->cid()] = $cyoa->title();
			}
			$this->addField([
				"label" => "Post/Thread/CYOA",
				"type" => \FormBuilder::Multi,
				"fields" => [[[
					"type" => \FormBuilder::Text,
					"name" => "post_id",
					"placeholder" => "Post ID",
					'validation' => [
						"required" => true,
						"num" => true,
					]
				],[
					"type" => \FormBuilder::Text,
					"name" => "thread_id",
					"placeholder" => "Thread ID",
					'validation' => [
						"required" => true,
						"num" => true,
					]
				],[
					"type" => \FormBuilder::Select,
					"name" => "cyoa_id",
					"options" => $cyoaValues,
					'validation' => [
						"required" => true,
						"num" => true,
					]
				]]]
			]);
		}
		$this->addField([
			"label" => "Name/Trip",
			"type" => \FormBuilder::Multi,
			"fields" => [[[
				"type" => \FormBuilder::Text,
				"name" => "name",
				"placeholder" => "Name",
				"value" => ($post ? $post->name() : ""),
			],[
				"type" => \FormBuilder::Text,
				"name" => "trip",
				"placeholder" => "Trip",
				"value" => ($post ? $post->trip() : ""),
			]]]
		],[
			"label" => "Timestamp",
			"type" => \FormBuilder::Text,
			"name" => "date",
			"value" => ($post ? $post->rawDate() : ""),
			'validation' => [
				"required" => true,
				"num" => true,
			]
		],[
			"label" => "Image",
			"type" => \FormBuilder::Text,
			"name" => "filename",
			"value" => ($post ? basename($post->imagePath()) : ""),
			'validation' => [
				"extension" => ["png","jpg","jpeg","gif","webm"],
			]
		],[
			"label" => "Lewd Image",
			"type" => \FormBuilder::Text,
			"name" => "image_lewd",
			"value" => ($post ? basename($post->imagePathLewd()) : ""),
			'validation' => [
				"extension" => ["png","jpg","jpeg","gif","webm"],
			]
		],[
			"label" => "Replies",
			"type" => \FormBuilder::Text,
			"name" => "replies",
			"value" => ($post ? implode(',',$post->replies()) : ""),
			'validation' => [
				"whitelist" => ["0-9",">",","],
			]
		],[
			"label" => "Is QM",
			"type" => \FormBuilder::Checkmark,
			"name" => "isOP",
			"value" => ($post ? $post->isQM() : ""),
		],[
			"label" => "Lewd",
			"type" => \FormBuilder::Checkmark,
			"name" => "lewd",
			"value" => ($post ? $post->lewd() : ""),
		],[
			"label" => "Canon",
			"type" => \FormBuilder::Checkmark,
			"name" => "canon",
			"value" => ($post ? $post->canon() : ""),
		],[
			"label" => "Comment",
			"type" => \FormBuilder::TextArea,
			"name" => "comment",
			"value" => ($post ? $post->comment() : ""),
		]);

		if($post){
			$this->addField([
				"label" => "",
				"name" => "update-submit",
				"type" => \FormBuilder::Submit,
				"value" => "Submit"	
			]);
			$value = "";
			if($thread->prevPost($post)) $value .= "<a href=\"/admin/cyoa/post/{$cyoa->shortname()}/{$thread->prevPost($post)->pid()}\">Previous</a> | ";
			if($thread->nextPost($post)) $value .= "<a href=\"/admin/cyoa/post/{$cyoa->shortname()}/{$thread->nextPost($post)->pid()}\">Next</a>";
			$this->addField([
				"label" => "",
				"type" => \FormBuilder::Stat,
				"value" => $value
			]);
		} else {
			$this->addField([
				"label" => "",
				"name" => "new-submit",
				"type" => \FormBuilder::Submit,
				"value" => "Submit"	
			]);
		}

		
	}
}

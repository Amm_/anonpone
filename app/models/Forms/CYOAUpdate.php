<?php
namespace Forms;

class CYOAUpdate extends \FormBuilder
{
	public function __construct(\DB $db, $cyoa = null) {
		parent::__construct(null, "/admin/cyoa/" . (($cyoa) ? $cyoa->shortname() : ""));

		$this->addField([
			"label" => "CYOAS",
			"type" => \FormBuilder::Title,
		],[
			"label" => "CYOA ID",
			"name" => "id-row",
			"type" => \FormBuilder::Multi,
			"fields" => [[[
				"type" => \FormBuilder::Stat,
				"value" => ($cyoa ? "<a href=\"/{$cyoa->shortName()}\">{$cyoa->cid()}</a>" : ""),
			],[
				"name" => "cyoa-id",
				"type" => \FormBuilder::Hidden,
				"value" => ($cyoa ? $cyoa->cid() : ""),
				'validation' => [
					'num' => true
				]
			]]]
		],[
			"label" => "Title",
			"name" => "title",
			"type" => \FormBuilder::Text,
			"value" => ($cyoa ? $cyoa->title() : ""),
			'validation' => [
				'required' => true,
				'maxlength' => 52
			]
		],[
			"label" => "Short Name",
			"name" => "shortname",
			"type" => \FormBuilder::Text,
			"value" => ($cyoa ? $cyoa->shortname() : ""),
			'validation' => [
				'required' => true,
				'maxlength' => 16,
			]
		],[
			"label" => "Board Name",
			"name" => "boardname",
			"type" => \FormBuilder::Text,
			"default" => "4chan/mlp",
			"value" => ($cyoa ? $cyoa->boardname() : ""),
		],[
			"label" => "Type",
			"name" => "type",
			"type" => \FormBuilder::Select,
			"options" => \CYOA\Type::class,
			"value" => ($cyoa ? $cyoa->type() : ""),
			'validation' => [
				'required' => true,
				'enum' => \CYOA\Type::class,
			],
		],[
			"label" => "Status",
			"name" => "status",
			"type" => \FormBuilder::Select,
			"options" => \CYOA\Status::class,
			"value" => ($cyoa ? $cyoa->status() : ""),
			'validation' => [
				'required' => true,
				'enum' => \CYOA\Status::class,
			],
		],[
			"label" => "Thumbnail",
			"name" => "thumbnail",
			"type" => \FormBuilder::Text,
			"value" => ($cyoa ? basename($cyoa->imagePath()) : ""),
			'validation' => [
				'required' => true,
				'extension' => ["png","jpg","jpeg","gif"],
			]
		],[
			"label" => "Tags",
			"name" => "tags",
			"type" => \FormBuilder::Text,
			"value" => ($cyoa ? implode(",",$cyoa->tags()) : ""),
			'validation' => [
				'required' => true,
			]
		],[
			"label" => "Description",
			"name" => "description",
			"type" => \FormBuilder::TextArea,
			"value" => ($cyoa ? $cyoa->description() : ""),
			'validation' => [
				'required' => true,
				'minlength' => 10,
			]
		]);


		//Live Search
		$filterFields = [];
		if($cyoa){
			$filterFields = $this->describeFilters($cyoa->filter(true));
		}
		$count = 1;
		if(\Input::exists('new-search-type')) $count = count(\Input::get('new-search-type')) - 1;
		if($count < 1) $count = 1;
		$filterFields = array_merge($filterFields,array_fill(0, $count,[
			[
				'name' => 'new-search-type[]',
				"type" => \FormBuilder::Select,
				"options" => \CYOA\Search::class,
				'validation' => [
					'enum' => \CYOA\Search::class,
				],
			],[
				'name' => 'new-search-term[]',
				"type" => \FormBuilder::Text,
			],[
				'name' => 'new-search-catalog[]',
				"type" => \FormBuilder::Hidden,
				'value' => 1
		]]));

		$this->addField([
			"label" => "Live Search",
			"name" => "liveSearch",
			"type" => \FormBuilder::Multi,
			"fields" => $filterFields,
		]);


		//Filters
		$filterFields = [];
		if($cyoa){
			$filterFields = $this->describeFilters($cyoa->filter(false));
		}
		$count = 1;
		if(\Input::exists('new-search-type')) $count = count(\Input::get('new-search-type')) - 1;
		if($count < 1) $count = 1;
		$filterFields = array_merge($filterFields,array_fill(0, $count,[
			[
				'name' => 'new-search-type[]',
				"type" => \FormBuilder::Select,
				"options" => \CYOA\Search::class,
				'validation' => [
					'enum' => \CYOA\Search::class,
				],
			],[
				'name' => 'new-search-term[]',
				"type" => \FormBuilder::Text,
			],[
				'name' => 'new-search-catalog[]',
				"type" => \FormBuilder::Hidden,
				'value' => 0
		]]));
		$this->addField([
			"label" => "Filter",
			"name" => "filter",
			"type" => \FormBuilder::Multi,
			"fields" => $filterFields,
		]);




		//Webhooks
		$fields = [];
		$webhookFactory = new \Webhook\Factory($db);
		$webhooks = $webhookFactory->getAll();
		$whooks = [];
		foreach($webhooks as $webhook){
			$whooks[$webhook->id()] = $webhook->name();
		}
		if($cyoa){
			foreach($cyoa->webhooks() as $webhook){
				$fields[] = [
					'name' => 'webhooks[]',
					"type" => \FormBuilder::Select,
					"options" => [$webhook->id() => $webhook->name(), 0 => "Delete"],
					"value" => $webhook->id()
				];
			}
		} else {
			$fields[] = [
				'name' => 'webhooks[]',
				"type" => \FormBuilder::Select,
				"options" => $whooks,
				'value' => 5
			];
		}
		$whooks = array(0 => "New Hook") + $whooks;
		$count = 1;
		if(\Input::exists('webhooks')) $count = count(\Input::get('webhooks')) - count($fields);
		if($count < 1) $count = 1;
		$fields = array_merge($fields,array_fill(count($fields)-1, $count,[
			'name' => 'webhooks[]',
			"type" => \FormBuilder::Select,
			"options" => $whooks,
		]));
		$this->addField([
			"label" => "Webhooks",
			"name" => "webhook",
			"type" => \FormBuilder::Multi,
			"fields" => [$fields],
		]);


		$this->addField([
			"label" => "",
			"name" => "submits",
			"type" => \FormBuilder::Multi,
			"fields" => [[[
				"name" => "cyoa-submit",
				"type" => \FormBuilder::Submit,
				"value" => "Send"		
			],[
				"name" => "cyoa-update",
				"type" => \FormBuilder::Submit,
				"value" => "Update"	
			]]]
		]);
	}

	private function describeFilters($filters){
		$filterFields = [];
		foreach($filters as $filter){
			$filterFields[] = [[
				'name' => 'search-id[]',
				'type' => \FormBuilder::Hidden,
				'value' => $filter->id(),
				'validation' => [
					'required' => true,
					'num' => true,
				]
			],[
				'name' => 'search-type[]',
				"type" => \FormBuilder::Select,
				"options" => \CYOA\Search::class,
				'value' => $filter->type(),
				'validation' => [
					'required' => true,
					'enum' => \CYOA\Search::class,
				],
			],[
				'name' => 'search-term[]',
				"type" => \FormBuilder::Text,
				'value' => $filter->term(),
				'validation' => [
					'required' => true,
				],
			],[
				'name' => 'search-catalog[]',
				"type" => \FormBuilder::Hidden,
				'value' => $filter->catalog(),
			],[
				'name' => "delete-filter-submit[{$filter->id()}]",
				"type" => \FormBuilder::Submit,
				'value' => 'delete',
			]];
		}
		return $filterFields;
	}

}

<?php
namespace Forms;

class Validation extends \FormBuilder
{
	public function __construct() {
		parent::__construct();
		$this->addField([
			"type" => \FormBuilder::Stat,
			"value" => "Enter your email below to send a new validation email.",
		],[
			"label" => "Email:",
			"type" => \FormBuilder::Text,
			"name" => "email",
			'validation' => [
				"required" => true,
    			'email' => true,
    			'exists' => ['user','email']
    		]
		],[
			"name" => "submit",
			"type" => \FormBuilder::Submit,
			"value" => "Send"
		]);
	}
}

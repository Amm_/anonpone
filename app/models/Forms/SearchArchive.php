<?php
namespace Forms;

class SearchArchive extends \FormBuilder
{
	public function __construct($cyoa = null) {
		parent::__construct(null, "/admin/cyoa/" . (($cyoa) ? $cyoa->shortname() : ""));

		$this->addField([
			"label" => "Search Archive",
			"type" => \FormBuilder::Title,
		],[
			"name" => "search",
			"type" => \FormBuilder::Text,
			"placeholder" => "search",
			'validation' => [
				'required' => true,
			]
		],[
			"name" => "archive-thread-submit",
			"type" => \FormBuilder::Submit,
			"value" => "Retrieve"	
		]);
	}
}

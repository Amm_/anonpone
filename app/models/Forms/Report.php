<?php
namespace Forms;

class Report extends \FormBuilder
{
	public function __construct($thing,$type) {
		switch($type){
			case \Report\Type::Post:
				parent::__construct(null, "/quest/{$thing->cid()}/Report/{$thing->pid()}");
				$options = [
					-3 => "Choose an option",
		        	\Report\Category::BrokenImage => \Report\Category::toString(\Report\Category::BrokenImage),
		        	\Report\Category::NonStoryPost => \Report\Category::toString(\Report\Category::NonStoryPost),
		        	\Report\Category::DuplicatePost => \Report\Category::toString(\Report\Category::DuplicatePost),
		        	\Report\Category::MissingPost => \Report\Category::toString(\Report\Category::MissingPost),
		        	\Report\Category::MissingThread => \Report\Category::toString(\Report\Category::MissingThread),
		        	\Report\Category::MissingLewd => \Report\Category::toString(\Report\Category::MissingLewd),
		        	\Report\Category::Other => \Report\Category::toString(\Report\Category::Other),
				];
				$this->addField([
		 			"name" => "other_id",
					"type" => \FormBuilder::Text,
					"value" => $thing->pid(),
					'validation' => [
						'required' => true,
						'num' => true,
					],
					'attributes' => [ "readonly" => ""]
				],[
					"name" => "category",
					"type" => \FormBuilder::Select,
					"options" => $options,
					'validation' => [
						'required' => true,
						'enum' => \Report\Category::class,
					]
		 		],[
					"name" => "return",
					"type" => \FormBuilder::Hidden,
					"value" => "report",
					'validation' => [
						'required' => true,
					]
		 		]);
				break;
		}

 		$this->addField([
 			"name" => "comment",
			"type" => \FormBuilder::TextArea,
			'validation' => [
				'required' => true,
				'maxlength' => 100,
			]
 		],[
			"name" => "submit",
			"type" => \FormBuilder::Submit,
			"value" => "Submit"
		]);
	}
}

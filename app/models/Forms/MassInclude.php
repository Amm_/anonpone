<?php
namespace Forms;

class MassInclude extends \FormBuilder
{
	public function __construct($cyoa = null) {
		parent::__construct(null, "/admin/cyoa/" . (($cyoa) ? $cyoa->shortname() : ""));

		$this->addField([
			"label" => "Mass Include/Exclude",
			"type" => \FormBuilder::Title,
		],[
			"name" => "values",
			"type" => \FormBuilder::TextArea,
			'validation' => [
				'required' => true,
				'whitelist' => ["0-9",">",",","\r\n|\r|\n"]
			]
		],[
			"name" => "submits",
			"type" => \FormBuilder::Multi,
			"fields" => [[[
				"name" => "include-submit",
				"type" => \FormBuilder::Submit,
				"value" => "Include"		
			],[
				"name" => "exclude-submit",
				"type" => \FormBuilder::Submit,
				"value" => "Exclude"	
			]]]
		]);
	}
}

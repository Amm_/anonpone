<?php
namespace Forms;

class ContactUs extends \FormBuilder
{
	public function __construct() {
		parent::__construct(null, "/contact");

		$this->addField([
			"name" => "email",
			"type" => \FormBuilder::Text,
			"placeholder" => "Email"
 		],[
			"name" => "subject",
			"type" => \FormBuilder::Text,
			"placeholder" => "Subject"
 		],[
			"name" => "body",
			"type" => \FormBuilder::TextArea,
			"placeholder" => "Subject",
			"style" => "height:200px",
			'validation' => [
				'required' => true,
				'minlength' => 10,
			]
 		],[
			"name" => "submit",
			"type" => \FormBuilder::Submit,
			"value" => "Send",
 		]);
	}
}

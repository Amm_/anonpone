<?php
namespace Forms;

class UpdateThread extends \FormBuilder
{
	public function __construct($cyoa = null) {
		parent::__construct(null, "/admin/cyoa/" . (($cyoa) ? $cyoa->shortname() : ""));

		$this->addField([
			"label" => "Add Threads",
			"type" => \FormBuilder::Title,
		],[
			"name" => "thread-id",
			"type" => \FormBuilder::Text,
			"placeholder" => "thread ID",
			'validation' => [
				'required' => true,
				'whitelist' => ["0-9",">"]
			]
		],[
			"name" => "submits",
			"type" => \FormBuilder::Multi,
			"fields" => [[[
				"name" => "thread-submit",
				"type" => \FormBuilder::Submit,
				"value" => "Retrieve"		
			],[
				"name" => "thread-all-submit",
				"type" => \FormBuilder::Submit,
				"value" => "Add All Posts"	
			]]]
		]);
	}
}

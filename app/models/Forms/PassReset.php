<?php
namespace Forms;

class PassReset extends \FormBuilder
{
	public function __construct() {
		parent::__construct();
		$this->addField([
			"label" => "Password",
			"type" => \FormBuilder::Password,
			"name" => "password",
			'validation' => [
				"required" => true,
				"minlength" => 6,
				"hasCapital" => true,
				"hasLowercase" => true,
				"hasNumber" => true,
			]
		],[
			"label" => "Repeat Password",
			"type" => \FormBuilder::Password,
			"name" => "password_repeat",
			'validation' => [
				"match" => "password",
			]
		],[
			"name" => "submit-pass",
			"type" => \FormBuilder::Submit,
			"value" => "Submit"	
		]
	}
}

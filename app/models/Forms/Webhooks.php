<?php
namespace Forms;

class Webhooks extends \FormBuilder
{
	public function __construct($testResults = []) {
		parent::__construct(null, "/admin/webhook/");

		$this->addField([
			"label" => "Webhooks",
			"type" => \FormBuilder::Title,
		],[
			"name" => "header",
			"type" => \FormBuilder::Horizontal,
			"fields" => [
				[
					"type" => \FormBuilder::Stat,
					"value" => "Name",
				],[
					"type" => \FormBuilder::Stat,
					"value" => "URL",
				],[
					"type" => \FormBuilder::Stat,
					"value" => "Interval",
				],[
					"type" => \FormBuilder::Stat,
					"value" => "Dead Alert",
				],[
					"type" => \FormBuilder::Stat,
					"value" => "Active",
				],[
					"type" => \FormBuilder::Stat,
					"value" => "",
				],[
					"type" => \FormBuilder::Stat,
					"value" => "Test Results",
				]
			]
		]);


		foreach(\Webhook\Handler::getAll() as $hook){
			$this->addField([
				"name" => "row",
				"type" => \FormBuilder::Horizontal,
				"fields" => [
				[
					"name" => "name[{$hook->id()}]",
					"type" => \FormBuilder::Text,
					"value" => $hook->name(),
					"placeholder" => "Name",
					'validation' => [
						"required" => true,
            			'maxlength' => 30,
					]
				],[
					"name" => "wurl[{$hook->id()}]",
					"type" => \FormBuilder::Text,
					"value" => $hook->URL(),
					"placeholder" => "URL",
					'validation' => [
						"required" => true,
						"url" => true,
            			'maxlength' => 200,
					]
				],[
					"name" => "interval[{$hook->id()}]",
					"type" => \FormBuilder::Select,
					"options" => \Webhook\Interval::class,
					"value" => $hook->interval(),
					'validation' => [
			            'required' => true,
			            'enum' => \Webhook\Interval::class,
			        ]
				],[
					"name" => "dedAlert[{$hook->id()}]",
					"type" => \FormBuilder::Checkmark,
					"value" => $hook->dedAlert(),
					'validation' => [
			            'required' => true,
			            'bool' => true,
			        ]
				],[
					"name" => "active[{$hook->id()}]",
					"type" => \FormBuilder::Checkmark,
					"value" => $hook->active(),
					'validation' => [
			            'required' => true,
			            'bool' => true,
			        ]
				],[
					"name" => "submits",
					"type" => \FormBuilder::Multi,
					"fields" => [[[
						"name" => "update-submit[{$hook->id()}]",
						"type" => \FormBuilder::Submit,
						"value" => "Modify"	
					],[
						"name" => "test-submit[{$hook->id()}]",
						"type" => \FormBuilder::Submit,
						"value" => "Test",
						"ignoreValidation" => true,
					],[
						"name" => "delete-submit[{$hook->id()}]",
						"type" => \FormBuilder::Submit,
						"value" => "Delete",
						"ignoreValidation" => true,
					]]],
				],[
					"type" => \FormBuilder::Stat,
					"value" => (array_key_exists($hook->id(), $testResults) ? $testResults[$hook->id()] : ""),
				]
			]
		]);
		}

		$this->addField([
			"name" => "row",
			"type" => \FormBuilder::Horizontal,
			"fields" => [
			[
				"name" => "new-name[]",
				"type" => \FormBuilder::Text,
				"placeholder" => "Name",
				'validation' => [
        			'maxlength' => 30,
        			'minlength' => 2,
				]
			],[
				"name" => "new-url[]",
				"type" => \FormBuilder::Text,
				"placeholder" => "URL",
				'validation' => [
        			'maxlength' => 200,
					"url" => true,
				]
			],[
				"name" => "new-interval[]",
				"type" => \FormBuilder::Select,
				"options" => \Webhook\Interval::class,
				'validation' => [
		            'enum' => \Webhook\Interval::class,
		        ]
			],[
				"name" => "new-dedAlert[]",
				"type" => \FormBuilder::Checkmark,
				'validation' => [
		            'bool' => true,
		        ]
			],[
				"name" => "new-active[]",
				"type" => \FormBuilder::Checkmark,
				'validation' => [
		            'bool' => true,
		        ]
			],[
				"name" => "new-submit",
				"type" => \FormBuilder::Submit,
				"value" => "Submit"	
			],[
				"type" => \FormBuilder::Stat,
				"value" => "",
			]
		]]);

	}
}

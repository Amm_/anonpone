<?php

class LoggerException extends Exception {
	function __construct($string) {
        parent::__construct($string);
    }
}

class ValidationException extends LoggerException {
	private $errorList = array();

	function __construct($string,$errors) {
        $this->errorList = $errors;
        parent::__construct($string);
    }
  	public function getMessages() {
	    return $this->errorList;
  	}
}

class DBException extends LoggerException {
	private $sqlError = array();

	function __construct($string,$sql) {
        $this->sqlError = $sql;
        parent::__construct($string);
    }
  	public function getSQLMessage() {
	    return $this->sqlError;
  	}
}
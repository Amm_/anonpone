<?php 
namespace Fanart;

class Fanart
{
	private $_db;
	private $_id,$_cid,$_pid,$_title,$_artist,$_lewd,$_priority,$_filename;

	public function __construct($db,$id = null) {
	    $this->_db = $db;

	    if($id && is_numeric($id)){
	    	$query = (new \QueryBuilder())->select("fanart")->where($id)->limit(1);
	    	if($this->_db->execute($query)->count()){
	    		$id = $this->_db->first();
	    	} else {
	    		throw new \LoggerException("No Fanart found with that ID");
	    	}

	    }
	    $this->assignArray($id);
	}
	public function assignArray($array): self{
   		if(!$array) return $this;
   		if(is_object($array)) $array = get_object_vars($array);
   		$this->assignFromArray("_cid",array("cyoa_id","_cid"),$array);
   		$this->assignFromArray("_pid",array("post_id","_pid"),$array);
   		if(!$this->_id) $this->assignFromArray("_id",array("id","_id"),$array);
   		$this->assignFromArray("_title",array("title","_title"),$array);
   		$this->assignFromArray("_artist",array("author","_artist"),$array);
   		$this->assignFromArray("_lewd",array("lewd","_lewd"),$array);
   		$this->assignFromArray("_priority",array("priority","_priority"),$array);
   		$this->assignFromArray("_filename",array("filename","_filename"),$array);

   		return $this;
   	}
   	private function assignFromArray($thisvariable,$potentialArray,$array): void{
   		foreach($potentialArray as $potential){
   			if(isset($array[$potential])) $this->{$thisvariable} = ((is_string($array[$potential])) ? trim($array[$potential]) : $array[$potential]);
   		}
   	}

	public function insert(): self {
		$data = [
			'cyoa_id' => $this->cid(),
			'post_id' => $this->pid(),
			'author' => $this->artist(),
			'title' => $this->title(),
			'priority' => 0,
			'lewd' => $this->lewd(),
			'filename' => $this->_filename,
		];

   		$validate = $this->validate($data);
		if(!$validate->fails()){
			$this->_filename = $this->saveImage(Sanitize::modifyURL($this->_filename));
			$data['filename'] = $this->_filename;

			try{
				if($this->pid()){
					$post = new \CYOA\Post($this->_db,$this->pid(),$this->cid());
				}
			} catch (\Exception $e){
				$this->pid = 0;
			}

			if($this->_filename){
				$query = (new \QueryBuilder())->select("fanart")->where(["cyoa_id","=",$this->cid(),"filename","=",$this->_filename]);
				if(!$this->_db->execute($query)->count()){
					$query = (new \QueryBuilder())->insert('fanart')->values($data);
					if($this->_db->execute($query)->error()){
						throw new \DBException("Fanart Insert Failed - Database {$this->title()}",$this->_db->error());
					} else {
						(new \CYOA\CYOA($this->_db,$this->cid()))->updateFanartStat();
					}
				} else {
					throw new \LoggerException("Duplicate Image Detected");
				}
			} else {
				throw new \LoggerException("Image Uploading Error");
			}
		} else {
			throw new \ValidationException("Fanart Insert Failed - {$this->title()} Validation", $validate->errors()->all('validation'));
		}
		return $this;
   	}
   	public function update(): self {
   		$data = [
			'cyoa_id' => $this->cid(),
			'post_id' => $this->pid(),
			'author' => $this->artist(),
			'title' => $this->title(),
			'priority' => $this->priority(),
			'lewd' => $this->lewd(),
			'filename' => $this->_filename,
		];
   		$validate = $this->validate($data);
		if(!$validate->fails()){
			$this->_filename = $this->saveImage($this->_filename);

			if($this->_filename){
				$data['filename'] = $this->_filename;
				$query = (new \QueryBuilder())->update('fanart')->set($data)->where($this->id());
				if($this->_db->execute($query)->error()) {
					throw new \DBException("Fanart Update Failed - Database {$this->title()}",$this->_db->error());
				}
			} else {
				throw new \LoggerException("Image Uploading Error");
			}
		} else {
			throw new \ValidationException("Fanart Update Failed - {$this->title()} Validation", $validate->errors()->all('validation'));
		}
		return $this;
   	}
   	public function delete(): void{
   		$query = (new \QueryBuilder())->delete("fanart")->where($this->id());
   		$this->_db->execute($query);
		if($this->_filename) \Image::delete($this->_filename);

		(new \CYOA\CYOA($this->_db,$this->cid()))->updateFanartStat();
   	}
   	private function saveImage(string $_filename): ?string{
   		$filename = \Image::save($_filename);
		if($filename && $_filename !== $filename){
			\Thumbnail::small(Config::get("folder/files") . \Image::filePath($filename));
			return $filename;
		} else if ($_filename === $filename){
			return $filename;
		}
		return null;
   	}

	public function id(): int { return $this->_id; }
   	public function cid(): int { return $this->_cid; }
   	public function pid(): int { return (($this->_pid) ? $this->_pid : 0); }
   	public function title(): string { return $this->_title; }
   	public function artist(): string { return (($this->_artist) ? $this->_artist : ""); }
   	public function priority(): int { return (($this->_priority) ? $this->_priority : 0); }
   	public function lewd(): bool { return $this->_lewd; }
   	public function imagePath(bool $internal = false): string {
		if($internal) return \Config::get("folder/files") . \Image::filePath($this->_filename);
		return \Config::get("website/image") . \Image::filePath($this->_filename);
	}
	public function image(bool $internal = false): string {
		$cookie = new \Cookie();
		$lewd = ($cookie->exists('lewd') && $cookie->get('lewd') == "true");
		if($this->lewd() && !$lewd) return (($internal) ? \Config::get("folder/base") . "anonpone/" : \Config::get("website/img") ) . "images/lewdv3.png";
		return $this->imagePath($internal);  
	}

	private function validate(iterable $data): \Validation{
		$validation = new \Validation(new \ErrorHandler());
		$validate = $validation->check($data, array(
			'cyoa_id' => array(
				'required' => true,
				'num' => true,
			),
			'post_id' => array(
				'num' => true,
			),
			'title' => array(
				'required' => true,
				'maxlength' => 128,
			),
			'author' => array(
				'maxlength' => 64,
			),
			'priority' => array(
				'required' => true,
				'num' => true,
			),
			'lewd' => array(
				'required' => true,
				'bool' => true,
			),
			'filename' => array(
				'required' => true,
				'extension' => array("jpg","jpeg","png","gif"),
			),

		));
		return $validate;
	}
}

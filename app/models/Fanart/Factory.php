<?php
namespace Fanart;

class Factory {
	private $_artists;
	private $_db;
	private $_cyoaFactory;

	public function __construct($db){
		$this->_db = $db;
	}

	public function getByPost(int $pid): ?Fanart {
		$query = (new \QueryBuilder())->select("fanart")->where(["post_id","=",$pid])->limit(1);
		if($this->_db->execute($query)->count()){
			return new Fanart($this->_db->first());
		}
		return null;
	}

	public function get(string $value = "%",$limit = false): iterable {
		$order = [];
		if(is_numeric($value)){
			$order[] = "priority";
			$order[] = "DESC";
		}
		$order[] = "id";
		$order[] = "DESC";

		$query = (new \QueryBuilder())->select("fanart")->where(["cyoa_id","LIKE",$value])->order($order)->limit($limit);
		if($this->_db->execute($query)->count()){
			$fanarts = [];
			foreach($this->_db->results() as $fanart){
				$fanarts[] = new Fanart($this->_db,$fanart);
			}
			return $fanarts;
		}
		return [];
	}

	public function getByArtist(string $value,$limit = false): iterable {
		$order = [];
		$order[] = "id";
		$order[] = "DESC";

		$query = (new \QueryBuilder())->select("fanart")->where(["author","LIKE",$value])->order($order)->limit($limit);
		if($this->_db->execute($query)->count()) {
			$fanarts = array();
			foreach($this->_db->results() as $fanart){
				$fanarts[] = new Fanart($this->_db,$fanart);
			}
			return $fanarts;
		}
		return [];
	}

	public function count(string $value = "%"): int {
		$query = (new \QueryBuilder())->select("fanart",["count(1) as count"])->where(["cyoa_id","=",$value]);
		if($this->_db->execute($query)->count()){
			return $this->_db->first()->count;
		}
		return 0;
	}

	public function countByArtist(string $value = "%"): int{
		$query = (new \QueryBuilder())->select("fanart",["count(1) as count"])->where(["author","LIKE",$value]);
		if($this->_db->execute($query)->count()){
			return $this->_db->first()->count;
		}
		return 0;
	}

	public function artists(): iterable {
		if(!$this->_artists){ 
			$query = (new \QueryBuilder())->select("fanart",["DISTINCT(author) as artist"]);
			$this->_artists = $this->_db->execute($query)->results();
		}
		return $this->_artists;
	}
}

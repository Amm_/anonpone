<?php
namespace CYOA;

abstract class Status extends \Enum {
	const Active = 1; 
	const Complete = 2;
	const Hiatus = 3;
	const Cancelled = 4;
	const Hidden = 5;
	const Disabled = 6;

	const colors = [
        1 => "blue",
        2 => "green",
        3 => "yellow",
        4 => "red",
        5 => "red",
        6 => "red",
  	];

      
	public static function color(int $value): string {
      	if (self::isValidValue($value, false) && array_key_exists($value,self::colors)) {
             return self::colors[$value];
        }
        return parent::toString($value);
	}
}

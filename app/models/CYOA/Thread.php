<?php
namespace CYOA;

class Thread extends Post{

	protected $_used,$_replyNum,$_subject,$_stats,$_page,$_channame,$_boardcode;
	protected $_posts = array();

	public function __construct(\DB $db,$value,int $cyoaid = null) {
		parent::__construct($db,$value,$cyoaid);
		if(is_numeric($value) || is_string($value)){
			$query = (new \QueryBuilder())->select("threads")->where(["thread_id","=",$value,"cyoa_id","=",$cyoaid])->limit(1);
			if(!$this->_db->execute($query)->count()){
				throw new \LoggerException("Invalid Thread ID or CYOA ID");
			}
			$post = $this->_db->first();
		}  else {
			$post = $value;
		}
		if($post) $this->assignArray($post);
	}

	public function assignArray($array){
   		if(!$array) return $this;
   		if(is_object($array)) $array = get_object_vars($array);
   		parent::assignArray($array);

   		if(isset($array["board"])){ 
	 		$bn = explode("/",$array["board"]);
		 	$this->_channame = $bn[0];
		 	$this->_boardcode = $bn[1];
	 	}
   		$this->assignFromArray("_boardcode",array("_boardcode"),$array);
   		$this->assignFromArray("_channame",array("_channame"),$array);
   		$this->assignFromArray("_page",array("_page","page"),$array);
   		$this->assignFromArray("_stats",array("_stats","stats"),$array);
   		if($this->_stats && !is_array($this->_stats)) $this->_stats = json_decode($this->_stats,true);
   		$this->assignFromArray("_subject",array("_subject","subject"),$array);
   		$this->assignFromArray("_replyNum",array("_replyNum","replyNum"),$array);
   		$this->assignFromArray("_used",array("_used","used"),$array);
   		if(isset($array["_posts"])){
   			foreach($array["_posts"] as $post){
   				$post = new Post($this->_db,$post);
   				$this->_posts[$post->pid()] = $post;
   			}
   		}

   		return $this;
	}

	public function insert(){
		$data = [
			'cyoa_id' => $this->cid(),
			'thread_id' => $this->tid(),
			'used' => $this->used(),
			'replyNum' => $this->replyCount(),
			'stats' => json_encode($this->_stats),
			'page' => $this->page(),
			'board' => $this->boardName(),
		];
   		$validate = $this->validate($data);
		if(!$validate->fails()){
			$query = (new \QueryBuilder())->insert('threads')->values($data);
			if($this->_db->execute($query)->error()){
				throw new \DBException("Thread Insert Error - Database {$this->cid()}-{$this->tid()}",$this->_db->error());
			} else {
				$this->markModified();
			}
		} else {
			throw new \ValidationException("Thread Insert Error - Validation {$this->cid()}-{$this->tid()}", $validateInsert->errors()->all('validation'));
		}
		return $this;
   	}
   	public function update(){
   		$data = [
			'cyoa_id' => $this->cid(),
			'thread_id' => $this->tid(),
			'used' => $this->used(),
			'replyNum' => $this->replyCount(),
			'stats' => json_encode($this->_stats),
			'page' => $this->page(),
			'board' => $this->boardName(),
			'subject' => $this->subject(),
		];
   		$validate = $this->validate($data);
		if(!$validate->fails()){
			$query = (new \QueryBuilder())->update('threads')->set($data)->where(["cyoa_id","=", $this->cid(), "thread_id","=", $this->tid()]);
			if($this->_db->execute($query)->error()){
				throw new \DBException("Thread Update Error - Database {$this->cid()}-{$this->tid()}",$this->_db->error());
			}
			$query = (new \QueryBuilder())->update('posts')->set(['lastmodified' => time()])->where(["cyoa_id","=", $this->cid(), "post_id","=", $this->pid()]);
			$this->_db->execute($query);
		} else {
			throw new \ValidationException("Thread Update Error - Validation {$this->cid()}-{$this->tid()}", $validateInsert->errors()->all('validation'));
		}
		return $this;
   	}
   	public function markModified(){
   		if(!$this->stat("modified")){
			$this->_stats["modified"] = 1;
			Handler::get($this->cid())->markModified();
			return @$this->update();
		}
		return true;
   	}

   	public function posts(int $post = null,bool $reset = false){
		if(!$this->_posts || $reset){
			$query = (new \QueryBuilder())->select("posts")->where(["thread_id","=",$this->tid(),"cyoa_id","=",$this->cid()])->order(["thread_id","ASC","date","ASC"]);
			if($this->_db->execute($query)->count()){
				$this->assignArray(["_posts" => $this->_db->results()]);
			} else {
				$this->_posts = true;
			}
		}
		if(!$post) return $this->_posts;
		if(is_array($this->_posts) && isset($this->_posts[$post])) return $this->_posts[$post];
		return null;
	}
	public function QMposts(): iterable {
		return ((!is_bool($this->posts())) ? array_filter($this->posts(),function($value){ return $value->isQM();}) : []);
	}
	public function nextPost($exPost): ?Post {
		if(is_numeric($exPost)) $exPost = new \Post($exPost,$this->cid());
		if(is_object($exPost)){
			foreach($this->QMposts() as $post){
				if($post->rawDate() > $exPost->rawDate()){ return $post; }
			}
		}
		return null;
	}
	public function prevPost($exPost): ?Post {
		if(is_numeric($exPost)) $exPost = new Post($exPost,$this->cid());
		if(is_object($exPost)){
			foreach(array_reverse($this->QMposts()) as $post){
				if($post->rawDate() < $exPost->rawDate()){ return $post; }
			}
		}
		return null;
	}
	public function firstPost(): ?Post { 
		$array = $this->QMposts();
		return reset($array); 
	}
	public function lastPost(): ?Post {	
		$array = $this->QMposts();
		return end($array);
	}
	public function board(): string { return $this->_boardcode; }
	public function chan(): string { return $this->_channame; }
	public function boardName(): string { return "{$this->_channame}/{$this->_boardcode}"; }
	public function used(): bool { return $this->_used; }
	public function replyCount(): ?int { return (int)$this->_replyNum; }
	public function subject(): ?string { return $this->_subject; }
	public function page(): ?int { return (($this->_page) ? $this->_page : 0); }
	public function stat($name = null){ return ((isset($name)) ? ((isset($this->_stats[$name])) ? $this->_stats[$name] : null) : $this->_stats); }
	public function toArray(): iterable{
		$array = parent::toArray();
		unset($array["_posts"]);
		unset($array["_comment"]);
		unset($array["_filename"]);
		unset($array["_filenameLewd"]);
		unset($array["_name"]);
		unset($array["_trip"]);
		unset($array["_lewd"]);
		unset($array["_replies"]);
		unset($array["_name"]);
		return $array;
	}

	private function validate($data): \Validation {
		$validation = new \Validation(new \ErrorHandler());
		$validate = $validation->check($data, array(
			'cyoa_id' => array(
				'required' => true,
				'maxlength' => 16,
				'minlength' => 1,
				'num' => true,
			),
			'thread_id' => array(
				'required' => true,
				'maxlength' => 16,
				'minlength' => 1,
				'num' => true,
			),
			'used' => array(
				'required' => true,
				'bool' => true,
			),
			'used' => array(
				'int' => true,
			),
			'page' => array(
				'int' => true,
			),
			'subject' => array(
				'maxlength' => 150,
			)
		));
		return $validate;
	}
	
	public function updateStats($manual = false){

    	$stats = $this->_stats;

      	if($manual || !is_array($stats) || !array_key_exists('modified',$stats) || $stats['modified'] == 1){
	      	$this->posts(null,true);
	      	$posts = $this->QMposts();
	      	$query = (new \QueryBuilder())->select("posts",["count(1) as count"])->where(["cyoa_id","=",$this->cid(),"post_id","<",$this->tid(),"isOP","=",1])
	        	->order(["post_id","ASC"]);
	        $postCount = $this->_db->execute($query)->first()->count;

	      	$threadTime = $this->firstPost()->rawDate() - $this->lastPost()->rawDate();
	      	$threadPosts = count($posts);
	      	$threadWords = 0;

	        $previous = "";
	        $current = "";
	        $count = 0;
	        $time = 0;
	        $sessions = 1;
	        $sessionBreak = 4*60*60;
	      	foreach($posts as $post){
	   			$threadWords += str_word_count(preg_replace("/>[0-9]{7,}/","",str_replace("&#039;","'",str_replace("&gt;",">",html_entity_decode(strip_tags(str_replace("<br>","\n",$post->comment())))))));

	            if(!$previous){
	               $previous = $post->rawDate();
	            }
	            $current = $post->rawDate();
	            $difference = $current - $previous;
	            if($difference < $sessionBreak && $difference > 5*60){
	               $count++;
	               $time += $current - $previous;
	            }
	            if($difference > $sessionBreak){
	               $sessions++;
	            }
	            $previous = $current;
	   		}
	        if($count == 0) $count = 1;

	   		$stats = array(
	   			'threadTime' => $threadTime,
	   			'threadPosts' => $threadPosts,
	   			'threadWords' => $threadWords,
	            'postCount' => $postCount,
	            'modified' => 0,
	            'sessionCount' => $sessions,
	            'sessionTime' => $time,
	            'sessionUpdates' => $count,
	   		);

	   		$this->_stats = $stats;
	   		//\FileServe::delete($this->cid(),$this->tid());
	   		try{
	   			@$this->update();
	   		} catch(\Exception $e){
	   			@$this->insert();
	   		}
	        
     	}
		return $stats;
	}
}
<?php
namespace CYOA;

class Factory {
	private $_cyoas = [];
	private $_db;

	public function __construct($db){
		$this->_db = $db;
	}

	public function set(bool $reset = false): void {
		$keys = [];
		$query = (new \QueryBuilder())->select("cyoa");
		foreach($this->_db->execute($query)->results() as $cyoa){
			$this->_cyoas[$cyoa->id] = new CYOA($this->_db,$cyoa);
		}
		$this->_cyoas = array_filter($this->_cyoas);
	}

	private function check(): bool {
		if(!sizeof($this->_cyoas)){
			$this->set();
			return true;
		}
		return false;
	}

	public function sort(int $sort = 1,iterable $cyoas = null): iterable {
		if(is_string($sort)) $sort = strtolower($sort);
		$this->check();

		if(!$cyoas){
			$cyoas = $this->get();
		}

		usort($cyoas, function($a, $b) use ($sort){
			switch($sort){
				case "latest":
				case 2:
					//Latest
					if(!$b->lastPost() || !$a->lastPost()) return -1;
					return ($b->lastPost()->rawDate() > $a->lastPost()->rawDate()) ? 1 : -1;
				case "alpha":
				case 3:
					//Alphabetic
					return strcmp($a->title(), $b->title());
				case "length (posts)":
				case 4:
					//Length (Posts)
					return $b->stat('totalPosts') <=> $a->stat('totalPosts');
				case "length (words)":
				case 5:
					//Length (Words)
					return $b->stat('totalWordCount') <=> $a->stat('totalWordCount');
				case "id":
				case 6:
					//ID
					return $b->cid() <=> $a->cid();
				case "newest":
				case 1:
				defaut:
					//Newest
					return ( ($b->firstPost() && $a->firstPost()) ? ( ($b->firstPost()->rawDate() > $a->firstPost()->rawDate()) ? 1 : -1) : -1);
			}
		});
		return $cyoas;
	}

	public function get($tags = [Status::Disabled]){
		$this->check();
		if(!is_array($tags)){
			return $this->getSingle($tags);
		}
		
		$cyoas = [];
		$cyoas = array_filter($this->_cyoas, function($value) use ($tags){ return !in_array($value->status(),$tags); });
		$cyoas = array_filter($cyoas);
		
		return $cyoas;
	}

	private function getSingle($identifier): ?CYOA{
		$cyoa = null;
		if(is_numeric($identifier) && array_key_exists($identifier, $this->_cyoas)){
			$cyoa = $this->_cyoas[$identifier];
		}else if(is_string($identifier)){
			$identifier = strtolower($identifier);
			foreach($this->_cyoas as $cyoat){
				if($cyoat->shortname() === $identifier){
					$cyoa = $cyoat;
					break;
				}
			}
		} else if(is_object($identifier)){
			$cyoa = $this->_cyoas[$identifier->cid()];
		}
		if($cyoa && $cyoa->status() === Status::Disabled){
			$cyoa = null;
		}
		return $cyoa;
	}
}

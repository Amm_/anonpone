<?php
namespace CYOA;

class Post {
	protected $_db;
	protected $_cid,$_pid,$_tid,$_name,$_trip,$_comment,$_QM,$_replies,$_lewd,$_canon,$_filenameLewd,$_date,$_filename,$_lastModified;

	function __construct(\DB $db,$value,int $cyoaid = null) {
		$this->_db = $db;

		if(is_numeric($value) || is_string($value)){
			$query = (new \QueryBuilder())->select("posts")->where(["post_id","=",$value,"cyoa_id","=",$cyoaid])->limit(1);
			if(!$this->_db->execute($query)->count()){
				throw new \LoggerException("Invalid Post ID or CYOA ID");
			}
			$post = $this->_db->first();
		}  else {
			$post = $value;
		}
		if($post) $this->assignArray($post);
   	}

   	public function assignArray($array){
   		if(!$array) return $this;
   		if(is_object($array)) $array = get_object_vars($array);
   		if(!$this->_cid) $this->assignFromArray("_cid",array("cyoa_id","_cid"),$array);
   		if(!$this->_pid) $this->assignFromArray("_pid",array("post_id","_pid"),$array);
   		if(!$this->_tid) $this->assignFromArray("_tid",array("thread_id","_tid"),$array);
   		$this->assignFromArray("_name",array("name","_name"),$array);
   		$this->assignFromArray("_trip",array("trip","_trip"),$array);
   		$this->assignFromArray("_comment",array("comment","_comment"),$array);
   		$this->assignFromArray("_QM",array("_QM","isOP"),$array);
   		// $this->assignFromArray("_replies",array("replies","_replies"),$array);
   		// if($this->_replies && !is_array($this->_replies)) $this->_replies = array_filter(array_unique(explode(",",$this->_replies)));
   		$this->assignFromArray("_lewd",array("lewd","_lewd"),$array);
   		$this->assignFromArray("_canon",array("canon","_canon"),$array);
   		$this->assignFromArray("_filenameLewd",array("image_lewd","_filenameLewd"),$array);
   		$this->assignFromArray("_filename",array("filename","_filename"),$array);
   		$this->assignFromArray("_date",array("date","_date"),$array);
   		$this->assignFromArray("_lastModified",array("_lastModified","lastmodified"),$array);

   		return $this;
   	}
   	protected function assignFromArray($thisvariable,$potentialArray,$array): void {
   		foreach($potentialArray as $potential){
   			if(isset($array[$potential])) $this->{$thisvariable} = $array[$potential];
   		}
   	}

   	public function insert() {
   		$data = [
			'cyoa_id' => $this->cid(),
			'post_id' => $this->pid(),
			'thread_id' => $this->tid(),
			'comment' => utf8_encode($this->comment()),
			'name' => $this->name(),
			'trip' => $this->trip(),
			//'replies' => implode(",",$this->replies()),
			'isOP' => $this->isQM(),
			'lewd' => $this->lewd(),
			'canon' => $this->canon(),
			'date' => $this->rawDate(),
			'lastmodified' => time(),
		];
   		$validate = $this->validate($data);
		if(!$validate->fails()){
			// if($this->_comment && !$this->replies() && preg_match_all("/&gt;&gt;([0-9]{6,8})/", $this->_comment, $output_array)){
			// 	$this->_replies = $output_array[1];
			// }
			$this->_filename = $this->saveImage($this->_filename);
			$this->_filenameLewd = $this->saveImage($this->_filenameLewd);
			if(strlen($this->_name) > 32) $this->_name = substr($this->_name,0,32);

			$data['image_lewd'] = $this->_filenameLewd;
			$data['filename'] = $this->_filename;

			if($this->_filenameLewd) $data['lewd'] = true;

			$query = (new \QueryBuilder())->insert('posts')->values($data);
			if($this->_db->execute($query)->error()){
				throw new \DBException("Post Insert Error - Database {$this->cid()}-{$this->tid()}-{$this->pid()}",$this->_db->error());
			} else {
				try{
					$thread = new Thread($this->tid(),$this->cid());
					$thread->markModified();
				} catch(Exception $e){
					Handler::get($this->cid())->markModified();
				}
			}
		} else {
			throw new \ValidationException("Post Insert Error - Validation {$this->cid()}-{$this->tid()}-{$this->pid()}", $validate->errors()->all('validation'));
		}
		return $this;
   	}
   	public function update() {
   		$data = [
			'cyoa_id' => $this->cid(),
			'post_id' => $this->pid(),
			'thread_id' => $this->tid(),
			'comment' => $this->comment(),
			'name' => $this->name(),
			'trip' => $this->trip(),
			//'replies' => implode(",",$this->replies()),
			'isOP' => $this->isQM(),
			'lewd' => $this->lewd(),
			'canon' => $this->canon(),
			'date' => $this->rawDate(),
			'lastmodified' => time(),
		];
   		$validate = $this->validate($data);
		if(!$validate->fails()){
			$this->_filename = $this->saveImage($this->_filename);
			$this->_filenameLewd = $this->saveImage($this->_filenameLewd);
			if($this->_filenameLewd === false) $this->_filenameLewd = null;
			if($this->_filename === false) $this->_filename = null;

			if($this->_filenameLewd) $data['lewd'] = true;

			$data['image_lewd'] = $this->_filenameLewd;
			$data['filename'] = $this->_filename;
			unset($data['cyoa_id']);
			unset($data['post_id']);
			unset($data['thread_id']);

			$query = (new \QueryBuilder())->update('posts')->set($data)->where(["cyoa_id","=", $this->cid(), "post_id","=", $this->pid(), "thread_id","=", $this->tid()]);
			if($this->_db->execute($query)->error()) {
				throw new \DBException("Post Update Error - Database {$this->cid()}-{$this->tid()}-{$this->pid()}",$this->_db->error());
			} else {
				$thread = new Thread($this->tid(),$this->cid());
				$thread->markModified();
				$thread->forget();
			}
		} else {
			throw new \ValidationException("Post Update Error - Validation {$this->cid()}-{$this->tid()}-{$this->pid()}", $validate->errors()->all('validation'));
		}
		return $this;
   	}
   	public function delete(bool $db = false): void {
   		if($db) {
   			$query = (new \QueryBuilder())->delete("posts")->where(["cyoa_id","=",$this->cid(),"thread_id","=",$this->tid(),"post_id","=",$this->pid()]);
   			$this->_db->execute($query);
   		}
		if($this->_filename) \Image::delete($this->_filename);
		if($this->_filenameLewd) \Image::delete($this->_filenameLewd);
   	}
   	private function saveImage($_filename){
   		$filename = \Image::save($_filename);
		if($filename && $_filename !== $filename){
			\Thumbnail::small(\Config::get("folder/files") . \Image::filePath($filename));
		}
		return $filename;
   	}

   	public function cid(): int { return $this->_cid; }
   	public function pid(): int { return $this->_pid; }
   	public function tid(): int { return $this->_tid; }
   	public function name(): string { return $this->_name; }
   	public function trip(): ?string { return $this->_trip; }
   	public function comment(): ?string { return $this->_comment; }
   	//public function replies(){ return ($this->_replies) ? array_filter($this->_replies) : array() ; }
   	public function replies(): iterable { preg_match_all('/&gt;&gt;([0-9]{4,15})/', $this->comment(), $output_array);
   		return array_unique($output_array[1]);
   	}
   	public function isQM(): bool { return (bool)$this->_QM; }
   	public function lewd(): bool { return (bool)$this->_lewd; }
   	public function canon(): bool { return (bool)$this->_canon; }
	public function lastModified(): int { return (int) $this->_lastModified; }
   	public function rawDate(): int { return (int)(($this->_date) ? $this->_date : 0); }
   	public function formatDate($type = "date"): string { 
   		switch(strtolower($type)){
   			case "full":
   				return date("Y-m-d h:i:s", $this->rawDate());
   			case "month":
   				return date("M d", $this->rawDate());
   			case "short":
   				return date("m/d/y", $this->rawDate());
   			case "fromnow":
   				$tsNow = new \DateTime("now");
   				$tsStart = (new \DateTime())->setTimestamp($this->rawDate());
   				$days = $tsNow->diff($tsStart)->format("%a");
   				if($tsNow->diff($tsStart)->format("%h") + ($days * 24) != 0){
					if($days > 2){
						return "-" . $days . "d";
					} else{
						return "-" . ($tsNow->diff($tsStart)->format("%h") + ($days * 24)) . "hr";
					}
				}
				return "";
   			case "date":
   			default:
   				return date("m/d/Y", $this->rawDate());
   		}
   	}
   	public function imagePath(bool $internal = false): ?string {
		if($internal) return ($this->_filename) ? \Config::get("folder/files") . \Image::filePath($this->_filename) : null;
		return ($this->_filename) ? \Config::get("website/image") . \Image::filePath($this->_filename) : null;
	}
	public function imagePathLewd(bool $internal = false): ?string {
		if($internal) return ($this->_filenameLewd) ? \Config::get("folder/files") . \Image::filePath($this->_filenameLewd) : null;
		return ($this->_filenameLewd) ? \Config::get("website/image") . \Image::filePath($this->_filenameLewd) : null;
	}
	public function image(bool $internal = false,bool $ignoreLewd = false): ?string {
		$cookie = new \Cookie();
		$lewd = (($cookie->exists('lewd') && $cookie->get('lewd') == "true") || $ignoreLewd);
		if(!$this->lewd()) return $this->imagePath($internal); 
		if(!$lewd && !$this->_filenameLewd) return (($internal) ? \Config::get("folder/base") . "anonpone/" : \Config::get("website/img") ) . "images/lewdv3.png";
		if(!$lewd && $this->_filenameLewd) return $this->imagePath($internal);
		if($lewd && !$this->_filenameLewd) return $this->imagePath($internal);
		if($lewd && $this->_filenameLewd) return $this->imagePathLewd($internal);
		return null;
		
	}
	public function toArray(): iterable{
		$array = get_object_vars($this);
		unset($array["_db"]);
		return $array;
	}

   	private function validate($data): \Validation {
		$validation = new \Validation(new \ErrorHandler());
		$validate = $validation->check($data, array(
			'cyoa_id' => array(
				'required' => true,
				'num' => true,
			),
			'post_id' => array(
				'required' => true,
				'num' => true,
			),
			'thread_id' => array(
				'required' => true,
				'num' => true,
			),
			'name' => array(
				'maxlength' => 100,
			),
			'trip' => array(
				'maxlength' => 16,
			),
			'isOP' => array(
				'required' => true,
				'bool' => true,
			),
			'lewd' => array(
				'required' => true,
				'bool' => true,
			),
			'canon' => array(
				'required' => true,
				'bool' => true,
			),
			'date' => array(
				'required' => true,
				'num' => true,
			)
		));
		return $validate;
	}
}

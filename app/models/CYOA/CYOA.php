<?php
namespace CYOA;

class CYOA {
	private $_db;

	private $_cid,$_title,$_shortname,$_description,$_tags,$_filename,$_type,$_boardcode,$_channame;
	private $_live,$_status,$_stats,$_lastModified,$_webhooks;

	private $_threads;
	private $_lastPost,$_firstPost;
	private $_filter;

	function __construct(\DB $db,$value = null) {
		$this->_db = $db;

		if(!is_null($value) && $value != ""){
			if(is_numeric($value)){
				$query = (new \QueryBuilder())->select("cyoa")->where($value)->limit(1);
				if(!$this->_db->execute($query)->count()){
					throw new \LoggerException("Invalid CYOA ID");
				}
				$value = $this->_db->first();
			} else if(is_string($value)){
				$query = (new \QueryBuilder())->select("cyoa")->where(["shortname","=",strtolower($value)])->limit(1);
				if(!$this->_db->execute($query)->count()){
					throw new \LoggerException("Invalid CYOA shortname");
				}
				$value = $this->_db->first();
			}

			$this->_cid = (int) $value->id;
			$this->_title = (string) $value->title;
			$this->_shortname = (string) $value->shortname;
			$this->_description = (string) $value->description;
			$this->_tags = explode(",",$value->tags);
			$this->_filename = (string) $value->thumbnail;
			$this->_type = (int) $value->type;
	 		$bn = explode("/",$value->boardname);
		 	$this->_channame = $bn[0];
		 	$this->_boardcode = $bn[1];

			$this->_live = (int) $value->live;
			$this->_status = (int) $value->status;
			$this->_stats = json_decode($value->stats,true);
			$this->_lastModified = (int) $value->statusmodified;
			$this->_webhooks = $value->webhooks;
		}
   	}

	public function update(): self {
		$data = [
			'title' => $this->title(),
			'description' => $this->description(),
			'shortname' => $this->shortname(),
			'type' => $this->type(),
			'tags' => implode(",",$this->tags()),
			'boardname' => $this->boardName(),
			'status' => $this->status(),
			'stats' => json_encode($this->_stats),
			'webhooks' => $this->webhooks(true),
		];
		$validate = $this->validate($data);
		if(!$validate->fails()){
			if($this->status() == Status::Complete){
				$this->setLive(false);
			}

			$filename = \Image::save($this->_filename);
			if($filename){
				\Thumbnail::medium(\Config::get("folder/files") . \Image::filePath($filename));
				\Thumbnail::small(\Config::get("folder/files") . \Image::filePath($filename));
				$this->_filename = $filename;
			}

			$data['thumbnail'] = (($this->_filename) ? $this->_filename : "");
			if($this->cid()){
				$query = (new \QueryBuilder())->update('cyoa')->set($data)->where($this->cid());
				if($this->_db->execute($query)->error()){
					throw new \DBException("CYOA Update Failed - Database {$this->title()}",$this->_db->error());
				}
			} else { 
				$validateInsert = $this->validateInsert($data);
				if(!$validateInsert->fails()) {
					$query = (new \QueryBuilder())->insert('cyoa')->values($data);
					if($this->_db->execute($query)->error()){
						throw new \DBException("CYOA Insert Failed - Database {$this->title()}",$this->_db->error());
					}
				} else {
					throw new \ValidationException("CYOA Insert Failed - {$this->title()} Validation", $validateInsert->errors()->all('validation'));
				}
			} 

			$query = (new \QueryBuilder())->select("cyoa")->where(["shortname","=",$this->shortname()]);
			if(!$this->_db->execute($query)->error()){
				return new CYOA($this->_db,$this->_db->first());
			}

		} else {
			throw new \ValidationException("CYOA Update Failed - {$this->title()} Validation", $validate->errors()->all('validation'));
		}
		return $this;
	}

	public function updateDetails($post): self {
		if(array_key_exists('search-id',$post)){
			for($count = 0; $count < count($post['search-id']); $count++){
				if(Search::isValidValue($post['search-type'][$count],false) && $post['search-term'][$count]){
					$query = (new \QueryBuilder())->update('search')->set([
						'type' => $post['search-type'][$count], 
						'cyoa_id' => $this->cid(), 
						'term' => $post['search-term'][$count],
						'catalog' => (int)$post['search-catalog'][$count]
					])->where($post['search-id'][$count]);
					$this->_db->execute($query);
				}
			}
			$this->_filter = null;
		}
		if(array_key_exists('new-search-term',$post)){
			for($count = 0; $count < count($post['new-search-term']); $count++){
				if(Search::isValidValue($post['new-search-type'][$count],false) && $post['new-search-term'][$count]){
					$query = (new \QueryBuilder())->insert('search')->values([
						'type' => $post['new-search-type'][$count], 
						'cyoa_id' => $this->cid(), 
						'term' => $post['new-search-term'][$count],
						'catalog' => (int)$post['new-search-catalog'][$count]
					]);
					$this->_db->execute($query);
				}
			}
			$this->_filter = null;
		}
		return $this;
	}

	public function markModified(): self {
   		if(!$this->stat("modified")){
			$this->_stats["modified"] = 1;
			return $this->update();
		}
		return $this;
	}

	public function threads($threadID = null,$reset = false) {
		if(!$this->_threads || $reset){
			$query = (new \QueryBuilder())->select('posts',['DISTINCT(thread_id) as thread_id'])->where(['cyoa_id','=',$this->_cid,'isOP','=',1])
				->order(['cyoa_id','ASC','thread_id','ASC']);
			
			$posts = [];
			$threads = [];
			foreach($this->_db->execute($query)->results() as $result){
				$posts[] = $result->thread_id;
			}
			$query = (new \QueryBuilder())->select("posts")->where(["cyoa_id","=",$this->cid(),"post_id","IN",$posts])
				->order(["cyoa_id","ASC","date","ASC","post_id","ASC"]);
			$threadsFromPosts = $this->_db->execute($query)->results();

			$query = (new \QueryBuilder())->select('threads')->where(["cyoa_id","=",$this->_cid]);
			$threadsFromThreads = [];

			foreach($this->_db->execute($query)->results() as $tThread){
				$threadsFromThreads[$tThread->thread_id] = $tThread;
			}

			foreach($threadsFromPosts as $value){
				if(empty((array) $threadsFromThreads[$value->thread_id])) {
					$query = (new \QueryBuilder())->insert("threads")->values([
						"cyoa_id" => $this->cid(),
						"thread_id" => $value->thread_id,
						"board" => $this->boardName(), 
						"used" => 1
					]);
					$this->_db->execute($query);
				}
				$threads[] = array_merge((array) $value, (array) $threadsFromThreads[$value->thread_id]);
			}
			if(!count($threads)) $threads = true;

			$this->_threads = $threads;
		}
		if(is_array($this->_threads) && is_array(end($this->_threads))){
			$tempThreads = $this->_threads;
			$this->_threads = [];
   			foreach($tempThreads as $tthread){
   				$tthread = (new Thread($this->_db,$tthread));
   				$this->_threads[$tthread->tid()] = $tthread;
   			}
		}
		if(!$threadID) return $this->_threads;
		if(isset($this->_threads[$threadID])) return $this->_threads[$threadID];
		return null;
	}
	public function imagePath($internal = false): string{
		if($internal) return ($this->_filename) ? \Config::get("folder/files") . \Image::filePath($this->_filename) : null;
		return ($this->_filename) ? \Config::get("website/image") . \Image::filePath($this->_filename) : null;
	}
	public function setFilename(string $filename): self {
		$this->_filename = $filename;
		return $this;
	}

	public function filterPost($post,$catalog = false): bool {
		if($this->type() !== Type::Image || array_key_exists('filename',$post) || array_key_exists('media',$post)){
			foreach($this->filter($catalog) as $filteritem){
				if($filteritem->validate($post)) return true;
			}
		}
		return false;
	}

	public function filter($catalog = false) {
		if(is_null($this->_filter)){
			$this->_filter = [];
			$query = (new \QueryBuilder())->select("search")->where(["cyoa_id","=",$this->cid()]);
			if($this->_db->execute($query)->count()){
				foreach($this->_db->results() as $result){
					switch($result->type){
						case Search::Name:
							$this->_filter[] = new \Search\Name($result);
							break;
						case Search::Size:
							$this->_filter[] = new \Search\Size($result);
							break;
						case Search::ID:
							$this->_filter[] = new \Search\ID($result);
							break;
						case Search::Filename:
							$this->_filter[] = new \Search\Filename($result);
							break;
						case Search::Subject:
							$this->_filter[] = new \Search\Subject($result);
							break;

					}
				}
			}
		}
		return array_filter($this->_filter,function($v) use ($catalog){ return (($catalog === $v->catalog()) ? true : false); });
	}
	public function lastPost(bool $reset = false): Post {
		$query = (new \QueryBuilder())->select("posts",["*"])->where(["cyoa_id","=",$this->cid(),"isOP","=",1])
			->order(["date","DESC"])->limit(1);
		if((!$this->_lastPost || $reset) && $this->_db->execute($query)->count()){
			$this->_lastPost = new Post($this->_db,$this->_db->first());
		} 
		return $this->_lastPost;
	}
	public function firstPost(bool $reset = false): Post {
		$query = (new \QueryBuilder())->select("posts")->where(["cyoa_id","=",$this->cid(),"isOP","=",1])
			->order(["date","ASC"])->limit(1);
		if((!$this->_firstPost || $reset) && $this->_db->execute($query)->count()){
			$this->_firstPost = new Post($this->_db,$this->_db->first());
		} 

		return $this->_firstPost;
	}
	public function lastThread(): Thread {
		$array = $this->threads();
		return end($array);
	}
	public function firstThread(): Thread {
		$array = $this->threads();
		return reset($array); 
	}
	public function nextThread($exPost) {
		if(is_numeric($exPost)) $exPost = new Thread($exPost,$this->cid());
		if(is_object($exPost)){
			foreach($this->threads() as $thread){
				if($thread->rawDate() > $exPost->rawDate()){ return $thread; }
			}
		}
		return null;
	}
	public function prevThread($exPost) {
		if(is_numeric($exPost)) $exPost = new Post($exPost,$this->cid());
		if(is_object($exPost)){
			foreach(array_reverse($this->threads()) as $thread){
				if($thread->rawDate() < $exPost->rawDate()){ return $thread; }
			}
		}
		return null;
	}
	public function postsLewd(): array{
		$lewd = [];
		$query = (new \QueryBuilder())->select("posts")->where(["cyoa_id","=",$this->cid(),"isOP","=",1,"lewd","=",1])
			->order(["post_id","ASC"]);
		if($this->_db->execute($query)->count()) {
			foreach( $this->_db->results() as $result){
				$lewd[] = new Post($this->_db,$result);
			}
		}
		return $lewd;
	}
	public function cid(): int { return $this->_cid; }
	public function title(): string { return $this->_title; }
	public function setTitle(string $title): self { $this->_title = $title; return $this; }
	public function shortname(): string { return $this->_shortname; }
	public function setShortname(string $shortname): self { $this->_shortname = $shortname; return $this; }
	public function description(): string { return $this->_description; }
	public function setDescription(string $description): self { $this->_description = $description; return $this; }
	public function tags($tag = null){ return (isset($tag) ? in_array($tag,$this->_tags) : $this->_tags); }
	public function setTags(string $tags): self { $this->_tags = explode(",",$tags); return $this; }
	public function type(): int { return $this->_type; }
	public function setType(int $type): self { 
		if(Type::isValidValue($type)) $this->_type = $type; 
		return $this; 
	}
	public function board(): string { return $this->_boardcode; }
	public function setBoard(string $boardcode): self { $this->_boardcode = $boardcode; return $this; }
	public function chan(): string { return $this->_channame; }
	public function setChan(string $channame): self { $this->_channame = $channame; return $this; }
	public function boardName(): string { return "{$this->_channame}/{$this->_boardcode}"; }
	public function status():int { return $this->_status; }
	public function setStatus(int $status): self { 
		if(Status::isValidValue($status)) $this->_status = $status; 
		return $this; 
	}
	public function stat($name = null){ return ((isset($name)) ? ((isset($this->_stats[$name])) ? $this->_stats[$name] : false) : $this->_stats); }
	public function page(): int {	
		$threads = $this->threads();
		$thread = end($threads);
		return $thread->page(); 
	}
	public function webhooks(bool $raw = false){	
		$webhookFactory = new \Webhook\Factory($this->_db);
		return ($raw) ? $this->_webhooks : $webhookFactory->get(explode(",",$this->_webhooks));
	}
	public function setWebhooks(string $webhooks): self { $this->_webhooks = $webhooks; return $this; }
	public function live(): bool { return $this->_live; }
	public function setLive(int $live): self { 
		$query = (new \QueryBuilder())->update("cyoa")->set(["live" => $live])->where($this->cid());
		if($this->_db->execute($query)->error()){
			throw new \DBException("CYOA Live Update Failed - Database {$this->title()}",$this->_db->error());
		} else {
			$this->_live = $live;
		}
		return $this;
	}
	public function lastModified(): int { return $this->_lastModified; }
	public function updateModified(): self { 
		$query = (new \QueryBuilder())->update("cyoa")->set(["statusmodified" => time()])->where($this->cid());
		if($this->_db->execute($query)->error()){
			throw new \DBException("CYOA Last Modified Update Failed - Database {$this->title()}",$this->_db->error());
		} else {
			$this->_lastModified = $lastModified;
		}
		return $this;
	}

	public function toArray(){
		$array = get_object_vars($this);
		unset($array["_db"]);
		unset($array["_threads"]);
		unset($array["_webhooks"]);
		$array["_lastPost"] = $this->lastPost()->toArray();
		$array["_firstPost"] = $this->firstPost()->toArray();
		return $array;
	}

	private function validateInsert($data){
		$validation = new \Validation(new \ErrorHandler());
		$validate = $validation->check($data, [
			'shortname' => [
				'required' => true,
				'unique' => ['cyoa','shortname'],
			],
			'thumbnail' => [
				'required' => true,
				'minlength' => 10,
				'maxlength' => 40
			]
		]);
		return $validate;
	}
	private function validate($data){
		$validation = new \Validation(new \ErrorHandler());
		$validate = $validation->check($data, [
			'title' => [
				'required' => true,
				'maxlength' => 52
			],
			'shortname' => [
				'required' => true,
				'maxlength' => 16,
			],
			'type' => [
				'required' => true,
				'enum' => Type::class,
			],
			'description' => [
				'required' => true,
				'minlength' => 10,
			],
			'status' => [
				'required' => true,
				'enum' => Status::class,
			]
		]);
		return $validate;
	}

	public function updateStats($manual = false): bool {
		$stats = $this->_stats;
		if(!$stats) $stats = [];
		if($this->threads(null,true)){
	        if($manual || !is_array($stats) || !isset($data['modified']) || $stats['modified'] == 1){
	   			$questTime = $this->lastPost(true)->rawDate() - $this->firstPost(true)->rawDate();
	   			$query = (new \QueryBuilder())->select("posts",["count(1) as count"])->where(["cyoa_id","=",$this->cid(),"isOP","=",1,"filename","!=",""]);
	   			$totalImages = $this->_db->execute($query)->first()->count;
	   			$query = (new \QueryBuilder())->select("posts",["count(1) as count"])->where(["cyoa_id","=",$this->cid(),"isOP","=",1]);
	   			$totalPosts = $this->_db->execute($query)->first()->count;

	   			$threads = $this->threads();

	   			$totalThreadTime = 0;
	   			$totalWordCount = 0;
	            $sessions = $time = $count = 0;
	   			foreach($threads as $thread){
	   				try{
	   					$result = (new Thread($this->_db,$thread->tid(),$thread->cid()))->updateStats();
	   				} catch (\Exception $e){
	   					continue;
	   				}
	   				$totalThreadTime += $result['threadTime'];
	   				$totalWordCount += $result['threadWords'];
	               	$sessions += $result['sessionCount'];
	               	$time += $result['sessionTime'];
	               	$count += $result['sessionUpdates'];
	   			}

	            $updateTime = round(($time/$count)/60);
	            $updatesPerSession = round($count/$sessions);
	            if($updatesPerSession < 2)
	            {
	               	$updateTime = 0;
	            }

	   			$stats = [
	   				'questTime' => $questTime,
	   				'totalImages' => intval($totalImages),
	   				'totalPosts' => intval($totalPosts),
	   				'totalThreads' => count($threads),
	   				'totalWordCount' => $totalWordCount,
	   				'totalThreadTime' => $totalThreadTime,
	               	'modified' => 0,
	               	'lewdExists' => $this->lewdExists(),
	               	'updatesPerSession' => $updatesPerSession,
	               	'updateTime' => $updateTime,
	               	'totalFanart' => (array_key_exists('totalFanart', $stats) ? $stats['totalFanart'] : 0),
	               	'firstPostDate' => $this->firstPost()->rawDate(),
	               	'lastPostDate' => $this->lastPost()->rawDate(),
	   			];

	            $this->_stats = $stats;
	            $this->threads(null,true);
		   		//\FileServe::delete($this->cid());
	            $this->update();
	            return true;
	        }
    	}
    	return false;
	}
	public function updateFanartStat(){
		$stats = $this->_stats;
		$stats['totalFanart'] = count(FanartHandler::get($this->cid()));
		$this->_stats = $stats;
		$this->update();
	}

	public function lewdExists(): bool {
		$query = (new \QueryBuilder())->select("posts")->where(["cyoa_id","=",$this->cid(),"isOP","=",1,"lewd","=",1])
      		->order(["cyoa_id","ASC","post_id","ASC","lewd","ASC"])->limit(1);
      	if($this->_db->execute($query)->count()) {
        	return true;
      	}
      	return false;
    }
}

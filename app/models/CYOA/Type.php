<?php
namespace CYOA;

abstract class Type extends \Enum {
	const Text = 1; 
	const Image = 2;
	const TextOnly = 3;
	const OCR = 4;
}

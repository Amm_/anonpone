<?php
namespace CYOA;

abstract class Search extends \Enum {
	const Name = 1; 
	const Size = 2;
	const ID = 3;
	const Filename = 4;
	const Subject = 5;
}
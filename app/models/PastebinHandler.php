<?php

class PastebinHandler {
	public static function getByPost($pid){
		$db = DB::getInstance();
		if($db->get("pastebin",array("post_id","=",$pid))->count()){
			return new Pastebin($db->first());
		}
		return false;
	}

	public static function get($value = "",$limit = false,$order1 = "added",$order2 = "DESC"){
		$value = (\CYOA\Handler::get($value)) ? \CYOA\Handler::get($value)->cid() : $value;
		$db = DB::getInstance();
		$order = array();

		if($order1 === "added" || $order1 === "created" || $order1 === "author"){
			$order[] = $order1;
			if($order2 === strtoupper("ASC") || $order2 === strtoupper("DESC")) $order[] = $order2;
			else $order[] = "DESC";
		} else {
			$order = array("added","DESC");
		}
		
		$where = array();
		if($value){
			$where = array("cyoa_id","=",$value);
		}
		
		if($db->get("pastebin",$where,$order,$limit)->count()){
			$pastebins = array();
			foreach($db->results() as $pastebin){
				$pastebins[] = new Pastebin($pastebin);
			}
			return $pastebins;
		}
		return array();
	}

	public static function count(){
		$db = DB::getInstance();
		if($db->select("COUNT(1) as count","pastebin")->count()){
			return $db->first()->count;
		}
		return 0;
	}
}
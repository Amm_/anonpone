<?php
namespace Search;

class Subject extends Generic {
	public function validate($value){
		$term = $this->term();
		if($term[0] !== "/"){ $term = "/" . $this->term() . "/i";}
		
		if(!empty($value['subject'])) {
			return preg_match($term,$value['subject']);
		}
		return false;
	}
}
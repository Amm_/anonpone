<?php
namespace Search;

class ID extends Generic {
	public function validate($value){
		$term = $this->term();
		if($term[0] !== "/"){ $term = "/" . $this->term() . "/";}

		if(!empty($value['id'])){
			return preg_match($term,$value['id']);
		} 
		return false;
		
	}
}

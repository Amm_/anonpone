<?php 
namespace Search;

class Size extends Generic{
	private $_height,$_width;

	function __construct($value) {
		parent::__construct($value);

		$result = explode("x",$value->term);
		$this->_width = $result[0];
		$this->_height = $result[1];
	}

	public function validate($post){
		if(!empty($post['height']) && !empty($post['width']) && $post['height'] == $this->_height && $post['width'] == $this->_width)	{
			return true;
		}
		return false;
	}
}
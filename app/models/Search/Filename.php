<?php
namespace Search;

class Filename extends Generic {
	public function validate($value){
		if(!empty($value['filename'])){
			return preg_match($this->term(),$value['filename']);
		}
		return false;
	}
}

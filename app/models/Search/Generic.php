<?php
namespace Search;

abstract class Generic {
	private $_cyoaID,$_type,$_id,$_catalog;
	private $_term;

	function __construct($value) {
		$this->_cyoaID = $value->cyoa_id;
		$this->_type = $value->type;
		$this->_term = $value->term;
		$this->_id = $value->id;
		$this->_catalog = $value->catalog;
	}

	abstract public function validate($value);

	public function id(){return (int)$this->_id;}
	public function cid(){return (int)$this->_cyoaID; }
	public function type(){return (int)$this->_type; }
	public function term(){return $this->_term; }
	public function catalog(){return (bool)$this->_catalog; }
}

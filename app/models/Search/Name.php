<?php 
namespace Search;

class Name extends Generic{
	private $_name,$_trip;

	function __construct($value) {
		parent::__construct($value);

		$result = explode("##",$value->term);
		if(count($result) == 2){
			$this->_name = $result[0];
			$this->_trip = $result[1];
		} else if($value->term[0] == "!"){
			$this->_trip = $value->term;
		} else {
			$this->_name = $value->term;
		}
	}

	public function validate($post){
		if($this->_name && $this->_trip){
			if(!empty($post['name']) && strtolower($this->_name) == strtolower(html_entity_decode($post['name'],ENT_QUOTES)) && !empty($post['trip']) && $this->_trip == $post['trip']){
				return true;
			}
		} else if($this->_trip){
			if(!empty($post['trip']) && $this->_trip === $post['trip']){
				return true;
			}
		} else if($this->_name){
			if(!empty($post['name']) && strtolower($this->_name) == strtolower(html_entity_decode($post['name'],ENT_QUOTES))){
				return true;
			}
		}
		return false;
	}
}
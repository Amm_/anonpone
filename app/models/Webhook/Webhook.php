<?php 
namespace Webhook;

class Webhook {
	private $_db;

	private $_id,$_name,$_url,$_interval,$_dedAlert,$_active;

	function __construct($db,$value) {
		$this->_db = $db;

		if(is_numeric($value)){
         $query = (new \QueryBuilder())->select("webhooks")->where(["id","=",$value]);
			if(!$this->_db->execute($query)->count()){
				throw new \LoggerException("Invalid webhook id");
			}
			$result =  $this->_db->first();
		} else {
			$result = $value;
		}
		$this->assignArray($result);
	}

	public function assignArray($array): self {
		if(!$array) return $this;
		if(is_object($array)) $array = get_object_vars($array);
		if(!$this->_id) $this->assignFromArray("_id",array("id","_id"),$array);
		$this->assignFromArray("_name",array("name","_name"),$array);
      $this->assignFromArray("_url",array("url","_url"),$array);
		$this->assignFromArray("_interval",array("intervval","interval","_interval"),$array);
      $this->assignFromArray("_dedAlert",array("dedalert","dedAlert","_dedAlert"),$array);
      $this->assignFromArray("_active",array("active","_active"),$array);
		return $this;
	}
	private function assignFromArray($thisvariable,$potentialArray,$array): void {
		foreach($potentialArray as $potential){
			if(isset($array[$potential])) $this->{$thisvariable} = ((is_string($array[$potential])) ? trim($array[$potential]) : $array[$potential]);
		}
	}

   public function insert(): self {
      $data = [
         'name' => $this->name(),
         'url' => $this->url(),
         'intervval' => $this->interval(),
         'dedAlert' => $this->dedAlert(),
         'active' => $this->active(),
      ];
      $validate = $this->validate($data);
      if(!$validate->fails()) { 
         $query = (new \QueryBuilder())->insert('webhooks')->values($data);
         if($this->_db->execute($query)->error()){
            throw new \DBException("Webhook Insert Failed - Database {$this->name()}",$this->_db->error());
         }
      } else {
         throw new \ValidationException("Webhook Insert Failed - {$this->name()} Validation", $validate->errors()->all('validation'));
      }
      return $this;
   }

   public function update(): self {
      $data = [
         'name' => $this->name(),
         'url' => $this->url(),
         'intervval' => $this->interval(),
         'dedAlert' => $this->dedAlert(),
         'active' => $this->active(),
      ];
      $validate = $this->validate($data);
      if(!$validate->fails()) {
         $query = (new \QueryBuilder())->update('webhooks')->set($data)->where(["id","=", $this->id()]);
         if($this->_db->execute($query)->error()) {
            throw new \DBException("Webhook Update Failed - Database {$this->name()}",$this->_db->error());
         }
      } else {
         throw new \ValidationException("Webhook Update Failed - {$this->name()} Validation", $validate->errors()->all('validation'));
      }
      return $this;
   }
   public function delete(){
      $query = (new \QueryBuilder())->delete("webhooks")->where(["id","=", $this->id()]);
      $this->_db->execute($query);
   }

   public function id(): int { return $this->_id;}
   public function name(): string { return $this->_name;}
   public function url(): string { return $this->_url;}
   public function interval(): int { return $this->_interval;}
   public function dedAlert(): bool { return $this->_dedAlert;}
   public function active(): bool { return $this->_active;}

   public function send(string $content, bool $override = false): ?array {
      if($this->active() || $override){
         $data = ["content" => $content];
         $data_string = json_encode($data);
         $opts = [
            'http' => [
               'method' => "POST",
               'header' => "Content-Type: application/json\r\n",
               'content' => $data_string,
            ]
         ];
         $context = stream_context_create($opts);
         @file_get_contents($this->url(), false, $context);
         return $http_response_header;
      }
   }

   private function validate($data): \Validation {
      $validation = new \Validation(new \ErrorHandler());
      $validate = $validation->check($data, array(
         'name' => array(
            'required' => true,
            'maxlength' => 30,
         ),
         'url' => array(
            'required' => true,
            'maxlength' => 200,
         ),
         'intervval' => array(
            'required' => true,
            'enum' => \Webhook\Interval::class,
         ),
         'dedAlert' => array(
            'required' => true,
            'bool' => true,
         ),
         'active' => array(
            'required' => true,
            'bool' => true,
         )
      ));
      return $validate;
   }
}
<?php
namespace Webhook;

abstract class Interval extends \Enum {
	const Thread = 1; 
	const Session = 2;
	const Post = 3;
	const None = 4;
}
<?php 
namespace Webhook;

class Factory {
	private $_hooks = [];
	private $_db;

	public function __construct(\DB $db){
		$this->_db = $db;
	}

	public function get($fid){
		if(is_array($fid)){
			$hooks = [];
			foreach($fid as $id){
				if($id) $hooks[$id] = $this->getSingle($id);
			}
			return $hooks;
		} else if($fid){
			return $this->getSingle($fid);
		}
		return [];
	}

	public function getAll(): array {
		$query = (new \QueryBuilder())->select("webhooks");

		if($this->_db->execute($query)->count()){
			$hooks = [];
			foreach($this->_db->results() as $item){
				$hooks[$item->id] = new \Webhook\Webhook($this->_db,$item);
			} 
			$this->_hooks = $hooks;
			return $hooks;
		}
		return [];
	}

	private function getSingle(int $id): Webhook {
		if(array_key_exists($id, $this->_hooks)){
			return $this->_hooks[$id];
		} else {
			$hook = new \Webhook\Webhook($this->_db,$id);
			$this->_hooks[$hook->id()] = $hook;
			return $hook;
		}
	}
}
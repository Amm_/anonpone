<?php
namespace Archive;

class MlpolArchive extends Infinity{
	public static function getURL($cyoa,$option,$thread = 0){
		switch($option){
			case self::ThreadURL:
				$url = "http://mlpol.net/:BOARD:/archive/:THREAD:.json";
				break;
			case self::ImageURL:
				$url = "https://mlpol.net/images/src/";
				break;
			case self::LinkURL:
				$url = "http://mlpol.net/:BOARD:/archive/:THREAD:";
				break;
			case self::CatalogURL:
				$url = "http://mlpol.net/:BOARD:/archive/catalog.json";
				break;
			default:
				throw new Exception("Invalid URL Enum");
		}
		$url = str_replace(":BOARD:",$cyoa->board(),$url);
		$url = str_replace(":THREAD:",$thread,$url);
		return $url;
	}
}

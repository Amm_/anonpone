<?php
namespace Archive;

class Factory{
	private $_live = [];
	private $_archive = [];
	private $_db;

	public function __construct(\DB $db){
		$this->_db = $db;
	}

	public function get(\CYOA\CYOA $cyoa): ?Archive {
      	if($cyoa){
	   		switch($cyoa->chan()){
	   			case '4chan':
	   				if(!array_key_exists('4chan',$this->_live)) $this->_live['4chan'] = new FourChan($this->_db);
	   				return $this->_live['4chan'];
	   			case 'mlpol':
	   				if(!array_key_exists('mlpol',$this->_live)) $this->_live['mlpol'] = new Infinity($this->_db);
	   				return $this->_live['mlpol'];
	   		}
      	}
		return null;
   }

	public function getArchive(\CYOA\CYOA $cyoa): ?Archive {
		switch($cyoa->boardName()){
			case '4chan/mlp':
			case '4chan/trash':
				if(!array_key_exists('4chan/mlp',$this->_archive)) $this->_archive['4chan/mlp'] = new FoolFukka($this->_db);
				return $this->_archive['4chan/mlp'];
			case 'mlpol/mlpol':
				if(!array_key_exists('mlpol/mlpol',$this->_archive)) $this->_archive['mlpol/mlpol'] = new MlpolArchive($this->_db);
				return $this->_archive['mlpol/mlpol'];
		}
		return null;
   }
}

<?php
namespace Archive;

class FoolFukka extends Archive{

	const archiveList = [
		'4chan/mlp' => "http://desuarchive.org/",
		'4chan/trash' => "http://desuarchive.org/",
	];

	public static function getURL($cyoa,$option,$thread = 0){
		switch($option){
			case self::ThreadURL:
				$url = "_/api/chan/thread/?board=:BOARD:&num=:POSTID:";
				break;
			case self::PostURL:
				$url = "_/api/chan/post/?board=:BOARD:&num=:POSTID:";
				break;
			case self::LinkURL:
				$url = ":BOARD:/post/:POSTID:";
				break;
			case self::SearchURL:
				$url = "_/api/chan/search/?board=:BOARD:&order=asc&type=op&ghost=none&deleted=not-deleted&subject=:SEARCH:";
				break;
			case self::ImageURL:
				return "";
			default:
				throw new Exception("Invalid URL Enum");
		}
		
		$url = self::archiveList[$cyoa->boardName()] . $url;
		$url = str_replace(":BOARD:",$cyoa->board(),$url);
		$url = str_replace(":POSTID:",$thread,$url);
		return $url;
	}

	public function process($cyoa,$manual = false){
		$response = [];

		if($cyoa->status() !== \CYOA\Status::Complete && $cyoa->status() !== \CYOA\Status::Disabled && $manual){
			$catalog = $this->getCatalog($cyoa,str_replace(":SEARCH:",$manual,static::getURL($cyoa,self::SearchURL)));
			$threads = $this->processThreads($catalog);
			$threads = $this->filterThreads($cyoa,$threads);
			if($threads){
				foreach($threads as $thread){
					$this->newReplies($cyoa,$thread);
					try{
						$this->parseThread($cyoa,$thread['pid'],true);
						$response[] = "{$thread['pid']} - New Posts Added";
					} catch (\Exception $e){
						$response[] = $e->getMessage();
					}
				}
				$cyoa->updateStats();
				return $response;
			} else {
				throw new \Exception("Archive Search - No Valid Threads Found");
			}
		}
		return false;
	}
   	public function processThreads($catalog){
   		if(array_key_exists('error',$catalog)) throw new \Exception("Archive Search - No Search Results Found");
   		$catalog = $catalog[0]['posts'];
   		$threads = [];

   		foreach($catalog as $thread){
   			$threads[] = $this->processThread($thread);
   		}
   		return $threads;
   	}
   	public function processPosts($posts){
   		$procPosts = [];
   		foreach($posts as $post){
			$procPosts[] = $this->processPost($post);
		}
		return $procPosts;
   	}
   	public function processThread($thread){
   		if(array_key_exists('error',$thread)) throw new \Exception("Archive Search - Invalid Thread");
   		if(!isset($thread['doc_id'])){
   			$thread = end($thread);
   			$tempThread['posts'] = [$thread['op']];
   			$tempThread['posts'] += $thread['posts'];
   			$thread = $tempThread;
   		}

   		$procThread = $this->processPost($thread['posts'][0]);
		$procThread['subject'] = (isset($thread['title'])) ? $thread['title'] : "";
		$procThread['page'] = 0;
		$procThread['replyNum'] = 0;  
		$procThread['posts'] = (isset($thread['posts'])) ? $this->processPosts($thread['posts']) : [];  
   		return $procThread;
   	}
   	public function processPost($post){
   		$procPost = [];
		$procPost['pid'] = (isset($post['num'])) ? $post['num'] : 0;
		$procPost['id'] = (isset($post['id'])) ? $post['id'] : 0;
		$procPost['time'] = (isset($post['timestamp'])) ? $post['timestamp'] : 0;
		$procPost['name'] = (isset($post['name'])) ? $post['name'] : "";
		$procPost['trip'] = (isset($post['trip'])) ? $post['trip'] : "";
		$procPost['comment'] = (isset($post['comment_processed'])) ? $post['comment_processed'] : "";

		$procPost['width'] = 0;
		$procPost['height'] = 0;
		$procPost['filename'] = "";
		$procPost['imageLink'] = "";
		if(isset($post['media'])){
			$procPost['width'] = $post['media']['media_w'];
			$procPost['height'] = $post['media']['media_h'];
			$procPost['filename'] = $post['media']['media_filename'];
			$procPost['imageLink'] = $post['media']['media_link'];
		}
		return $procPost;
   	}
}

<?php
namespace Archive;

abstract class Archive{
	private $_db;
	private $_catalog;
	private $_threads = [];
	private $_filters = [];

	private $_clean = [
		"“" => "\"",
		"”" => "\"",
		"‘" => "'",
		"’" => "'",
	];

	const ThreadURL = 1;
	const ImageURL = 2;
	const LinkURL = 3;
	const CatalogURL = 4;
	const PostURL = 5;
	const SearchURL = 6;

	public function __construct(\DB $db) {
       	$this->_db = $db;
   	}

   	public function process(\CYOA\CYOA $cyoa,$manual = false){
   		$response = [];

		if($cyoa->status() !== \CYOA\Status::Complete && $cyoa->status() !== \CYOA\Status::Disabled){
			$catalog = $this->getCatalog($cyoa,static::getURL($cyoa,self::CatalogURL));
			$threads = $this->processThreads($catalog);
			$threads = $this->filterThreads($cyoa,$threads);
			
			if(count($threads) > 0)	{
				$live = false;
				foreach($threads as $thread){
					$parseThread = false;
					$newReplies = $this->newReplies($cyoa,$thread);
					if($newReplies > 4 || $manual || !array_key_exists('lastReplies',$thread)){ 
						$parseThread = true;
					} else {
						foreach(array_slice($thread['lastReplies'],-$newReplies) as $post){
							if($cyoa->filterPost($post)){
								$parseThread = true;
								break;
							}
						}
					}
					try{
						if($parseThread) $this->parseThread($cyoa,$thread['pid'],$manual);
						$response[] = "{$thread['pid']} - New Posts Added";
					} catch (\ValidationException $e){
						$response[] = $e->getMessage();
						$response = array_merge($response,$e->getMessages());
					} catch (\Exception $e){
						$response[] = $e->getMessage();
					}

					$query = (new \QueryBuilder())->select("threads")->where(["thread_id","=",$thread['pid'],"cyoa_id","=",$cyoa->cid()]);
					if(!$live && $this->_db->execute($query)->first()){
						if($this->_db->first()->used){
							$live = true;
						}
					}
				}
				$this->changeLiveState($cyoa,$live);
				$cyoa->updateStats();
			} else {
	   			$this->changeLiveState($cyoa,false);
	   		}
	   		$this->updateStatus($cyoa);
			return $response;
		}
		return false;
   	}

   	abstract public function processThreads($threads);
   	abstract public function processThread($thread);
   	abstract public function processPosts($posts);
   	abstract public function processPost($post);
   	protected function cleanComment(string $string):string {
   		foreach($this->_clean as $key => $value){
   			$string = str_replace($key,$value,$string);
   		}
   		return $string;
   	}

   	protected function getCatalog(\CYOA\CYOA $cyoa,$url){
   		if(!empty($this->_catalogs[$cyoa->boardName()])){
			$catalog = $this->_catalogs[$cyoa->boardName()];
		} else {
			$catalog = $this->getPage($url);
			if($catalog){
				$this->_catalogs[$cyoa->boardName()] = $catalog;
			} else {
				throw new \LoggerException("{$cyoa->title()} - Catalog Retrieving Error");
			}
		}
		if($catalog){
			return $catalog;
		}
		throw new \LoggerException("{$cyoa->title()} - Catalog Retrieving Error");
   	}

   	protected function getPage($url){
   		if(\Outbound::isJSON($url)){
			return json_decode(file_get_contents($url),true);
		}
		return false;
   	}

   	protected function filterThreads(\CYOA\CYOA $cyoa,$threads): array {
   		$searchedThreads = [];

   		foreach($threads as $thread){
			if(	$cyoa->filterPost($thread,true) || $cyoa->threads($thread['pid'])){
				$searchedThreads[] = $thread;
			}
   		}
   		return $searchedThreads;
   	}

	protected function updateStatus(\CYOA\CYOA &$cyoa): void {
   		if($cyoa->status() !== \CYOA\Status::Complete && $cyoa->status() !== \CYOA\Status::Disabled){
   			$post = $cyoa->lastPost(true);

   			if($post && abs(time() - $post->rawDate()) > 30*24*60*60){
				if($cyoa->status() !== \CYOA\Status::Hiatus && $cyoa->status() !== \CYOA\Status::Cancelled && $cyoa->status() !== \CYOA\Status::Hidden && !in_array("non-cyoa",$cyoa->tags())){
   					$cyoa->assignArray(["_status" => \CYOA\Status::Hiatus])->update();

   					$string = "**{$cyoa->title()}** has gone into hiatus";
   					foreach($cyoa->webhooks() as $webhook){
						$webhook->send($string);
					}
   				}
   			} else {
   				if($cyoa->status() !== \CYOA\Status::Active && $cyoa->lastModified() < $post->rawDate()){
   					$cyoa->assignArray(["_status" => \CYOA\Status::Active])->update();
   				}
   			}
   		}
   	}
	protected function changeLiveState(\CYOA\CYOA &$cyoa, bool $value): void {
   		if($value == true && $cyoa->live() != 1){
   			$cyoa->assignArray(["_live" => 1])->update();
   		} else if ($value == false && $cyoa->live() != 0) {
   			$cyoa->assignArray(["_live" => 0])->update();
   		}
   	}

   	protected function newReplies(\CYOA\CYOA $cyoa,$chanThread): int {
   		$query = (new \QueryBuilder())->select("threads")->where(["cyoa_id","=",$cyoa->cid(),"thread_id","=",$chanThread['pid']]);
		if($this->_db->execute($query)->exists()){
			$archiveThread = $this->_db->first();
			$replies = $archiveThread->replyNum;
			if($chanThread['page'] - $archiveThread->page > 0 && $chanThread['page'] === 9 && (bool) $archiveThread->used){
				$boardURL = static::getURL($cyoa,self::LinkURL,$chanThread['pid']);
				$string = "**{$cyoa->title()}** has fallen to page 9: {$boardURL}"; 
				foreach($cyoa->webhooks() as $webhook){
					if($webhook->dedAlert()) $webhook->send($string);
				}
			}

   			$query = (new \QueryBuilder())->update("threads")->where(["cyoa_id","=",$cyoa->cid(),"thread_id","=",$chanThread['pid']])->set(["replyNum" => $chanThread['replyNum'],"page" => $chanThread['page']]);
   			$this->_db->execute($query);

			if($replies < $chanThread['replyNum']){
				return $chanThread['replyNum'] - $replies;
			}
		} else {
			$data = [
				'_cid' => $cyoa->cid(),
				'_tid' => $chanThread['pid'],
				'used' => 0,
				'replyNum' => $chanThread['replyNum'],
				'subject' =>  $this->cleanComment($chanThread['subject']),
				'page' => $chanThread['page'],
				'board' => $cyoa->boardName()
			];
			(new \CYOA\Thread($this->_db, $data))->insert();
			return 1000;
		}
		return 0;
   	}

   	public function parseThread(\CYOA\CYOA $cyoa,int $threadID,$manual = 0): bool{
   		$used = 0;
		$newPosts = [];
		$sessionStart = 0;
		$lastSession = 0;

		if(!empty($this->_threads[$threadID]) && !$manual){
			$fullThread = $this->_threads[$threadID];
			$this->count[$threadID] .=  " & " . $cyoa->title();
		} else {
			$fullThread = $this->processThread($this->getPage(static::getURL($cyoa,self::ThreadURL,$threadID)));
			$this->_threads[$threadID] = $fullThread;
			$this->newReplies($cyoa,$fullThread);
			$this->count[$threadID] = $cyoa->title();
		}
		if($fullThread) {
			try{
				$archiveThread = new \CYOA\Thread($this->_db,$threadID,$cyoa->cid());
				if($archiveThread->lastPost()){
					$lastSession = $archiveThread->lastPost()->rawDate() ?: 0;
				}
			} catch (\Exception $e){
				$lastSession = 0;
				$archiveThread = false;
			}
			foreach($fullThread['posts'] as $chanPost){
				if(!$archiveThread || !$archiveThread->posts($chanPost["pid"])){
					$comment = "";
					if(!empty($chanPost['comment'])){
						$comment = $chanPost['comment'];
					}
					if($cyoa->filterPost($chanPost)){
						$used = 1;
						$isOP = 1;
						if($sessionStart == 0){
							$sessionStart = $chanPost['time'];
						}
					} else {
						$isOP = 0;
					}
					$newPost = [
						'post_id' => $chanPost['pid'],
						'date' => $chanPost['time'],
						'cyoa_id' => $cyoa->cid(),
						'thread_id' => $threadID,
				 		'lewd' => 0,
				 		'canon' => 1,
				 		'isOP' => $isOP,
				 		'comment' => $this->cleanComment($comment),
				 		'name' =>  $this->cleanComment($chanPost['name']),
				 		'trip' => $chanPost['trip'],
					];

					$ocrfallback = true;
					if($isOP && $cyoa->type() === \CYOA\Type::OCR){
						try{
							$newPost['comment'] = \OCR::getText(static::getURL($cyoa,self::ImageURL) . $chanPost['imageLink']);
							$ocrfallback = false;
						} catch(\Exception $e){

						}
					}
					if(!empty($chanPost['imageLink']) && $cyoa->type() !== \CYOA\Type::TextOnly && $ocrfallback){
						if( ($isOP || !in_array("general",$cyoa->tags()) && ($chanPost['width'] < 4000 && $chanPost['height'] < 4000) )){
							$newPost['filename'] = static::getURL($cyoa,self::ImageURL) . $chanPost['imageLink'];
						}
					}
					$newPosts[] = $newPost;
				}
			}

			if(($used || $manual == 1) && count($newPosts) > 0){
				$boardURL = static::getURL($cyoa,self::LinkURL,$threadID);

				$query = (new \QueryBuilder())->update("threads")->set(["cyoa_id","=", $cyoa->cid(),"thread_id","=", $threadID])->where(["used" => 1]);
				$this->_db->execute($query);
				foreach($newPosts as $post)	{
					$post = (new \CYOA\Post($post))->insert();
				}
				if($archiveThread) $archiveThread->markModified();
				$cyoa->markModified();

				if(PHP_SAPI === "cli"){
					if(!$archiveThread && !in_array("non-cyoa",$cyoa->tags()) && $cyoa->status() != \CYOA\Status::Disabled){
						$string = "A new thread of **{$cyoa->title()}** has started: {$boardURL}"; 
						foreach($cyoa->webhooks() as $webhook){
							$webhook->send($string);
						}
					} else if($used && !in_array("non-cyoa",$cyoa->tags()) && $cyoa->status() != \CYOA\Status::Disabled){
						$gap = $sessionStart - $lastSession;
						if($gap > 60*60*3){
							$hours = floor($gap/(60*60));
							$string = "A new (-{$hours}h) session of **{$cyoa->title()}** has started: {$boardURL}"; 
							foreach($cyoa->webhooks() as $webhook){
								if($webhook->interval() === \Webhook\Interval::Session) $webhook->send($string);
							}
						}
						$string = "A new post of **{$cyoa->title()}** has been posted: {$boardURL}"; 
						foreach($cyoa->webhooks() as $webhook){
							if($webhook->interval() === \Webhook\Interval::Post) $webhook->send($string);
						}
					}
				}

				return true;
			} else if(count($newPosts) < 1 && $manual){
				throw new \LoggerException("{$threadID} - No New Posts");
			}
		}else if($manual){
			throw new \LoggerException("{$threadID} - Live Thread Not Found");
		}
		return false;
   	}
}

<?php
namespace Archive;

class Infnity extends Archive{
	const $_chans = [
		'mlpol' => "http://mlpol.net/"
	];
	public static function getURL($cyoa,$option,$thread = 0){
		switch($option){
			case self::ThreadURL:
				$url = ":BOARD:/:THREAD:.json";
				break;
			case self::ImageURL:
				$url = "images/src/";
				break;
			case self::LinkURL:
				$url = ":BOARD:/:THREAD:";
				break;
			case self::CatalogURL:
				$url = ":BOARD:/catalog.json";
				break;
			default:
				throw new \Exception("Invalid URL Enum");
		}
		$url = self::$_chans[$cyoa->chan()] . $url;
		$url = str_replace(":BOARD:",$cyoa->board(),$url);
		$url = str_replace(":THREAD:",$thread,$url);
		return $url;
	}

   	public function processThreads($catalog){
   		$threads = [];
   		foreach($catalog as $thread){
   			$threads[] = $this->processThread($thread);
   		}
   		return $threads;
   	}
   	public function processPosts($posts){
   		$procPosts = [];
   		foreach($posts as $post){
			$procPosts[] += $this->processPost($post);
		}
		return $procPosts;
   	}
   	public function processThread($thread){
   		$procThread = [];
   		if(is_array($thread)) {
   			if(count($thread)){
   				$procThread['posts'] = $this->processPosts($thread);
   			}
   		} else {
   			$procThread['page'] = $thread['Page'];
			$procThread['subject'] = $thread['Subject'];
			$procThread['replyNum'] = $thread['ReplyCount'];
			$procThread['pid'] = $thread['ThreadId'];
			$procThread['filename'] = $thread['FileObject']['filename'];
			$procThread['width'] = $thread['FileObject']['Width'];
			$procThread['height'] = $thread['FileObject']['Height'];
   		}
   		return $procThread;
   	}

   	public function processPost($post){
   		$posts = [];
   		$procPost = [];
		$procPost['pid'] = $post['PostId'];
		$procPost['id'] = $post['PosterUUID'];
		$procPost['name'] = $post['Name'];
		$procPost['trip'] = $post['Tripcode'];
		$procPost['time'] = $post['Timestamp'];
		$procPost['comment'] = $post['Body'];

		$procPost['width'] = 0;
		$procPost['height'] = 0;
		$procPost['imageLink'] = "";
		$procPost['filename'] = "";

		if(count($post['FileList']) > 0){
			$file = array_unshift($post['FileList']);
			$procPost['width'] = $file['Width'];
			$procPost['height'] = $file['Height'];
			$procPost['imageLink'] = $file['FileHash'] . "." . $thread['FileObject']['Extension'];
			$procPost['filename'] = $file['Filename'];
			$posts[] = $procPost;
			if(count($post['FileList']) > 0){
				foreach($post['FileList'] as $file){
					$posts[] = [
						'pid' => ++$post['PostId'],
						'id' => $post['PosterUUID'],
						'name' => $post['Name'],
						'trip' => $post['Tripcode'],
						'time' => ++$post['Timestamp'],
						'comment' => "",
						'width' => $file['Width'],
						'height' => $file['Height'],
						'imageLink' => $file['FileHash'] . "." . $thread['FileObject']['Extension'],
						'filename' => $file['Filename'],
					];
				}
			}
		}
		return $posts;
   	}
}

<?php
namespace Archive;

class FourChan extends Archive{
	public static function getURL($cyoa,$option,$thread = 0){
		switch($option){
			case self::ThreadURL:
				$url = "http://a.4cdn.org/:BOARD:/thread/:THREAD:.json";
				break;
			case self::ImageURL:
				$url = "http://i.4cdn.org/:BOARD:/";
				break;
			case self::LinkURL:
				$url = "https://sys.4chan.org/:BOARD:/imgboard.php?res=:THREAD:";
				break;
			case self::CatalogURL:
				$url = "http://a.4cdn.org/:BOARD:/catalog.json";
				break;
			default:
				throw new \Exception("Invalid URL Enum");
		}
		$url = str_replace(":BOARD:",$cyoa->board(),$url);
		$url = str_replace(":THREAD:",$thread,$url);
		return $url;
	}

   	public function processThreads($catalog){
   		$threads = [];
   		foreach($catalog as $page){
   			$pageNum = $page['page'];
   			foreach($page['threads'] as $thread){
   				$thread['page'] = $pageNum;
   				$threads[] = $this->processThread($thread);
   			}
   		}
   		return $threads;
   	}
   	public function processPosts($posts){
   		$procPosts = [];
   		foreach($posts as $post){
			$procPosts[] = $this->processPost($post);
		}
		return $procPosts;
   	}
   	public function processThread($thread){
   		$procThread = $this->processPost($thread);

		$procThread['page'] = (isset($thread['page'])) ? $thread['page'] : 0;
		$procThread['subject'] = (isset($thread['sub'])) ? $thread['sub'] : "";
		$procThread['replyNum'] = (isset($thread['replies'])) ? $thread['replies'] : 0;
		$procThread['lastReplies'] = (isset($thread['last_replies'])) ? $this->processPosts($thread['last_replies']) : [];
		$procThread['posts'] = (isset($thread['posts'])) ? $this->processPosts($thread['posts']) : [];
   		return $procThread;
   	}
   	public function processPost($post){
   		$procPost = array();
		$procPost['pid'] = (isset($post['no'])) ? $post['no'] : 0;
		$procPost['id'] = (isset($post['id'])) ? $post['id'] : 0;
		$procPost['width'] = (isset($post['w'])) ? $post['w'] : 0;
		$procPost['height'] = (isset($post['h'])) ? $post['h'] : 0;
		$procPost['time'] = (isset($post['time'])) ? $post['time'] : 0;
		$procPost['imageLink'] = (isset($post['tim'])) ? $post['tim'] . $post['ext'] : "";
		$procPost['name'] = (isset($post['name'])) ? $post['name'] : "";
		$procPost['trip'] = (isset($post['trip'])) ? $post['trip'] : "";
		$procPost['filename'] = (isset($post['filename'])) ? $post['filename'] . $post['ext'] : "";
		$procPost['comment'] = (isset($post['com'])) ? $post['com'] : "";
		return $procPost;
   	}
}

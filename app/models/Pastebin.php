<?php

class Pastebin {

   public static function getID($string){
      $string = preg_replace("/pastebin/","",$string);
      $string = preg_match("/[0-9a-z]{8}/i",$string,$output);
      return $output[0];
   }
   private $_db;
   private $_memory;
   private static $apiMeta = "https://pastebin.com/api_scrape_item_meta.php?i=[ID]";
   private static $apiRaw = "https://pastebin.com/api_scrape_item.php?i=[ID]";

   private $_id,$_author,$_title,$_deleted,$_tags,$_description,$_cid,$_pid,$_size;
   private $_created,$_checked,$_added;
   private $_loaded;
   private $_revisions = array();

   private $_temptitle;

   public function __construct($value) {
      $this->_db = DB::getInstance();
      $this->_memory = Memory::getInstance();

      if(is_string($value)){
         if(!$this->_db->get("pastebin",array("id","=",$value))->count()){
            throw new Exception('Invalidpaste ID');
         }
         $value = $this->_db->first();
      }

      $this->assignArray($value);
   }

   public function assignArray($array){
      if(!$array) return;
      if(is_object($array)) $array = get_object_vars($array);
      if(!$this->_id) $this->assignFromArray("_id",array("id","_id"),$array);
      $this->assignFromArray("_cid",array("cyoa_id","_cid"),$array);
      $this->assignFromArray("_pid",array("post_id","_pid"),$array);
      $this->assignFromArray("_author",array("author","_author"),$array);
      $this->assignFromArray("_deleted",array("deleted","_deleted"),$array);
      $this->assignFromArray("_description",array("description","_description"),$array);
      $this->assignFromArray("_size",array("size","_size"),$array);
      $this->assignFromArray("_tags",array("tags","_tags"),$array);
      if($this->_tags && !is_array($this->_tags)) $this->_tags = explode(",",$this->_tags);
      $this->assignFromArray("_created",array("created","_created"),$array);
      $this->assignFromArray("_checked",array("checked","_checked"),$array);
      $this->assignFromArray("_added",array("added","_added"),$array);
      if($this->_id && !$this->_loaded){
         if($this->_db->get("pastebinRevisions",array("id","=",$this->id()),array("edited","DESC"))->count()){
            $this->_loaded = true;
            $this->_revisions = $this->_db->results();
         }
      }

      return $this;
   }
   protected function assignFromArray($thisvariable,$potentialArray,$array){
      foreach($potentialArray as $potential){
         if(isset($array[$potential])) $this->{$thisvariable} = $array[$potential];
      }
   }
   public function insert(&$errorhandler){
      if(!$errorhandler) $errorhandler = new ErrorHandler();
      if($this->exists($errorhandler, true)){
         $validate = $this->validate();
         if(!$validate->fails()){
            $data = array(
               'id' => $this->id(),
               'cyoa_id' => $this->cid(),
               'post_id' => $this->pid(),
               'author' => $this->author(),
               'description' => $this->description(),
               'deleted' => 0,
               'tags' => implode(",",$this->tags()),
               'size' => $this->size(),
               'created' => $this->created(),
               'checked' => $this->checked(),
               'added' => $this->added(),
            );

            if(!$this->_db->insert('pastebin',$data)){
               $errorhandler->addError("Pastebin Update error: {$this->_db->error()}", 'error');
            } else {
               $this->addRevision($errorhandler);
            }
         } else {
            $errorhandler->addErrors($validate->errors());
         }
      } else {
         $errorhandler->addError("Pastebin Upload error: Paste doesn't exist", 'error');
      }
      return $this;
   }
   public function update(&$errorhandler){
      $validate = $this->validate();
      if(!$validate->fails()){
         $data = array(
            'cyoa_id' => $this->cid(),
            'post_id' => $this->pid(),
            'author' => $this->author(),
            'description' => $this->description(),
            'deleted' => 0,
            'tags' => implode(",",$this->tags()),
            'size' => $this->size(),
            'created' => $this->created(),
            'checked' => $this->checked(),
            'added' => $this->added(),
         );

         if(!$this->_db->update('pastebin',array("id","=",$this->id()),$data)){
            $errorhandler->addError("Pastebin Update error: {$this->_db->error()}", 'error');
         }
      } else {
         $errorhandler->addErrors($validate->errors());
      }
      return $this;
   }

   public function exists(&$errorhandler,$manual = false){
      $url = str_replace("[ID]",$this->id(),static::$apiMeta);
      if((!$this->deleted() && $this->author() && time() - $this->checked() > 5*60*60 && time() - $this->edited() < 182*24*60*60) || $manual){
         $json = file_get_contents($url);
         if($json === "Error, we cannot find this paste."){
            $this->assignArray(array("_deleted" => 1,"_checked" => time()))->update($errorhandler);
         } else if(Outbound::getResponseCode($http_response_header) == 200) {
            $json = json_decode($json,true);
            if(!$this->_created) $this->_created = $json[0]["date"];
            if(!$this->_added) $this->_added = time();
            if(!$this->_author) $this->_author = $json[0]["user"];
            $this->_checked = time();

            if($this->size() != $json[0]["size"]){
               $this->_size = $json[0]["size"];
               $this->_tempTitle = $json[0]["title"];
               return true; 
            }
         }
      }
      return false;
   }

   public function addRevision(&$errorhandler){
      $now = time();
      $url = str_replace("[ID]",$this->id(),static::$apiRaw);
      $content = file_get_contents($url);

      if($content && Outbound::getResponseCode($http_response_header) == 200){
         $insert = array(
            "title"=> $this->_tempTitle ?: $this->title(),
            "edited" => $now,
            "id" => $this->id(),
            "content" => $content
         );
         $this->_revisions[] = $insert;

         $this->_db->insert("pastebinRevisions",$insert);
         
      }
      return $this;
   }

   public function id(){ return $this->_id; }
   public function cid(){ return $this->_cid; }
   public function pid(){ return $this->_pid; }
   public function author(){ return $this->_author; }
   public function deleted(){ return (($this->_deleted) ? true : false);; }
   public function description(){ return $this->_description; }
   public function tags(){ return $this->_tags; }
   public function size(){ return $this->_size; }
   public function created($option = ""){ return $this->formatDate($this->_created,$option); }
   public function checked($option = ""){ return $this->formatDate($this->_checked,$option); }
   public function added($option = ""){ return $this->formatDate($this->_added,$option); }

   public function title($revision = 0){ return (isset($this->_revisions[$revision])) ? $this->_revisions[$revision]->title : ""; }
   public function content($revision = 0){ return (isset($this->_revisions[$revision])) ? $this->_revisions[$revision]->content : ""; }
   public function printContent($revision = 0){
      $content = htmlspecialchars($this->content($revision));
         $content = preg_replace('/^(&gt;.*?)$/ism', '<div style="display:inline-block;" class="greentext">$1</div>', $content);
         @$content = preg_replace("#[[:alpha:]]+://[^<>[:space:]]+[[:alnum:]/]#","<a href=\"\\0\">\\0</a>", $content);
      return nl2br($content);
   }
   public function edited($option = "",$revision = 0){ return (isset($this->_revisions[$revision])) ? $this->formatDate($this->_revisions[$revision]->edited,$option) : ""; }

   private function formatDate($string,$option = ""){
      switch($option){
         case "short":
            return date("m/d/y", $string);
         default:
            return $string;
      }
   }

   private function validate(){
      $validation = new Validation(new ErrorHandler());
      $validate = $validation->check(get_object_vars($this), array(
         '_id' => array(
            'required' => true,
            'maxlength' => 16,
            'minlength' => 1,
         ),
      ));
      return $validate;
   }
}
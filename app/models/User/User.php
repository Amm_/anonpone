<?php
namespace User;

class User{
	private static $_currentUser;
	private $_db,
			$_cookieName,
			$_sessionName;

	private $_id,$_username,$_password,$_salt,$_name,$_joined,$_email,$_failedLogins;

	private $_isLoggedIn = false;
	private $_roles = [];

	public function __construct($db,$user = null){
		$this->_db = $db;
		$this->_sessionName = \Config::get('session/session_name');
		$this->_cookieName = \Config::get('remember/cookie_name');

		$session = new \Session();
		if(!$user){
			if($session->exists($this->_sessionName)){
				$user = $session->get($this->_sessionName);

				if($this->find($user)){
					$this->_isLoggedIn = true;
				} else {
					$this->_isLoggedIn = false;
					$this->logout();
				}
			}	
		} else {
			$this->find($user);
		}
	}

	public function find($user = null): bool {
		if($user){
			if(filter_var($user, FILTER_VALIDATE_EMAIL)){
				$field = "email";
			} else if(is_numeric($user)) {
				$field = "id";
			} else {
				$field = "username";
			}

			$query = (new \QueryBuilder())->select('user')->where([$field, '=', $user])->limit(1);

			if($this->_db->execute($query)->count()){
				$data = $this->_db->first();
				$this->_id = $data->id;
				$this->_username = $data->username;
				$this->_password = $data->password;
				$this->_salt = $data->salt;
				$this->_name = $data->name;
				$this->_joined = $data->joined;
				$this->_email = $data->email;
				$this->_failedLogins = $data->failedLogins;

				\Timing::mark();	
				$query = (new \QueryBuilder())->select("user_role as t1 JOIN user_roles as t2 ON t1.role_id = t2.role_id",
		       		["t1.role_id as role_id","t2.role_name as role_name"])
		       		->where(["t1.user_id","=",$data->id]);
		       	$this->_db->execute($query);
		       	\Timing::mark();	

		        foreach($this->_db->results() as $row) {
		            $this->roles[$row->role_name] = Role::getRolePerms($this->_db,$row->role_id);
		        }
				return true;
			}
		}
		return false;
	}

	public function update(array $fields = [], int $id = null): void {

		if(!$id && $this->isLoggedIn())	{
			$id = $this->id();
		}

		$query = (new \QueryBuilder())->update('user')->set($fields)->where($id);
		if(!$this->_db->execute($query)){
			throw new \Exception('There was a problem updating.');
		}
	}

	public function create(array $fields = []): void {
		$query = (new \QueryBuilder())->insert('user')->values($fields);
		if(!$this->_db->execute($query)){
			throw new \Exception('There was a problem creating an account.');
		}
	}

	public function isLoggedIn(): bool { return $this->_isLoggedIn; }
	public function id(): int { return $this->_id; }
	public function username(): string { return $this->_username; }
	public function password(): string { return $this->_password; }
	public function salt(): string { return $this->_salt; }
	public function joined(): int { return $this->_joined; }
	public function email(): string { return $this->_email; }
	public function name(): string { return $this->_name; }
	public function failedLogins(): int { return $this->_failedLogins; }
	public function setFailedLogins(int $num): void { 
		$this->update(["failedLogins" => $num]); 
		$this->_failedLogins = $num;
	}

	public function hasPrivilege(string $perm): bool {
        foreach ($this->roles as $role) {
            if ($role->hasPerm($perm)) {
                return true;
            }
        }
        return false;
    }
	public function hasRole(string $roleName): bool {
	    return isset($this->roles[$roleName]);
	}
	public function removeRoles(...$roles): void {
		$query = (new \QueryBuilder())->delete("user_role");
        foreach ($roles as $role_id) {
        	$query->where(["role_id","=",$role_id,"user_id","=",$this->id()]);
	   		$this->_db->execute($query);
	   	}
	}
	public function removeAllRoles(): void {
		$query = (new \QueryBuilder())->delete("user_role")->where(["user_id","=",$this->id()]);
        $this->_db->execute($query);
	}
    public function insertRoles(...$roles): bool {
    	$query = (new \QueryBuilder())->insert("user_role");
        foreach ($roles as $role_id) {
        	$query->values(["user_id"=>$this->id(),"role_id"=>$role_id]);
            $this->_db->execute($query);
        }
        return true;
    } 
    public function getRoles(): array {
    	return array_keys($this->roles);
    }
    public function ban(): bool {
        $this->removeAllRoles();
        return $this->insertRoles(6);
    }

	public function exists(): bool {
		if(!empty($this->_password)){
			return true;
		}
		return false;
	}
	public function getLast(): ?array {
		$query = (new \QueryBuilder())->select('user')->where(['joined','DESC'])->limit(1);
		return $this->_db->execute($query)->first();
	}

}

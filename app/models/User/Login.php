<?php
namespace User;

class Login {
	private $_user;
	private $_db,
			$_cookieName,
			$_sessionName,
			$_error;
	public const MaxFailedLogins = 5;
	public const ipApprovalTime = 60*60*24*30;
	private $_cookie;
	private $_hash;

	public function __construct($db, $user){
		$this->_db = $db;
		$this->_user = $user;
		$this->_cookie = new \Cookie();
		$this->_hash = new \Hash();

		$this->_sessionName = \Config::get('session/session_name');
		$this->_cookieName = \Config::get('remember/cookie_name');
	}
	
	public function getUser(): User {
		return $this->_user;
	}
	public function error(): string {
		return $this->_error;
	}

	public function validPassword($password): bool {
		if($this->_user->password() === $this->_hash->make($password, $this->_user->salt())){
			return true;
		}
		return false;
	}

	public function isGoodIP(): bool {
		$query = (new \QueryBuilder())->select("user_ips")->where([
			"user_id","=",$this->_user->id(),
			"ip_address","=",$_SERVER['REMOTE_ADDR']
		]);
		if($this->_db->execute($query)->count()) {
			if($this->_db->first()->approved >= time() - self::ipApprovalTime){
				$result = true;
				$query = (new \QueryBuilder())->update("user_ips")->set(["approved" => time()])->where([
					"user_id","=",$this->_user->id(),
					"ip_address","=",$_SERVER['REMOTE_ADDR']
				]);
				$this->_db->execute($query);
			} else {
				$result = false;
			}
		}
		else {
			$query = (new \QueryBuilder())->insert("user_ips")->values([
				"user_id" => $this->_user->id(),
				"ip_address" => $_SERVER['REMOTE_ADDR']
			]);
			$this->_db->execute($query);
			$result = false;
		}
		return $result;
	}
	public function addGoodIP(): void {
		$query = (new \QueryBuilder())->select("user_ips")->where([
			"user_id","=",$this->_user->id(),
			"ip_address","=",$_SERVER['REMOTE_ADDR']
		]);
		if(!$this->_db->execute($query)->count()) {
			$query = (new \QueryBuilder())->insert("user_ips")->values([
				"user_id" => $this->_user->id(),
				"ip_address" => $_SERVER['REMOTE_ADDR'], 
				"approved" => time()
			]);
			$this->_db->execute($query);
		} else {
			$query = (new \QueryBuilder())->update("user_ips")->set(["approved" => time()])->where([
				"user_id","=",$this->_user->id(),
				"ip_address","=",$_SERVER['REMOTE_ADDR']
			]);
			$this->_db->execute($query);
		}
	}
	
	public function login(string $username = null,string $password = null,bool $remember = true): bool {
		$session = new \Session();
		if(!$username && !$password && $this->_user->exists()){
			$session->put($this->_sessionName, $this->_user->id());
		} else {
			if($this->_user->find($username)){
				$validation = new Validation($this->_db,$this->_user);
				if(!$this->_user->hasPrivilege("banned")){
					if(!$this->_user->hasPrivilege("unvalidated")){
						if($this->_user->failedLogins() < self::MaxFailedLogins){
							if($this->validPassword($password)) {
								if($this->isGoodIP()){
									$session->put($this->_sessionName, $this->_user->id());
									$this->_user->setFailedLogins(0);

									if($remember){
										$hash = $this->_hash->unique();
										$query = (new \QueryBuilder())->select('user_sessions')->where(['user_id', '=', $this->_user->id()])->limit(1);
										$hashCheck = $this->_db->execute($query);

										if(!$hashCheck->count()){
											$query = (new \QueryBuilder())->insert('user_sessions')->values([
												'user_id' => $this->_user->id(),
												'hash' => $hash
											]);
											$this->_db->execute($query);
										} else {
											$hash = $hashCheck->first()->hash;
										}

										$this->_cookie->put($this->_cookieName, $hash, \Config::get('remember/cookie_expiry'));
									}
									return true;
								} else {
									if(!$validation->exists(\User\ValidationType::NewIP)) { $validation->send(\User\ValidationType::NewIP); }
									$this->_error = 'You are attempting to log in from a new IP, please check your email to confirm your IP address. Go <a href="/user/validate?type=' . \User\ValidationType::NewIP .'"">here</a> to resend the email';
								}
							} else {
								$this->_user->setFailedLogins($this->_user->failedLogins() + 1);
								$this->_error = "Invalid username/password combination";
							}
						} else { 
							if(!$validation->exists(\User\ValidationType::ForcedReset)) $validation->send(\User\ValidationType::ForcedReset);
							$this->_error = "You have entered too many invalid passwords, please check your email for a password reset link. Go <a href=\"/user/forgot\">here</a> to resend the email";
						}
					} else $this->_error = "This account needs to be validated, check your email for a validation link. Go <a href=\"/user/validate?type=" . \User\ValidationType::Creation . "\">here</a> to resend the email";
				} else $this->_error = "This account has been banned";
			} else $this->_error = "Username not found";
		}
		return false;
	}

	public function logout(): void{
		if($this->_user->id()) {
			$query = (new \QueryBuilder())->delete('user_sessions')->where(['user_id', '=', $this->_user->id()]);
			$this->_db->execute($query);
		}	

		$session = new \Session();
		$session->delete($this->_sessionName);
		$this->_cookie->delete($this->_cookieName);
	}
}

<?php
namespace User;

abstract class ValidationType extends \Enum {
	const Creation = 1; 
	const PasswordReset = 2;
	const NewIP = 3;
	const ForcedReset = 4;
}

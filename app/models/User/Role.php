<?php
namespace User;

class Role {
    public $permissions;
    private $_db;

    protected function __construct($db) {
        $this->permissions = [];
        $this->_db = $db;
    }

    // return a role object with associated permissions
    public static function getRolePerms($db,int $role_id): Role {
        $role = new Role($db);
        $query = (new \QueryBuilder())->select("user_role_perm as t1 JOIN user_permissions as t2 ON t1.perm_id = t2.perm_id",
            ["t2.perm_desc as perm_desc"])->where(["t1.role_id","=",$role_id]);

        foreach($db->execute($query)->results() as $row) {
            $role->permissions[$row->perm_desc] = true;
        }
        return $role;
    }

    // check if a permission is set
    public function hasPerm(string $permission): bool {
        return isset($this->permissions[$permission]);
    }

    public function insertRole($role_name): bool {
        $query = (new \QueryBuilder())->insert("user_roles")->values(["role_name"=>$role_name]);
        return $this->_db->execute($query);
    }

    // insert a new role permission association
    public function insertPerm(int $role_id, int $perm_id): bool {
        $query = (new \QueryBuilder())->insert("user_role_perm")->values(["role_id"=>$role_id,"perm_id"=>$perm_id]);
        return $this->_db->execute($query);
    }

    // delete array of roles, and all associations
    public function deleteRoles(...$roles): void {
        $query = (new \QueryBuilder())->delete("user_roles as t1 JOIN user_role as t2 on t1.role_id = t2.role_id JOIN user_role_perm as t3 on t1.role_id = t3.role_id");
        foreach($roles as $role_id){
            $query->where(["t1.role_id","=",$role_id]);
            $this->_db->execute($query);
        }
    }
   
}

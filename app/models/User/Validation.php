<?php
namespace User;

class Validation {
	private $_db, $_user;
	public const MaxApprovalTime = 60*15;
	//5 minutes

	public function __construct($db, $user){
		$this->_db = $db;
		$this->_user = $user;
	}

	public function exists(int $type): bool {
		if(ValidationType::isValidValue($type)){
			$query = (new \QueryBuilder())->select('user_validation')->where([
				'u_id',"=",$this->_user->id(),
				"type","=",$type
			]);
			return $this->_db->execute($query)->exists();
		}
		return false;
	}
	public function send(int $type): void {
		if(ValidationType::isValidValue($type)){
			$hashGenerator = new \Hash();
			$query = (new \QueryBuilder())->delete('user_validation')->where([
				'u_id','=',$this->_user->id(),
				'type','=',$type
			]);
			$this->_db->execute($query);
			$hash = $hashGenerator->unique();
			$query = (new \QueryBuilder())->insert('user_validation')->values([
				'u_id' => $this->_user->id(),
				'hash' => $hash,
				'dt' => time(),
				'type' => $type,
			]);
			$this->_db->execute($query);

		    $emailTitle = "/mlp/ CYOA - ";
			$emailBody = "";
			$toEmail = $this->_user->email();
			$fromEmail = "noreply@" . \Config::get('website/url');

			$headers = ["From: {$fromEmail}",
			    "Reply-To: {$toEmail}",
			    "X-Mailer: PHP/" . PHP_VERSION
			];
			$headers = implode("\r\n", $headers);

			switch($type){
				case ValidationType::Creation:
					$emailTitle .= "Welcome!";
					$emailBody = 'Hello! To finish joining our website we\'d like you to validate your account via this link: ' . 
					\Config::get('website/url') . "user/validate/{$hash}?type=" . ValidationType::Creation;
					break;
				case ValidationType::PasswordReset:
					$emailTitle .= "Password Reset";
					$emailBody = 'We recieved a request to reset the password on your account. If this was intended, please follow this link: '  . 
					\Config::get('website/url') . 'user/forgot/' . $hash;
					break;
				case ValidationType::NewIP: 
					$emailTitle .= "Confirm new IP";
					$emailBody = 'We recieved a login from a new IP on your account, if this is you then please follow this link: ' . 
					\Config::get('website/url') . "user/validate/{$hash}?type=" . ValidationType::NewIP;
					break;
				case ValidationType::ForcedReset: 
					$emailTitle .= "Failed Login";
					$emailBody = 'You account has been locked due to failed logins, please reactivate your account by following this link: ' . 
					\Config::get('website/url') . "user/validate/{$hash}?type=" . ValidationType::ForcedReset;
					break;
			}
			mail($toEmail,$emailTitle,$emailBody,$headers);
		} else {
			throw new \LoggerException("Invalid Validation Type");
		}
	}

	public function validate(string $hash, int $type): void {
		$query = (new \QueryBuilder())->select('user_validation')->where(['hash','=',$hash,'type',"=",$type])->limit(1);
		$userID = $this->_db->execute($query)->first();
		$session = new \Session();

		if($userID){
			$userID = $userID->u_id;
			$query = (new \QueryBuilder())->delete('user_validation')->where(['hash','=',$hash,'type',"=",$type]);
			$this->_db->execute($query);
			if(!$this->_db->error()){
				$user = new User($userID);
				switch($type){
					case \User\ValidationType::Creation:
						$user->removeRoles(5);
						$user->insertRoles(9);
						$session->flash('success', 'Your account has been validated, you can now log in!');
						break;
					case \User\ValidationType::NewIP:
						(new \User\Login($user))->addGoodIP();
						$session->flash('success', 'Your IP has been validated, you can now log in!');
						break;
					case \User\ValidationType::ForcedReset:
						$user->setFailedLogins(0);
						$session->flash('success', 'Your account has been unlocked, you can now log in!');
						break;
				}
			}
			\Redirect::to('/user/login');
		} else {
			$session->flash('error', 'Invalid Validation Code');
			\Redirect::to("/user/validate?type={$type}");
		}
	}

}
<?php 
namespace Report;

class Factory {
	private $_countAll = 0;
	private $_countNew = 0;
	private $_reportsAll = [];
	private $_reportsNew = [];

	private $_db;

	public function __construct($db){
		$this->_db = $db;
	}

	public function get($new = true){
		if($new){
			if($this->_reportsNew) return $this->_reportsNew;
			$query = (new \QueryBuilder())->select("reports")->where(["complete","=",0]);
			$this->_countNew = $this->_db->execute($query)->count();
		} else {
			if($this->_reportsAll) return $this->_reportsAll;
			$query = (new \QueryBuilder())->select("reports");
			$this->_db->execute($query);
			$this->_countAll = $this->_db->count();
		} 

		if($this->_db->count()){
			$reports = [];
			foreach($this->_db->results() as $item){
				$reports[] = new Report($this->_db,$item);
			}
			if($new) $this->_reportsNew = $reports;
			else $this->_reportsAll = $reports;
			return $reports;
		}

		return [];
	}

	public function count($new = true){
		$this->get($new);
		if($new) return$this->_countNew;
		else return $this->_countAll;
	}
}
<?php 
namespace Report;

class Report {
	private $_db;

	private $_id,$_cid,$_otherid,$_type,$_category,$_date,$_complete,$_comment;

	function __construct($db,$value) {
		$this->_db = $db;

		if(is_numeric($value)){
			$query = (new \QueryBuilder())->select("reports")->where($value);
			if($this->_db->execute($query)->error()){
				throw new \LoggerException("Invalid Report ID");
			}
			$result =  $this->_db->first();
		} else {
			$result = $value;
		}
		$this->assignArray($result);
	}

	public function assignArray($array): self {
   		if(!$array) return $this;
   		if(is_object($array)) $array = get_object_vars($array);
   		if(!$this->_id) $this->assignFromArray("_id",array("id","_id"),$array);
   		$this->assignFromArray("_cid",["cyoa_id","_cid"],$array);
   		$this->assignFromArray("_category",["category","_category"],$array);
   		$this->assignFromArray("_date",["date","_date"],$array);
   		$this->assignFromArray("_complete",["complete","_complete"],$array);
   		$this->assignFromArray("_comment",["comment","_comment"],$array);
   		$this->assignFromArray("_type",array("type","_type"),$array);
   		$this->assignFromArray("_otherid",array("other_id","_otherid"),$array);
   		return $this;
   	}
   	private function assignFromArray($thisvariable,$potentialArray,$array): void{
   		foreach($potentialArray as $potential){
   			if(isset($array[$potential])) $this->{$thisvariable} = ((is_string($array[$potential])) ? trim($array[$potential]) : $array[$potential]);
   		}
   	}

   	public function insert(): self {
   		$this->_comment = strip_tags($this->_comment);
		$data = array(
			'cyoa_id' => $this->cid(),
			'category' => $this->category(),
			'type' => $this->type(),
			'complete' => 0,
			'comment' => $this->comment(),
			'other_id' => $this->otherID(),
			'date' => time(),
		);

   		$validate = $this->validate($data);
		if(!$validate->fails()) {
			if(!$this->exists()){
				$query = (new \QueryBuilder())->insert('reports')->values($data);
				if($this->_db->execute($query)->error()){
					throw new \DBException("Report Insertion Failed - Database",$this->_db->error());
				}
			} else {
				throw new \LoggerException("Report Already Exists");
			}
		} else {
			throw new \ValidationException("Report Insert Failed - Validation", $validate->errors()->all('validation'));
		}
		return $this;
   	}
   	public function markComplete(): bool {
   		$query = (new \QueryBuilder())->update("reports")->set(["complete" => 1])->where($this->id());
   		if(!$this->_db->execute($query)->error()) {
   			return true;
   		} 
      	return false;
   	}
   	private function exists(): bool {
   		$query = (new \QueryBuilder())->select('reports')->where([
			'cyoa_id','=',$this->cid(),
			'other_id','=',$this->otherID(),
			'type','=',$this->type(),
			'category','=',$this->category(),
			'complete','=',0
		])->limit(1);
		if($this->_db->execute($query)->count()){
			return true;
		}
		return false;
	}

   	public function id(): int { return $this->_id;}
   	public function cid(): int { return $this->_cid;}
   	public function category(): int { return $this->_category;}
   	public function type(): string { return $this->_type;}
   	public function complete(): bool { return $this->_complete;}
   	public function comment(): string { return ($this->_comment) ? $this->_comment : "";}
   	public function otherID(): string { return $this->_otherid;}
   	public function rawDate(): int { return (($this->_date) ? $this->_date : 0);}
   	public function formatDate($type = "date"): string { 
   		switch(strtolower($type)){
   			case "full":
   				return date("Y-m-d h:i:s", $this->rawDate());
   			case "month":
   				return date("M d", $this->rawDate());
   			case "short":
   				return date("m/d/y", $this->rawDate());
   			case "fromnow":
   				$tsNow = new \DateTime("now");
   				$tsStart = (new \DateTime())->setTimestamp($this->rawDate());
   				$days = $tsNow->diff($tsStart)->format("%a");
   				if($tsNow->diff($tsStart)->format("%h") + ($days * 24) != 0){
					if($days > 2){
						return "-" . $days . "d";
					} else{
						return "-" . ($tsNow->diff($tsStart)->format("%h") + ($days * 24)) . "hr";
					}
				}
				return "";
   			case "date":
   			default:
   				return date("m/d/Y", $this->rawDate());
   		}
   	}

   	private function validate($data): \Validation {
		$validation = new \Validation(new \ErrorHandler());
		$validate = $validation->check($data, array(
			'other_id' => array(
				'required' => true,
				'maxlength' => 10,
				'num' => true,
			),
			'category' => array(
				'required' => true,
				'enum' => Category::class,
			),
			'type' => array(
				'required' => true,
				'enum' => Type::class,
			),
			'comment' => array(
				'maxlength' => 50
			),
			'complete' => array(
				'required' => true,
				'bool' => true
			)
		));
		return $validate;
	}
}
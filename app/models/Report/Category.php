<?php
namespace Report;

abstract class Category extends \Enum {
	const BrokenImage = 1; 
	const NonStoryPost = 2;
	const MissingPost = 3;
	const MissingThread = 4;
	const MissingLewd = 5;
	const Other = 6;
	const DuplicatePost = 7; 
	const MissingThumbnail = 8;

	const IncorrectTitle = 50;
	const IncorrectArtist = 51;

	const Duplicate = 90;
	const UpdateTags = 91;
	const UpdateDescription = 92; 
	const BlankStory = 93;
	const NonPonyPaste = 94;
	const RemoveStory = 95;

      const LongForm = array(
            1 => "Broken Image",
            2 => "Non-Story Post",
            3 => "Missing Post",
            4 => "Missing Thread",
            5 => "Missing Lewd",
            7 => "Duplicate Post",
            8 => "Missing Thumbnail",
            50 => "Incorrect Title",
            51 => "Incorrect Artist",
            91 => "Update Tags",
            92 => "Update Description",
            93 => "Blank Story",
            94 => "Non-Pony Paste",
            95 => "Remove Story"
      );

      //Override
	public static function toString($value): string {
      	if (self::isValidValue($value, false) && array_key_exists($value,self::LongForm)) {
                  return self::LongForm[$value];
            }
            return parent::toString($value);
	}
}
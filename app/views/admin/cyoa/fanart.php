<?php include \Config::get('folder/includes') . "/header.php" ?>


<form action="" method="post">
<div class="row">
	<div class='col-md-6'>
		
		<div class="well">
			<table class="table table-condensed">
				<tr>
					<td></td>
					<td>
						<?php echo "<a href=\"/quest/{$data['cyoa']->shortname()}\">{$cyoa->title()}</a> | <a href=\"/{$cyoa->shortname()}/fanart\">Fanart</a> | <a href=\"/all\">All</a>"; ?>
					</td>
				</tr>
				<tr>
					<td>Title</td>
					<td><input type="text" name="title" size="40" value=<?php echo "\"{$fanart->title()}\""; ?>></td>
				</tr>
				<tr>
					<td>Artist</td>
					<td><input type="text" name="author" size="40" value=<?php echo "\"{$fanart->artist()}\""; ?>></td>
				</tr>
				<tr>
					<td>Image</td>
					<td><input type="text" name="filename" size="40" value=<?php echo "\"". basename($fanart->imagePath(true)) ."\""; ?>></td>
				</tr>
				<tr>
					<td>Post ID</td>
					<td><input type="text" name="post_id" size="40" value=<?php echo "\"{$fanart->pid()}\""; ?>></td>
				</tr>
				<tr>
					<td>Lewd</td>
					<td><input type="hidden" name="lewdCheck"> <input type="checkbox" name="lewd" size="40" value="1"<?php if(($fanart && $fanart->lewd())) { echo "checked";} ?>></td>
				</tr>
				<tr>
					<td></td>
					<td><input type="submit" name="update-submit" value="Send"></td>
				</tr>
			</table>
		</div>
	</div>
	<div class='col-md-6'>
		<?php
			if($fanart){
				if($fanart->imagePath(true)){
					echo "<img class=\"img-responsive\" src=\"{$fanart->imagePath()}\">";
				}
			}
		?>
	</div>
</div>
</form>


<?php include \Config::get('folder/includes') . "/footer.php" ?>

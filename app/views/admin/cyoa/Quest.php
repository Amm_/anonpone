<?php
namespace Views\Admin\CYOA;

class Quest extends \Views\Common{
	private $_questListBase;
	private $_questThreadListBase;

	public function setContent(): void {
		$this->cyoa_form = $this->updateForm->build();

		$this->threads_form = "";
		$this->archive_form = "";
		$this->mass_edit_form = "";
		$this->hidden_new = "";
		$this->hidden_existing = "";
		$this->thread_list = "";

		if($this->cyoa){
			$this->hidden_new = "hidden";
			$this->tags = "";
			$this->threads_form = $this->updateThread->build();
			$this->archive_form = $this->searchArchive->build();
			$this->mass_edit_form = $this->massInclude->build();
			
			foreach($this->cyoa->threads() as $thread){
				$this->thread_list .= $this->buildThreadListRow($this->cyoa,$thread);
			}
		} else {
			$this->hidden_existing = "hidden";
			$this->tags = $this->countTags();
		}

		$this->quest_list = "";
		foreach($this->cyoaFactory->get([]) as $cyoa){
			$this->quest_list .= $this->buildListRow($cyoa);
		}
	}

	private function countTags(): string {
		$tags = [];
		$return = "";
        foreach($this->cyoaFactory->get([]) as $c){
            $tags = array_merge($c->tags(),$tags);
        }
        $counts = array_count_values($tags);
        arsort($counts);
        foreach($counts as $key => $value){
            $return .= $key . ": " . $value . "<br>";
        }

        return $return;
	}

	private function buildListRow($cyoa): string{
		if(!$this->_questListBase) $this->_questListBase = file_get_contents(\Config::get("folder/template") . "admin/CYOA/questListCell.html");
		$return = trim($this->_questListBase . " ");
		$return = str_replace("__shortname__", $cyoa->shortname(),$return);
		$return = str_replace("__id__", $cyoa->cid(),$return);
		$return = str_replace("__title__", $cyoa->title(),$return);

		return $return;
	}

	private function buildThreadListRow($cyoa, $thread): string{
		if(!$this->_questThreadListBase) $this->_questThreadListBase = file_get_contents(\Config::get("folder/template") . "admin/CYOA/threadListCell.html");
		$return = trim($this->_questThreadListBase . " ");
		$return = str_replace("__shortname__", $cyoa->shortname(),$return);
		$return = str_replace("__cid__", $cyoa->cid(),$return);
		$return = str_replace("__tid__", $thread->pid(),$return);

		return $return;
	}
}
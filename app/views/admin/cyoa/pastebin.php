<?php include \Config::get('folder/includes') . "/header.php" ?>

<div class="row">
	<div class='col-md-12'>
        <form action="" method="POST">
		<table class="table table-condensed">
			<tr>
				<td>Pastebin ID:</td>
				<td><?php echo $data['pastebin']->id(); ?><input type="hidden" name="id" value="<?php echo $data['pastebin']->id(); ?>"></td>
			</tr>
			<tr>
				<td>Tags:</td>
				<td><input type="text" name="tags" value="<?php echo implode(',',$data['pastebin']->tags()); ?>" size="50"></td>
			</tr>
			<tr>
				<td>Tags:</td>
				<td><textarea cols="50" name="description"><?php echo $data['pastebin']->description(); ?></textarea></td>
			</tr>
			<tr>
				<td>CYOA ID:</td>
				<td>
					<select name="cyoaid">
						<?php
							echo "<option value='0'>No CYOA</option>";
							foreach($data['cyoas'] as $cyoa){
								echo "<option ";
								if($cyoa->cid() == $data['pastebin']->cid()){
									echo "selected ";
								}
								echo "value='{$cyoa->cid()}'>{$cyoa->title()}</option>";
							}
						?>
					</select>
				</td>
			</tr>
			<tr>
				<td></td>
				<td>
					<input type="submit" name="update-submit" value="update">
				</td>
			</tr>
		</table>
		</form>
	</div>
</div>


<?php include \Config::get('folder/includes') . "/footer.php" ?>
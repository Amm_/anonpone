<?php include \Config::get('folder/includes') . "/header.php" ?>


<form action="" method="post">
<div class="row">
	<div class='col-md-6'>
		<div class="well">
			<?php echo $forms['postUpdate']->build();?>
		</div>
	</div>
	<div class='col-md-6'>
		<?php
			if($post){
				echo "<article id='greenBox' class='thread'  style='width:100%;overflow-y:auto;padding-top:0px;margin:0;max-height:500px;font-size: 13px;'>";
				echo \Construct::post($post,$thread,$cyoa,null,!$post->isQM());
				echo "</article>";
			}
		?>
	</div>
</div>
<div class="row">
	<div class='col-md-6'>
		<?php
			if($post){
				if($post->imagePath()){
					echo "<img class=\"img-responsive\" src=\"{$post->imagePath()}\">";
				}
			}
		?>
	</div>
	<div class='col-md-6'>
		<?php
			if($post){
				if($post->imagePathLewd()){
					echo "<img class=\"img-responsive\" src=\"{$post->imagePathLewd()}\">";
				}
			}
		?>
	</div>
</div>
</form>

<?php include \Config::get('folder/includes') . "/footer.php" ?>
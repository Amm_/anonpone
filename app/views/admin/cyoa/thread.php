<?php include \Config::get('folder/includes') . "/header.php" ?>

<div class="row">
	<div class='col-md-12'>
		<h2><?php
		 	echo "<input type='hidden' name='thread-id' value='{$thread->tid()}'>";
			echo "<a href=\"/quest/{$cyoa->shortname()}/{$thread->tid()}\">{$thread->tid()}</a> / ";
			echo "<input type='hidden' name='cyoa-id' value='{$thread->cid()}'>";
			echo "<a href=\"/admin/cyoa/{$cyoa->shortname()}/\">{$cyoa->title()}</a>";
			?>
		</h2>
	<div>
</div>
<div class="row">
	<div class='col-md-12'>
	<form acition="" method="POST">
		<input type="text" name="subject" size="40" value="<?php if($thread) { echo $thread->subject();} else if(\Input::exists()){ echo \Input::get('subject');} ?>">
		<input type="submit" name="thread-submit" value="Send">
	</form>
	<div>
</div>
<div class="row">
	<div class='col-md-6'>
		<table class="table table-condensed">
			<?php
				foreach($thread->posts() as $post){
					echo "<tr>";
					echo "<td>";
					if($post->isQM()){
						echo "<a href=\"/quest/{$cyoa->shortname()}/{$post->pid()}\">QM</a>";
					}else{
						echo "#";
					}
					echo "</td>";
					echo "<td>";
					echo "<a href=\"/admin/cyoa/post/{$cyoa->shortname()}/{$post->pid()}\">{$post->pid()}</a>";
					echo "</td>";
					echo "<td>";
					if($post->imagePath()){
						echo "<a href=\"{$post->imagePath()}\">Image</a>";
					}
					echo "</td>";
					echo "<td>";
					if(!$post->canon()){
						echo "NC";
					}
					echo "</td>";
					echo "<td>";
					if($post->lewd()){
						echo "L";
					}
					echo "</td>";
					echo "<td>";
					$normalPost = "";
					$normalPost .= "<form action=\"/admin/update\" method=\"POST\">";
					$normalPost .= "<input type=\"hidden\" name=\"cyoa_id\" value=\"{$post->cid()}\">";
					$normalPost .= "<input type=\"hidden\" name=\"post_id\" value=\"{$post->pid()}\">";
					$normalPost .= "<input type=\"hidden\" name=\"link\" value=\"/admin/thread/{$cyoa->shortname()}/{$post->tid()}/";
					$normalPost .= "\">";

					$normalPost .= "<select name=\"selection\" onchange=\"this.form.submit()\">";
					$normalPost .= "<option value=\"nan\">Tools</option>";
					if($post->imagePath()){
						$normalPost .= "<option value=\"imageRefresh\">Refresh Image</option>";
						$normalPost .= "<option value=\"image\">Remove Image</option>";
					}
					if($post->canon()){
						$normalPost .= "<option value=\"notCanon\">Mark as not Canon</option>";
					} else{
						$normalPost .= "<option value=\"canon\">Mark as canon</option>";
					}
					if($post->lewd() && $post->imagePath()){
						$normalPost .= "<option value=\"notLewd\">Mark as not Lewd</option>";
					} 
					else if(!$post->lewd() && $post->imagePath()){
						$normalPost .= "<option value=\"lewd\">Mark as Lewd</option>";
					}
					if($post->isQM()){
						$normalPost .= "<option value=\"notQM\">Mark as not QM</option>";
					} 
					$normalPost .= "</select>";
					$normalPost .= "</form>";
					echo $normalPost;
					echo "</td>";
					echo "</tr>";
				}
			?>
			
		</table>
	</div>
</div>


<?php include \Config::get('folder/includes') . "/footer.php" ?>

<?php
namespace Views\Admin;

class Reports extends \Views\Common{
	private $_reportBase;

	public function setContent(): void {
		$this->report_list = "";
		foreach($this->reports->get(true) as $report){
			$this->report_list .= $this->buildRow($report, $this->cyoaFactory->get($report->cid()));
		}
	}

	private function buildRow($report, $cyoa): string{
		if(!$this->_reportBase) $this->_reportBase = file_get_contents(\Config::get("folder/template") . "admin/reportCell.html");
		$return = trim($this->_reportBase . " ");
		$return = str_replace("__category__", \Report\Category::toString($report->category()),$return);
		$return = str_replace("__cyoa_shortname__", $cyoa->shortname(),$return);
		$return = str_replace("__cyoaid__", $cyoa->cid(),$return);
		$return = str_replace("__postid__", $report->otherID(),$return);
		$return = str_replace("__comment__", $report->comment(),$return);
		$return = str_replace("__date__", $report->formatDate(),$return);
		$return = str_replace("__reportid__", $report->id(),$return);

		return $return;

	}
}

<?php
namespace Views\Admin;

class Index extends \Views\Common{
	public function setContent(): void {
		$this->setSettings();
		$this->setActivity();

		$this->months = \Input::get("monthsInput") ?: 1; 
	}

	public function setActivity(): void {
		$this->activity_table = "";
		foreach($this->activity as $cyoa){
			$this->activity_table .= "<tr>";
			$this->activity_table .= "<td>";
			$this->activity_table .= "<a href=\"/quest/{$this->cyoaFactory->get($cyoa->cyoa_id)->shortname()}\">{$this->cyoaFactory->get($cyoa->cyoa_id)->title()}</a>";
			$this->activity_table .= "<a href=\"/admin/cyoa/{$this->cyoaFactory->get($cyoa->cyoa_id)->shortname()}\">#</a>";
			$this->activity_table .= "</td>";
			$this->activity_table .= "<td>";
			$this->activity_table .= "{$cyoa->count} updates";
			$this->activity_table .= "</td>";
			$this->activity_table .= "<td>";
			$this->activity_table .= $this->cyoaFactory->get($cyoa->cyoa_id)->lastPost()->formatDate();
			$this->activity_table .= "</td>";
			$this->activity_table .= "</tr>";
		}
	}

	public function setSettings(): void{
		$this->setting_update_interval_output = ($this->settings->get("updateInterval") ? $this->settings->get("updateInterval") : "Disabled");
		$this->setting_update_interval_input = ($this->settings->get("updateInterval") ? $this->settings->get("updateInterval") : 0);
		$this->setting_update_interval_time = ($this->settings->get("updateTime") ? Round((time() - $this->settings->get("updateTime"))/1000) . "s" : 0);

		$this->setting_disable_site_check = ($this->settings->get("disableSite") ? "checked" : "");
		$this->setting_disable_site_reason = ($this->settings->get("disableSiteReason") ? $this->settings->get("disableSiteReason") : "");
		$this->setting_disable_timing_check = ($this->settings->get("disableTiming") ? "checked" : "");

		$sizeQuery = (new \QueryBuilder())->select("information_schema.tables",['table_schema "DB Name"', 'Round(Sum(data_length + index_length), 1) as "size"'])->where(["table_schema","=",\Config::get('mysql/db')])->group("table_schema");
		if($this->_db->execute($sizeQuery)->count()){
			$this->db_disk_size = \Sanitize::displaySize($this->_db->first()->size);
		} else {
			$this->db_disk_size = "N/A";
		}

		$memory = explode(" ",preg_replace('/\s\s+/', ' ',trim(shell_exec("/usr/bin/top -b -n1 | grep mysql"))))[5];
		if(endsWith($memory,"m")){
			$this->db_memory_size = $memory;
		} else {
			$this->db_memory_size = \Sanitize::displaySize($memory*1000);
		}

		$this->setting_dbcache_check = ($this->settings->get("dbCache") ? "checked" : "");

		$f = \Config::get("folder/files");
		$io = popen ( '/usr/bin/du -sk ' . $f, 'r' );
		$size = fgets ( $io, 4096);
		$size = substr ( $size, 0, strpos ( $size, "\t" ) );
		pclose ( $io );
		$io = popen ( 'find ' . $f . ' -type f | wc -l', 'r' );
		$amount = fgets ( $io, 4096);
		pclose ( $io );

		$this->total_images_disk_size = \Sanitize::displaySize($size*1000);
		$this->total_images_amount = $amount;

		$this->total_disk_size = \Sanitize::displaySize(disk_total_space("/") - disk_free_space("/")) . "/" . \Sanitize::displaySize(disk_total_space("/"));
	}
}
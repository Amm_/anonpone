<?php include \Config::get('folder/includes') . "/header.php" ?>
<?php include \Config::get('folder/includes') . "/searchModal.php" ?>
<style>
.menuicon
{
	font-size:22px;
}
</style>
<script>
	// function cloneRow() {
 //      var row = document.getElementById("exampleRow"); // find row to copy
 //      var table = document.getElementById("submissionTable"); // find table to append to
 //      var clone = row.cloneNode(true); // copy children too
 //      clone.id = "newID"; // change id or other attributes/contents
 //      table.appendChild(clone); // add new row to end of table
 //    }
    function cloneRow(author) {
      var row = document.createElement('tr'); // create row node
      if(author)
      {
      	row.appendChild(document.createElement('td'));
      }
      var col = document.createElement('td'); // create column node
      var col2 = document.createElement('td'); // create second column node
      var col3 = document.createElement('td'); // create second column node
      var col4 = document.createElement('td'); // create second column node
      row.appendChild(col); // append first column to row
      row.appendChild(col2); // append second column to row
      row.appendChild(col3); // append second column to row
      row.appendChild(col4); // append second column to row
      col.innerHTML = '<input type="text" name="pasteID[]" required>'; // put data in first column
      col2.innerHTML = '<input type="text" name="tags[]">'; // put data in second column
      col3.innerHTML = '<textarea type="text" name="description[]" maxlength="200"></textarea>'; // put data in second column
      col4.innerHTML = '<a onclick="this.parentNode.parentNode.parentNode.removeChild(this.parentNode.parentNode);"><span class="glyphicon glyphicon-remove-circle menuicon" aria-hidden="true"></span></a>'; // put data in second column
      var table = document.getElementById("submissionTable"); // find table to append to
      table.appendChild(row); // append row to table
    }
</script>

<div class="modal fade"  style="color:black" id="pasteSubmission" tabindex="-1" role="dialog" aria-labelledby="submissionLabel">
    <div class="modal-dialog <?php echo (array_key_exists('author',$data)) ? 'modal-lg' : 'modal-md';  ?>" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="myModalLabel">Paste Submission</h4>
        </div>
        <form action="" method="POST">
        <div class="modal-body">
	        <div class="row">
	        	<div class="col-md-12">
	        		<!-- <p>
	        			Suggested tags include characters, humanized/anthro, eqg, specific thread names, lewd, and fetishes.
	        		</p>
					<p>
						Submitting an already existing link will update that paste.
					</p> -->
					<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
					  <div class="panel panel-default">
					    <div class="panel-heading" role="tab" id="headingOne">
					      <h4 class="panel-title">
					       	<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
					          Tagging Guide
					        </a>
					      </h4>
					    </div>
					    <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
					      <div class="panel-body">
					      <p>Tagging is submitted as a comma seperated list. So if a story had Anonymous, Twilight, is clop, and has anal in it, you would submit "Anonymous,Twilight,clop,anal".</p>
					      These are some common things to keep in mind when adding tags.
					      	<ul>
					      		<li><b>Characters:</b> Tags of the main characters in the story, including Anon. Anonpone is seperate from Anon.</li>
					      		<li><b>Genre:</b> What genre does it fall into? Examples include fluff, clop, SoL, romance, adventure, dark, edgy, etc.</li>
					      		<li><b>Fetishes:</b> Include any odd fetishes like goo, vore, scat, watersports, macro, etc.</li>
					      		<li><b>World:</b> If it's in a specific world (Industria, /foe/, Princess Applejack) be sure to include it.</li> 
					      		<li><b>Series:</b> If it's just one part of a multi-pastebin story, include a tag for that specific story.</li>
					      		<li><b>Sub-Groups:</b> If it's aimed at a specific 4chan subgroup (bats, moths, satyrs, PiE, AiE, emo, OiE) add it.</li>
					      		<li><b>Bodies:</b> Tag anthro, humanized, and eqg.</li>
					      		<li><b>Status:</b> Complete/WIP.</li>
					      	</ul>
					      	These are all simply suggestions, not necessary. Just remember, at the end of the day tags are to help people find the content they want. So add the tags that you think people might search for.
					      	If the story involves anal, add the anal tag, if it involves cross-dressing, add cross-dressing.<br>

	       	 				<!-- <form action="" method="POST">
					      	<input type="text" name="authorLink" id="authorLink"> <input type="submit" class="btn btn-primary" name="submitAuthor" value="Retrieve Author Pastebins"></button>
					      	</form> -->
					      </div>
					    </div>
					  </div>
					 </div>

	       	 		
					<table class="table table-condensed">

						<thead>
						<tr id="headrow">
						<?php
							if(array_key_exists('author',$data))
							{
								$name = substr($data['authorLink'],22);
								echo "<th><a href=\"{$data['authorLink']}\">{$name}</a>: Title</th>";
							}
						?>
							<th>
								Link:
							</th>
							<th>
								Tags:
							</th>
							<th>
								Description
							</th>
							<th>
							</th>
						</tr>
						</thead>
						<tbody id="submissionTable">
						<?php
							if(array_key_exists('author',$data)):
								$true = true;
								foreach($data['author'] as $pastebin)
								{
						?>
							<tr>
								<td>
								<b>
									<?php 
										echo "<a target=\"_blank\" href=\"http://pastebin.com/$pastebin[1]\">PB</a> - "; 
										if($pastebin[2])
										{
											echo "<a href=\"/pastes/story/$pastebin[1]\">{$pastebin[0]}</a>";
										}
										else
										{
											echo $pastebin[0];
										}
									?>
								</b>
									<?php 
										if($pastebin[2]) {echo "<br>Updated: <i>". date('d/m/y',strtotime($pastebin[2])) . "</i>";} 
									?>
								</td>
								<td>
									<input type="text" name="pasteID[]" required value="<?php echo $pastebin[1]; ?>">
								</td>
								<td>
									<input type="text" name="tags[]" <?php echo ($pastebin[2]) ? "disabled" : ""; ?>>
								</td>
								<td>
									<textarea type="text" name="description[]" maxlength="200" <?php echo ($pastebin[2]) ? "disabled" : ""; ?>></textarea>
								</td>
								<?php if($true): ?>
								<td>
									<a onclick="cloneRow(true)">
										<span class="glyphicon glyphicon-plus menuicon" aria-hidden="true"></span>
									</a>
								</td>
								<?php 
									$true = false; 
									else:
								?>
								<td>
									<a onclick="this.parentNode.parentNode.parentNode.removeChild(this.parentNode.parentNode);">
										<span class="glyphicon glyphicon-remove-circle menuicon" aria-hidden="true"></span>
									</a>
								</td>
							<?php endif; ?>
							</tr>
						<?php
								}
							else:
						?>
							<tr>
								<td>
									<input type="text" name="pasteID[]" required>
								</td>
								<td>
									<input type="text" name="tags[]">
								</td>
								<td>
									<textarea type="text" name="description[]" maxlength="200"></textarea>
								</td>
								<td>
									<a onclick="cloneRow(false)">
										<span class="glyphicon glyphicon-plus menuicon" aria-hidden="true"></span>
									</a>
								</td>
							</tr>
						<?php endif; ?>
						</tbody>
					</table>
	        	</div>
	        </div>
        </div>
        <div class="modal-footer">
        	<input type="submit" name="submitPaste" class="btn btn-primary" value="Submit">
          	<button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
        </div>
        </form>
      </div>
    </div>
  </div>
  <div class="modal fade" id="bookmarkModal" tabindex="-1" role="dialog" aria-labelledby="bookmarkLabel">
    <div class="modal-dialog modal-md" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="myModalLabel">Bookmarks</h4>
        </div>
        <div class="modal-body">
        <ul id="bookmark">
        </ul>
        </div>
      </div>
    </div>
  </div>
<div class='row text-center'>
	<div class='col-md-12 center-block'>
		<h2>Pony Pastebins</h2>
	</div>
</div>
<div class='row'>
	<div class='col-md-9'>
		<p>
			This section of the website is designed as a utility for archiving and/or searching for pony stories that have been put on Pastebin. It is entirely user dependent with submissions being taken from users and processed using the form to the right. To refresh a pastebin simply resubmit it to the archive.
		</p>
		<p>
			If you are a writer and you don't want your work to remain on the archive submit a report with evidence that you are who you say you are and it'll be removed. 
		</p>
	</div>

	<div class='col-md-3'>
	<a class="btn btn-primary" style="display:block" data-toggle="modal" data-target="#pasteSubmission">Submit</a>
	<a class="btn btn-primary" style="display:block;margin-top:5px;" data-toggle="modal" data-target="#searchModal">Search</a>
	<a class="btn btn-primary" style="display:block;margin-top:5px;" data-toggle="modal" data-target="#bookmarkModal">Bookmarks</a>
	</div>
</div>
<div class='row'>
	<div class='col-md-12 center-block'>
		<?php echo $data['pages']->displayPages(); ?>
		<table class="table table-condensed table-striped">
		<thead>
			<tr>
				<th>
					PB
				</th>
				<?php
					if($user->isLoggedIn())	{
						echo "<td>";
						echo "</td>";
					}

				?>
				<th>
					<a href="#" <?php
						echo "onclick=\"";
						echo "createCookie('paste_sort',";
						if(\Cookie::exists('paste_sort') && \Cookie::get('paste_sort') == "AuthorAsc")
						{
							echo "'AuthorDesc'";
						} else{
							echo "'AuthorAsc'";
						}
						echo ",30);";
						echo "location.reload();";
						echo "\"";
					?> >
					Author
					</a>
				</th>
				<th>
					Title
				</th>
				<th>
					Description
				</th>
				<th width="20%">
					Tags
				</th>
				<th>
					Words
				</th>
				<th style="width:104px;">
						<a href="#" <?php
						echo "onclick=\"";
						echo "createCookie('paste_sort',";
						if(\Cookie::exists('paste_sort') && \Cookie::get('paste_sort') == "AddedAsc")
						{
							echo "'AddedDesc'";
						} else{
							echo "'AddedAsc'";
						}
						echo ",30);";
						echo "location.reload();";
						echo "\"";
					?> >
					Added
				</th>
			</tr>
		</thead>
		<tbody>
			<?php
				foreach($data['pastebins'] as $pastebin){
					echo "<tr>";
						// echo "<td>";
						// echo "<a href=\"/pastes/story/{$pastebin->id}\">{$pastebin->id}</a>";
						// echo "</td>";
						echo "<td>";
						if(!$pastebin->deleted()){
							echo " <a href=\"http://pastebin.com/{$pastebin->id()}/\">PB</a>";
						}
						echo "</td>";
						if($user->isLoggedIn())	{
							echo "<td>";
							echo "<a href=\"/admin/pastebin/{$pastebin->id()}\">#</a>";
							echo "</td>";
						}
						echo "<td>";
						echo "<a href=\"/search/?author={$pastebin->author()}\">{$pastebin->author()}</a>";
						echo "</td>";
						echo "<td>";
							echo "<a href=\"/pastes/story/{$pastebin->id()}\">";
							echo htmlspecialchars($pastebin->title());
							echo "</a>";
						echo "</td>";
						echo "<td>";
						echo htmlspecialchars($pastebin->description());
						echo "</td>";
						echo "<td>";
						$count = 0;
						foreach($pastebin->tags() as $tag){
							if($count <= 4)	{
								echo "<a href=\"/search/?tags=" . htmlspecialchars($tag) . "\"><span class=\"badge\">" . htmlspecialchars($tag) . "</span></a>";
							}
							$count++;
						}
						echo "</td>";
						echo "<td>";
						echo str_word_count($pastebin->content());
						echo "</td>";
						echo "<td>";
						if(Cookie::exists('date_type') && Cookie::get('date_type') == "Created") {
							echo $pastebin->created("short");
						}else{
							echo $pastebin->added("short");
						}
						echo "</td>";
						$user = new \User\User();
						if($user->isLoggedIn()){
							echo "<td>";
							echo "<a href=\"/admin/pastebin/{$pastebin->id()}\">E</a>";
							echo "</td>";
						}

					echo "</tr>";
				}
			?>
		</tbody>
		</table>
		<?php echo $data['pages']->displayPages(); ?>
	</div>
</div>

<script>
var story = "pastebin";
var link = "void";
readBookmarks();

<?php
if(array_key_exists('author',$data))
	echo "$('#pasteSubmission').modal('show')";
?>
</script>


<?php include \Config::get('folder/includes') . "/footer.php" ?>
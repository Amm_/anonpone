<?php include \Config::get('folder/includes') . "/header.php" ?>



<div class="modal fade" id="report" tabindex="-1" role="dialog" aria-labelledby="reportLabel">
    <div class="modal-dialog modal-sm" role="document">
      <div class="modal-content">
        <div class="modal-header">
          	<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          	<h4 class="modal-title" id="myModalLabel">Report</h4>
        </div>
        <form action="/update/report" method="POST">
        <div class="modal-body">
        Paste: <?php echo htmlspecialchars($data['pastebin']->title()); ?><br><br>
        <select name="report" required>
        	<?php
                echo "<option value=\"" . \Report\Category::Duplicate . "\">" . \Report\Category::toString(\Report\Category::Duplicate) . "</option>";
                echo "<option value=\"" . \Report\Category::UpdateTags . "\">" . \Report\Category::toString(\Report\Category::UpdateTags) . "</option>";
                echo "<option value=\"" . \Report\Category::UpdateDescription . "\">" . \Report\Category::toString(\Report\Category::UpdateDescription) . "</option>";
                echo "<option value=\"" . \Report\Category::BlankStory . "\">" . \Report\Category::toString(\Report\Category::BlankStory) . "</option>";
                echo "<option value=\"" . \Report\Category::NonPonyPaste . "\">" . \Report\Category::toString(\Report\Category::NonPonyPaste) . "</option>";
                echo "<option value=\"" . \Report\Category::RemoveStory . "\">" . \Report\Category::toString(\Report\Category::RemoveStory) . "</option>";
                echo "<option value=\"" . \Report\Category::Other . "\">" . \Report\Category::toString(\Report\Category::Other) . "</option>";
            ?>
		</select><br><br>
		<input type="hidden" name="other_id" value="<?php echo $data['pastebin']->id(); ?>"/>
		<input type="hidden" name="type" value="<?php echo \Report\Type::Paste; ?>">
		<input type="hidden" name="redirect" value="/pastes/story/<?php echo $data['pastebin']->id(); ?>"/>
		<textarea id="reportBox" name="comment"></textarea><br>
          Maximum of 100 characters.
        </div>
        <div class="modal-footer">
          <input type="submit" name="submit" class="btn btn-primary" value="Submit">
          <button type="button" class="btn btn-primary" data-dismiss="modal">Cancel</button>
        </div>
        </form>
      </div>
    </div>
  </div>

<div class='row'>
	<div class='col-md-9'>
		<h1><a href=<?php echo "\"https://www.pastebin.com/{$data['pastebin']->id()}\""; ?>><?php echo htmlspecialchars($data['pastebin']->title()); ?></a>
			<small>By <?php echo "<a href=\"/search/?author={$data['pastebin']->author()}\">{$data['pastebin']->author()}</a>"; ?> </small>
		</h1>

	</div>
</div>
<div class='row'>
	<div class='col-md-9'>
	</div>
	<div class='col-md-3'>
		<b>Created:</b> <?php echo $data['pastebin']->created("short"); ?>
	</div>
</div>
<div class='row'>
	<div class='col-md-9'>
		<?php echo htmlspecialchars($data['pastebin']->description()); ?>
	</div>
	<div class='col-md-3'>
		<b>Edited:</b> <?php echo $data['pastebin']->edited("short"); ?>
	</div>
</div>
<div class='row'>
	<div class='col-md-9'>
		<?php 
			foreach($data['pastebin']->tags() as $tag)
			{
				echo "<a href=\"/search/?tags=$tag\"><span class=\"badge\">$tag</span></a>";
			}
		?>
	</div>
	<div class='col-md-3'>
		<b>Added:</b> <?php echo $data['pastebin']->added("short"); ?>
	</div>
</div>
<div class='row'>
	<div class='col-md-12 center-block'>
		<article id="greenBox" class="thread" style="width:100%;overflow-y:auto;font-size: 13px;margin:0px">
			<article class="post" style="margin:0px;">
				<div class="post_wrapper">
					<div class="post_data">
						<a class="btn btn-default pull-right" data-toggle="modal" data-target="#report"><span class="glyphicon glyphicon-warning-sign" aria-hidden="true"></span></a>
						<a id="bookmarkButton" class='btn btn-default pull-right' onclick="addBookmark('<?php echo $data['pastebin']->title; ?>')" ><span class="glyphicon glyphicon-bookmark" aria-hidden="true"></span></a> 
						<br>
						<?php echo $data['pastebin']->printContent(); ?>
					</div>
				</div>
			</article>
		</article>
	</div>
</div>

<script>
var link = "/pastes/story/<?php echo $data['pastebin']->id(); ?>";
var story = "pastebin";
var newTitle = "<?php echo $data['pastebin']->title(); ?>"
readBookmarks();
</script>


<?php include Config::get('folder/includes') . "/footer.php" ?>
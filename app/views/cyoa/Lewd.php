<?php
namespace Views\CYOA;

class Lewd extends Common {
	private $_galleryCell;

	public function setContent(): void {
		$this->type = "gallery";
		parent::setContent();
		$this->registerStatic("gallery.css");
		$this->registerStatic("navigation.js");
		$this->registerStatic("reveal.js");

		$this->reply_list = "";
		$this->_galleryCell = file_get_contents(\Config::get("folder/template") . "CYOA/galleryCell.html");
		foreach($this->posts as $post){
			if($post->image()){
				$newCell = trim($this->_galleryCell . " ");
				$newCell = str_replace("__id__",$post->pid(),$newCell);
				$newCell = str_replace("__title__",$post->pid(),$newCell);
				$newCell = str_replace("__image_link__",\Thumbnail::small($post->image()),$newCell);
				$newCell = str_replace("__image__",$post->image(),$newCell);
				$newCell = str_replace("__url__","/quest/{$this->cyoa->shortname()}/post/{$post->pid()}",$newCell);
				if($post->lewd()){
					$newCell = str_replace("__link__",($post->imagePathLewd() ?: $post->imagePath()),$newCell);
				} else {
					$newCell = str_replace("__link__",$post->imagePath(),$newCell);
				}
				$newCell = str_replace("__pin_hide__","hidden",$newCell);
				$this->reply_list .= $newCell;
			}
		}
		if(!$this->reply_list) $this->reply_list = "There's nothing's here";
	}
}
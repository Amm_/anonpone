<?php
namespace Views\CYOA;

class Thread extends Common{
	public function setContent(): void {
		$this->type = "thread";
		parent::setContent();
		$this->registerStatic("navigation.js");
		$this->registerStatic("preFetch.js");
		$this->registerStatic("reveal.js");
		$this->registerStatic("report.js");
		$this->registerStatic("postMenu.js");

		$this->thread_num = $this->thread->tid();
		$this->archive_link = $this->archive->getURL($this->cyoa,\Archive\Archive::LinkURL,$this->thread->pid());

		$this->reply_list = "";
		foreach($this->thread->QMposts() as $post){
			if($post->image()) $this->reply_list .= $this->getImage($this->cyoa,$post);
			$this->reply_list .= $this->buildPost($post,$this->archive,false);	
		}
	}
}
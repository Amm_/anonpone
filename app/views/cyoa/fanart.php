<?php
namespace Views\CYOA;

class Fanart extends Common {
	private $_galleryCell;

	public function setContent(): void {
		parent::setContent();
		$this->registerStatic("gallery.css");
		$this->registerStatic("reveal.js");

		$this->page_list = $this->pages->displayPages();

		$this->reply_list = "";
		$this->_galleryCell = file_get_contents(\Config::get("folder/template") . "CYOA/galleryCell.html");
		foreach($this->fanarts as $fanart){
			$newCell = trim($this->_galleryCell . " ");
			$newCell = str_replace("__id__",$fanart->id(),$newCell);
			$newCell = str_replace("__title__",$fanart->title() . (($fanart->artist()) ? " by " . $fanart->artist() : ""),$newCell);
			$newCell = str_replace("__image_link__",\Thumbnail::small($fanart->image()),$newCell);
			$newCell = str_replace("__image__",$fanart->image(),$newCell);
			$newCell = str_replace("__url__",$fanart->imagePath(),$newCell);
			$newCell = str_replace("__link__",$fanart->imagePath(),$newCell);
			$newCell = str_replace("__pin_hide__",($fanart->priority()) ? "" : "hidden",$newCell);
			$this->reply_list .= $newCell;
		}
		if(!$this->reply_list) $this->reply_list = "There's nothing's here";
	}
}
<?php
namespace Views\CYOA;

class Post extends Common{
	public function setContent(): void {
		parent::setContent();
		$this->registerStatic("navigation.js");
		$this->registerStatic("preFetch.js");
		$this->registerStatic("reveal.js");
		$this->registerStatic("report.js");
		$this->registerStatic("postMenu.js");

		$this->post_num = $this->post->pid();
		$this->thread_num = $this->post->tid();
		$this->archive_link = $this->archive->getURL($this->cyoa,\Archive\Archive::LinkURL,$this->post->pid());

		$this->parent_image = $this->getImage($this->cyoa,$this->post);

		$this->reply_list = "";
		foreach($this->post->replies() as $reply){
			if($this->thread->posts($reply) && !$this->thread->posts($reply)->isQM()) $this->reply_list .= $this->buildPost($this->thread->posts($reply),$this->archive);	
		}
		$this->parent_post = $this->buildPost($this->post,$this->archive, false);
	}
}
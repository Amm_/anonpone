<?php
namespace Views\CYOA;

class Search extends Common{
	public function setContent(): void {
		parent::setContent();

		
		$this->registerStatic("report.js");
		$this->page_list = $this->pages->displayPages();
		$this->page_information = $this->pages->displayPageAmounts();
		$this->search_text = htmlentities(\Input::get("q")) ?? "";

		$this->reply_list = "";
		$threadID = 0;
		foreach($this->posts as $post){
			if($threadID !== $post->tid()){ 
				$threadID = $post->tid();
				$this->reply_list .= "<h3>Thread <a href=\"/quest/{$this->cyoa->shortname()}/thread/{$threadID}\">{$post->tid()}</a> {$this->cyoa->threads($threadID)->formatDate("short")}</h3>";
			}
			$this->reply_list .= $this->buildPost($post,$this->archive);	
		}
		if(!$this->reply_list && !$this->search_text) $this->reply_list = "Please Enter Search Terms";
		if(!$this->reply_list && $this->search_text) $this->reply_list = "<img src=\"/images/choosie_shrug.png\" class=\"dunno-bro\"/>";
	}
}
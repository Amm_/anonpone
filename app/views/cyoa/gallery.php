<?php
namespace Views\CYOA;

class Gallery extends Lewd {
	private $_galleryCell;

	public function setContent(): void {
		parent::setContent();
		$this->thread_num = $this->thread->tid();
		$this->archive_link = $this->archive->getURL($this->cyoa,\Archive\Archive::LinkURL,$this->thread->pid());
	}
}
<?php
namespace Views\CYOA;

class Common extends \Views\Common{

	private $_videoCell;
	private $_imgCell;
	private $_postBase;

	public function __construct(\DB $db,\User\User $user,string $string){
		parent::__construct($db, $user,"CYOA/cyoaHeader");
		$this->cyoa_body = file_get_contents(\Config::get("folder/template") . "CYOA/{$string}.html");
		$this->registerStatic("header.css");
		$this->registerStatic("galleryOverlay.css");
		$this->registerStatic("gallery.js");
	}
	public function setContent(): void {
		$this->type = ($this->type ?? "post");
		$this->page_title = strip_tags($this->cyoa->title());
		$this->page_description = strip_tags($this->cyoa->description());
		$this->page_image = \Thumbnail::medium($this->cyoa->imagePath());

		$this->header_thumbnail = \Thumbnail::medium($this->cyoa->imagePath());
		$this->shortname = $this->cyoa->shortname();
		$this->cyoa_title = $this->cyoa->title();
		$this->cyoa_desc = $this->cyoa->description();
		$this->cyoa_id = $this->cyoa->cid();

		$background = \CYOA\Status::color($this->cyoa->status());
		$this->cyoa_badges = "";
		$this->cyoa_badges .= "<a class=\"badge {$background}\" href=\"/?search=" . \CYOA\Status::toString($this->cyoa->status()) . "\">" . \CYOA\Status::toString($this->cyoa->status()) . "</a>";
		foreach($this->cyoa->tags() as $tag){
			$this->cyoa_badges .= "<a class=\"badge\" href=\"/?search=" . $tag . "\">" . $tag . "</a>";
		}
		$this->cyoa_duration = $this->secondsToTime($this->cyoa->stat('questTime'),true);
		$this->cyoa_words = number_format($this->cyoa->stat('totalWordCount'));
		
		$this->cyoa_posts = $this->cyoa->stat('totalPosts');
		$this->cyoa_avgthread = ($this->cyoa->stat('totalThreads')) ? $this->secondsToTime(round($this->cyoa->stat('totalThreadTime')/$this->cyoa->stat('totalThreads')),false) : "";
		$this->cyoa_avgwords = ($this->cyoa->stat('totalThreads')) ? number_format(round($this->cyoa->stat('totalWordCount')/$this->cyoa->stat('totalThreads')),0) : "";
		$this->cyoa_avgposts = ($this->cyoa->stat('totalThreads')) ? round($this->cyoa->stat('totalPosts')/$this->cyoa->stat('totalThreads')) : "";
		$this->thread_count = count($this->cyoa->threads());

		$this->thread_list = "";
		foreach($this->cyoa->threads() as $thread){
			$this->thread_list .= "<li ";
			$this->thread_list .= ($this->thread && $this->thread->tid() == $thread->tid()) ? "class=\"selected\"" : "";
			$this->thread_list .=	  "><a href=\"/quest/{$this->cyoa->shortname()}/{$this->type}/{$thread->tid()}#cyoanavbar\">" . substr((($thread->subject()) ? $thread->subject() : $thread->tid()), 0, 35) . "</a></li>";
		}

		if($this->thread){
			$this->thread_hidden = "";
			$this->threadid = $this->thread->tid();

			$this->thread_duration = $this->secondsToTime($this->thread->stat('threadTime'),false);
			$this->thread_words = number_format($this->thread->stat('threadWords'));
			$this->thread_posts = $this->thread->stat('threadPosts');

			$postCount = 0;
			foreach($this->thread->posts() as $p){
				if($p->isQM()){
					$postCount++;
				}
				if($p->pid() == $p->pid()){
					break;
				}
			}
			$this->cyoa_posts = $this->thread->stat('postCount') + $postCount . "/" . $this->cyoa->stat('totalPosts');

			$nextThread = $this->cyoa->nextThread($this->thread);
			if($nextThread){
				$this->next_thread_link = " href=\"/quest/{$this->cyoa->shortname()}/{$this->type}/{$nextThread->tid()}#cyoanavbar\"";
				$this->next_thread_disable = "";
			} else { 
				$this->next_thread_link = "";
				$this->next_thread_disable = "disabled";
			}
			$prevThread = $this->cyoa->prevThread($this->thread);
			if($prevThread){
				$this->prev_thread_link = " href=\"/quest/{$this->cyoa->shortname()}/{$this->type}/{$prevThread->tid()}#cyoanavbar\"";
				$this->prev_thread_disable = "";
			} else { 
				$this->prev_thread_link = "";
				$this->prev_thread_disable = "disabled";
			}
		} else {
			$this->threadid = $this->cyoa->firstPost()->pid();
			$this->thread_hidden = "hidden";
			$this->thread_duration = "";
			$this->thread_words = "";
			$this->thread_posts = "";

			$this->next_thread_link = "";
			$this->prev_thread_link = "";
			$this->next_thread_disable = "disabled";
			$this->prev_thread_disable = "disabled";
		}

		if($this->post){
			$this->postid = $this->post->pid();
			$this->post_list = "";
			$this->post_list_disable = "";
			foreach($this->thread->QMposts() as $post){
				$this->post_list .= "<li ";
				$this->post_list .= ($this->post->pid() == $post->pid()) ? "class=\"selected\"" : ""; 
				$this->post_list .= "><a href=\"/quest/{$this->cyoa->shortname()}/{$this->type}/{$post->pid()}#cyoanavbar\">{$post->pid()}</a></li>";
			}
			$nextPost = $this->thread->nextPost($this->post);
			if($nextPost){
				$this->next_post_link = " href=\"/quest/{$this->cyoa->shortname()}/{$this->type}/{$nextPost->pid()}#cyoanavbar\"";
				$this->next_post_disable = "";
			} else { 
				if($nextThread){
					$this->next_post_link = " href=\"/quest/{$this->cyoa->shortname()}/{$this->type}/{$nextThread->firstPost()->pid()}#cyoanavbar\"";
					$this->next_post_disable = "";
				} else {
					$this->next_post_link = "";
					$this->next_post_disable = "disabled";
				}
			}
			$prevPost = $this->thread->prevPost($this->post);
			if($prevPost){
				$this->prev_post_link = " href=\"/quest/{$this->cyoa->shortname()}/{$this->type}/{$prevPost->pid()}#cyoanavbar\"";
				$this->prev_post_disable = "";
			} else { 
				if($prevThread){
					$this->prev_post_link = " href=\"/quest/{$this->cyoa->shortname()}/{$this->type}/{$prevThread->lastPost()->pid()}#cyoanavbar\"";
					$this->prev_post_disable = "";
				} else {
					$this->prev_post_link = "";
					$this->prev_post_disable = "disabled";
				}
			}
		} else {
			if($this->thread) $this->postid = $this->thread->tid();
			else $this->postid = "first";

			$this->next_post_link = "";
			$this->prev_post_link = "";
			$this->post_list = "";
			$this->next_post_disable = "disabled";
			$this->prev_post_disable = "disabled";
			$this->post_list_disable = "disabled";
		}
	}

	protected function buildPost(\CYOA\Post $post,\Archive\Archive $archive,bool $useImage = true): string{
		if(!$this->_postBase) $this->_postBase = file_get_contents(\Config::get("folder/template") . "CYOA/postCell.html");
		$newPost = trim($this->_postBase . " ");
		if($useImage && $post->imagePath()){
			$newPost = str_replace("__has_image__","",$newPost);
			$newPost = str_replace("__gallery_hidden__","galleryItem",$newPost);
			$newPost = str_replace("__post_image_url___",$post->imagePath(),$newPost);
			$newPost = str_replace("__post_image_thumbnail__",\Thumbnail::small($post->image()),$newPost);
			if($post->lewd()){
				$newPost = str_replace("__post_lewd_url__",($post->imagePathLewd() ?: $post->imagePath()),$newPost);
			} else {
				$newPost = str_replace("__post_lewd_url__","",$newPost);
			}
		} else {
			$newPost = str_replace("__has_image__","hidden",$newPost);
			$newPost = str_replace("__gallery_hidden__","",$newPost);
			$newPost = str_replace("__post_image_url___","",$newPost);
			$newPost = str_replace("__post_image_thumbnail__","",$newPost);
			$newPost = str_replace("__post_lewd_url__","",$newPost);
		}
		$newPost = str_replace("__post_name__",$post->name() ?? "Anonymous",$newPost);
		$newPost = str_replace("__cyoa_title__",$post->cid(), $newPost);
		$newPost = str_replace("__post_trip__",$post->trip(),$newPost);
		$newPost = str_replace("__post_time__",$post->formatDate("full"),$newPost);
		$newPost = str_replace("__post_id__",$post->pid(),$newPost);
		$newPost = str_replace("__post_archive_link__",$archive->getURL($this->cyoa,\Archive\Archive::LinkURL,$post->pid()),$newPost);
		$newPost = str_replace("__post_content__",$post->comment(),$newPost);

		return $newPost;
	}

	protected function getImage(\CYOA\CYOA $cyoa,\CYOA\Post $post): string {
		$return = "";
		if($post->image()){
			$ext = pathinfo($post->image(), PATHINFO_EXTENSION);
			if( $ext == 'webm' || $ext == 'gifv'){
				if(!$this->_videoCell) $this->_videoCell = file_get_contents(\Config::get("folder/template") . "CYOA/videoCell.html");
				$return = trim($this->_videoCell . " ");
				$return = str_replace("__ext__",$ext,$return);
				$return = str_replace("__src__",$post->image(),$return);
			} else {
				if(!$this->_imgCell) $this->_imgCell = file_get_contents(\Config::get("folder/template") . "CYOA/imageCell.html");
				$return = trim($this->_imgCell . " ");
				$return = str_replace("__image_link__", $post->image(),$return);
				if($post->lewd()){
					$return = str_replace("__lewd_url__", ($post->imagePathLewd() ?: $post->imagePath()),$return);
					$return = str_replace("__link__", ($post->imagePathLewd() ?: $post->imagePath()),$return);
				} else {
					$return = str_replace("__link__", $post->image(),$return);
					$return = str_replace("__lewd_url__", $post->image(),$return);
				}
			}

			$return = str_replace("__post_image_url__", "/{$cyoa->shortname()}/post/{$post->pid()}",$return);
			$return = str_replace("__postid__", $post->pid(),$return);
		}
		return $return;
	}

	private function secondsToTime(int $seconds,bool $switch): string {
	    try {
	    	
		    $dtF = new \DateTime("@0");
		    $dtT = new \DateTime("@$seconds");
		    if($switch){
		    	$interval = $dtF->diff($dtT);
		    	if($interval->y > 0){
		    		return $interval->format('%y years, %m months, %d days');
		    	} else if($interval->m > 0)	{
		    		return $interval->format('%m months, %d days');
		    	}
		    	return $interval->format('%d days');
		    } else {
		    	return $dtF->diff($dtT)->format('%a days, %h hours, %i minutes');
		    }
	    } catch (\Exception $e) {
	    	return "";
	    }
	}
}
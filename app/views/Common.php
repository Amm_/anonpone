<?php
namespace Views;

class Common extends \Core\View{


	public function __construct($db, $user,$string){
		parent::__construct($db, $user,$string);
		$this->page_title = "Anonpone - MLP Quests";
		$this->page_description = "A gathering of the /mlp/ board of 4chan's Quests (Choose Your Own Adventures)";
		$this->page_image = "https://img.anonpone.com/image/70/702911977a64ac701f4b9f3f963344c0_small.jpg";
	}
	public function render(): string {
		\Timing::mark("Start Render");
		$this->setAlerts();
		$this->setContent();
		\Timing::mark("Set Content");
		parent::render();
		\Timing::mark("Rendered Page");

		\Timing::mark("End");
		$this->_document = str_replace("__footerTime__",\Timing::total() . "s",$this->_document);
		$this->_document = str_replace("__footerQueries__",$this->_db->queryCount() . " queries",$this->_document);
		$this->_document = str_replace("__footerMemory__",\Sanitize::displaySize(memory_get_peak_usage()),$this->_document);
		$this->setTiming();
		$this->_document = str_replace("__timing__",$this->timing,$this->_document);

		return $this->_document;
	}

	public function setContent(): void {}

	private function setAlert(string $element,string $value,string $type = "error"): string {
		$clone = $element . " ";
		$clone = str_replace("__type__",$type,$clone);
		$clone = str_replace("__content__",$value,$clone);
		return $clone;
	}
	public function setAlerts(): void {
		$result = "";
		$session = new \Session();

		$element = "<li class=\"alertItem __type__\" role=\"alert\">__content__</li>";
		$list = [];
		if(isset($this->alerts)){
			foreach($this->alerts->all('error') as $error){
				$list[] = $this->setAlert($element,$error,"error");
			}
		}
        if($session->exists("error")){
			$list[] = $this->setAlert($element,$session->flash("error"),"error");
        }
		if(isset($this->alerts)){
	        foreach($this->alerts->all('success') as $success){
				$list[] = $this->setAlert($element,$success,"success");
	        }
		}
	    if($session->exists("success")){
			$list[] = $this->setAlert($element,$session->flash("success"),"success");
	    }
	    if(count($list)){
	    	$result .= "<ul class=\"alertGroup\">";
	    	$result .= implode($list);
	    	$result .= "</ul>";
	    }
	    $this->alerts = $result;
	}
	
	public function setTiming(): void {
		if($this->_user->isLoggedIn()){
			$this->timing = \Timing::display();
		} else {
			$this->timing = "";
		}
	}
}

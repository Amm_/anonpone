<?php
namespace Views;

class Index extends Common {

	public function setContent(): void {
		$this->setCells();
		$this->setFanart();
		$this->registerStatic("catalog.css");
		$this->registerStatic("catalog.js");
	}
	public function setCells(): void {
		$base = file_get_contents(\Config::get("folder/template") . "indexCell.html");
		$cells = [];
		$hiddenCells = [];
		$now = time();
		$wordcount = $imgcount = $postcount = $threadcount = 0;
		$idlist = [];
		foreach($this->cyoas as $cyoa){
			$newCell = trim($base . " ");
			$newCell = str_replace("__title__",$cyoa->title(),$newCell);
			$newCell = str_replace("__shortname__",$cyoa->shortname(),$newCell);
			$newCell = str_replace("__desc__",$cyoa->description(),$newCell);
			$newCell = str_replace("__id__",$cyoa->cid(),$newCell);
			$newCell = str_replace("__image__",\Thumbnail::thumbPath($cyoa->imagePath(),"medium"),$newCell);
			if($cyoa->status() === \CYOA\Status::Complete || $cyoa->status() === \CYOA\Status::Cancelled){
				$newCell = str_replace("__rss_hidden__","hidden",$newCell);
			}else{
				$newCell = str_replace("__rss_hidden__","",$newCell);
			}

			if($cyoa->stat("lastPostDate") && $now - $cyoa->stat("lastPostDate") > 6*30*24*60*60){
				$newCell = str_replace("__date__",date("m/d/y", $cyoa->stat("lastPostDate")),$newCell);
			} else {
				$newCell = str_replace("__date__",date("M d", $cyoa->stat("lastPostDate")),$newCell);
			}


			$background = \CYOA\Status::color($cyoa->status());
			$badges = "";
			$badges .= "<a class=\"badge {$background}\">" . \CYOA\Status::toString($cyoa->status()) . "</a>";
			$badges .= "<a class=\"badge\">" . implode($cyoa->tags(),"</a><a class=\"badge\">") . "</a>";
			$newCell = str_replace("__tags__",$badges,$newCell);

			$stats = "";
			$stats .= "Words:".number_format($cyoa->stat('totalWordCount'));
	  		$stats .= "<br>Posts:".$cyoa->stat('totalPosts');
	  		$stats .= "<br>Images:".$cyoa->stat('totalImages');
	  		$stats .= "<br>Updates/Session:".$cyoa->stat('updatesPerSession');
	  		if($cyoa->stat('updatesPerSession') > 1){
	  			$stats .= "<br>Update/Time:".$cyoa->stat('updateTime') ."m";
	  		}
	  		$newCell = str_replace("__stats__",$stats,$newCell);

	  		$wordcount += $cyoa->stat('totalWordCount');
			$imgcount += $cyoa->stat('totalImages');
			$postcount += $cyoa->stat('totalPosts');
			$threadcount += $cyoa->stat('totalThreads');

			if($cyoa->status() === \CYOA\Status::Hidden || $cyoa->tags("miniquest")){
				$newCell = str_replace("__hidden__","hidden",$newCell);
				$hiddenCells[] = $newCell;
			} else {
				$newCell = str_replace("__hidden__","",$newCell);
				$cells[] = $newCell;
			}
		}
		$quests = count($this->cyoas);
		$newCell = "<div id=\"stats\" class=\"panel \">Quests: " . number_format($quests) . "<br>" . 
			"Threads: " . number_format($threadcount) . "<br>" . 
			"Posts: " . number_format($postcount) . "<br>" .
			"Images: " . number_format($imgcount) . "<br>" . 
			"Words: " . number_format($wordcount) . "</div>";
		$cells[] = $newCell;
		$cells = array_merge($cells,$hiddenCells);
		$this->cells = implode($cells);
	}

	public function setFanart(): void {
		$fanart = [];
		$count = 0;
		foreach($this->fanarts as $art){
			$fanart[] = "<img src=\"" . \Thumbnail::thumbPath($art->image(),"small") . "\">";
			if(++$count >= 2) break;
		}
		$this->fanarts = implode($fanart);
	}
}

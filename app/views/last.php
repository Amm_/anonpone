<html>
	<head>
		<style>
			h1 { 
	            font-size:30px;
	            -webkit-text-stroke: 1px black;
	            color: white;
	            text-shadow: 3px 3px 0 #000,-1px -1px 0 #000,1px -1px 0 #000,-1px 1px 0 #000,1px 1px 0 #000;
	            margin:0;
	        }
		</style>
  		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
  		<script>
  			<?php echo "var url = \"/api/last/{$data['type']}/{$data['cyoa']->shortname()}/json\";\n"; ?>
  			<?php echo "var timestamp = new Date({$data['cyoa']->lastPost()->rawDate()}*1000);"; ?>
  			
  			function get(url){
  				$.getJSON( url, function(data) {
  					if(data){
  						<?php echo $data['type'];?>(data);
  					}
  				});
  			}
  			setInterval(function(){get(url)},20*1000);

  			function post(data){
  				$("text").text(data._pid);
  			}
  			function time(data){
  				timestamp = new Date(data._date*1000);
  				updateTime();
  			}
  			function image(data){
          console.log(data._filename);
  				if(data._filenameLewd){
  					$('#image').attr('src', 'https://img.anonpone.com/image/' + data._filenameLewd.substr(0,2) + "/" + data._filenameLewd);
  				}	else if(data._filename){
  					$('#image').attr('src', 'https://img.anonpone.com/image/' + data._filename.substr(0,2) + "/" + data._filename);
  				}
  			}
  			function updateTime(){
  				var time = $('#timestamp');
  				if(time){
  					var offset = -5;
  					var d = new Date();
		            utc = d.getTime() + d.getTimezoneOffset() * 60000;
		            var today = new Date(utc + (3600000*offset));
		            //utc = new Date(utc);
		            //var today = nd;
  					   var diffMs = (today - timestamp);
  					   //diffMs = diffMs - (new Date().getTimezoneOffset() * 60 * 1000) + (360 * 60 * 1000);
			        var diffHrs = Math.floor((diffMs) / 3600000); // hours
			        var diffMins = Math.floor(((diffMs % 86400000) % 3600000) / 60000); // minutes
			        var diffSecs = Math.floor((((diffMs % 86400000) % 3600000) % 60000) / 1000); // seconds
			        time.text(diffHrs + ":" + diffMins + ":" + diffSecs);
  				}
  			}
  			get(url);
  			setInterval(function(){ get(url)},1000);
  		</script>
	</head>
	<body>
	<?php
		if($data['type'] == "post")
		{
			echo "<h1 id='text'>{$data['cyoa']->lastPost()->pid()}</h1>";
		}
		else if ($data['type'] == "time")
		{
			echo "<h1 id='timestamp'>{$data['cyoa']->lastPost()->rawDate()}</h1>";
		}
		else if ($data['type'] == "image")
		{
			echo "<img id='image' width=\"300px\"src=\"{$data['cyoa']->lastPost()->imagePath()}\">";
		}
	?>
	</body>
</html>
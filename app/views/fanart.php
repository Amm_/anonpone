<?php include \Config::get('folder/includes') . "/header.php" ?>

<script type="text/javascript" data-no-instant>
    function submitForm() {
        var form = document.getElementById("reportForm");
        form.submit();
    }
</script>
<style>
.menuicon
{
	font-size:22px;
}
.artistInput
{
	width:115px;
}
.postInput{
	width:85px;
}
.ui-autocomplete {
    z-index: 1511;
}
</style>
<div class="modal fade"  style="color:black" id="fanartSubmission" tabindex="-1" role="dialog" aria-labelledby="submissionLabel" data-no-instant>
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="myModalLabel">Fanart Submission</h4>
        </div>
        <form action="" method="POST">
        <div class="modal-body">
        <div class="row">
        	<div class="col-md-12">
       	 		
				<table class="table table-condensed">
					<thead>
					<tr id="headrow">
						<th>
							<div data-toggle="tooltip" data-placement="top" title="Link to where the image is hosted. Uploading is not supported. Required.">Link:</div>
						</th>
						<th>
							<div data-toggle="tooltip" data-placement="top" title="A description of the image. Required.">Title:</div>
						</th>
						<th>
							<div data-toggle="tooltip" data-placement="top" title="The artist of the image. Not required.">Artist:</div>
						</th>
						<th>
							<div data-toggle="tooltip" data-placement="top" title="The quest. Required.">Quest:</div>
						</th>
						<th>
							<div data-toggle="tooltip" data-placement="top" title="Associates an image with an OP post, used for drawing of scenes in a story. Not Required.">Post ID:</div>
						</th>
						<th>
							<div data-toggle="tooltip" data-placement="top" title="Whether the image is lewd or not. Not Required">Lewd:</div>
						</th>
						<th>
							<a onclick="cloneFRow()">
								<span class="glyphicon glyphicon-plus menuicon" aria-hidden="true" title="Add Row"></span>
							</a>
						</th>
					</tr>
					</thead>
					<tbody id="submissionTable">
						<tr>
							<td>
								<input type="url" name="link[0]" required>
							</td>
							<td>
								<input type="text" name="title[0]" required maxlength="128">
							</td>
							<td>
								<div class="ui-widget">
								<input type="text" class="artistInput tags" name="artist[0]" maxlength="64">
								</div>
							</td>
							<td>
								<select name="cyoa_shortname[0]">
								<?php
									 foreach($cyoas as $cyoa)
									 {
									 	echo "<option ";
									 	echo "value=\"{$cyoa->shortname()}\">{$cyoa->title()}</option>";
									 }
								?>
								</select>
							</td>
							<td>
								<input type="number" class="postInput" name="post_id[0]" maxlength="12">
							</td>
							<td>
								<input type="checkbox" name="lewd[0]">
							</td>
							<td>
								<a title="Remove Row" onclick="this.parentNode.parentNode.parentNode.removeChild(this.parentNode.parentNode);">
									<span class="glyphicon glyphicon-remove-circle menuicon" aria-hidden="true"></span>
								</a>
							</td>
						</tr>
					</tbody>
				</table>
        	</div>
        </div>
        </div>
        <div class="modal-footer">
        	<input type="submit" name="submitFanart" class="btn btn-primary" value="Submit">
        	<input type="hidden" name="token" value="<?php  ?>">
          	<button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
        </div>
        </form>
      </div>
    </div>
</div>
<div class="modal fade" id="report" tabindex="-1" role="dialog" aria-labelledby="reportLabel" style="z-index: 1550;">
    <div class="modal-dialog modal-sm" role="document">
      <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="myModalLabel">Report</h4>
        </div>
        <form id="reportForm" action="/update/report" method="POST">
            <div class="modal-body">
                <p>
                 <select name="category" required>
                    <?php
                        echo "<option value=\"" . \Report\Category::BrokenImage . "\">" . \Report\Category::toString(\Report\Category::BrokenImage) . "</option>";
                        echo "<option value=\"" . \Report\Category::IncorrectTitle . "\">" . \Report\Category::toString(\Report\Category::IncorrectTitle) . "</option>";
                        echo "<option value=\"" . \Report\Category::IncorrectArtist . "\">" . \Report\Category::toString(\Report\Category::IncorrectArtist) . "</option>";
                    ?>
                </select>
                </p>
                <input type="hidden" id="reportImageID" name="other_id" value="">
                <input type="hidden" name="type" value="<?php echo \Report\Type::Fanart; ?>">
                <input type="hidden" id="reportCYOAID" name="cyoa_id" value=""/>
                <input type="hidden" name="redirect" value="/all"/>
                <textarea id="reportBox" name="comment"></textarea><br>
                  Maximum of 100 characters.
            </div>
            <div class="modal-footer">
              <button class="g-recaptcha btn btn-primary" style="margin-top:5px" data-sitekey=<?php echo \Config::get("config/recaptcha/siteKey") ?> data-callback="submitForm" type="submit" class="form-control" aria-describedby="basic-addon1" value="Submit" data-badge="inline"  required>Submit</button>
                <button type="button" style="margin-top:5px" class="btn btn-primary" data-dismiss="modal">Cancel</button>
            </div>
        </form>
      </div>
    </div>
</div>
 
<style>
.my-gallery figcaption {
  display: none;
}
.pswp__button--share {
	display:none;
}
.thumbnail
{
	max-height:250px;
}
</style>
<?php echo $pages->displayPages(); echo $pages->displayPageAmounts();?>
<div class='row text-center my-gallery' itemscope itemtype="http://schema.org/ImageGallery" data-no-instant>
<?php
	if(!isset($fanart) || count($fanart) == 0){
		echo "There's nothing here :(";
	}else {
		foreach($fanart as $fanart){
			if($user->isLoggedIn() && \Cookie::exists('admin') && \Cookie::get('admin') == 1 && false){
				echo "<div class=\"col-xs-6 col-md-2\" style=\"height:250px\">";
					echo "<a href=\"{$fanart->image()}\" class=\"thumbnail\" >";
						echo "<img src=\"" . \Thumbnail::thumbPath($fanart->image(),"small") . "\"  style=\"max-height:180px\" class=\"img-responsive\">";
					echo '</a>';
					echo "P: {$fanart->priority()} L: {$fanart->lewd()}";
					if($fanart->pid()){
						echo " <a href=\"/quest/{$cyoa->shortname()}/{$fanart->pid()}\">{$fanart->pid()}</a>";
					}
					echo "<form action=\"/quest/{$cyoa->shortname()}/fanart\" method=\"POST\">";
						echo "<input type=\"hidden\" name=\"fanart_id\" value=\"{$fanart->id()}\">";
						echo "<select name=\"selection\" onchange=\"this.form.submit()\">";
							echo "<option value=\"nan\">Tools</option>";
							echo "<option value=\"increasePriority\">Increase Priority</option>";
							echo "<option value=\"decreasePriority\">Decrease Priority</option>";
							echo "<option value=\"deleteImage\">Delete Image</option>";
							if($fanart->lewd()){
								echo "<option value=\"notLewd\">Make Not Lewd</option>";
							}else{
								echo "<option value=\"makeLewd\">Make Lewd</option>";
							}
						echo "</select>";
					echo "</form>";
				echo "</div>";
			}else{
				$size = getimagesize($fanart->imagePath(true));
				$cyoa = \CYOA\Handler::get($fanart->cid());
				//$size = [0,0];

				echo '<figure class="col-xs-6 col-md-2"  style="height:175px" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject" >';
					echo "<a href=\"" . $fanart->imagePath() ."\" class=\"thumbnail\" itemprop=\"contentUrl\" data-size=\"{$size[0]}x{$size[1]}\">";
						echo "<img src=\"" . \Thumbnail::thumbPath($fanart->image(),"small") . "\" class=\"img-responsive\" style=\"max-height:220px\" itemprop=\"thumbnail\" onclick=\"document.getElementById('reportImageID').value = {$fanart->id()};document.getElementById('reportCYOAID').value = {$fanart->cid()};\">";

						if(pathinfo($fanart->imagePath(), PATHINFO_EXTENSION) === "gif"){
							echo "<span style=\"color:black;opacity:.7;width:30px;height:20px;border:1px black solid;position:absolute;left:15px;top:0px;background-color:#fff\" aria-hidden=\"true\">GIF</span>";
						}
					echo '</a>';
					echo "<figcaption itemprop=\"caption description\">" . htmlentities($fanart->title());
					if($fanart->artist()){
						echo  " by " . htmlentities($fanart->artist());
					}
					if($cyoa){
						echo " from <a href=\"/quest/{$cyoa->shortname()}/\">{$cyoa->title()}</a>";
					}
					
					echo "</figcaption>";
				echo '</figure>';
			}
		}
	}
?>
</div>
<?php echo $pages->displayPages();?>
<!-- Root element of PhotoSwipe. Must have class pswp. -->
<div class="pswp" tabindex="-1" role="dialog" aria-hidden="true">

    <!-- Background of PhotoSwipe. 
         It's a separate element, as animating opacity is faster than rgba(). -->
    <div class="pswp__bg"></div>

    <!-- Slides wrapper with overflow:hidden. -->
    <div class="pswp__scroll-wrap">

        <!-- Container that holds slides. PhotoSwipe keeps only 3 slides in DOM to save memory. -->
        <!-- don't modify these 3 pswp__item elements, data is added later on. -->
        <div class="pswp__container">
            <div class="pswp__item"></div>
            <div class="pswp__item"></div>
            <div class="pswp__item"></div>
        </div>

        <!-- Default (PhotoSwipeUI_Default) interface on top of sliding area. Can be changed. -->
        <div class="pswp__ui pswp__ui--hidden">

            <div class="pswp__top-bar">

                <!--  Controls are self-explanatory. Order can be changed. -->

                <div class="pswp__counter"></div>

                <button class="pswp__button pswp__button--close" title="Close (Esc)"></button>

                <button class="pswp__button pswp__button--share" title="Share"></button>

                <button class="pswp__button pswp__button--fs" title="Toggle fullscreen"></button>

                <button class="pswp__button pswp__button--zoom" title="Zoom in/out"></button>
                <button class="pswp__button pswp__button--zoom" title="Zoom in/out"></button>
                <button class="pswp__button glyphicon glyphicon-warning-sign" style="background:initial;color:white;" aria-hidden="true" data-toggle="modal" data-target="#report" title="Report"></button>

                <!-- Preloader demo http://codepen.io/dimsemenov/pen/yyBWoR -->
                <!-- element will get class pswp__preloader--active when preloader is running -->
                <div class="pswp__preloader">
                    <div class="pswp__preloader__icn">
                      <div class="pswp__preloader__cut">
                        <div class="pswp__preloader__donut"></div>
                      </div>
                    </div>
                </div>
            </div>

            <div class="pswp__share-modal pswp__share-modal--hidden pswp__single-tap">
                <div class="pswp__share-tooltip"></div> 
            </div>

            <button class="pswp__button pswp__button--arrow--left" title="Previous (arrow left)">
            </button>

            <button class="pswp__button pswp__button--arrow--right" title="Next (arrow right)">
            </button>

            <div class="pswp__caption">
                <div class="pswp__caption__center"></div>
            </div>

          </div>

        </div>

</div>
<script>
var item;
if(item)
{
	tmpItem = item.slice(1,-1);
	itemArr = tmpItem.split("/");

	var lastReadDiv = document.getElementById("lastReadDiv")
	lastReadDiv.href = item;
	removeClass(lastReadDiv,"hidden");
	lastReadDiv.innerHTML = lastReadDiv.innerHTML + itemArr[1];

}

readBookmarks();
function switchAdmin()
{
	var admin = readCookie('admin');
	if(admin == "1")
	{
		createCookie('admin',"0",24);
	}
	else
	{
		createCookie('admin',"1",24);
	}
	window.location = window.location.href;
}

$(document).bind('domChanged', function(){
    addAutocomplete();
});
addAutocomplete();
function addAutocomplete() {
    var availableTags = [<?php 
    	foreach($artists as $artist){
    		echo "'{$artist->artist}',";
    	}
    ?>];
    $( ".tags" ).autocomplete({
      source: availableTags
    });
}
</script>


<script>
var initPhotoSwipeFromDOM = function(gallerySelector) {

    // parse slide data (url, title, size ...) from DOM elements 
    // (children of gallerySelector)
    var parseThumbnailElements = function(el) {
        var thumbElements = el.childNodes,
            numNodes = thumbElements.length,
            items = [],
            figureEl,
            linkEl,
            size,
            item;

        for(var i = 0; i < numNodes; i++) {

            figureEl = thumbElements[i]; // <figure> element

            // include only element nodes 
            if(figureEl.nodeType !== 1) {
                continue;
            }

            linkEl = figureEl.children[0]; // <a> element

            size = linkEl.getAttribute('data-size').split('x');

            // create slide object
            item = {
                src: linkEl.getAttribute('href'),
                w: parseInt(size[0], 10),
                h: parseInt(size[1], 10)
            };



            if(figureEl.children.length > 1) {
                // <figcaption> content
                item.title = figureEl.children[1].innerHTML; 
            }

            if(linkEl.children.length > 0) {
                // <img> thumbnail element, retrieving thumbnail url
                item.msrc = linkEl.children[0].getAttribute('src');
            } 

            item.el = figureEl; // save link to element for getThumbBoundsFn
            items.push(item);
        }

        return items;
    };

    // find nearest parent element
    var closest = function closest(el, fn) {
        return el && ( fn(el) ? el : closest(el.parentNode, fn) );
    };

    // triggers when user clicks on thumbnail
    var onThumbnailsClick = function(e) {
        e = e || window.event;
        e.preventDefault ? e.preventDefault() : e.returnValue = false;

        var eTarget = e.target || e.srcElement;

        // find root element of slide
        var clickedListItem = closest(eTarget, function(el) {
            return (el.tagName && el.tagName.toUpperCase() === 'FIGURE');
        });

        if(!clickedListItem) {
            return;
        }

        // find index of clicked item by looping through all child nodes
        // alternatively, you may define index via data- attribute
        var clickedGallery = clickedListItem.parentNode,
            childNodes = clickedListItem.parentNode.childNodes,
            numChildNodes = childNodes.length,
            nodeIndex = 0,
            index;

        for (var i = 0; i < numChildNodes; i++) {
            if(childNodes[i].nodeType !== 1) { 
                continue; 
            }

            if(childNodes[i] === clickedListItem) {
                index = nodeIndex;
                break;
            }
            nodeIndex++;
        }



        if(index >= 0) {
            // open PhotoSwipe if valid index found
            openPhotoSwipe( index, clickedGallery );
        }
        return false;
    };

    // parse picture index and gallery index from URL (#&pid=1&gid=2)
    var photoswipeParseHash = function() {
        var hash = window.location.hash.substring(1),
        params = {};

        if(hash.length < 5) {
            return params;
        }

        var vars = hash.split('&');
        for (var i = 0; i < vars.length; i++) {
            if(!vars[i]) {
                continue;
            }
            var pair = vars[i].split('=');  
            if(pair.length < 2) {
                continue;
            }           
            params[pair[0]] = pair[1];
        }

        if(params.gid) {
            params.gid = parseInt(params.gid, 10);
        }

        return params;
    };

    var openPhotoSwipe = function(index, galleryElement, disableAnimation, fromURL) {
        var pswpElement = document.querySelectorAll('.pswp')[0],
            gallery,
            options,
            items;

        items = parseThumbnailElements(galleryElement);

        // define options (if needed)
        options = {

            // define gallery index (for URL)
            galleryUID: galleryElement.getAttribute('data-pswp-uid'),

            getThumbBoundsFn: function(index) {
                // See Options -> getThumbBoundsFn section of documentation for more info
                var thumbnail = items[index].el.getElementsByTagName('img')[0], // find thumbnail
                    pageYScroll = window.pageYOffset || document.documentElement.scrollTop,
                    rect = thumbnail.getBoundingClientRect(); 

                return {x:rect.left, y:rect.top + pageYScroll, w:rect.width};
            }

        };

        // PhotoSwipe opened from URL
        if(fromURL) {
            if(options.galleryPIDs) {
                // parse real index when custom PIDs are used 
                // http://photoswipe.com/documentation/faq.html#custom-pid-in-url
                for(var j = 0; j < items.length; j++) {
                    if(items[j].pid == index) {
                        options.index = j;
                        break;
                    }
                }
            } else {
                // in URL indexes start from 1
                options.index = parseInt(index, 10) - 1;
            }
        } else {
            options.index = parseInt(index, 10);
        }

        // exit if index not found
        if( isNaN(options.index) ) {
            return;
        }

        if(disableAnimation) {
            options.showAnimationDuration = 0;
        }

        // Pass data to PhotoSwipe and initialize it
        gallery = new PhotoSwipe( pswpElement, PhotoSwipeUI_Default, items, options);
        gallery.init();
    };

    // loop through all gallery elements and bind events
    var galleryElements = document.querySelectorAll( gallerySelector );

    for(var i = 0, l = galleryElements.length; i < l; i++) {
        galleryElements[i].setAttribute('data-pswp-uid', i+1);
        galleryElements[i].onclick = onThumbnailsClick;
    }

    // Parse URL and open gallery if it contains #&pid=3&gid=1
    var hashData = photoswipeParseHash();
    if(hashData.pid && hashData.gid) {
        openPhotoSwipe( hashData.pid ,  galleryElements[ hashData.gid - 1 ], true, true );
    }
};

// execute above function
initPhotoSwipeFromDOM('.my-gallery');
</script>

<?php include \Config::get('folder/includes') . "/footer.php" ?>
